<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class Interests extends Model
{
    protected $table = 'freyo_user_interests';
	protected $fillable = [
		'user_id',
		'category_id',
	];
	public $timestamps = false;
}
