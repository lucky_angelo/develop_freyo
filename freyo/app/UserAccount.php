<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    protected $table = 'freyo_user';
	protected $fillable = [
		'email',
		'password',
		'last_login',
		'last_ip',
		'reset_hash',
		'salt',
		'failed_login',
		'confirmation_key',
		'confirmation_expiration',
		'device_unique_id',
	];

    protected $hidden = ['password', 'salt'];
	public $timestamps = false;
}
