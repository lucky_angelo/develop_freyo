<?php

namespace Freyo\Http\Requests;

use Freyo\Http\Requests\Request;

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_add' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|min:6',
            'birthdate' => 'date_format:Y-m-d',
        ];
    }
}
