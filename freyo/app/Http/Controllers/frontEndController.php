<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;

use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;

class frontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function home(){
        return view('mobile.pages.test');
    }
}
