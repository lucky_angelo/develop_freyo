<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;


use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;
use Freyo\UserToken;
use Freyo\LastNotify;
use Freyo\MagazinePublish;
use Freyo\MagazineSubscribe;
use Freyo\UserAccount;
use DB;

use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Adapter\Gcm as GcmAdapter,
    Sly\NotificationPusher\Collection\DeviceCollection,
    Sly\NotificationPusher\Model\Device,
    Sly\NotificationPusher\Model\Message,
    Sly\NotificationPusher\Model\Push
;

class PushNotificationController extends Controller
{
    protected $fUToken;
    protected $fNotify;
    protected $fMPublish;
    protected $fMSubscribe;
    protected $fUAccount;

    public function __construct(){
        
        $this->fUToken = new UserToken();
        // $this->fNotify = new LastNotify();
        $this->fMPublish = new MagazinePublish();
        $this->fMSubscribe =  new MagazineSubscribe();
        $this->fUAccount = new UserAccount();
    }

	public function user_device_token(Request $request){
        session_start();
        $data = explode('::', $request->input('RegInfos'));
        // $this->fUToken->where('token', $data[0])
        // ->update(['token' => '']);
        // $this->fUToken->where('token', $data[0])->forceDelete();
        $this->fUToken->where('unique_id', $data[1])->forceDelete();
        // $usertoken = $this->fUToken->select('token')->where('token',$data[0])->take(1)->get();
        $usertoken = $this->fUToken->select('token')->where('unique_id',$data[1])->take(1)->get();
        if(count($usertoken) < 1) {
            $this->fUToken->insert(
                [
                'token' => $data[0],
                'unique_id' => $data[1],
                'os' => $data[2],
                'date_created' => \Carbon\Carbon::now(),
                ]
            );
        }
        // \Session::put('device_token', $data[0]);
        // \Session::put('device_os', $data[2]);

    }

    public function user_device_token_ios(Request $request){
        // session_start();
        // var_dump($request->input('RegInfos'));
        $data = explode('::', $request->input('RegInfos'));
        // $this->fUToken->where('token', $data[0])
        // ->update(['token' => '']);
        // $this->fUToken->where('token', $data[0])->forceDelete();
        $this->fUToken->where('unique_id', $data[1])->forceDelete();
        // $usertoken = $this->fUToken->select('token')->where('token',$data[0])->take(1)->get();
        $usertoken = $this->fUToken->select('token')->where('unique_id',$data[1])->take(1)->get();
        if(count($usertoken) < 1) {
            $this->fUToken->insert(
                [
                'token' => $data[0],
                'unique_id' => $data[1],
                'os' => $data[2],
                'date_created' => \Carbon\Carbon::now(),
                ]
            );
        }
        // \Session::put('device_token', $data[0]);
        // \Session::put('device_os', $data[2]);

    }

    public function getUserToken() {
        $usertokens = $this->fUToken->select(['token', 'unique_id', 'os'])->get();
        return $usertokens;
    }

    public function index(Request $request)
    {
        // echo app_path();
        $security = $request->input('security');
        $title = $request->input('title');
        $messageBody = $request->input('message');
        $magazine_id = $request->input('magazine');
        // dd($request->all());
        if(isset($security) && $security == 'ed70ce2e8edfc') {
            
            if($magazine_id > 0) {
                $userTokens = $this->fMSubscribe->where(['magazine_subscribe.magazine_id' => $magazine_id])
                ->where(['magazine_publisher.publish' => 'yes'])
                ->join('freyo_user', 'magazine_subscribe.user_id', '=', 'freyo_user.user_id')
                ->join('freyo_user_token', 'freyo_user.device_unique_id', '=', 'freyo_user_token.unique_id')
                ->join('magazine_publisher', 'magazine_subscribe.magazine_id', '=', 'magazine_publisher.magazine_id')
                ->get(['token', 'os']);
            } else { 
                $userTokens = $this->getUserToken();
            }
                


            // 
            // dd($userTokens);
            $deviceList = [];
            $deviceListiOS = [];
            foreach($userTokens as $usrTkn) {
                if($usrTkn->os == 'Android') {
                    $deviceList[] = \PushNotification::Device($usrTkn->token);
                } 
                if($usrTkn->os == 'iOS') {
                    $deviceListiOS[] = \PushNotification::Device($usrTkn->token);
                }
            }
            // dd($deviceList);
            if($deviceList) {
                $devices = \PushNotification::DeviceCollection($deviceList);
            }
            if($deviceListiOS) {
                $devicesiOS = \PushNotification::DeviceCollection($deviceListiOS);
            }
            // dd($deviceListiOS);
            // dd($devices);
            // dd($devicesiOS);
            if($deviceList) {
                $message = new Message($title.'::'.$messageBody);
                $a = \PushNotification::app('Freyo')
                ->to($devices) 
                ->send($message);
                // dd($a);
            }
            
            if($deviceListiOS) {
                \PushNotification::app(['environment' => 'development',
                    'certificate' => app_path().'/pushcert.pem',
                    'passPhrase'  => '',
                    'service'     => 'apns'
                ]);
                $messageiOS = new Message($messageBody); 
                $b = \PushNotification::app('FreyoiOS')
                ->to($devicesiOS)
                ->send($messageiOS);
                // dd($b);
            }
            // return 'true';
            
             
            
        } else {
            return 'false';
        } 
    }
}