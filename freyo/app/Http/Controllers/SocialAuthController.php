<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;

use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();   
    }   

    public function callback()
    {
        // when facebook call us a with token   
    }
}
