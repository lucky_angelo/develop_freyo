<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Cookie\CookieJar;
use Freyo\Http\Requests\RegisterRequest;
use Freyo\Http\Requests\UserActivateRequest;
use Freyo\Http\Requests\ResetPasswordRequest;
use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;
use Freyo\Http\Controllers\CacheController;
use Freyo\Http\Controllers\magazineController;
use Freyo\UserAccount;
use Freyo\UserProfile;
use Freyo\ContactForm;
use Freyo\SystemVariable;
use Hash;
use Socialite;

class UserController extends Controller
{
    protected $fCached;
	protected $fuserID;
	protected $fUAccount;
	protected $fUProfile;
	protected $fQuery;

	public function __construct(Request $request){
		if($request->cookie(\Config::get('constants.user_cookie_name')) !== null){
			$this->fuserID = $request->cookie(\Config::get('constants.user_cookie_name'));
		}
        else {
            $this->fuserID = 0;
        }
		$this->fUAccount = new UserAccount();
		$this->fUProfile = new UserProfile();
	}

    public function submit(RegisterRequest $request, CookieJar $cookies){
        $useraccount = $this->fUAccount->select('user_id')->where('email',$request->input('email_add'))->take(1)->get();
        $registerStatus = [];
        // if($request->input('password') != $request->input('confirm_password')){
        //     $registerStatus['status'] = "error";
        //     $registerStatus['message'] = "Confirm Password does not match Password!";
        //     return \Response::json($registerStatus);
        // } 
        if(count($useraccount) < 1) {
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass = array();
            $confirmation_code = array();
            $alphaLength = strlen($alphabet) - 1;
            for ($i = 0; $i < 5; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            for ($i = 0; $i < 30; $i++) {
                $n = rand(0, $alphaLength);
                $confirmation_code[] = $alphabet[$n];
            }
            $confirmation_key = implode($confirmation_code);
            $salty = implode($pass);
            $userpassword = $request->input('password').$salty;
            $hashedpassword = Hash::make($userpassword);


            $device_id = null;
            if(\Session::has('unique_id')) {
                 $device_id = \Session::get('unique_id');
            }
            // dd($device_id);

            $user_id = $this->fUAccount->insertGetId(
                [
                'email' => $request->input('email_add'), 
                'password' => $hashedpassword,
                'salt' => $salty,
                'confirmation_key' => $confirmation_key,
                'device_unique_id' => $device_id,
                // 'confirmation_expiration' => \Carbon\Carbon::now()->addDay(),
                ]
            );

            $cookies->queue(cookie(\Config::get('constants.user_cookie_name'), $user_id,60*24*365*5));


            $gender = ($request->input('gender')!=null)?$request->input('gender'):'unknown';
            $full_name = $request->input('first_name').' '.$request->input('last_name');
            $this->fUProfile->insert(
                [
                'user_id' => $user_id,
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'email' => $request->input('email_add'),
                'province' => $request->input('province'),
                'city' => $request->input('city'),
                'phone_number' => $request->input('mobile_number'),
                'sex' => $gender,
                'birthdate' => $request->input('birthdate'),
                'date_created' => \Carbon\Carbon::now(),
                ]
            );

            /**
             * Send activation code
             */
            // $activation_url = "<a href='http://freyo.com/activate?cId=$confirmation_key' style='border-color: #cccccc; color: #333333; cursor: pointer; display:inline-block; padding:6 12px; text-align:center; vertical-align: middle;'>Confirm E-Mail</a>";
            $user_emailAdd = $request->input('email_add');
            $activation_url = "http://freyo.com/activate?cId=$confirmation_key&cEmail=$user_emailAdd";
            $this->email_from = \Config::get('mail.username');
            $this->email_subject = "Freyo Account E-mail Confirmation";
            $this->email_message = "Dear ".$full_name.",\r\n\r\nThank you for registering.\r\n\r\nPlease click on the link below to confirm your Freyo E-mail.\r\n\r\n$activation_url \r\n\r\nFrom,\r\nFreyo\r\nwww.freyo.com\r\n";
            $this->email_to = $user_emailAdd;

            Mail::raw($this->email_message, function($mail) {
                $mail->from($this->email_from)
                    ->to($this->email_to)
                    ->subject($this->email_subject);
            });
            $registerStatus['status'] = 'registered';
            $registerStatus['message'] = 'Registered successfuly! Redirecting to dashboard.';
            // return \Redirect::to(asset('/dashboard'));
            return \Response::json($registerStatus);
        } else {
            $registerStatus['status'] = "existing";
            $registerStatus['message'] = "E-mail already exists!";
            return \Response::json($registerStatus);
        }
        // return \Response::json($registerStatus);
        
        
    }

    public function fbsubmit(Request $request, CookieJar $cookies){
        if($request->input('error') !== null &&  $request->input('error')!= "")
        {
            return \Redirect::to(asset('/?app=banana&redirectto=login'));
        }

        if(null == $request->input('__id')) {
            try {
                $fbuser = Socialite::driver('facebook')->fields([
                    'id', 'first_name', 'last_name', 'email', 'gender', 'age_range', 'verified', 'picture'
                ])->user();
                // dd($fbuser);
            } catch(Exception $e) {
                $registerStatus['status'] = "error";
                $registerStatus['message'] = "Something went wrong. try again.";
                return \Response::json($registerStatus);
            }
            $fbuser = $fbuser->user;
        }
        
        
        if(null == $request->input('__id')) {
            $fbID = (isset($fbuser['id'])) ? $fbuser['id'] : 'error';
            // $fbemail = (isset($fbuser['email'])) ? $fbuser['email'] : $fbID.'@no-email.com';
            $gender = (isset($fbuser['gender'])) ? $fbuser['gender'] : 'unknown';
            $first_name = (isset($fbuser['first_name'])) ? $fbuser['first_name'] : '';
            $last_name = (isset($fbuser['last_name'])) ? $fbuser['last_name'] : '';
            $birthdate = "0000-00-00";
            if(isset($fbuser['age_range']['max'])){
                $birthdate = \Carbon\Carbon::now()->subYears($fbuser['age_range']['max'])->toDateString();
            }elseif(isset($fbuser['age_range']['min'])){
                $birthdate = \Carbon\Carbon::now()->subYears($fbuser['age_range']['min'])->toDateString();
            }
            $noemail = $fbID.'@no-email.com';
            $noemailaccount = $this->fUAccount->select('user_id')->where('email',$noemail)->take(1)->get();
            
            if(count($noemailaccount) == 1) {
                $fbuser['email'] = $noemail;
            }

            if(isset($fbuser['email'])) {
                $fbemail = $fbuser['email'];
            } else {
                return \Redirect::to(asset('/api/fbemail'))->with([
                    'fb_id' => $fbID,
                    'f_name' => $first_name,
                    'l_name' => $last_name,
                    'gender' => $gender,
                    'birthdate' => $birthdate,
                    'is_silhouette' => $fbuser['picture']['data']['is_silhouette'],
                ]);
                // return \Response::json([
                //     'fb_id' => $fbID,
                //     'f_name' => $first_name,
                //     'l_name' => $last_name,
                //     'gender' => $gender,
                //     'birthdate' => $birthdate,
                //     'is_silhouette' => $fbuser['picture']['data']['is_silhouette'],
                // ]);
            }

            if($fbuser['picture']['data']['is_silhouette']) {

            } else {
                $arrContextOptions=array(
                    "ssl"=>array(
                        "verify_peer"=>false,
                        "verify_peer_name"=>false,
                    ),
                );
                $uniqueid = md5(uniqid(rand(), true));  
                $uniqueid = substr($uniqueid,0,5);
                $destPath = \Config::get('constants.url_file_profile_pic');
                $pfname = $first_name.'_'.$last_name.'.png';
                $profile_picture = str_replace(' ', '_', $pfname);
                $profile_picture = $uniqueid.'_'.$profile_picture;
                $basePath = base_path()."/../public_html/"; 
                $destPath = \Config::get('constants.url_file_profile_pic');
                // return $basePath.$destPath;
                // $path = url('/').'/panel/_files/profilepic/';
                // return $basePath.$destPath.'/'.$profile_picture;
                try {
                    $useraccount = $this->fUAccount->select('user_id')->where('email',$fbemail)->take(1)->get();
                    if(count($useraccount) < 1) {
                        file_put_contents('panel/_files/profilepic/'.$profile_picture, file_get_contents('http://graph.facebook.com/'.$fbID.'/picture?type=large', false, stream_context_create($arrContextOptions)));
                    } 
                    
                } catch(Exception $e) {
                    return $e;
                }
                
            }
        } else {
            
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $gender = $request->input('gender');
            $birthdate = $request->input('birthdate');
            $fbemail = $request->input('email');
            $province = $request->input('province');
            $city = $request->input('city');
            $mobile_num = $request->input('mobile_number');
            if(null == $request->input('is_silhouette')) {
                try {
                    $arrContextOptions=array(
                        "ssl"=>array(
                            "verify_peer"=>false,
                            "verify_peer_name"=>false,
                        ),
                    );
                    $uniqueid = md5(uniqid(rand(), true));  
                    $uniqueid = substr($uniqueid,0,5);
                    $destPath = \Config::get('constants.url_file_profile_pic');
                    $pfname = $first_name.'_'.$last_name.'.png';
                    $profile_picture = str_replace(' ', '_', $pfname);
                    $profile_picture = $uniqueid.'_'.$profile_picture;
                    $useraccount = $this->fUAccount->select('user_id')->where('email',$fbemail)->take(1)->get();
                    if(count($useraccount) < 1) {
                        file_put_contents('panel/_files/profilepic/'.$profile_picture, file_get_contents('http://graph.facebook.com/'.$request->input('fb_id').'/picture?type=large', false, stream_context_create($arrContextOptions)));
                    }
                } catch(Exception $e) {

                }
            }
            // return \Response::json([
            //     'fname' => $first_name,
            //     'lname' => $last_name,
            //     'email' => $fbemail,
            //     'province' => $province,
            //     'city' => $city,
            //     'mobile_num' => $mobile_num,
            // ]);
        }
       

        $useraccount = $this->fUAccount->select('user_id')->where('email',$fbemail)->take(1)->get();
        $registerStatus = [];

        if(count($useraccount) < 1) {
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $pass = array();
            $confirmation_code = array();
            $alphaLength = strlen($alphabet) - 1;
            for ($i = 0; $i < 5; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            for ($i = 0; $i < 30; $i++) {
                $n = rand(0, $alphaLength);
                $confirmation_code[] = $alphabet[$n];
            }
            $confirmation_key = implode($confirmation_code);
            $salty = implode($pass);
            $userpassword = 'fb123'.$salty;
            $hashedpassword = Hash::make($userpassword);

            $device_id = null;
            if(\Session::has('unique_id')) {
                 $device_id = \Session::get('unique_id');
            }

            $user_id = $this->fUAccount->insertGetId(
                [
                'email' => $fbemail, 
                'password' => $hashedpassword,
                'salt' => $salty,
                'confirmation_key' => $confirmation_key,
                'confirmed' => 'yes',
                'last_login' => \Carbon\Carbon::now(),
                'last_ip' => $request->ip(),
                'device_unique_id' => $device_id,
                ]
            );

            $cookies->queue(cookie(\Config::get('constants.user_cookie_name'), $user_id,60*24*365*5));

            $full_name = $first_name.' '.$last_name;
            $this->fUProfile->insert(
                [
                'user_id' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $fbemail,
                'province' => (isset($province)) ? $province : 0,
                'city' => (isset($city)) ? $city : 0,
                'phone_number' => (isset($mobile_num)) ? $mobile_num : '',
                'sex' => $gender,
                'profile_picture' => (isset($profile_picture)) ? $profile_picture : null,
                'birthdate' => $birthdate,
                'date_created' => \Carbon\Carbon::now(),
                'facebook_login' => 'yes'
                ]
            );

            // /**
            //  * Send activation code
            //  */
            // // $activation_url = "<a href='http://freyo.com/activate?cId=$confirmation_key' style='border-color: #cccccc; color: #333333; cursor: pointer; display:inline-block; padding:6 12px; text-align:center; vertical-align: middle;'>Confirm E-Mail</a>";
            // $user_emailAdd = $fbemail;
            // $activation_url = "http://freyo.com/activate?cId=$confirmation_key&cEmail=$user_emailAdd";
            // $this->email_from = \Config::get('mail.username');
            // $this->email_subject = "Freyo Account E-mail Confirmation";
            // $this->email_message = "Dear ".$full_name.",\r\n\r\nThank you for registering.\r\n\r\nPlease click on the link below to confirm your Freyo E-mail.\r\n\r\n$activation_url \r\n\r\nFrom,\r\nFreyo\r\nwww.freyo.com\r\n";
            // $this->email_to = $user_emailAdd;

            // // Mail::raw($this->email_message, function($mail) {
            // //     $mail->from($this->email_from)
            // //         ->to($this->email_to)
            // //         ->subject($this->email_subject);
            // // });

            // $registerStatus['status'] = "registered";
            // $registerStatus['message'] = "Account registered! Please login to your e-mail to confirm your e-mail!";
        } else {
            $cookies->queue(cookie(\Config::get('constants.user_cookie_name'), $useraccount[0]->user_id,60*24*365*5));
            $this->fUAccount->where('user_id', $useraccount[0]->user_id)->update(['last_login' => \Carbon\Carbon::now(), 'last_ip' => $request->ip()]);
            $registerStatus['status'] = "existing";
            $registerStatus['message'] = "E-mail already exists!";
        }
        return \Redirect::to(asset('/dashboard'));
        // return \Response::json($registerStatus);
    }

    public function activate(UserActivateRequest $request){
        $confirmation_key = $request->input('cId');
        $confirm_email = $request->input('cEmail');
        $successful = $this->fUAccount->where('confirmation_key', $confirmation_key)
            ->where('email', $confirm_email)
            // ->where('confirmation_expiration', ">=", \Carbon\Carbon::now())
            ->update(['confirmed' => 'yes', 'confirmation_key' => '']);

        if($successful < 1){
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Account Not Confirmed');
            \Session::flash('flash_message_body', 'Your confirmation link is invalid. Please try again or contact us at admin@freyo.com');
        }else{
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Account Confirmed');
            \Session::flash('flash_message_body', 'Welcome to Freyo.');
        }
        return view('layouts.master');
    }

    public function checkLogin($fEmail, $fPassword){
        $aResult = [];
        $fUserData = $this->fUAccount->select('user_id','password','salt','confirmed', 'facebook_login', 'device_unique_id')
        ->where('email', $fEmail)->take(1)->get();
        $device_id = null;
        if(\Session::has('unique_id')) {
            $device_id = \Session::get('unique_id');
        }

        if(count($fUserData) > 0){
            $hashPass = $fUserData[0]->password;
            $inputPass = $fPassword.$fUserData[0]->salt;

            if (Hash::check($inputPass, $hashPass)) {
                // if($fUserData[0]->device_unique_id == null || $fUserData[0]->device_unique_id == '') {
                $this->fUAccount->where('email', $fEmail)->update(['device_unique_id' => $device_id]);
                // }
                $this->fUAccount->where('email', $fEmail)->update(['last_login' => \Carbon\Carbon::now(), 'last_ip' => \Request::ip()]);
                
                $aResult['login'] = true;
                $aResult['user_id'] = $fUserData[0]->user_id;
                $aResult['confirmed'] = true;
                // if($fUserData[0]->confirmed == "no" && $fUserData[0]->facebook_login == "no") {
                //     $aResult['confirmed'] = false;
                // } 
            }
            else{
                //incorrect credentials
                $aResult['login'] = false;
            }
        }else{
            //user does not exist
            $aResult['login'] = false;
        }
        return $aResult;
    }

    public function getUserProfile(){
        // $this->fCached = new CacheController('the-freyo-user-'.$this->fuserID);

    	$meArray = [
    	    'email',
            'user_id',
            'first_name',
    	    'last_name',
    	    'province',
    	    'city',
    	    'phone_number',
    	    'birthdate',
    	    'profile_picture',
    	    'sex',
            'following',
            'favorites',
            'interests',
    	];

    	$this->fQuery = $this->fUProfile
    		->select($meArray)
    		// ->join('freyo_user_profile', 'freyo_user.user_id', '=', 'freyo_user_profile.user_id')
    		->where('user_id', $this->fuserID)
    		->take(1);

        return \Response::json($this->fQuery->get());
    	// return \Response::json($this->fCached->get($this->fQuery));
    }

    public function updateUserProfile(Request $request){
        $full_name = $request->input('first_name').' '.$request->input('last_name');
    	$updateFields = array(
            'first_name' => $request->input('first_name'),
    		'last_name' => $request->input('last_name'),
    		// 'sex' => $request->input('gender'),
    		'city' => $request->input('city'),
    		'province' => $request->input('province'),
            'date_updated' => \Carbon\Carbon::now(),
    		);
    	if($request->input('mobile_num') !== null && $request->input('mobile_num') !== ""){
            $updateFields['phone_number'] = $request->input('mobile_num');
        }
        else{
            $updateFields['phone_number'] = "";
    	}
    	if($request->input('birthdate') !== null && $request->input('birthdate') !== ""){
            $updateFields['birthdate'] = $request->input('birthdate');
    	}
        else{
            $updateFields['birthdate'] = "";
        }
    	$updateResult = array(
            'message' => "",
            'new_birthdate' => $updateFields['birthdate'],
            'new_phone_number' => $updateFields['phone_number'],
            // 'new_sex' => $updateFields['sex'],
            'new_city' => $updateFields['city'],
            'new_province' => $updateFields['province'],
            'new_firstname' => $updateFields['first_name'],
            'new_lastname' => $updateFields['last_name'],
            'new_name' => $full_name
            );
    	if($request->input('old_password') !== "" && $request->input('new_password') !== "" && $request->input('confirm_password') !== ""){
    		if($request->input('confirm_password') !== $request->input('new_password')){
    			return array('status' => 'error', 'message' => 'Please confirm new password!');
    		}
			$salty = md5(uniqid(rand(), true));
			$salty = substr($salty,0,5);
    		$oldPass = $request->input('old_password');
    		$newPass = Hash::make($request->input('new_password').$salty);
    		$fUserData = $this->fUAccount->select('user_id','password','salt')
    		->where('user_id', $this->fuserID)->take(1)->get();
    		if(count($fUserData) > 0){
    		    $hashPass = $fUserData[0]->password;
    		    $inputPass = $oldPass.$fUserData[0]->salt;

    		    if (Hash::check($inputPass, $hashPass)) { 
    		        $this->fUAccount->where('user_id', $this->fuserID)->update(['password' => $newPass, 'salt' => $salty]);
    		        $updateResult['message'] = "Changed password successful!";
    		    }
    		    else{
    		    	return array('status' => 'error', 'message' => 'Incorrect password!');
    		    }
    		}
    	}

    	$this->fUProfile->where('user_id', $this->fuserID)->update($updateFields);
    	$updateResult['status'] = "success";
    	if($updateResult['message'] != ""){
    		$updateResult['message'] = $updateResult['message'] . " Profile updated!";
    	}
    	$updateResult['message'] = "Profile updated!";
        // $cachepull = \Cache::pull('the-freyo-user-'.$this->fuserID);
    	return \Response::json($updateResult);
    }

    public function getProfilePic($ProfPicFile = ""){
    	return asset('api/profile_pic/'.$ProfPicFile);
    	// if($ProfPicFile != "" && $ProfPicFile != "null"){
    	// 	$path = \Config::get('constants.url_file_profile_pic')."/".$ProfPicFile;
    	// 	if(file_exists(base_path()."/../public_html/mobile/".$path)){
    	// 		return asset('api/profile_pic/'.$path);
    	// 	}
    	// }
    	// return "http://m.freyo.com/assets/mobile/images/gintoki_shock.jpg";
    }

    public function updateProfilePic(Request $request){
    	$pResult = array('status' => 'error', 'message' => 'No file uploaded!');
    	if($request->hasFile('profile_pic')){
    		$profpic_file = $request->file('profile_pic');
			$uniqueid = md5(uniqid(rand(), true));	
    		$uniqueid = substr($uniqueid,0,5);
    		$profpic_name = $profpic_file->getClientOriginalName();
    		$profpic_name = str_replace(" ", "_", $profpic_name);
    		$profpic_name = $uniqueid.'_'.$profpic_name;
    		$destPath = \Config::get('constants.url_file_profile_pic');
    		$basePath = base_path()."/../public_html/";
    		if($profpic_file->isValid()){
    			if($profpic_file->getMaxFileSize() > $profpic_file->getClientSize()){
	    			switch ($profpic_file->getClientMimeType()) {
	    				case 'image/gif':
	    				case 'image/jpeg':
	    				case 'image/pjpeg':
	    				case 'image/png':
	    					$profpic_file->move($basePath.$destPath, $profpic_name);
	    					$fOldPic = $this->fUProfile->select('profile_picture')->where('user_id', $this->fuserID)->take(1)->get();
	    					if(count($fOldPic) > 0){
	    						if(file_exists($basePath.$destPath.'/'.$fOldPic[0]->profile_picture) && $fOldPic[0]->profile_picture !== null){
	    							unlink($basePath.$destPath.'/'.$fOldPic[0]->profile_picture);
	    						}
	    					}
	    					$this->fUProfile->where('user_id', $this->fuserID)->update(['profile_picture' => $profpic_name]);
	    					$pResult = array('status'=>'success', 'message' => 'Profile Picture updated! It will change in a second.', 'new_pic' => $profpic_name);//asset($destPath.'/'.$profpic_name));
	    					break;
	    				
	    				default:
	    					$pResult = array('status'=>'error', 'message' => 'jpg, gif, or png file only!');
	    					break;
	    			}
	    		}
	    		else{
	    			$pResult = array('status'=>'error', 'message' => 'File size is too large (Max: 5MB)!');
	    		}
    			
    		}
    		else {
    			$pResult = array('status'=>'error', 'message' => 'File is not valid!');
    		}
    	}
    	return $pResult;
    }

    public function signout(Request $request){
        $fapp_version = "";
        if($request->input('version') !== null){
            $fapp_version = "&version=".$request->input('version');
        }
        if(\Session::has('fapp_version')) {
            \Session::forget('fapp_version');
        }
        $device_id = "";
        if(\Session::has('unique_id')) {
            $device_id = '&deviceid='.\Session::get('unique_id');
        }
        if($request->cookie(\Config::get('constants.user_cookie_name')) !== null){
           return \Redirect::to('/?app=banana'.$fapp_version)->withCookie(\Cookie::forget(\Config::get('constants.user_cookie_name')));
        }
        return \Redirect::to('/?app=banana'.$fapp_version.$device_id);
    }

    public function requestreset(Request $request){
        if($request->input('request_email') == null || $request->input('request_email') == ""){
            return \Redirect::to('/?app=banana');
        }
        $requestStatus = array();
        $emailExist = $this->fUAccount->select('freyo_user.user_id','freyo_user.email', 'freyo_user_profile.first_name', 'freyo_user_profile.last_name')
        ->join('freyo_user_profile', 'freyo_user.user_id', '=', 'freyo_user_profile.user_id')
        ->where('freyo_user.email', $request->input('request_email'))
        ->take(1)->get();
        if(count($emailExist)>0){
            $reset_key = md5(uniqid(rand(),true));
            $this->fUAccount->where('user_id', $emailExist[0]->user_id)
            ->update(['reset_hash' => $reset_key, 'confirmation_expiration' => \Carbon\Carbon::now()->addDay()]);

            $full_name = $emailExist[0]->first_name.' '.$emailExist[0]->last_name;
            $user_emailAdd = $emailExist[0]->email;
            $reset_url = "http://freyo.com/resetpassword?cId=$reset_key&cEmail=$user_emailAdd";
            $this->email_from = \Config::get('mail.username');
            $this->email_subject = "Freyo Reset Password";
            $this->email_message = "Dear ".$full_name.",\r\n\r\nPlease click on the link below to change your password. This link is only valid for 24 hours. \r\n\r\n$reset_url\r\n\r\nFrom,\r\nFreyo\r\nhttp://freyo.com\r\n";
            $this->email_to = $user_emailAdd;

            Mail::raw($this->email_message, function($mail) {
                $mail->from($this->email_from)
                    ->to($this->email_to)
                    ->subject($this->email_subject);
            });
            $requestStatus['status'] = "success";
            $requestStatus['message'] = "A reset link has been sent to your email.";
        }else{
            $requestStatus['status'] = "error";
            $requestStatus['message'] = "Incorrect email address!";
        }
        return \Response::json($requestStatus);
    }
    public function confirmreset(UserActivateRequest $request){
        $reset_key = $request->input('cId');
        $reset_email = $request->input('cEmail');
        $showResetForm = "";
        $successful = $this->fUAccount->select('user_id','email')
            ->where('reset_hash', $reset_key)
            ->where('email', $reset_email)
            ->where('confirmation_expiration', ">=", \Carbon\Carbon::now())
            ->take(1)->get();
        // dd($successful);
        $resetArray = array();
        if(count($successful) > 0){
            $showResetForm = "showit";
            $resetArray = array(
                'showResetForm' => $showResetForm,
                'resetKey' => $reset_key,
                'resetEmail' => $reset_email
            );
        }else{
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Reset Link Invalid');
            \Session::flash('flash_message_body', 'Your reset link is invalid or expired. Please request again or contact us at admin@freyo.com');
        }
        return view('layouts.reset', $resetArray);
    }
    public function passwordreset(Request $request){
        if($request->input('resetkey') == null || $request->input('resetkey') == "" || $request->input('email') == null || $request->input('email') == ""){
            return \Redirect::to('/?app=banana');
        }
        if($request->input('password') !== $request->input('confirm_password')){
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Reset Link Invalid');
            \Session::flash('flash_message_body', 'Your reset link is invalid or expired. Please request again or contact us at admin@freyo.com');
            return view('layouts.master');
        }
        $reset_key = $request->input('resetkey');
        $reset_email = $request->input('email');

        $salty = md5(uniqid(rand(), true));
        $salty = substr($salty,0,5);
        $newPass = Hash::make($request->input('password').$salty);

        $resetFields = array(
            'reset_hash' => "",
            'salt' => $salty,
            'password' => $newPass,
            'reset_by' => \Carbon\Carbon::now(),
        );
        $successful = $this->fUAccount
            ->where('reset_hash', $reset_key)
            ->whereNotNull('reset_hash')
            ->where('reset_hash', '<>', "")
            ->where('email', $reset_email)
            ->where('confirmation_expiration', ">=", \Carbon\Carbon::now())
            ->update($resetFields);

        if($successful > 0){
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Password Changed');
            \Session::flash('flash_message_body', 'Your password has been changed.');
        }else{
            \Session::flash('flash_message_close', false);
            \Session::flash('flash_message_title', 'Reset Password Error');
            \Session::flash('flash_message_body', 'Password changed has encounter some error. Please try again or contact us at admin@freyo.com');
        }
        return view('layouts.master');
    }

    public function favorite_magazine(Request $request){
        // $this->fCached = new CacheController("favorite-magazine-user-$this->fuserID");
        if($this->fuserID == 0){
            return array('status' => 'signin', 'message' => 'Please login to subscribe!');
        }
        $magazine_id = $request->input('magazine_id');
        $userfavorite = $this->fUProfile->select('favorites')
        ->where('user_id',$this->fuserID)
        ->take(1);
        $userfavorite = $userfavorite->get();

        if(count($userfavorite) > 0){
            $favorites = explode(',',$userfavorite[0]->favorites);
            if(in_array($magazine_id,$favorites)){
                return array('status' => 'exist', 'message' => 'Already subscribed!');
            }
            $newfavorites = $magazine_id;
            foreach ($favorites as $key => $value) {
                if($value != ""){
                    $newfavorites .= ",".$value;
                }
            }
            $this->fUProfile->where('user_id', $this->fuserID)
            ->update(['favorites' => $newfavorites]);
            $magazine = new magazineController($request);
            $mTitle = $magazine->update_favorites($magazine_id);

            return array('status' => 'success', 'message' => "<i class='fa fa-check'></i> Successfully subscribed to <b>$mTitle->title</b>");
        }
        return array('status' => 'error', 'messsage' => 'Failed to subscribe');
    }

    public function contact_support(Request $request){
        // return array('status' => 'success', 'message' => 'We have received your concern, we will attend to it asap!');
        // $recaptchakey = $request->input('g-recaptcha-response');
        // $secNOTret = env('RECAPTCHA_SECRET');
        // $gresponse = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secNOTret&response=$recaptchakey"));
        // if($gresponse){
        //     dd($gresponse);
        // }else{
        //     return "FALSE!";
        // }
        $fUserData = json_decode($this->getUserProfile()->getContent(),true);
        if(count($fUserData) > 0){
            $fUserData = $fUserData[0];
            $fContactF = new ContactForm();
            $full_name = $fUserData['first_name'].' '.$fUserData['last_name'];
            $newSupportID = $fContactF->insertGetId(
                [
                'field1' => $full_name, 
                'field3' => $fUserData['email'],
                'field4' => $fUserData['phone_number'],
                'field7' => $request->input('sup_subject'),
                'field8' => $request->input('sup_message'),
                'datepost' => \Carbon\Carbon::now(),
                ]);

            // Send Notification Admin Email
            if($this->get_sysvar_value('contact_emailnotice') == 'yes')
            {
                $this->cemail_from = \Config::get('mail.username');
                $this->cemail_to = $this->get_sysvar_value('contact_adminemail');
                $this->cemail_subject = str_replace('[[contact_id]]', $newSupportID, $this->get_sysvar_value('contact_noticesubject'));
                $this->cemail_message = stripslashes(
                    str_replace('[[message]]', $request->input('sup_message'), $this->get_sysvar_value('contact_noticecontent'))
                );

                // Send to Admin
                if(!empty($this->cemail_from) && !empty($this->cemail_to))
                {
                    Mail::raw($this->cemail_message, function($mail) {
                        $mail->from($this->cemail_from)
                            ->to($this->cemail_to)
                            ->subject($this->cemail_subject);
                    });
                }

                // Send CC
                if(!empty($this->cemail_from))
                {
                    $cc = $this->get_sysvar_value('contact_sendextra');
                    if(!empty($cc))
                    {
                        $arr_email = explode(',', $cc);
                        if(count($arr_email) > 0)
                        {
                            foreach($arr_email as $val)
                            {
                                $this->cemail_cc = $val;

                                Mail::raw($this->cemail_message, function($mail) {
                                    $mail->from($this->cemail_from)
                                        ->to($this->cemail_cc)
                                        ->subject($this->cemail_subject);
                                });
                            }
                        }
                    }
                }
            }

            // Send Auto-Response Email
            if($this->get_sysvar_value('contact_autoresponse') == 'yes')
            {
                $this->cemail_from = \Config::get('mail.username');
                $this->cemail_to = $fUserData['email'];
                $this->cemail_subject = $this->get_sysvar_value('contact_emailsubject');
                $this->cemail_message = stripslashes(
                    str_replace('[[message]]', $request->input('sup_message'), $this->get_sysvar_value('contact_emailcontent'))
                );

                Mail::raw($this->cemail_message, function($mail) {
                    $mail->from($this->cemail_from)
                        ->to($this->cemail_to)
                        ->subject($this->cemail_subject);
                });
            }
            return array('status' => 'success', 'message' => 'We have received your concern, we will attend to it asap!');
        }
        return array('status' => 'error', 'message' => 'Error in processing your request!');
    }

    protected function get_sysvar_value($key = false){
        if($key !== false)
            return SystemVariable::select('value')->where('attribute', $key)->get()[0]['value'];
        else
            return false;
    }

    public function logged_in_checker(Request $request){
        if($request->cookie(\Config::get('constants.user_cookie_name')) !== null){
            $this->fuserID = $request->cookie(\Config::get('constants.user_cookie_name'));
            if($this->fuserID !== null && $this->fuserID > 0){
                return "yes";
            }
        }
        return "no";
    }

    public function resend_confirmation(Request $request){
        $useraccount = $this->fUAccount->select('user_id')->where('email',$request->input('email_add'))->take(1)->get();
        $registerStatus = [];
        if(count($useraccount) == 1) {
            $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
            $confirmation_code = array();
            $alphaLength = strlen($alphabet) - 1;
            for ($i = 0; $i < 30; $i++) {
                $n = rand(0, $alphaLength);
                $confirmation_code[] = $alphabet[$n];
            }
            $confirmation_key = implode($confirmation_code);
            $this->fUAccount->where('user_id', $useraccount[0]->user_id)->update(['confirmation_key' => $confirmation_key]);

            /**
             * Send activation code
             */
            
            $user_emailAdd = $request->input('email_add');
            $activation_url = "http://freyo.com/activate?cId=$confirmation_key&cEmail=$user_emailAdd";
            $this->email_from = \Config::get('mail.username');
            $this->email_subject = "RESEND Freyo Account E-mail Confirmation";
            $this->email_message = "Thank you for registering.\r\n\r\nPlease click on the link below to confirm your e-mail.\r\n\r\n$activation_url \r\n\r\nFrom,\r\nFreyo\r\nwww.freyo.com\r\n";
            $this->email_to = $user_emailAdd;

            Mail::raw($this->email_message, function($mail) {
                $mail->from($this->email_from)
                    ->to($this->email_to)
                    ->subject($this->email_subject);
            });

            $registerStatus['status'] = "email_sent";
            $registerStatus['message'] = "We've sent email confirmation to <b>$user_emailAdd</b>.
            <br/>Please click the link in the message to activate your account.
            <br/>Make sure to check your junk or spam folder. The sender is <b>admin@freyo.com</b>";
        } else {
            $registerStatus['status'] = "not_exist";
            $registerStatus['message'] = "E-mail does not exists!";
        }
        return \Response::json($registerStatus);
    }

    public function add_interest(Request $request){
        // $this->fCached = new CacheController("favorite-magazine-user-$this->fuserID");
        if($this->fuserID == 0){
            return array('status' => 'signin', 'message' => 'Please login to subscribe!');
        }
        $cat_id = $request->input('category_id');
        $userInterest = $this->fUProfile->select('interests')
        ->where('user_id',$this->fuserID)
        ->take(1);
        $userInterest = $userInterest->get();

        if(count($userInterest) > 0){
            $interests = explode(',',$userInterest[0]->interests);
            if(in_array($cat_id,$interests)){
                return array('status' => 'exist', 'message' => 'Already set!');
            }
            $newinterest = $cat_id;
            foreach ($interests as $key => $value) {
                if($value != ""){
                    $newinterest .= ",".$value;
                }
            }
            $this->fUProfile->where('user_id', $this->fuserID)
            ->update(['interests' => $newinterest]);

            return array('status' => 'success');
        }
        return array('status' => 'error', 'messsage' => 'Failed');
    }

    public function remove_interest(Request $request) {
        $cat_id = $request->input('category_id');
        $userInterest = $this->fUProfile->select('interests')
        ->where('user_id',$this->fuserID)
        ->take(1);
        $userInterest = $userInterest->get();

        if(count($userInterest) > 0){
            $interests = explode(',',$userInterest[0]->interests);
            // if(in_array($cat_id,$interests)){
            //     // return array('status' => 'exist', 'message' => 'Already set!');
            //     unset()
            // }
            
            foreach($interests as $i => $d) {
                if($d == $cat_id) {
                    unset($interests[$i]);
                }
            }
            
            $newinterests = implode(",", $interests);
            // dd($newinterests);
            $this->fUProfile->where('user_id', $this->fuserID)
            ->update(['interests' => $newinterests]);

            return array('status' => 'success');
        }
        return array('status' => 'error', 'messsage' => 'Failed');
    }
    
}