<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Cookie\CookieJar;
use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;

class CookieController extends Controller
{
    public function showCookie(Request $request){
        return $request->cookie('freyo_user');
    }

    // public function setCookie(CookieJar $cookies){
    //     $cookies->queue(cookie('freyo_user', '1', 60*24*14));
    // }
}
