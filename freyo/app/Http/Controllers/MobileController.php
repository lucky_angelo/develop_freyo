<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Cookie\CookieJar;

use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;
use Freyo\Http\Controllers\UserController;
use Freyo\PHProvince;
use Freyo\PHCities;
use Jenssegers\Agent\Agent;
use DB;


class MobileController extends Controller
{
	protected $fProvinces;
	protected $fCities;
    protected $fQuery;

    public function index(Request $request, CookieJar $cookies)
    {
        session_start();
        if(\Agent::isAndroidOS()){
        	$latest_version = 3.1;
        }
        elseif(\Agent::is('iPhone') || \Agent::is('OS X')){
        	$latest_version = 3;
        } else {
        	$latest_version = 3;
        }
        $updated_app_link = "#";
        $launchArray = array();
        $not_latest_version = false;
        
    	$data_app = $request->input('app');
    	$is_app = false;
        $fblogin = false;
        //CHECK IF OPEN IN APP
        if(isset($data_app) && $data_app == "banana"){
            \Session::put('is_app', 'true');
            $is_app = true;
        }
        // GET device unique_id
        if(null !== $request->input('deviceid')) {
            \Session::put('unique_id', $request->input('deviceid'));
        }

        //CHECK IF APP VERSION IS UPDATED
       	if(isset($_SESSION['fapp_version'])){
            if($_SESSION['fapp_version'] > 1) {
                $fblogin = true; 
            }

            if($_SESSION['fapp_version'] == $latest_version){
                $not_latest_version = false;
            } else {
                $not_latest_version = true;
                if(\Agent::isAndroidOS()){
                    $updated_app_link = "https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo";
                }
                elseif(\Agent::is('iPhone') || \Agent::is('OS X')){
                    $updated_app_link = "https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8&google.com";
                }
            }
            
        }

        $data_version = $request->input('version');
        if(isset($data_version)){
            $_SESSION['fapp_version'] = $data_version;
            if($data_version > 1) {
                $fblogin = true;
                
            }

            if($data_version == $latest_version) {
                $not_latest_version = false;
            } else {
                $_SESSION['fapp_version'] = $data_version;
                $not_latest_version = true;
                if(\Agent::isAndroidOS()){
                    $updated_app_link = "https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo";
                }
                elseif(\Agent::is('iPhone') || \Agent::is('OS X')){
                    $updated_app_link = "https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8&google.com";
                }
            }
        } 

        if(\Agent::isAndroidOS()){
        	$is_app = true;
            // $fblogin = true; // to be removed
        }
        // if(\Agent::is('iPhone') || \Agent::is('OS X')){
        //     $fblogin = true;
        //     $not_latest_version = false;
        // }
        $launchArray['not_latest_version'] = $not_latest_version;
        $launchArray['updated_app_link'] = $updated_app_link;
        $launchArray['show_FBLogin'] = $fblogin;

        if((\Agent::is('iPhone') || \Agent::is('OS X') || (\Agent::isSafari() && !\Agent::isAndroidOS())) && !$is_app){
            return \Redirect::to('http://freyo.com/');
        }
        if(!$is_app){
            return \Redirect::to('http://freyo.com/');
        }
        // dd(base_path());
        // dd($request->ip());
        // if($request->ip() == "49.145.161.19" || $request->ip() == "112.198.243.181" || $request->ip() == "180.191.130.2" || $request->ip() == "112.198.246.104" || $request->ip() == "180.191.135.86" || $request->ip() == "180.191.79.252"){
            
        // }else{
        //     // dd($request->ip());
        //     // return \Redirect::to('http://freyo.com/');
        // }
        // $request->session()->flush();
        // dd($request->cookie('laravel_session'));4
        if($request->input('si_username') !== null && $request->input('si_password') !== null){
            $fUser = new UserController($request);
            $email = $request->input('si_username');
            $password = $request->input('si_password');
            
            $loginResult = $fUser->checkLogin($email, $password);
            $loginStatus = [];
            if($loginResult['login']){
                if($loginResult['confirmed']){
                    $cookies->queue(cookie(\Config::get('constants.user_cookie_name'), $loginResult['user_id'],60*24*14));
                    $loginStatus['status'] = "success";
                    $loginStatus['message'] = "You are successfully logged in. Please wait.";
                }
                else{
                    $loginStatus['status'] = "not_confirmed";
                    $loginStatus['message'] = "Account(<b>$email</b>) not yet activated. Please click the link in the email we've sent you. If you didn't receive any email please click below. <button class='btn btn-default resend_email_confirmation' style='margin: 10px auto; display: block;' data-email='$email'>Resend email confirmation</button>";
                    $loginStatus['email'] = $email;
                }
            }else{
            	$loginStatus['status'] = "error";
                $loginStatus['message'] = "Incorrect Password or Email.";
            }
            return \Response::json($loginStatus);
        }

        if($request->cookie(\Config::get('constants.user_cookie_name')) !== null){
            return \Redirect::to('/dashboard');
        }

        return view('mobile.launchscreen', $launchArray);
    }

    public function getCities($provinceID){
    	$this->fCities = new PHCities();
    	$meArray = [
            'id',
            'name'
        ];

        $this->fQuery = $this->fCities
            ->select(
                $meArray
            )
            ->where('province_id', $provinceID);

        return $this->fQuery->get();
    }

    public function getProvinceCity(){
    	$this->fProvinces = new PHProvince();

    	$meArray = [
            'id',
            'name',
        ];

        $this->fQuery = $this->fProvinces
            ->select(
                $meArray
            );

        $fItems = $this->fQuery->orderByRaw(\DB::raw("id=47 desc, id asc "))->get();

        $addItems = [];
        if(count($fItems) > 0)
        {
            $ctr = 0;
            foreach ($fItems as $items) {
                $addItems[$ctr] = $items;
                $addItems[$ctr]['cities'] = $this->getCities($items->id);
                $ctr++;
            }
        }

        return $addItems;
    }

}
