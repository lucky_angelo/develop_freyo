<?php

namespace Freyo\Http\Controllers;

use Illuminate\Http\Request;

use Freyo\Http\Requests;
use Freyo\Http\Controllers\Controller;
use Freyo\Http\Controllers\CacheController;
use Freyo\Http\Controllers\UserController;
use Freyo\MagazinePublish;
use Freyo\MagazinePages;
use Freyo\MagazineFeatured;
use Freyo\MagazineRating;
use Freyo\MagazineCategory;
use Freyo\MagazineLikes;
use Freyo\MagazineAds;
use Freyo\MagazineAdsReport;
use Freyo\MagazineSubscribe;
use Freyo\ProfanityModel;
use Freyo\MagazineAdsClickReport; // Added: 7-13-16
use Freyo\MagazineComment;
use Freyo\Interests;

use DB;

class magazineController extends Controller
{
    protected $fCached;
    protected $fMagazine;
    protected $fCategory;
    protected $fPages;
    protected $fFeatured;
    protected $fRating;
    protected $fSubscribe;
    protected $fUserController;
    protected $fAds;
    protected $fAdsReport;
    protected $fAdsClickReport;
    protected $fLikes;
    protected $fQuery;
    protected $fUserAge;
    protected $fUserLocation = "";
    protected $fUserSex;
    protected $fUser_ID;
    protected $adsID = array(0);
    protected $fUserDetails;
    protected $magIDFilter = array(0);
    protected $isFeatured;
    protected $fProfanity;
    protected $fComments;
    protected $fInterests;
    protected $fQuery2;
    public function __construct(Request $request)
    {
        if($request->cookie(\Config::get('constants.user_cookie_name')) !== null){
            $this->fUser_ID = $request->cookie(\Config::get('constants.user_cookie_name'));
        }else{
            $this->fUser_ID = 0;
        }

        $this->fMagazine = new MagazinePublish();
        $this->fPages = new MagazinePages();
        $this->fFeatured = new MagazineFeatured();
        $this->fRating = new MagazineRating();
        $this->fCategory = new MagazineCategory();
        $this->fLikes = new MagazineLikes();
        $this->fSubscribe = new MagazineSubscribe();
        $this->fAds = new MagazineAds();
        $this->fAdsReport = new MagazineAdsReport();
        $this->fUserController = new UserController($request);
        $this->magIDFilter = array(0);
        $this->isFeatured = FALSE;
        $this->fProfanity = new ProfanityModel();
        $this->fAdsClickReport = new MagazineAdsClickReport(); // Added: 7-13-16
        $this->fComments = new MagazineComment();
        $this->fInterests = new Interests();
    }

    public function get_rating_by_user($fMagazineID, $fUserID) {
        

        $getArray = [
            'magazine_rating.rating_id',
            'magazine_rating.user_id',
            'magazine_rating.rating',
        ];

        $this->fQuery = $this->fRating
        ->select($getArray)
        ->where('user_id', $fUserID)
        ->where('magazine_id', $fMagazineID);
        $result = $this->fQuery->get();
        return $result;
    }

    public function get_ratings_reviews($fMagazineID, $fCount = 10){
        // $this->fCached = new CacheController('ratings-reviews-of-magazine-'.$fMagazineID);
        
        $meArray = [
            'magazine_rating.rating_id',
            'magazine_rating.user_id',
            'magazine_rating.rating',
            'magazine_rating.review',
            'freyo_user_profile.first_name',
            'freyo_user_profile.last_name',
            'freyo_user_profile.profile_picture'
        ];

        $bw = $this->fProfanity->get();
        foreach($bw as $i => $bws) {
            $badWords[$i] = $bws->bad_word;
        }

        $badWords = implode($badWords, "|");

        $this->fQuery = $this->fRating
            ->join('freyo_user_profile', 'magazine_rating.user_id', '=', 'freyo_user_profile.user_id')
            ->select(
                $meArray
            )
            ->where('magazine_rating.magazine_id', $fMagazineID)
            ->orderBy('magazine_rating.date_created', 'DESC')
            ->take($fCount);
        $result = $this->fQuery->get();
        $cleaned = [];
        foreach($result as $i => $r) {
            $foundDirty = preg_match_all("/\b(".$badWords.")\b/i", $r->review, $matches);
            if($matches[0]) {
                $words = array_unique($matches[0]);
                
                $original = [];
                foreach($words as $ow) {
                    // $original[] = "/\b(".$ow.")\b/i";
                    $original[] = "/\b(".$ow.")\b/i";
                }
                
            }
            if($foundDirty) {
                $replace = '*****';
                $review = preg_replace($original, $replace, $r->review);
                $cleaned[] = ['rating_id' => $r->rating_id,
                    'user_id' => $r->user_id,
                    'rating' => $r->rating,
                    'review' => $review,
                    'first_name' => $r->first_name,
                    'last_name' => $r->last_name,
                    'profile_picture' => $r->profile_picture,
                ];
            } else {
                $cleaned[] = ['rating_id' => $r->rating_id,
                    'user_id' => $r->user_id,
                    'rating' => $r->rating,
                    'review' => $r->review,
                    'first_name' => $r->first_name,
                    'last_name' => $r->last_name,
                    'profile_picture' => $r->profile_picture,
                ];
            }
            
        }
        return $cleaned;
    }


    public function get_latest_magazine($fCategoryID = "false", $fCount = "false", $fPicks = "false", $fFeatured = "false"){
        $this->fCached = new CacheController("latest-magazine-$fCategoryID-$fCount-$fPicks-$fFeatured");

        $meArray = [
            'magazine_publisher.magazine_id',
            'magazine_publisher.title',
            'magazine_publisher.magazine_cover',
            'magazine_publisher.details',
            // 'magazine_publisher.issue_date',
            'magazine_publisher.views',
            'magazine_publisher.favorites',
            'magazine_publisher.date_updated',
            'magazine_category.category_id',
            'magazine_category.parent_id',
            DB::raw('AVG(COALESCE(magazine_rating.rating,0)) AS rating, COUNT(magazine_rating.rating) AS total_rating, magazine_category.name AS category_name')
        ];

        $this->fQuery = $this->fMagazine
            ->leftJoin('magazine_category', 'magazine_publisher.category_id', '=', 'magazine_category.category_id')
            ->leftJoin('magazine_rating', 'magazine_publisher.magazine_id', '=', 'magazine_rating.magazine_id')
            ->select(
                $meArray
            )
            ->where('magazine_publisher.publish', '=', 'yes')
            // ->where('magazine_publisher.issue_date', '<=', \Carbon\Carbon::now()->toDateString())
            ->orderBy('magazine_publisher.date_updated', 'desc')
            ->groupBy('magazine_publisher.magazine_id');

        if($fCategoryID == '-1'){
            $fPicks = "yes";
        }
        elseif($fCategoryID != "false" && $fCategoryID != 0){
            $this->fQuery->where('magazine_publisher.category_id' , $fCategoryID);
                // ->orWhere('magazine_category.parent_id' , $fCategoryID);
        }
        if($fCount != "false" && $fPicks != "yes") {
            $this->fQuery->take($fCount);
        }
        if($fPicks == "no" || $fPicks == "yes"){
            
            $interests = $this->get_user_interests($this->fUser_ID);
            // dd($interests);
            if(isset($interests) || $interests !== null) {
                $this->fQuery->whereIn('magazine_publisher.category_id' , $interests);
            }
             else {
                // $this->fQuery->where('magazine_publisher.freyo_pick' , $fPicks);
                $this->fQuery->where('magazine_publisher.category_id' , 0);
            }
           
            // dd($interests);
        }
        if($fFeatured == "no" || $fFeatured == "yes"){
            $this->fQuery->where('magazine_publisher.featured' , $fFeatured);
        }

        if(count($this->magIDFilter) > 0 && $this->isFeatured){
            $this->fQuery->whereNotIn('magazine_publisher.magazine_id', $this->magIDFilter);
        }

        // $fItems = $this->fCached->get($this->fQuery);
        $fItems = $this->fQuery->get();

        // dd($fItems);
        $addItems = [];
        if(count($fItems) > 0)
        {
            $ctr = 0;
            foreach ($fItems as $items) {
                $addItems[$ctr] = $items;
                $addItems[$ctr]['reviews'] = $this->get_ratings_reviews($items->magazine_id);
                $addItems[$ctr]['ratebyuser'] = $this->get_rating_by_user($items->magazine_id, $this->fUser_ID);
                $addItems[$ctr]['issues'] = $this->get_issues($items->category_id, $items->magazine_id);
                $addItems[$ctr]['comments'] = $this->get_comments($items->magazine_id);
                $ctr++;
            }
        }

        return $addItems;
    }

    // public function get_recommended_magazine() {
    //     $this->fCached = new CacheController("recomended-magazines");

    //     $meArray = [
    //         'magazine_publisher.magazine_id',
    //         'magazine_publisher.title',
    //         'magazine_publisher.magazine_cover',
    //         'magazine_publisher.details',
    //         // 'magazine_publisher.issue_date',
    //         'magazine_publisher.views',
    //         'magazine_publisher.favorites',
    //         'magazine_publisher.date_updated',
    //         'magazine_category.category_id',
    //         'magazine_category.parent_id',
    //         DB::raw('AVG(COALESCE(magazine_rating.rating,0)) AS rating, COUNT(magazine_rating.rating) AS total_rating, magazine_category.name AS category_name')
    //     ];

    //     $this->fQuery = $this->fMagazine
    //         ->leftJoin('magazine_category', 'magazine_publisher.category_id', '=', 'magazine_category.category_id')
    //         ->leftJoin('magazine_rating', 'magazine_publisher.magazine_id', '=', 'magazine_rating.magazine_id')
    //         ->select(
    //             $meArray
    //         )
    //         ->where('magazine_publisher.publish', '=', 'yes')
    //         // ->where('magazine_publisher.issue_date', '<=', \Carbon\Carbon::now()->toDateString())
    //         ->orderBy('magazine_publisher.date_updated', 'desc')
    //         ->groupBy('magazine_publisher.magazine_id');
    //         $interests = $this->get_user_interests($this->fUser_ID);
    //         // dd($interests);
    //         if(isset($interests) || $interests !== null) {
    //              $this->fQuery->whereIn('magazine_publisher.category_id' , $interests);
    //         }
    //          else {
    //             $this->fQuery->where('magazine_publisher.freyo_pick' , $fPicks);
    //         }
    //         $fItems = $this->fQuery->get();
    // }

    public function get_user_interests($fUserID) {
        $this->fQuery2 = $this->fInterests
        ->select(['magazine_category.category_id'])
        ->leftJoin('magazine_category', 'magazine_category.category_id', '=', 'freyo_user_interests.category_id')
        ->where('user_id', $fUserID);
        // ->where('freyo_user_interests.user_id', '!=', $fUserID);
        // 
        $fItems = $this->fQuery2->get();
        // dd($fItems);
        if(count($fItems) > 0) {
            $this->fQuery2 = $this->fCategory
            ->select(['magazine_category.category_id']);
            foreach($fItems as $i => $value) {   
                $this->fQuery2->orWhere('parent_id', $value->category_id);
                // echo '<pre>'.$value->category_id.'</pre>';
            }
            $fItems2 = $this->fQuery2->get();
            $a = array_merge($fItems->toArray(), $fItems2->toArray());
            $interests_array = [];
            $i = 0;
            foreach($a as $val) {
                $interests_array[$i] = $val['category_id'];
                $i++;
            }
            // $result = $fItems->merge($fItems2);
            return $interests_array;
        } else {
            return null;
        }
        // $last = end($fItems);
        // $res = [];
        // $c = 0;
        
    }
// CASE (SELECT magazine_likes.page_id FROM magazine_likes WHERE magazine_likes.user_id = "'.$this->fUser_ID.'" AND magazine_pages.page_id = magazine_likes.page_id LIMIT 1) WHEN "null" THEN "no" ELSE "yes" END AS liked
    public function get_magazine_pages($fMagazineID = FALSE){
        $theckey = "magazine-pages-of-$fMagazineID";
        if($this->fUser_ID == 0){
            $theckey = "0-magazine-pages-of-$fMagazineID";
        }
        $this->fCached = new CacheController($theckey);

        if($fMagazineID !== FALSE){
            $meArray = [
                'magazine_publisher.magazine_id',
                'magazine_publisher.title',
                'magazine_publisher.category_id',
                'magazine_pages.page_id',
                'magazine_pages.magazine_page',
                'magazine_pages.magazine_content',
                'magazine_pages.video_webm',
                'magazine_pages.video_ogv',
                'magazine_pages.video_poster',
                'magazine_pages.page_number',
                'magazine_pages.page_type',
                DB::raw('(SELECT magazine_likes.page_id FROM magazine_likes WHERE magazine_likes.user_id = "'.$this->fUser_ID.'" AND magazine_pages.page_id = magazine_likes.page_id LIMIT 1) AS liked,DATE_FORMAT(magazine_pages.date_uploaded,"%Y-%m-%d") as date_uploaded,COUNT(magazine_likes.user_id) AS likes')
            ];

            $this->fQuery = $this->fPages
                ->leftJoin('magazine_publisher', 'magazine_pages.magazine_id', '=', 'magazine_publisher.magazine_id')
                ->leftJoin('magazine_likes', 'magazine_pages.page_id', '=', 'magazine_likes.page_id')
                ->select(
                    $meArray
                )
                ->where('magazine_pages.magazine_id', $fMagazineID)
                ->orderBy('magazine_pages.page_number', 'ASC')
                ->groupBy('magazine_pages.page_id');


            if($this->fUser_ID == 0){
                $this->fQuery->take(5);
            }
            // $fItems = $this->fCached->get($this->fQuery);
            $fItems = $this->fQuery->get();

            $this->fUserData = json_decode($this->fUserController->getUserProfile()->getContent(),true);
            if(count($this->fUserData) > 0){
                $this->fUserData = $this->fUserData[0];
                $birthdate = $this->fUserData['birthdate'];
                $birthdate = explode("-", $birthdate);
                $this->fUserAge = \Carbon\Carbon::createFromDate($birthdate[0], $birthdate[1], $birthdate[2])->age;
                $this->fUserSex = $this->fUserData['sex'];
                if($this->fUserData['province'] != 0 ){
                    $this->fUserLocation = $this->fUserData['province'];
                }else{
                    $this->fUserLocation = -6;
                }
            }else{
                $this->fUserAge = -6;
                $this->fUserSex = "all";
                $this->fUserLocation = -6;
            }
            foreach ($fItems as $key => $items) {
                if($items->page_type == 'ads'){
                    $parentID = $this->category_parent($items->category_id);
                    $parentID = $parentID['parent_id'];
                    if($parentID == 0){
                        $parentID = -1;
                    }
                    $fItems[$key]['magazine_page'] = $this->get_magazine_ads($items->magazine_page, $items->magazine_id, $items->category_id, $parentID);
                }
            }
            if(count($fItems) > 0){
                array_push($this->magIDFilter, $fItems[0]->magazine_id);
                $fItems[0]['related_magazines'] = $this->get_related_magazine($fItems[0]->category_id);
                 $fItems[0]['other_magazines'] = $this->get_other_magazine();
            }

            // dd($fItems->toArray());
            $this->fMagazine->where('magazine_id', '=', $fMagazineID)
            ->increment('views');

            if($this->fUser_ID != 0){
                $this->fSubscribe->where('magazine_id', '=', $fMagazineID)
                ->where('user_id', '=', $this->fUser_ID)
                ->update(['date_last_viewed' => \Carbon\Carbon::now()]);
            }

            return \Response::json($fItems);
        }
    }

    public function get_featured_magazines(Request $request){
        $ctr = 0;
        $featuredMagazines = array();
        $freyoPicks = $this->get_latest_magazine("false", 3, "yes");
        if(count($freyoPicks) > 0){
            $featuredMagazines[$ctr]['category_id'] = '-1';
            $featuredMagazines[$ctr]['category_name'] = 'Recommended';
            $featuredMagazines[$ctr]['magazines'] = $freyoPicks;
            $ctr++;
            foreach ($freyoPicks as $key => $value) {
                array_push($this->magIDFilter, $value->magazine_id);
            }
        } else {
            $a = [];
            $featuredMagazines[$ctr]['category_id'] = '-1';
            $featuredMagazines[$ctr]['category_name'] = 'Recommended';
            $featuredMagazines[$ctr]['magazines'] = $a;
            $ctr++;
        }
        
        $categoryIDs = $this->fCategory
        ->select('parent_id','category_id','name')
        // ->orderBy('name', 'asc');
        ->orderBy('sequence', 'asc');
        $this->fCached = new CacheController("all-magazine-category");
        $categoryIDs = $this->fCached->get($categoryIDs);
        // dd($categoryIDs);
        // $categoryIDs = $categoryIDs->get();
        
        // $fUserDetails = json_decode($this->fUserController->getUserProfile()->getContent(),true);
        // if(count($categoryIDs) == 'awdkl'){
            // $fFavorites = explode(',', $fUserDetails[0]['favorites']);
            // foreach ($categoryIDs as $key => $value) {
            //     if($this->category_parent($value->category_id) !== false){
            //         $parentCat = $this->category_parent($value->category_id);
            //         $userFavorites = $this->get_latest_magazine($parentCat['category_id'], 3, 'no');
            //         if(count($userFavorites) > 0){
            //             $featuredMagazines[$ctr]['category_id'] = $parentCat['category_id'];
            //             $featuredMagazines[$ctr]['category_name'] = $parentCat['name'];
            //             $featuredMagazines[$ctr]['magazines'] = $userFavorites;
            //             $ctr++;
            //         }
            //     }
            // }
        // }
        $this->isFeatured = true;
        $most_recent = $this->get_latest_magazine("false");

        if(count($most_recent) > 0){
            $featuredMagazines[$ctr]['category_id'] = "0";
            $featuredMagazines[$ctr]['category_name'] = "";
            $featuredMagazines[$ctr]['magazines'] = $most_recent;
        }

        return \Response::json($featuredMagazines);
    }

    public function get_featured_slider($fCategoryID = FALSE, $fCount = FALSE){
        $this->fCached = new CacheController("featured-slider-$fCategoryID-$fCount");
        $meArray = [
            'featured_id',
            'featured_image',
            'category_id',
            'date_posted',
        ];

        $this->fQuery = $this->fFeatured
            // ->join('magazine_category', 'magazine_featured.category_id', '=', 'magazine_category.category_id')
            ->select(
                $meArray
            )
            ->where('date_posted', '<=', \Carbon\Carbon::now()->toDateString())
            ->orderBy('date_posted', 'desc');

        if($fCount !== FALSE){
            $this->fQuery->take($fCount);
        }
        

        if($fCategoryID !== FALSE){
            $this->fQuery->where('magazine_featured.category_id' , $fCategoryID);
        }

        $fItems = $this->fQuery->get();

        return $fItems;
    }

    public function category_parent($fCatID){
        $this->fCached = new CacheController("magazine-category-parent-$fCatID");
        $parentID = $this->fCategory->select('parent_id','category_id','name')
            ->where('magazine_category.category_id', $fCatID)->take(1);
        $parentID = $this->fCached->get($parentID);
        if(count($parentID) > 0){
            return $parentID[0];
            // if($parentID[0]->parent_id == 0){
            //     return $parentID[0];
            // }
            // $parentID = $this->fCategory->select('parent_id','category_id','name')
            // ->where('magazine_category.category_id', $parentID[0]->parent_id)->take(1)->get();
            // if(count($parentID) > 0){
            //     return $parentID[0];
            // }
        }
        return false;
    }

    public function rate_magazine(Request $request){
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $bw = $this->fProfanity->get();
        foreach($bw as $i => $bws) {
            $badWords[$i] = $bws->bad_word;
        }
        $badWords = implode($badWords, "|");
        $foundDirty = preg_match_all("/\b(".$badWords.")\b/i", $request->input('review'), $matches);
        if($matches[0]) {
            $words = array_unique($matches[0]);
            
            $original = [];
            foreach($words as $ow) {
                $original[] = "/\b(".$ow.")\b/i";
            }
            
        }
        if($foundDirty) {
            $replace = '*****';
            $review = preg_replace($original, $replace, $request->input('review'));
        } else {
            $review = $request->input('review');
        }
        if($fUserID != null && $fUserID != 0) {
            $rateArray = array(
                'magazine_id' => $request->input('magazine_id'),
                'user_id' => $fUserID,
                'rating' => $request->input('rating'),
                'review' => $request->input('review'),
                'date_created' => \Carbon\Carbon::now(),
            );
            $this->fRating->insert($rateArray);
            return array('status' => 'success', 'bws' => $badWords, 'cleaned' => $review, 'rating' => $request->input('rating'));
        }
        return array('status' => 'error');
    }

    public function submit_comment(Request $request) {
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $bw = $this->fProfanity->get();
        foreach($bw as $i => $bws) {
            $badWords[$i] = $bws->bad_word;
        }
        $badWords = implode($badWords, "|");
        $foundDirty = preg_match_all("/\b(".$badWords.")\b/i", $request->input('review'), $matches);
        if($matches[0]) {
            $words = array_unique($matches[0]);
            
            $original = [];
            foreach($words as $ow) {
                $original[] = "/\b(".$ow.")\b/i";
            }
            
        }
        if($foundDirty) {
            $replace = '*****';
            $review = preg_replace($original, $replace, $request->input('comment'));
        } else {
            $review = $request->input('comment');
        }
        if($fUserID != null && $fUserID != 0) {
            $rateArray = array(
                'magazine_id' => $request->input('magazine_id'),
                'user_id' => $fUserID,
                'comment' => $request->input('comment'),
                'date_created' => \Carbon\Carbon::now(),
                );
            $this->fComments->insert($rateArray);
            return array('status' => 'success', 'bws' => $badWords, 'cleaned' => $review);
        }
        return array('status' => 'error');
    }

    public function update_favorites($mag_id = FALSE){
        if($mag_id !== FALSE){
            $this->fMagazine->where('magazine_id', '=', $mag_id)
            ->increment('favorites');
            return $this->fMagazine->select('title')
            ->where('magazine_id', $mag_id)->first();
        }
    }

    public function like_magazine_page(Request $request){
        $page_id = $request->input('page_id');
        // $this->fCached = new CacheController("user-$this->fUser_ID-like-magazine-page-$page_id");
        if($this->fUser_ID == 0){
            return array('status' => 'signin', 'message' => 'Please login to like!');
        }
        elseif($page_id !== null){
            $userliked = $this->fLikes->select('user_id')
            ->where('user_id', $this->fUser_ID)
            ->where('page_id', $page_id)
            ->take(1);
            $userliked = $userliked->get();

            if(count($userliked) > 0){
                return array('status' => 'exist', 'message' => 'Already liked!');
            }
            $this->fLikes->insert([
                'page_id' => $page_id,
                'user_id' => $this->fUser_ID,
            ]);
            return array('status' => 'success', 'message' => 'Successfully liked!');
        }
        return array('status' => 'error', 'messsage' => 'Failed to liked');
    }

    public function unlike_magazine_page(Request $request){
        $page_id = $request->input('page_id');
        $like_id = $request->input('like_id');
        $magazine_id = $request->input('magazine_id');
        // $this->fCached = new CacheController("user-$this->fUser_ID-like-magazine-page-$page_id");
        if($this->fUser_ID == 0){
            return array('status' => 'signin', 'message' => 'Please login to unlike!');
        }
        // elseif($page_id !== null && $like_id !== null){
        elseif($page_id !== null && $this->fUser_ID !== null){
            $this->fLikes->where('user_id', $this->fUser_ID)
            ->where('page_id', $page_id)
            // ->where('likes_id', $like_id)
            ->delete();
            if($magazine_id !== null){
                if(\Cache::has("magazine-pages-of-$magazine_id")){
                    \Cache::forget("magazine-pages-of-$magazine_id");
                }
                 if(\Cache::has("0-magazine-pages-of-$magazine_id")){
                    \Cache::forget("0-magazine-pages-of-$magazine_id");
                }
            }
            return array('status' => 'success', 'message' => 'Successfully unliked!');
        }
        return array('status' => 'error', 'messsage' => 'Failed to unlike');
    }

    public function get_liked_magazines(){
        // $this->fCached = new CacheController("user-$this->fUser_ID-liked-magazine-pages");
        $meArray = [
            'magazine_likes.likes_id',
            'magazine_pages.magazine_id',
            'magazine_pages.page_id',
            'magazine_pages.magazine_page',
            'magazine_pages.page_type',
            DB::raw('DATE_FORMAT(magazine_pages.date_uploaded,"%Y-%m-%d") as date_uploaded')
        ];
        $this->fQuery = $this->fLikes
            ->join('magazine_pages', 'magazine_likes.page_id', '=', 'magazine_pages.page_id')
            ->select($meArray)
            ->where('magazine_likes.user_id', $this->fUser_ID)
            ->where('magazine_pages.page_type', '<>', "video")
            ->orderBy('magazine_likes.likes_id', 'desc');

        return \Response::json($this->fQuery->get());
    }

    public function subscribe_magazine(Request $request){
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        if($fUserID == 0){
            return array('status' => 'signin', 'message' => 'Please login to subscribe!');
        }
        $magazine_id = $request->input('magazine_id');

        $isSubscribe = $this->fSubscribe->select('subscribe_id')
        ->where('magazine_id', $magazine_id)
        ->where('user_id', $fUserID)
        ->take(1)->get();

        if(count($isSubscribe) == 1){
            return array('status' => 'exist', 'message' => 'Already subscribed!');
        }

        $subscribe_id = $this->fSubscribe->insertGetId(
            [
            'user_id' => $fUserID,
            'magazine_id' => $magazine_id,
            'date_last_viewed' => \Carbon\Carbon::now(),
            'date_created' => \Carbon\Carbon::now(),
            ]
        );
        $mTitle = $this->update_favorites($magazine_id);
        return array('status' => 'success', 'message' => "<i class='fa fa-check'></i> Successfully subscribed to <b>$mTitle->title</b>");
    }

    public function get_subscribed_magazines(Request $request){
        $meArray = [
            'magazine_publisher.magazine_id',
            'magazine_publisher.title',
            'magazine_publisher.magazine_cover',
            'magazine_publisher.details',
            // 'magazine_publisher.issue_date',
            'magazine_publisher.date_updated',
            'magazine_publisher.views',
            'magazine_publisher.favorites',
            'magazine_category.category_id',
            'magazine_category.parent_id',
            DB::raw('AVG(COALESCE(magazine_rating.rating,0)) AS rating,magazine_category.name AS category_name')
        ];
        $fSubMags = array();
        $fUserDetails = json_decode($this->fUserController->getUserProfile()->getContent(),true);
        if(count($fUserDetails) > 0){
            $fSubscribes = explode(',', $fUserDetails[0]['favorites']);
            if($fSubscribes[0] != ""){
                $this->fCached = new CacheController($fUserDetails[0]['favorites']."-subscribed-magazines");
                $this->fQuery = $this->fMagazine
                    ->leftJoin('magazine_category', 'magazine_publisher.category_id', '=', 'magazine_category.category_id')
                    ->leftJoin('magazine_rating', 'magazine_publisher.magazine_id', '=', 'magazine_rating.magazine_id')
                    ->select(
                        $meArray
                    )
                    // ->where('magazine_publisher.issue_date', '<=', \Carbon\Carbon::now()->toDateString())
                    ->whereIn('magazine_publisher.magazine_id', $fSubscribes)
                    // ->orderBy('magazine_publisher.issue_date', 'desc')
                    ->groupBy('magazine_publisher.magazine_id');

                $fSubMags = $this->fCached->get($this->fQuery);
            }
        }
        return \Response::json($fSubMags);
    }

    public function advertisement_viewed(Request $request){
        $fads_id = $request->input('ads_id');

        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        if($fUserID == null){
            $fUserID = 0;
        }
        if($fads_id > 0){
            $this->fAds->where('ads_id','=',$fads_id)->increment('view');
            $adsData = $this->fAds->where('ads_id','=',$fads_id)->first();
            if($adsData->view >= $adsData->impression){
                $this->fAds->where('ads_id', '=', $fads_id)
                ->update(['status' => 'e']);
            }

            $reportArray = array(
                'magazine_id' => $request->input('mag_id'),
                'user_id' => $fUserID,
                'ads_id' => $fads_id,
                'page_num_viewed' => $request->input('page_num'),
                'datetime_viewed' => \Carbon\Carbon::now(),
                );
            $this->fAdsReport->insert($reportArray);
        }

    }

    public function advertisement_clicked(Request $request) {
        $fads_id = $request->input('ads_id');

        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        if($fUserID == null){
            $fUserID = 0;
        }
        if($fads_id > 0){
            $this->fAds->where('ads_id','=',$fads_id)->increment('click');
            // $adsData = $this->fAds->where('ads_id','=',$fads_id)->first();
            // if($adsData->view >= $adsData->impression){
            //     $this->fAds->where('ads_id', '=', $fads_id)
            //     ->update(['status' => 'e']);
            // }

            $reportArray = array(
                'magazine_id' => $request->input('mag_id'),
                'user_id' => $fUserID,
                'ads_id' => $fads_id,
                'datetime_clicked' => \Carbon\Carbon::now(),
                );
            $this->fAdsClickReport->insert($reportArray);
        }
    }

    public function get_magazine_ads($adsType, $magID, $catID, $catParent){
        $adsSelectArray = [
            'ads_id',
            'type',
            'content',
            'content_webm',
            'content_ogv',
            'ads_timer',
            'ads_link'
        ];

        for ($i=0; $i<=15; $i++) { 
            // $this->fCached = new CacheController("$i-magazine-ads-of-$adsType-$magID-$catID-$catParent");
            $this->fQuery = $this->fAds->select($adsSelectArray)
            ->whereNotIn('ads_id', $this->adsID)
            ->where('status', 'a');

            if($adsType !== 'all'){
                $this->fQuery->where('type', $adsType);
            }

            switch ($i) {
                case 0:
                    $this->fQuery->where('section', 'issue')
                    ->where('section_id',$magID)
                    ->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                    ->where('gender', $this->fUserSex)
                    ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                    break;
                case 1:
                    $this->fQuery->where('section', 'issue')
                    ->where('section_id',$magID)
                    ->where(function($query){
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', $this->fUserSex)
                        ->where('location', '0')
                        ->orWhere(function($qury){
                            $qury->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                            ->where('gender', 'all')
                            ->whereRaw('find_in_set(?,location)', [$this->fUserLocation])
                            ->orWhere(function($qry){
                                $qry->where('gender', $this->fUserSex)
                                ->where('age', '-1')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 2:
                    $this->fQuery->where('section', 'issue')
                    ->where('section_id',$magID)
                    ->where(function($query) {
                    $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', 'all')
                        ->where('location', '0')
                        ->orWhere(function($gquery){
                            $gquery->where('gender', $this->fUserSex)
                            ->where('age', '-1')
                            ->where('location', '0')
                            ->orWhere(function($lquery){
                                    $lquery->where('gender', 'all')
                                    ->where('age', '-1')
                                    ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                                });
                        });
                    });
                    break;
                case 3:
                    $this->fQuery->where('section', 'issue')
                    ->where('section_id',$magID)
                    ->where('age', '-1')
                    ->where('gender', 'all')
                    ->where('location', '0');
                    break;
                case 4:
                    $this->fQuery->where('section', 'magazine')
                    ->where('section_id',$catID)
                    ->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                    ->where('gender', $this->fUserSex)
                    ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                    break;
                case 5:
                    $this->fQuery->where('section', 'magazine')
                    ->where('section_id',$catID)
                    ->where(function($query){
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', $this->fUserSex)
                        ->where('location', '0')
                        ->orWhere(function($qury){
                            $qury->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                            ->where('gender', 'all')
                            ->whereRaw('find_in_set(?,location)', [$this->fUserLocation])
                            ->orWhere(function($qry){
                                $qry->where('gender', $this->fUserSex)
                                ->where('age', '-1')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 6:
                    $this->fQuery->where('section', 'magazine')
                    ->where('section_id',$catID)
                    ->where(function($query) {
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', 'all')
                        ->where('location', '0')
                        ->orWhere(function($gquery){
                            $gquery->where('gender', $this->fUserSex)
                            ->where('age', '-1')
                            ->where('location', '0')
                            ->orWhere(function($lquery){
                                $lquery->where('age', '-1')
                                ->where('gender', 'all')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 7:
                    $this->fQuery->where('section', 'magazine')
                    ->where('section_id',$catID)
                    ->where('age', '-1')
                    ->where('gender', 'all')
                    ->where('location', '0');
                    break;
                case 8:
                    $this->fQuery->where('section', 'category')
                    ->where(function($query) use ($catID, $catParent) {
                        $query->where('section_id',$catID)
                              ->orWhere('section_id',$catParent);
                        })
                    ->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                    ->where('gender', $this->fUserSex)
                    ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                    break;
                case 9:
                    $this->fQuery->where('section', 'category')
                    ->where(function($query) use ($catID, $catParent){
                        $query->where('section_id',$catID)
                              ->orWhere('section_id',$catParent);
                        })
                    ->where(function($query){
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', $this->fUserSex)
                        ->where('location', '0')
                        ->orWhere(function($qury){
                            $qury->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                            ->where('gender', 'all')
                            ->whereRaw('find_in_set(?,location)', [$this->fUserLocation])
                            ->orWhere(function($qry){
                                $qry->where('gender', $this->fUserSex)
                                ->where('age', '-1')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 10:
                    $this->fQuery->where('section', 'category')
                    ->where(function($query) use ($catID, $catParent) {
                        $query->where('section_id',$catID)
                              ->orWhere('section_id',$catParent);
                        })
                    ->where(function($query) {
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', 'all')
                        ->where('location', '0')
                        ->orWhere(function($gquery){
                            $gquery->where('age', '-1')
                            ->where('gender', $this->fUserSex)
                            ->where('location', '0')
                            ->orWhere(function($lquery){
                                $lquery->where('age', '-1')
                                ->where('gender', 'all')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 11:
                    $this->fQuery->where('section', 'category')
                    ->where(function($query) use ($catID, $catParent) {
                        $query->where('section_id',$catID)
                              ->orWhere('section_id',$catParent);
                        })
                    ->where('age', '-1')
                    ->where('gender', 'all')
                    ->where('location', '0');
                    break;
                case 12:
                    $this->fQuery->where('section', 'all')
                    ->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                    ->where('gender', $this->fUserSex)
                    ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                    break;
                case 13:
                    $this->fQuery->where('section', 'all')
                    ->where(function($query){
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', $this->fUserSex)
                        ->where('location', '0')
                        ->orWhere(function($qury){
                            $qury->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                            ->where('gender', 'all')
                            ->whereRaw('find_in_set(?,location)', [$this->fUserLocation])
                            ->orWhere(function($qry){
                                $qry->where('gender', $this->fUserSex)
                                ->where('age', '-1')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                case 14:
                    $this->fQuery->where('section', 'all')
                    ->where(function($query) {
                        $query->where('age', '<=', $this->fUserAge)->where('age2', '>=', $this->fUserAge)
                        ->where('gender', 'all')
                        ->where('location', '0')
                        ->orWhere(function($gquery){
                            $gquery->where('age', '-1')
                            ->where('gender', $this->fUserSex)
                            ->where('location', '0')
                            ->orWhere(function($lquery){
                                $lquery->where('age', '-1')
                                ->where('gender', 'all')
                                ->whereRaw('find_in_set(?,location)', [$this->fUserLocation]);
                            });
                        });
                    });
                    break;
                default:
                    $this->fQuery->where('section', 'all')
                    ->where('age', '-1')
                    ->where('gender', 'all')
                    ->where('location', '0');
                    break;
            }

            // $adsItems = $this->fCached->get($this->fQuery)->toArray();
            $adsItems = $this->fQuery->get()->toArray();
            if(count($adsItems) > 0){
                shuffle($adsItems);
                $adsItems = $adsItems[0];
                array_push($this->adsID, $adsItems['ads_id']);
                return array('ads_type' => $adsItems['type'],
                'ads_content' => $adsItems['content'],
                'content_webm' => $adsItems['content_webm'],
                'content_ogv' => $adsItems['content_ogv'],
                'ads_id' => $adsItems['ads_id'],
                'ads_timer' => $adsItems['ads_timer'],
                'ads_link' => $adsItems['ads_link']
                );
            }
        }
        
        return array('ads_type' => 'image',
            'ads_content' => 'default-adsimage.png',
            'ads_id' => '0');

        return $adsToShow;

        $addAds = [];

        if(count($fItems) > 0)
        {
            $adsCount = array_count_values(array_column($fItems->toArray(), 'page_type'));
            $adsCount = $adsCount['ads'];
            // while($adsCount != 0){
                $ctr = 0;
                foreach ($fItems as $key => $items) {
                    // $addAds[$ctr] = $items;
                    if($items->page_type == 'ads'){
                        $fItems[$key]['magazine_page'] = array('ads_type' => $items->magazine_page, 'ads_content' => 'video or image');
                    }
                    $ctr++;
                }
            // }
        }
        // dd($addAds);
        // $this->fQuery = $this->fAds->select()
        //     ->whereNotIn('ads_id', $this->adsID)
        //     ->where('age','=>', $this->fUserAge)
        //     ->where('age2','<=', $this->fUserAge)
        // array('ads_type' => $items->magazine_page, 'ads_content' => 'video or54654546 image');
        // return $fItems;
    }

    public function get_related_magazine($catID = FALSE){
        if($catID !== FALSE){
            $parentID = $this->category_parent($catID);
            $parentID = $parentID['parent_id'];

            $this->fCached = new CacheController("wcache-related-magazine-$catID");

            $meArray = [
                'magazine_publisher.magazine_id',
                'magazine_publisher.title',
                'magazine_publisher.magazine_cover'
            ];

            $this->fQuery = $this->fMagazine
                ->leftJoin('magazine_category', 'magazine_publisher.category_id', '=', 'magazine_category.category_id')
                ->select(
                    $meArray
                )
                ->where(function($query) use ($catID, $parentID) {
                    $query->where('magazine_publisher.category_id', $catID)
                          ->orWhere('magazine_category.parent_id', $parentID)
                          ->where('magazine_category.parent_id', '<>', 0);
                    })
                ->whereNotIn('magazine_publisher.magazine_id', $this->magIDFilter);

            $rItems = $this->fCached->get($this->fQuery);

            $rItems = $rItems->toArray();

            shuffle($rItems);

            $rItems = array_slice($rItems, 0, 3);

            foreach ($rItems as $key => $value) {
                array_push($this->magIDFilter, $value['magazine_id']);
            }

            return $rItems;

        }
        return FALSE;
    }

    public function get_other_magazine(){

        $this->fCached = new CacheController("wcache-other-magazines");

        $meArray = [
            'magazine_id',
            'title',
            'magazine_cover'
        ];

        $this->fQuery = $this->fMagazine
            ->select(
                $meArray
            )
            ->whereNotIn('magazine_id', $this->magIDFilter)
            ->orderBy('date_updated', 'desc')
            ->take(3);

        $rItems = $this->fQuery->get();

        $rItems = $rItems->toArray();

        return $rItems;
    }

    public function get_issues($cat_id, $mag_id) {
        $this->fCached = new CacheController("issues-category-$cat_id");
        $this->fQuery = $this->fMagazine
        ->where('magazine_id', '!=', $mag_id)
        ->where('category_id', $cat_id);
        $issues = $this->fQuery->get();
        // $issues = $issues->toArray();

        // return \Response::json($issues);
        return $issues;
    }

    public function get_comments($mag_id, $fromCommPage = 0) {
         $meArray = [
            'magazine_comments.comment_id',
            'magazine_comments.magazine_id',
            'magazine_comments.user_id',
            'magazine_comments.comment',
            'magazine_comments.date_created',
            'freyo_user_profile.first_name',
            'freyo_user_profile.last_name',
            'freyo_user_profile.profile_picture'
        ];

        $bw = $this->fProfanity->get();
        foreach($bw as $i => $bws) {
            $badWords[$i] = $bws->bad_word;
        }

        $badWords = implode($badWords, "|");

        $this->fQuery = $this->fComments
            ->join('freyo_user_profile', 'magazine_comments.user_id', '=', 'freyo_user_profile.user_id')
            ->select(
                $meArray
            )
            ->where('magazine_comments.magazine_id', $mag_id)
            ->orderBy('magazine_comments.date_created', 'DESC');
        $result = $this->fQuery->get();
        $cleaned = [];
        if(count($result)){
            foreach($result as $i => $r) {
                $foundDirty = preg_match_all("/\b(".$badWords.")\b/i", $r->comment, $matches);
                $datetime = new \DateTime($r->date_created);
                // $datetime = new \DateTime('2016-05-12 07:42:00');
                $dateiso = $datetime->format(\DateTime::ISO8601);
                $readable = $datetime->format('M d, Y');
                if($matches[0]) {
                    $words = array_unique($matches[0]);
                    
                    $original = [];
                    foreach($words as $ow) {
                        // $original[] = "/\b(".$ow.")\b/i";
                        $original[] = "/\b(".$ow.")\b/i";
                    }
                    
                }
                $fname = strtolower($r->first_name);
                $fname = ucwords($fname);
                $lname = strtolower($r->last_name);
                $lname = ucwords($lname);
                if($foundDirty) {
                    
                    $replace = '*****';
                    $comment = preg_replace($original, $replace, $r->comment);
                    $cleaned[] = ['comment_id' => $r->comment_id,
                        'user_id' => $r->user_id,
                        'magazine_id' => $mag_id,
                        'comment' => $comment,
                        'first_name' => $fname,
                        'last_name' => $lname,
                        'profile_picture' => $r->profile_picture,
                        'date_created' => $dateiso,
                        'date_readable' => $readable,
                    ];
                } else {
                    $cleaned[] = [
                        'comment_id' => $r->comment_id,
                        'user_id' => $r->user_id,
                        'magazine_id' => $mag_id,
                        'comment' => $r->comment,
                        'first_name' => $fname,
                        'last_name' => $lname,
                        'profile_picture' => $r->profile_picture,
                        'date_created' => $dateiso,
                        'date_readable' => $readable,
                    ];
                }
                
            }
        } else {
            $cleaned[] = [
                'magazine_id' => $mag_id,
                'showMessage' => true,
            ];
        }
        if($fromCommPage != 0){
            return \Response::json($cleaned);
        } else {
            return $cleaned;
        }
    }

    public function get_single_magazine($fMagID){
        $this->fCached = new CacheController("single-magazine-$fMagID");

        $meArray = [
            'magazine_publisher.magazine_id',
            'magazine_publisher.title',
            'magazine_publisher.magazine_cover',
            'magazine_publisher.details',
            // 'magazine_publisher.issue_date',
            'magazine_publisher.views',
            'magazine_publisher.favorites',
            'magazine_publisher.date_updated',
            'magazine_category.category_id',
            'magazine_category.parent_id',
            DB::raw('AVG(COALESCE(magazine_rating.rating,0)) AS rating, COUNT(magazine_rating.rating) AS total_rating, magazine_category.name AS category_name')
        ];

        $this->fQuery = $this->fMagazine
            ->leftJoin('magazine_category', 'magazine_publisher.category_id', '=', 'magazine_category.category_id')
            ->leftJoin('magazine_rating', 'magazine_publisher.magazine_id', '=', 'magazine_rating.magazine_id')
            ->select(
                $meArray
            )
            ->where('magazine_publisher.publish', '=', 'yes')
            ->where('magazine_publisher.magazine_id', $fMagID)
            // ->where('magazine_publisher.issue_date', '<=', \Carbon\Carbon::now()->toDateString())
            ->orderBy('magazine_publisher.date_updated', 'desc')
            ->groupBy('magazine_publisher.magazine_id');

        

        // $fItems = $this->fCached->get($this->fQuery);
        $fItems = $this->fQuery->get();

        // dd($fItems);
        $addItems = [];
        if(count($fItems) > 0)
        {
            $ctr = 0;
            foreach ($fItems as $items) {
                $addItems[$ctr] = $items;
                $addItems[$ctr]['reviews'] = $this->get_ratings_reviews($items->magazine_id);
                $addItems[$ctr]['ratebyuser'] = $this->get_rating_by_user($items->magazine_id, $this->fUser_ID);
                $addItems[$ctr]['issues'] = $this->get_issues($items->category_id, $items->magazine_id);
                $addItems[$ctr]['comments'] = $this->get_comments($items->magazine_id);
                $ctr++;
            }
        }

        return $addItems;
    }

    public function get_interests(Request $request) {
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $this->fQuery = $this->fInterests
        ->select(['freyo_user_interests.category_id', 'magazine_category.name'])
        ->leftJoin('magazine_category', 'magazine_category.category_id', '=', 'freyo_user_interests.category_id')
        ->where('user_id', $fUserID);
        // ->where('freyo_user_interests.user_id', '!=', $fUserID);

        $fUserInterests = $this->fQuery->get();
        // $fUserInterests = implode(",", $fUserInterests);
        $ui = [];
        foreach($fUserInterests as $key => $value) {
            $ui[] = $value->category_id;
        }
        $this->fQuery = $this->fCategory
        ->select(['magazine_category.name', 'magazine_category.category_id'])
        // ->leftJoin('freyo_user_interests', 'freyo_user_interests.category_id', '=', 'magazine_category.category_id')
        ->where('magazine_category.parent_id', '=', 0)
        ->whereNotIn('category_id', $ui);
        // ->where('freyo_user_interests.user_id', '!=', $fUserID);

        $fItems = $this->fQuery->get();
        return \Response::json($fItems);
    }

    public function submit_interest(Request $request) {
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $cat_id = $request->input('category_id');

        $isInterest = $this->fInterests
        ->where('user_id', $fUserID)
        ->where('category_id', $cat_id)
        ->take(1)->get();

        if(count($isInterest) == 1) {
            return array('status' => 'exist', 'message' => 'Already set!');
        }
        $subscribe_id = $this->fInterests->insertGetId(
            [
            'user_id' => $fUserID,
            'category_id' => $cat_id,
            
            ]
        );
        return array('status' => 'success', 'message' => "");
    }

    public function delete_interest(Request $request) {
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $cat_id = $request->input('category_id');

        
        $this->fInterests->where([
            'user_id' => $fUserID,
            'category_id' => $cat_id,
            ])->delete();
        return array('status' => 'success', 'message' => "");
    }

    public function user_interests(Request $request) {
        $fUserID = $request->cookie(\Config::get('constants.user_cookie_name'));
        $this->fQuery = $this->fInterests
        ->select(['freyo_user_interests.category_id', 'magazine_category.name'])
        ->leftJoin('magazine_category', 'magazine_category.category_id', '=', 'freyo_user_interests.category_id')
        ->where('user_id', $fUserID);
        // ->where('freyo_user_interests.user_id', '!=', $fUserID);

        $fItems = $this->fQuery->get();
        return $fItems;
    }

}