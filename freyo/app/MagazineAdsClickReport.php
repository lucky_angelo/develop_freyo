<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineAdsClickReport extends Model
{
    protected $table = 'ads_click_report';
    protected $fillable = [
    	'magazine_id',
        'user_id',
        'ads_id',
        'datetime_clicked',
    ];
    public $timestamps = false;
}
