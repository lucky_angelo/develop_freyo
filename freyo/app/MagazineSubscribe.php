<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineSubscribe extends Model
{
    protected $table = 'magazine_subscribe';
	protected $fillable = [
		'magazine_id',
		'user_id',
		'date_last_viewed',
		'date_created',
	];
	public $timestamps = false;
}
