<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class ContactForm extends Model
{
    protected $table = 'contact_form';
	protected $fillable = [
		'date_post',
		'field1',
		'field3',
		'field4',
		'field7',
		'field8',
	];
	public $timestamps = false;
}
