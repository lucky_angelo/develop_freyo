<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineRating extends Model {

	protected $table = 'magazine_rating';
	protected $fillable = [
		'rating_id',
		'magazine_id',
		'user_id',
		'rating',
		'review',
		'date_created',
		'date_updated'
	];
	public $timestamps = false;

}
