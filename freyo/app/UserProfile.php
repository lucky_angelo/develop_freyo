<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'freyo_user_profile';
	protected $fillable = [
		'user_id',
		'first_name',
		'last_name',
		'email',
		'province',
		'city',
		'phone_number',
		'birthdate',
		'sex',
		'profile_picture',
		'following',
		'favorites',
		'date_created',
		'date_updated'
	];
	public $timestamps = false;
}
