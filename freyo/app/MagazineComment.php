<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineComment extends Model
{
    protected $table = 'magazine_comments';
	protected $fillable = [
		'comment_id',
		'magazine_id',
		'user_id',
		'comment',
		'date_created',
		'date_updated',
	];
	public $timestamps = false;
}
