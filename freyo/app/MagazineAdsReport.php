<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineAdsReport extends Model {

	protected $table = 'ads_report';
	protected $fillable = [
		'ads_id',
		'user_id',
		'magazine_id',
		'page_num_viewed',
		'datetime_viewed',
	];
	public $timestamps = false;

}
