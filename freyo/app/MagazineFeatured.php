<?php namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class MagazineFeatured extends Model {

	protected $table = 'magazine_featured';

}
