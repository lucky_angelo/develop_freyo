<?php

namespace Freyo;

use Illuminate\Database\Eloquent\Model;

class ProfanityModel extends Model
{
    protected $table = 'freyo_profanity_table';
}
