<!DOCTYPE html>

<html>

<head>

	<title>Freyo - Password Reset</title>

<meta name="description" content="Freyo is a mobile application that showcase the online photo magazines here in the philippines for free. And help the user to access their favorite online photo magazines anytime and anywhere.">

<meta name="keywords" content="freyo, digital magazine, online photo magazines, magazines, digital online magazines, photo magazine, showbiz, celebrity, entertainment, life, stylist, relationship, love, career, fashion, beauty, health, well-being, culture">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>


	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,300' rel='stylesheet' type='text/css'>

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Lato:400,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/mobile/css/validationEngine.jquery.css') }}">

	<!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}"> -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76379014-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body id="skrollr-body">
@include('blocks.rnotification')
@include('blocks.resetpassword')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{{ asset('assets/mobile/js/jquery.validationEngine-en.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/mobile/js/jquery.validationEngine.js') }}"></script>

<script src="{{asset('assets/js/scrollIt.min.js')}}" type="text/javascript"></script>

<script type="text/javascript" src="{{asset('assets/js/common.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/js/skrollr.min.js')}}"></script>

<script type="text/javascript">

	var s = skrollr.init();

</script>

</body>

</html>