@extends('mobile.master')



@section('content')
	<?
		$displaySU = "display:none;";
		$displaySI = "display:none;";
		$displayWalkthrough = "";
		if(Input::get('redirectto') !== null && Input::get('redirectto') == 'signup'){
			$displaySU = "";
			$displayWalkthrough = "display:none;";
		}
		elseif(Input::get('redirectto') !== null && Input::get('redirectto') == 'login'){
			$displaySI = "";
			$displayWalkthrough = "display:none;";
		}
	?>
	@include('mobile.blocks.walkthrough')



 	{{-- @include('mobile.blocks.welcome') --}}

	

	{{-- @include('mobile.blocks.choose_category') --}}

	

	@include('mobile.blocks.signup')



	@include('mobile.blocks.signin')

	@include('mobile.blocks.forgotpassword')

	<!-- if(isset($show_FBLogin) && $show_FBLogin == true && !1) -->
	@if(isset($not_latest_version) && $not_latest_version == true)
	<!-- <div id="update-modal" class="modal signout-dialog-box text-center" role="dialog">
	  <div class="modal-backdrop in"></div>
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title">New Update</h4>
	      </div>
	      <div class="modal-body">
	        <p>Please update to new version for better user experience.</p>
	      </div>
	      <div class="modal-footer">
	        <a data-dismiss="modal" class="update-later">Later</a>
	        <a href="{{ $updated_app_link }}" class="sign-out-now">Update</a>
	      </div>
	    </div>
	  </div>
	</div> -->
	@endif
@stop 