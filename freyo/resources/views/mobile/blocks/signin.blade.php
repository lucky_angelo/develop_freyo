<div class="container-fluid f-si-formpage" style="<? echo $displaySI; ?>">
    <div class="fma-vertical-responsive">
        <!-- <a class="btn_si_to_welcome" href="#"><i class="fa fa-arrow-left"></i></a> -->
        <div class="row freyo-logo-black text-center">
            <img class="center-block" src="{{ asset('assets/mobile/images/freyo-logo.png') }}">
            <!-- <h3>Sign in to your Freyo account</h3> -->
            <h3>THE DIGITAL MAGAZINE REVOLUTION</h3>
        </div>
        <div class="row f-signup-form">
            <form action="<? echo asset('/') ?>" method="post" class='sign_in'>
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <div class="row fsu-input">
                    <span>Email Address</span>
                    <input class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" required name="si_username" 
                    data-errormessage-value-missing="Email is required!" 
                    data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>Password</span>
                    <input class="validate[required] fsu-input-box col-xs-12" type="password" required name="si_password"
                    data-errormessage-value-missing="Password is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="">
                    <div class="login_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                </div>
                
                {{-- <a class="btn_submit_sign_in text-center" href="#">Sign In</a> --}}
                <input id="sign-in" type="submit" class="btn btn_submit_sign_in text-center" value="SIGN IN">

                <div class="row text-center forgot-password-row">
                    <!-- <span class="forgot-password-text">Forgot your password? </span> -->
                    <!-- <span class="btn_forgot_user_pass" data-toggle="modal" data-target="#resetModal">Let's reset</span> -->
                    <span class="btn_forgot_user_pass">Forgot password?</span>
                   <!--  data-toggle="modal" data-target="#resetModal" -->
                </div>
            </form>
        </div>
        @if(isset($show_FBLogin) && $show_FBLogin == TRUE)
        <div class="row f-su-separator text-center">
            <!-- <div class="fsu-middle-line"></div> -->
            <span>OR</span>
        </div>
        <div class="row social-media-signin text-center">
            <!-- <a class="signin_fb" href="{{ asset('/api/redirect') }}"><i class="fa fa-facebook"></i>Sign in with Facebook</a> -->
            <a class="signin_fb" href="{{ asset('/api/redirect') }}">SIGN IN WITH FACEBOOK</a>
           <!--  <a class="signin_gp" href="https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo"><i class="fa fa-google-plus"></i>Google Plus</a>
            <a class="signin_tw" href="https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8"><i class="fa fa-twitter"></i>Twitter</a> -->
        </div>
        @endif
        <div class="row text-center">Dont Have an account? <span class="btn_go_sign_up">SIGN UP</span></div>

        
    </div>
</div>
<!-- <div id="resetModal" class="modal fade resetpassword-modal" role="dialog" style="display:none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
                <form class="reset-password-form">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="row fsu-input fsu-active">
                        <span>Email Address</span>
                        <input class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" required name="request_email" data-errormessage-value-missing="Email is required!" data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                    </div>
                    <div class="requestreset_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                    <input type="submit" data-resetted="no" class="btn btn_submit_resetpassword text-center" value="Reset Password">
                </form>
            </div>
        </div>
    </div>
</div> -->

 