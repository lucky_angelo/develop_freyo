<div class="container-fluid f-launch-screen text-center" style="display:none;">
	<div class="fma-vertical-responsive">
		<div class="row">
			<img class="center-block" src="{{ asset('assets/mobile/images/freyo-logo.png') }}">
		</div>
		<div class="row f-ls-welcome">
			<h1>Welcome to Freyo</h1>
			<h3>The Digital Magazine<br/>Revolution</h3>
		</div>
		<div class="row">
			<a class="btn_fsign_in btn_go_sign_in" href="#">SIGN IN</a>
		</div>
		<div class="row f-ls-swipe">
			<span><i class="fa fa-angle-left fal-1"></i> <i class="fa fa-angle-left fal-2"></i> <i class="fa fa-angle-left fal-3"></i> Swipe left to sign up</span>
		</div>

        <!-- <div class="row">
        	<a class="guest_sign_in" href="{{asset('/dashboard')}}">No Thanks, let me browse without signing in</a>
        </div> -->
	</div>
</div>