<div class="container-fluid f-su-interests text-center" style="display:none;">
	<div class="fma-vertical-responsive">			
		<a class="btn_back_to_welcome" href="#"><i class="fa fa-arrow-left"></i></a>
		
		<div class="row f-su-text">
			<p>Select your interests and<br/>
			We will customize your<br/>
			reading experience</p>
		</div>
		<div class="row f-su-category-container">
			<div class="col-xs-4">
				<div class="cat-fashion f-su-categories text-center">
					{{-- <img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-fashion.png')}}"> --}}
					<img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-fashion-a.png')}}">
				</div>
			</div>
			<div class="col-xs-4">
				<div class="cat-travel f-su-categories text-center">
					<img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-travel.png')}}">
					{{-- <img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-travel-a.png')}}"> --}}
				</div>
			</div>
			<div class="col-xs-4">
				<div class="cat-celebrity f-su-categories text-center">
					<img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-celebrity.png')}}">
					{{-- <img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-celebrity-a.png')}}"> --}}
				</div>
			</div>
			<div class="col-xs-4">
				<div class="cat-wedding f-su-categories text-center">
					<img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-wedding.png')}}">
					{{-- <img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-wedding-a.png')}}"> --}}						
				</div>
			</div>
			<div class="col-xs-4">
				<div class="cat-sport f-su-categories text-center">
					{{-- <img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-sport.png')}}"> --}}
					<img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-sports-a.png')}}">
				</div>
			</div>
			<div class="col-xs-4">
				<div class="cat-car f-su-categories text-center">
					<img class="img-responsive center-block unchosen-cat" src="{{asset('assets/mobile/images/cat-cars.png')}}">
					{{-- <img class="img-responsive center-block chosen-cat" src="{{asset('assets/mobile/images/cat-cars-a.png')}}"> --}}
				</div>
			</div>
		</div>

		<div class="row">
			<a class="btn_next_step" href="#">NEXT STEP</a>
		</div>
	</div>		
</div>