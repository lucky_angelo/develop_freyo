<div class="container-fluid f-su-formpage" style="<? echo $displaySU; ?>">
    <div class="fma-vertical-responsive">
        <a class="btn_back_to_interests" href="#"><i class="fa fa-arrow-left"></i></a>
        <div class="row fsu-almost-there">
            <h4>REGISTER WITH FREYO</h4>
            <!-- <p>Tell us some details and we’ll get you on your own way</p> -->
        </div>
        <div class="row f-signup-form">
            <form action="<?php echo asset('/register') ?>" method="post" class="fsu-form">
                
                <div class="form-item-1">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <div class="row fsu-input">
                    <span>FIRST NAME</span>
                    <input id="first_name" class="validate[required] fsu-input-box col-xs-12" type="text" name="first_name" maxlength="35" required data-errormessage-value-missing="First Name is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>LAST NAME</span>
                    <input id="last_name" class="validate[required] fsu-input-box col-xs-12" type="text" name="last_name" maxlength="35" required data-errormessage-value-missing="Last Name is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>EMAIL ADDRESS</span>
                    <input id="email_add" class="validate[required,custom[email]] fsu-input-box col-xs-12" type="email" name="email_add" required data-errormessage-value-missing="Email is required!" 
                    data-errormessage-custom-error="Format: sample@sample.com" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>PASSWORD</span>
                    <input id="password" class="validate[required,minSize[6]] fsu-input-box col-xs-12" type="password" name="password" required data-errormessage-value-missing="Password is required!" data-errormessage-range-underflow="Minimum of 6 characters!" data-prompt-position="topLeft"/>
                    <a href="#" id="show-it"><i class="fa fa-eye"></i></a>
                </div>
                <!-- <div class="row fsu-input">
                    <span>Confirm Password</span>
                    <input id="confirm_password" class="validate[required, equals[password]] fsu-input-box col-xs-12" type="password" name="confirm_password" required data-errormessage-value-missing="Confirm your password!" data-errormessage-pattern-mismatch="Does not match Password!" data-prompt-position="topLeft"/>
                    <p>Password needs to be 6 characters or longer</p>
                </div> -->
                <div class="row fsu-input">
                    <a class="btn text-center btn-page-1" href="#">NEXT</a>
                </div> 
                </div>
                <div class="form-item-2" style="display: none">
               
                <div class="row fsu-input">
                    <span>MOBILE NUMBER</span>
                    <input id="mobile_number" class="validate[custom[integer]] fsu-input-box col-xs-12" type="text" name="mobile_number" data-errormessage-custom-error="Number only!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>BIRTHDATE</span>
                    <input id="birthdate" class="validate[custom[date]] fsu-input-box col-xs-12" type="date" name="birthdate" data-errormessage-custom-error="Format(YYYY-MM-DD): ex. 2016-02-14" data-prompt-position="topLeft"/>
                    <!-- <select><option>YYYY</option></select> 
                    <select>
                        <option value="01">Jan</option>
                        <option value="02">Feb</option>
                        <option value="03">Mar</option>
                        <option value="04">Apr</option>
                        <option value="05">May</option>
                        <option value="06">Jun</option>
                        <option value="07">Jul</option>
                        <option value="08">Aug</option>
                        <option value="09">Sep</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                    </select> 
                    <select>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select> -->
                </div>
                {{--  <div class="row fsu-input fsu-prof-pic fsu-active">
                    <span>Profile Picture</span>
                    <input class="fsu-input-box col-xs-12" type="file" name="profile_picture" accept="image/*" />
                </div> --}}
                <div class="row fsu-input f-select-address">
                    <div class="col-xs-12 no-gutters">
                        ADDRESS
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="province" class="ph-provinces">
                            <option>Province</option>
                        </select>
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="city" class="ph-cities">
                            <option>City/Municipality</option>
                        </select>
                    </div>
                </div>
                <!-- <div class="row fsu-input fsu-last-input f-select-gender">
                    <div class="col-xs-2 no-gutters">
                        Gender
                    </div>
                    <div class="col-xs-3 no-gutters">
                        <input id="gmale" type="radio" name="gender" value='male' class="validate[required]" data-prompt-position="topLeft"/>
                        <label for="gmale">Male</label>
                    </div>                    
                    <div class="col-xs-3 no-gutters">
                        <input id="gfemale" type="radio" name="gender" value='female' class="validate[required]" data-prompt-position="topLeft"/>
                        <label for="gfemale">Female</label>
                    </div>
                    <div class="col-xs-3 no-gutters">
                        <input id="gunknown" type="radio" name="gender" value='unknown' class="validate[required]" data-prompt-position="topLeft"/>
                        <label for="gunknown">Other</label>
                    </div>  
                </div> -->
                <div class="row fsu-input fsu-last-input f-select-gender">
                    <div class="col-xs-12 no-gutters">
                        GENDER
                    </div>
                    <div class="col-xs-4 no-gutters">
                      <select name="gender" class="validate[required]">
                        <option value="male">M</option>
                        <option value="female">F</option>
                        <option value="unknown">Other</option>
                      </select>
                    </div>                    
                     
                </div>
                <div class="">
                    <div class="registration_message" style="display:none;">
                        <span class='message'>
                            
                        </span>
                    </div>
                </div>
                <input type="submit" class="btn btn_submit_sign_up text-center" value="Sign Up">
                {{-- <a class="btn_submit_sign_up text-center" href="#">Sign Up</a> --}}
                <div class="row text-center below-the-submitbutton">
                    <p>Already have an account? <a class="btn_go_sign_in a-su-sign-in" href="#">SIGN IN</a></p>
                </div>
                </div>
                
            </form>
        </div>
        {{-- <div class="row f-su-separator text-center">
            <div class="fsu-middle-line"></div>
            <span>Or sign up with</span>
        </div>
        <div class="row social-media-signup text-center">
            <a class="signup_fb" href=""><i class="fa fa-facebook"></i>Facebook</a>
            <a class="signup_gp" href=""><i class="fa fa-google-plus"></i>Google Plus</a>
            <a class="signup_tw" href=""><i class="fa fa-twitter"></i>Twitter</a>
        </div> --}}
        
    </div>
</div>
 