<div ng-if="userisloggedin == 'no'" ui-content-for="navbarLeft">
</div>
<!-- <div ui-content-for="title" ng-if="userisloggedin == 'no'">
    <a href="#" ng-click="gobacktoregister()" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>DASHBOARD
</div> -->
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>

<div class="scrollable">
    <div class="scrollable-content f-top-charts" ui-scroll-bottom="tnrAddItems()">
        <div ng-init="magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/3)+15 | number:0); sliderUrl = '<?php echo asset("/api/freyo_image/slider");?>'+'/'+((userScreenWidth+50) | number:0); smallMagCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/120'; magBrowser = '<?php echo asset('magazine/browser'); ?>'">
            <ui-state default="1" id="activeTab"></ui-state>
            <div class="btn-group justified nav-tabs">
                <a class="btn btn-default active" ui-class="{'active': activeTab == 1}" ui-set="{'activeTab': 1}">TOP CHARTS</a>
                <a class="btn btn-default" ui-class="{'active': activeTab == 2}" ui-set="{'activeTab': 2}">NEW RELEASE</a>
            </div>
            <div ui-if="activeTab == 1" class="ng-scope" ng-include="topChartsUrl()">
            </div>
            <div ui-if="activeTab == 2" class="ng-scope" ng-include="newReleaseUrl()">
            </div>
        </div>
    </div>
</div>