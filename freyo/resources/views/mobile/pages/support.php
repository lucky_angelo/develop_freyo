<div ui-content-for="navbarRight">
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
  	<div class="scrollable-content" style="padding-top: 50px;">
		<form class="support-form">
		<input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
		<div class="row f-editprof-form dInputEvent">
		    <div class="row fsu-input">
		        <span>Subject *</span>
		        <input class="validate[required] fsu-input-box col-xs-12" type="text" name="sup_subject" class="validate[required]" data-errormessage-value-missing="Please write a subject!" data-prompt-position="topLeft"/>
		    </div>                    
		    <div class="row fsu-input support-message-input">
		    	<span>Message *</span>
		        <textarea row='3' name="sup_message" class="validate[required] fsu-input-box" data-errormessage-value-missing="Please write a message!" data-prompt-position="topLeft"></textarea>
		    </div>
		    <!-- <div class="f-recaptcha-container"
			    vc-recaptcha
			    key="'6LeOpBYTAAAAAHLd8JuMwkdNOBA4fLYO__5xDlBb'"
			></div> -->
		    <div class="support-message" style="display:none;">
		        <span class='message'>
		            
		        </span>
		    </div>
		    <div class="dSubmitSupport btn_update_profile text-center" dontsendagain="no">SUBMIT</div>
		</div>
		</form>
	</div>
</div>