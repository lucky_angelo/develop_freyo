<div ui-content-for='navbarRight'>
    <!-- <div class="page-options">
        <i class='fa fa-bookmark dFavoriteAction favorite-action dConfirmSubscribe'></i>
        <i class='fa fa-share-alt share-action dShareAction' where-share='mbrowser'></i>
    </div> -->
    <a href="/search">
        <div class="btn">
            <i class="fa fa-search"></i>
        </div>
    </a>
</div>

<div ng-if="!logged_in" ui-content-for="navbarLeft">
</div>

<div ui-content-for="title">
    <a ng-if="!logged_in" href="<?php echo asset('dashboard');?>" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>{{ browserTitle }}
</div>
<!-- <i class="pull-right fa fa-angle-right"></i> -->
<!-- <i class="pull-left fa fa-angle-left"></i> -->
<div class="scrollable">
    <div class="scrollable-content">
    <!-- ,nav:true,navText:["<i class=&#x27;fa fa-angle-left pull-left&#x27;></i>","<i class=&#x27;fa fa-angle-right pull-right&#x27;></i>"] -->
    <!-- ,stagePadding: 25 -->
    <!-- autoHeight:true, -->
        <div class="owlCarousel magazines-browser-slider" data-options='{items:1,startPosition:{{activeItem}}, mouseDrag: false, touchDrag: false }'
        ng-init="magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth) | number:0); magViewUrl = '<?php echo asset('magazine/view'); ?>'; commentsUrl = '<?php echo asset('comments');?>'">
            <div class="item block-magazine owlCarouselItem" ng-repeat="magazine in browserMagazines">
                <div class="magazine-cover-container" browser-slider data-magazine-id="{{magazine.magazine_id}}" fmag-title="{{magazine.title}}">
                    <a class="is-loading" href="{{magViewUrl+'/'+magazine.magazine_id}}">
                        <img ng-src='{{magCover+"/"+magazine.magazine_cover}}' class="magazine-cover-photo" alt="">
                    </a>
                </div> 
                <div class="magazine-information-container">
                    <div class="arrowup-overlay"><i class="fa fa-chevron-up"></i></div>
                    <div class="magazine-header">
                    	<span class="magazine-category-name">{{ magazine.category_name }}</span>
                        <span class="magazine-title">{{ magazine.title }}</span>
                        <div class="rate">
                            <input type="hidden" class="rating"  value="{{magazine.rating}}" readonly="true" />
                        </div>
                        <!-- <p class="magazine-published-date">
                            {{magazine.issue_date | date:'MMMM y'}}
                        </p> -->
                    </div>
                    <!-- <div class="magazine-options-container">
                        <div class="views">
                            <i class='fa fa-eye'></i> <span> {{magazine.views}}</span>
                        </div>
                        <div class="favorites">
                            <i class='fa fa-bookmark-o'></i> <span> {{magazine.favorites}}</span>
                        </div>
                        <div class="rate">
                            <input type="hidden" class="rating"  value="{{magazine.rating}}" readonly="true" /> <span class="review-count">({{magazine.reviews.length+(magazine.reviews.length>1?' Reviews':' Review')}})</span>
                        </div>
                    </div> -->
                    <!-- <div class="magazine-details-container" ng-if="magazine.details != ''">
                        <div class="row">
                            <div class="col-xs-2">
                                <h3 class='label'>Details</h3>
                            </div>
                            <div class="col-xs-10">
                                <p>{{ magazine.details }}</p>
                            </div>
                        </div>
                    </div> -->
                    <div class="magazine-details-container" ng-if="magazine.details != ''">
                        <div class="row">
                            <div class="col-xs-12">
                                <p>{{ magazine.details }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="f-separator"></div>
                    <div class="reviews-comments" style="display: none;">
                        <div ng-if="logged_in " class="magazine-review-rating-container" >
                            <div class="dropdown">
                                <a href="#" class="dropdown-btn">
                                    <img class="img-responsive ratings-more" width="20" src="<?php echo asset('assets/mobile/images/more.png');?>">
                                </a>
                                <div id="dropdown-more" class="dropdown-content">
                                    <ul class="dropdown-list">
                                        <li class="dropdown-more-item"><a href="#" class="share-action dShareAction" where-share="mbrowser">SHARE</a></li>
                                        <li class="dropdown-more-item">
                                            <a href="{{commentsUrl+'/'+magazine.magazine_id}}">
                                                <span class="view-more">COMMENT</span>
                                            </a>
                                        </li>
                                        <li class="dropdown-more-item">
                                            <a href="#" class="dFavoriteAction favorite-action dConfirmSubscribe">
                                                SUBSCRIBE
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <form class="rate-review-form" data-ratemagazine="{{ magazine.magazine_id }}">
                                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                <input type="hidden" name="magazine_id" ng-value="magazine.magazine_id">
                                <!-- <div class="review-input-container">
                                    <textarea row='3' name="review" class="validate[required]" data-errormessage-value-missing="Please review the magazine!" data-prompt-position="topLeft"></textarea>
                                </div> -->
                                 <!-- && (objectIndexOf(magazine.reviews, user_profile.user_id, 'user_id') == -1)  -->
                                <div class="review-rate-container remove-when-submitted" ng-if="objectIndexOf(magazine.reviews, user_profile.user_id, 'user_id') == -1">
                                    <input type="hidden" class="rating rating-magazine dSubmitRating" name="rating" data-step="1"/>
                                    <div class="col-xs-12 no-gutters" style="line-height: 10px"><span>TAP TO RATE</span></div>
                                    <div class="error-rating-message" style="display:none;">Please rate from 1-5!</div>
                                </div>
                                <div class="review-rate-container" ng-if="objectIndexOf(magazine.reviews, user_profile.user_id, 'user_id') > -1" ng-repeat="userRating in magazine.ratebyuser">
                                    <input type="hidden" class="rating " name="rating" value="{{ userRating.rating }}" readonly="true" />
                                    <div class="col-xs-12 no-gutters" style="line-height: 10px"><span>YOU RATED</span></div>
                                </div>

                                
                                <!-- <button class="submit-review dSubmitRating remove-when-submitted" ng-if="objectIndexOf(magazine.reviews, user_profile.user_id, 'user_id') == -1">
                                    Submit
                                </button> -->
                            </form>
                        </div>
                        
                        <!-- <div class="magazine-reviews-container" data-revmagazine="{{ magazine.magazine_id }}">
                         | limitTo: 3
                            <div class="review-item" ng-if="magazine.reviews.length > 0" ng-repeat="rateview in magazine.reviews">
                                <div class="review-header" ng-init="revPicUrl = '<?php echo asset('/api/profile_pic');?>';">
                                    <img class="reviewer-photo" ng-src="{{rateview.profile_picture!=null?(revPicUrl+'/'+rateview.profile_picture):(revPicUrl+'/default-picture.jpg')}}">
                                    <h3 class="reviewer-name">{{rateview.first_name+' '+rateview.last_name}}</h3>
                                    <div class="reviewer-rating-container">
                                        <input type="hidden" class="rating" value="{{ rateview.rating }}" readonly="true" />
                                    </div>
                                </div>
                                <div class="review-message-container">
                                    <p>{{ rateview.review }}</p>
                                </div>
                            </div>
                        </div> -->
                        <div class="magazine-reviews-container" data-revmagazine="{{ magazine.magazine_id }}">
                        
                            <!-- <div class="review-item" ng-if="magazine.reviews.length > 0" ng-repeat="rateview in magazine.reviews | limitTo: 3">
                                <div class="review-header" ng-init="revPicUrl = '<?php echo asset('/api/profile_pic');?>';">
                                    
                                    <span class="reviewer-name">{{rateview.first_name+' '+rateview.last_name}}</span>
                                </div>
                                <div class="review-message-container">
                                    <p>{{ rateview.review }}</p>
                                </div>
                            </div> -->
                            <div>
                                <h5>COMMENTS</h5>
                            </div>
                            <div class="review-item" ng-if="magazine.comments.length > 0" ng-repeat="comments in magazine.comments | limitTo: 3">
                                <div class="review-header" ng-init="revPicUrl = '<?php echo asset('/api/profile_pic');?>';">
                                    
                                    <span class="reviewer-name">{{ comments.first_name+' '+comments.last_name }}</span>
                                </div>
                                <div class="review-message-container">
                                    <p>{{ comments.comment }}</p>
                                </div>
                                <div class="review-no-comment" ng-if="comments.showMessage == true">
                                    <span class="no-comment-msg">No comments</span>
                                </div>
                            </div>
                            <!-- <div class="review-item" ng-if="magazine.comments.length < 1" >
                                <div class="review-no-comment">
                                    <span class="no-comment-msg">No comments</span>
                                </div>
                            </div>
                            <div class="review-item" ng-if="magazine.comments.showMessage == true" >
                                <div class="review-no-comment">
                                    <span class="no-comment-msg">No comments</span>
                                </div>
                            </div> -->
                            <div class="more-review">
                                <a href="{{commentsUrl+'/'+magazine.magazine_id}}">
                                    <span class="view-more">VIEW ALL COMMENTS</span>
                                </a>
                            </div>
                        </div>
                        <div class="f-separator"></div>
                        <div class="other-issues-container" ng-init="magBrowser = '<?php echo asset('magazine/browser'); ?>'">
                        	<div class="other-issues-title">
                        		OTHER ISSUES
                        	</div>
                            <!-- touchDrag: false, mouseDrag: false, nav: true, navText:["<i class=&#x27;fa fa-angle-left pull-left&#x27;></i>","<i class=&#x27;fa fa-angle-right pull-right&#x27;></i>"] } -->
                        	<div class="owlCarouselOther other-issues-slider" data-options='{ items: 2 , margin: 15, stagePadding: 30} '>
                        		<div class="owlCarouselItemsOther" ng-repeat="otherIssues in magazine.issues">
                        			<a href="{{magBrowser+'/'+otherIssues.category_id+'/'+otherIssues.magazine_id}}">
    				                    <div class="is-loading">
    				                        <img class='magazing-cover' ng-src='{{magCover+"/"+otherIssues.magazine_cover}}'>
    				                    </div>
    				                    <p class="magazine-title">{{otherIssues.title}}</p>
    				                </a>
                        		</div>
                        	</div>
                            <!-- <div class="owlCarouselOther other-issues-slider" ng-if="magazine.issues.length <= 2" data-options='{ items: 2 , margin: 15, stagePadding: 30, touchDrag: false, mouseDrag: false }'>
                                <div class="owlCarouselItemsOther" ng-repeat="otherIssues in magazine.issues">
                                    <a href="{{magBrowser+'/'+otherIssues.category_id+'/'+otherIssues.magazine_id}}">
                                        <div class="is-loading">
                                            <img class='magazing-cover' ng-src='{{magCover+"/"+otherIssues.magazine_cover}}'>
                                        </div>
                                        <p class="magazine-title">{{otherIssues.title}}</p>
                                    </a>
                                </div>
                            </div> -->
                            <div class="no-issues-message" ng-if="magazine.issues.length == 0" '>
                                <span>No other issues yet</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
