<div class="unlike-instruction">Double tap any image to unclip</div>
<div class="fmagazines fliked-mags text-center" ng-init="magUrl = '<?php echo asset("/api/freyo_image/mag_page");?>'+'/'+((userScreenWidth/3)+15 | number:0);">
    <div class="f-magazine-item col-xs-4 dUnlikeAction" data-magpage-id="{{magazine.page_id}}" data-maglike-id="{{magazine.likes_id}}" data-magazine-id="{{magazine.magazine_id}}" ng-repeat="magazine in likedMags | limitTo:lItemLimit">
    	<div class="is-loading">
        	<img class='magazing-cover' ng-src='{{magUrl+"/"+magazine.magazine_page+"/"+magazine.date_uploaded}}'>
    	</div>
    </div>
</div>