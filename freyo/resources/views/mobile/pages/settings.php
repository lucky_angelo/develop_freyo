<div ui-content-for="navbarRight">
<!--     <div class="btn">
        <a class="btn_signout" href="#" ui-turn-on="modal1">
            <i class="fa fa-sign-out"></i>
        </a>
    </div> -->
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
    <div class="scrollable-content" ui-scroll-bottom="lsAddItems()">
        <div class="f-settings-body">
            <div class="f-settings-header"><h4>SETTINGS</h4></div>
            <!-- <div class="f-settings-section settings-notification">
                <div class="f-section-header">NOTIFICATIONS</div>
                <div class="f-section-body">
                    <div class="f-section-item">
                        New Publication
                    </div>
                    <div class="f-separator"></div>
                    <div class="f-section-item">
                        Updates
                    </div>
                </div>
            </div> -->
            <div class="f-settings-section settings-account">
                <div class="f-section-header">ACCOUNT</div>
                <div class="f-section-body">
                    <div class="f-section-item">
                        <a href="<?php echo asset('profile/edit'); ?>"><span>Edit Profile</span></a>
                    </div>
                    <div class="f-separator"></div>
                    <!-- <div class="f-section-item">
                        <a href="#"><span>Change Password</span></a>
                    </div>
                    <div class="f-separator"></div> -->
                    <div class="f-section-item">
                        <a href="<?php echo asset('interest'); ?>"><span>Interests</span></a>
                    </div>
                    <!-- <div class="f-section-item">
                        Linked Accounts
                    </div> -->
                </div>
            </div>
            <div class="f-settings-section settings-support">
                <div class="f-section-header">SUPPORT</div>
                <div class="f-section-body">
                    <div class="f-section-item">
                        <a href="<?php echo asset('support');?>"><span>Report a Problem</span></a>
                    </div>
                    <div class="f-separator"></div>
                    <div class="f-section-item">
                        <a ui-turn-on="modal1" href="#"><span>Sign out</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
