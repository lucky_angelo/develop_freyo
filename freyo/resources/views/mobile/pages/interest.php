<div ui-content-for="navbarRight">
<!--     <div class="btn">
        <a class="btn_signout" href="#" ui-turn-on="modal1">
            <i class="fa fa-sign-out"></i>
        </a>
    </div> -->
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
    <div class="scrollable-content" ui-scroll-bottom="lsAddItems()">
        <div class="f-settings-body">
            <div class="f-settings-header"><h4>SETTINGS</h4></div>
            <div class="f-settings-section settings-notification">
                <div class="f-section-header f-interest-header">INTERESTS</div>
                <div class="f-section-body f-interests">
                    <form class="remove-interests-form">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="f-interest-item" ng-repeat="ui in userInterests">
                            <!-- <span>{{ ui.name }}</span> -->
                            <input id="remove-{{ $index }}" class="dRemoveInterest" type="radio" name="rem-interests" hidden value="{{ ui.category_id }}"><label for="remove-{{ $index }}" >{{ ui.name }}&nbsp;&nbsp;&nbsp;x</label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="f-separator"></div>
            <div class="f-settings-section settings-account">
                <div class="f-section-header-i">SUGGESTED INTERESTS</div>
                <div class="f-section-body f-interests">
                    <form class="interests-form">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="f-interest-item" ng-repeat="i in interests">
                            <input id="check-{{ $index }}" class="dSubmitInterest" type="radio" name="interests" hidden value="{{ i.category_id }}"><label for="check-{{ $index }}" >{{ i.name }}</label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
