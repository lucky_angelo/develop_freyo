<div ui-content-for="navbarRight">
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
    <div class="scrollable-content">
        <div class="col-xs-12 f-nav-page" ng-if="user_profile.user_id == 1">
            <a href="<?php echo asset('freyoswipe/42');?>">
                <span>Swipe</span>
            </a>
        </div>
        <div class="edit-profile-page dInputEvent">
            <form class="editprofpic-form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <div class="row text-center f-editprof-pic">
                <div class="btn_change_pic">
                    <!-- <img class="edit-profile-pic img-circle center-block" src="<?php echo asset('assets/mobile/images/gintoki_shock.jpg') ?>">           -->
                    <img class="edit-profile-pic img-circle center-block" ng-src="{{url_profile_pic}}">
                    <div class="img-overlay"></div>
                    <span class="f-select-gender">
                        <div style="position:relative; padding-top: 15px;">
                            <!-- <span>
                                <i class="fa fa-camera"></i>
                                <br/>EDIT PHOTO
                            </span> -->
                            <img src="<?php echo asset('assets/mobile/images/photo-camera.png') ?>" width="50px" height="40px">
                            <label for="fprof_pic" class="prof-pic-cheat">
                            </label>
                            <input id="fprof_pic" type="file" name="profile_pic" class="dChangePic browse_profile_pic" accept="image/*">
                        </div>
                    </span>
                </div>
                <div class="profilepic-message text-center" style="display:none;">
                    <span class='message'>

                    </span>
                </div>
            </div>
            </form>
            <form class="editprof-form">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <div class="row f-editprof-form">
                <div class="row fsu-input fsu-active">
                    <span>Email Address</span>
                    <div ng-bind="user_profile.email"></div>
                    <!-- <input class=" fsu-input-box col-xs-12" type="email" name="email_add" ng-value="user_profile.email" disabled="" /> -->
                </div>
                <div class="row fsu-input">
                    <span>First Name</span>
                    <input class="dAlphabetOnly validate[required] fsu-input-box col-xs-12" type="text" name="first_name" maxlength="35" ng-value="user_profile.first_name" required data-errormessage-value-missing="First Name is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>Last Name</span>
                    <input class="dAlphabetOnly validate[required] fsu-input-box col-xs-12" type="text" name="last_name" maxlength="35" ng-value="user_profile.last_name" required data-errormessage-value-missing="Last Name is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>Old Password</span>
                    <input id="old_password" class="validate[condRequired[new_password]]  fsu-input-box col-xs-12" type="password" name="old_password" data-errormessage-value-missing="Password is required!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>New Password</span>
                    <input id="new_password" class="validate[minSize[6]]  fsu-input-box col-xs-12" type="password" name="new_password" data-errormessage-range-underflow="Minimum of 6 characters!" data-prompt-position="topLeft"/>
                </div>
                <div class="row fsu-input">
                    <span>Confirm New Password</span>
                    <input id="confirm_password" class="validate[equals[new_password], condRequired[new_password]]  fsu-input-box col-xs-12" type="password" name="confirm_password" data-errormessage-value-missing="Confirm Password!" data-errormessage-pattern-mismatch="Does not match New Password!" data-prompt-position="topLeft" />
                    <p>Password needs to be 6 characters or longer</p>
                </div>
                <div class="dSelectAddress row fsu-input f-select-address" province-id="{{user_profile.province}}" city-id="{{user_profile.city}}">
                    <div class="col-xs-12 no-gutters">
                        Address
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="province" class="ph-provinces">
                            <option>Province</option>
                        </select>
                    </div>
                    <div class="col-xs-6 no-gutters">
                        <select name="city" class="ph-cities">
                            <option>City/Municipality</option>
                        </select>
                    </div>
                </div>
                <div class="row fsu-input">
                    <span>Phone Number</span>
                    <input class="dPhoneNumber fsu-input-box col-xs-12" type="text" name="mobile_num" ng-value="user_profile.phone_number" />
                </div>
                <div class="row fsu-input">
                    <span>Birthdate</span>
                    <input class="validate[custom[date]]  fsu-input-box col-xs-12" type="date" name="birthdate" data-errormessage-custom-error="Format(YYYY-MM-DD): ex. 2016-02-14" data-prompt-position="topLeft"/>
                </div>
                <!-- <div class="row btn-group edit-choose-gender">
                    <a style="width:50%;" class="btn dGenderEvent" data-gender="male" href="#">Male</a>
                    <a style="width:50%;" class="btn dGenderEvent" data-gender="female" href="#">Female</a>
                    <a style="width:40%;" class="btn dGenderEvent gChosen" data-gender="unknown" href="#">Rather not say</a>
                </div> -->
                <!-- <div class="row fsu-input f-select-gender">
                    <div class="col-xs-2 no-gutters">
                        Gender
                    </div>
                    <div class="col-xs-3 no-gutters">
                        <input id="gmale" type="radio" name="gender" ng-model="user_profile.sex" value='male' />
                        <label for="gmale">Male</label>
                    </div>
                    <div class="col-xs-3 no-gutters">
                        <input id="gfemale" type="radio" name="gender" ng-model="user_profile.sex" value='female' />
                        <label for="gfemale">Female</label>
                    </div>
                    <div class="col-xs-3 no-gutters">
                        <input id="gunknown" type="radio" name="gender" ng-model="user_profile.sex" value='unknown' />
                        <label for="gunknown">Other</label>
                    </div>
                </div> -->
                <div class="update-message" style="display:none;">
                    <span class='message'>

                    </span>
                </div>
                <div class="dUpdateProf btn_update_profile text-center">UPDATE PROFILE</div>
            </div>
            </form>
        </div>
    </div>
</div>