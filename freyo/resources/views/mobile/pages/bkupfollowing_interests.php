<div class="row">
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/fashion');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-fashion.jpg') ?>">
				</a>
			</div>
			<p class="cat-name">Fashion</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a>
		</div>
	</div>	
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/travel');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-travel.jpg') ?>">
				</a>
			</div>
			<p class="cat-name">Travel</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/celebrity');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-celebrity.jpg') ?>">			    					
				</a>
			</div>
			<p class="cat-name">Celebrities</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a>
		</div>
	</div>	
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/weddings');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-wedding.jpg') ?>">
				</a>
			</div>
			<p class="cat-name">Weddings</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/sports');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-sport.jpg') ?>">
				</a>
			</div>
			<p class="cat-name">Sports</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a> </div>
	</div>	
	<div class="col-xs-6">
		<div class="follow-box center-block text-center">
			<div class="cat-background-img">
				<a href="<?php echo asset('magazine/category/cars');?>">
					<img src="<?php echo asset('assets/mobile/images/followbg-car.jpg') ?>">
				</a>
			</div>
			<p class="cat-name">Cars</p>
			<a ng-click="followClick()" class="btn dToggleFollow" ng-class="followClass" href="#">{{ followText }}</a>
		</div>
	</div>
</div>