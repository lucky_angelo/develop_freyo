<div ng-init="magBrowser = '<?php echo asset('magazine/browser'); ?>'"></div>
<div ui-content-for="navbarLeft">
</div>

<div ui-content-for="navbarRight">
	<!-- <div class="right-category-btns">
		<ui-state default="1" id="activeTab"></ui-state>
		<i class="fa fa-square btn_mag_single_view" ui-class="{'active': activeTab == 1}" ui-set="{'activeTab': 1}"></i>
		<i class="fa fa-th-large btn_mag_grid_view" ui-class="{'active': activeTab == 2}" ui-set="{'activeTab': 2}"></i>
	</div> -->
</div>

<div ui-content-for="title">
  <a href="{{magBrowser+'/0/'+magID}}" class="magazine-left-button"><i class="fa fa-arrow-left"></i> <span class="f-magazine-lefttitle" >{{magazineTitle}}</span></a>
</div>

<div class="scrollable">
  <div class="scrollable-content view-magazine-background">
  	<div class="owlCarousel magazines-viewer" data-options="{items:1, loop:false ,stagePadding:0}"
    ng-init="magUrl = '<?php echo asset("/api/freyo_image/mag_page");?>'+'/'+((userScreenWidth) | number:0); magVidUrl = '<?php echo asset(Config::get('constants.url_file_magazine'));?>'; adsUrl = '<?php echo asset("/api/freyo_image/advertisement");?>'+'/'+((userScreenWidth) | number:0); adsVidUrl = '<?php echo asset(Config::get('constants.url_file_banner'));?>';">
  	    <div class="item magazine-pages owlCarouselItem" ng-repeat="mag in magPages">
  	        <div class="magazine-page-container" ng-switch="mag.page_type">
  	        	<div ng-switch-when="image" class="page-to-like" data-page-id="{{mag.liked!=null?'0':mag.page_id}}">
  	            	<img ng-src="{{magUrl+'/'+mag.magazine_page+'/'+mag.date_uploaded}}" class="dLikeAction img-responsive center-block">
  	        	</div>
  	        	<div ng-switch-when="video" class="page-to-like" data-page-id="{{mag.liked!=null?'0':mag.page_id}}">
  	            	<video class="dLikeAction center-block img-responsive" poster="{{magUrl+'/'+mag.video_poster+'/'+mag.date_uploaded}}" controls>
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.magazine_page}}" type="video/mp4">
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.video_webm}}" type="video/webm">
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.video_ogv}}" type="video/ogg">
  	            		Video not supported.
  	            	</video>
  	        	</div>
  	        	<div ng-switch-when="ads" class="magazine-ads-container" data-ads-type="{{mag.magazine_page.ads_type}}" data-ads-id="{{mag.magazine_page.ads_id}}" data-page-num="{{mag.page_number}}" data-mag-id="{{mag.magazine_id}}" data-ads-timer="{{mag.magazine_page.ads_timer}}">
                <div ng-if="mag.magazine_page.ads_type">
                  <img ng-if="mag.magazine_page.ads_type == 'image'" ng-src="{{adsUrl+'/'+mag.magazine_page.ads_content}}" class="img-responsive center-block">
                  <video ng-if="mag.magazine_page.ads_type == 'video'" class="center-block img-responsive">
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.ads_content}}" type="video/mp4">
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.content_webm}}" type="video/webm">
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.content_ogv}}" type="video/ogg">
                    Video not supported.
                  </video>
                </div>
  	        	</div>
  	        </div>
  	        <div class="magazine-page-details" ng-switch="mag.page_type">
                <span ng-switch-when="ads">ADVERTISEMENT</span>
  	            <span class="m-page-likes {{mag.liked!=null?'i-liked':''}}" ng-switch-default>
        					<i class="fa fa-heart"></i>
        					<i class="liked-texts">{{ mag.liked!=null?("you"+(mag.likes>1?(" and "+(mag.likes-1)+" others like this"):(" like this"))):(mag.likes==0?"No like":mag.likes+(mag.likes==1?" other like this":" others like this"))}}</i>
        				</span>
  	            <span class="m-page-number">{{mag.page_number}}/{{magTotalPage}}</span>
  	        </div>
  	    </div>
        <div class="item magazine-pages owlCarouselItem" ng-if="!logged_in && showToRegister">
           <div class="magazine-page-container teaser-logintoview text-center">
            <img ng-src="{{magUrl+'/login-viewmore.jpg/1900-01-01'}}" class="img-responsive center-block">
            <div class="teaser-image-content">
              <h2>There's more to see...</h2>
              <h5>Sign up to browse the full content</h5>
              <a class="teaser-goregister" href="#" ng-click="gobacktoregister()">SIGN UP</a>
              <a class="teaser-gologin" href="#" ng-click="gobacktologin()">Already a member? Log in here.</a>
            </div>
            <img ng-src="{{magVidUrl+'/teaser-freyologo.png'}}" class="teaser-freyologo center-block">
           </div>
        </div>
  	</div>
  </div>
</div>