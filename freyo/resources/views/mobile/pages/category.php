<div ui-content-for="navbarLeft">
	<i class="fa fa-bars"></i> <span class="f-category-lefttitle" ng-bind="category"></span>
</div>
<div ui-content-for="navbarRight">
	<div class="right-category-btns">
		<span class="btn_nav_follow">FOLLOW</span> 
		<a class="btn_share_category" href=""><i class="fa fa-share-alt"></i></a>
	</div>
</div>
<div ui-content-for="title">
  
</div>


<div class="scrollable">
  <div class="scrollable-content">
      <section class="">
        <ui-state default="1" id="activeTab"></ui-state>
        <div ng-class="tabCategoryClass()" class="btn-group justified nav-tabs">
          <a class="btn btn-default active" ui-class="{'active': activeTab == 1}" ui-set="{'activeTab': 1}">FEATURED</a>
          <a class="btn btn-default" ui-class="{'active': activeTab == 2}" ui-set="{'activeTab': 2}">TOP CHARTS</a>
          <a class="btn btn-default" ui-class="{'active': activeTab == 3}" ui-set="{'activeTab': 3}">NEW RELEASE</a>
        </div> 
        <div ui-if="activeTab == 1" class="ng-scope">
<!--           <div class="owl-carousel owlCarousel" data-options="{loop:true,  
                nav : false, stagePadding: 10, items : 1, autoplay : true}">
            <div> <img src="/assets/image/slider1.png" alt="" class="img-responsive"> </div>
            <div> <img src="/assets/image/slider2.png" alt="" class="img-responsive"> </div>
            <div> <img src="/assets/image/slider3.png" alt="" class="img-responsive"> </div>
          </div> -->
          Featured
        </div>
        <div ui-if="activeTab == 2" class="ng-scope" ng-include="topChartsUrl()"></div>
        <div ui-if="activeTab == 3" class="ng-scope" ng-include="newReleaseUrl()"></div>
      </section>
  </div>
</div>