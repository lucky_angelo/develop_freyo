<!-- <div class="f-magazine-box" ng-repeat="magazine in magazines"
ng-init="magCategory=(magazine.parent_id==0?magazine.category_id:magazine.parent_id)"> -->
<div class="f-magazine-box" ng-repeat="magazine in magazines | limitTo: nrItemLimit"
ng-init="magCategory=0">
    <a href="{{magBrowser+'/'+magCategory+'/'+magazine.magazine_id}}">
        <div class="is-loading">
            <img class='magazine-cover' ng-src='{{smallMagCover+"/"+magazine.magazine_cover}}'>
        </div>
    </a>
    <div class="mag-details-container">
        <h6 class="magazine-category mc-fashion">{{magazine.category_name}}</h6>
        <a href="{{magBrowser+'/'+magCategory+'/'+magazine.magazine_id}}">
            <h4 class="mb-magazine-title">{{($index+1)+". "+magazine.title}}</h4>
        </a>
        <!-- <h5 class="magazine-issue-date">{{magazine.issue_date | date:'MMMM y'}}</h5> -->
        <div class="magazine-rating">
            <input type="hidden" class="rating" value="{{magazine.rating}}" readonly="true" />
        </div>
        <div class="btn-group">
            <a href="{{magBrowser+'/'+magCategory+'/'+magazine.magazine_id}}" class="btn-xs btn-default btn_magazine_comment"><i class="fa fa-comment"></i> Comment</a>
            <a class="btn-xs btn-default btn_magazine_favorite dSubscribeAction dSubscribeConfirmation" data-magazine-id="{{ magazine.magazine_id }}" fmag-title="{{magazine.title}}"><i class="fa fa-bookmark"></i></a>
            <a class="btn-xs btn-default btn_magazine_share dShareAction" where-share='mdashboard' fmag-title="{{magazine.title}}" fmag-image="{{magCover+'/'+magazine.magazine_cover}}"><i class="fa fa-share-alt"></i></a>
            <div class="magazine-views-favs">
                <span class="md-views">{{magazine.views+(magazine.views>1?' views':' view')}}</span>
                <span>•</span>
                <span class="md-favorites">{{magazine.favorites+(magazine.favorites>1?' subscribers':' subscriber')}}</span>
            </div>
        </div>
    </div>
</div>