<div ui-content-for='navbarRight'>
    
        <a href="/search">
        <div class="btn">
            <i class="fa fa-search"></i>
        </div>
        </a>
    
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
    <div class="scrollable-content">
        <div class="f-comments-header">
            COMMENTS
        </div>
        <div class="f-comments-body">
            <div class="f-comments">
                <ul class="f-comment-list">
                    <li class="f-comment-item" ng-repeat="comment in comments" ng-if="comments.length > 0">
                        <div class="col-xs-12 no-gutters f-comments-name">{{ comment.first_name+" "+comment.last_name }}</div>
                        <div class="col-xs-12 no-gutters f-comments-message">{{ comment.comment }}<br><span am-time-ago="comment.date_created" class="time-ago">  </span></div>
                        <!-- <span class="time-ago"> {{ comment.date_created | fromNow }} </span> -->
                        <span class="no-comment-msg"  ng-if="comment.showMessage == true">Be the first to comment</span>
                    </li>
                    <!-- <li class="f-comment-item">
                        <span class="no-comment-msg">Be the first to comment</span>
                    </li> -->
                </ul>
                <!-- <span>{{ comments }}</span>
                <span>{{ a }}</span> -->
            </div>
        </div>
        <div class="f-comment-box">
            <form class="comment-form">
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <input type="hidden" name="magazine_id" ng-value="magazineID">
                <input type="text" name="comment" placeholder="ADD COMMENT" class="f-cmnts-input validate[required]" data-errormessage-value-missing="Please insert comment!" data-prompt-position="topLeft"">
                <button class="f-submit-comment dSubmitComment"> > </button>
            </form>
        </div>
    </div>
</div>