<div class="scrollable container-fluid f-sidenav">
    <!-- <div class="col-xs-12 f-close-menu" ui-turn-off='uiSidebarLeft'>
        <a class="btn_close_sidenav" href="#"><i class="fa fa-times"></i>&nbsp;<span>Close Menu</span></a>
    </div> -->
    <div class="col-xs-12 f-side-freyo-logo">
		<img class="img-responsive" src="<?php echo asset('assets/mobile/images/freyo-logo.png');?>" width="100px">
    </div>
    <div class="f-nav-items" ui-turn-off='uiSidebarLeft'>
        
        <!-- <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('profile/subscriptions');?>">
				<img src="<?php echo asset('assets/mobile/images/following-nav.png');?>" style="top:15px;"> <span>Subscription</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('profile/likes');?>">
				<img src="<?php echo asset('assets/mobile/images/myclippings-nav.png');?>"> <span>Clippings</span>
			</a>
        </div> -->
        <!-- <div class="col-xs-12 f-nav-page">
			<a href="<?php //echo asset('search');?>">
				<img src="<?php //echo asset('assets/mobile/images/browse-nav.png');?>"> <span>Browse</span>
			</a>
		</div> -->
        <div class="col-xs-12 f-nav-page my-profile-nav">
            <a href="<?php echo asset('profile/likes');?>">
				<span>MY PROFILE</span>
			</a>
            <!-- <a class="edit-profile-nav" href="<?php echo asset('profile/edit');?>"><i class="fa fa-pencil-square-o"></i></a> -->
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('top_charts') ?>">
                <span>TOP CHARTS</span>
            </a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('dashboard');?>">
				<span>FEATURED</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="/search">
                <span>SEARCH</span>
            </a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('settings');?>">
				<span>SETTINGS</span>
			</a>
        </div>
        <!-- <div class="col-xs-12 f-nav-page">
            <a href="<?php echo asset('support');?>">
				<img src="<?php echo asset('assets/mobile/images/support-nav.png');?>"> <span>Support</span>
			</a>
        </div>
        <div class="col-xs-12 f-nav-page">
            <a ui-turn-on="modal1" href="#">
				<img src="<?php echo asset('assets/mobile/images/signout-nav.png');?>"> <span>Sign Out</span>
			</a>
        </div> -->

    </div>
    <div class="col-xs-12 f-profile-details">
        <a href="<?php echo asset('profile/edit');?>">
			<!-- <img class="f-profile-pic img-circle" src="<?php echo asset('assets/mobile/images/gintoki_shock.jpg') ?>"> -->
			<div class="col-xs-4 no-padding">
				<img class="f-profile-pic img-circle" ng-src="{{url_profile_pic}}">
			</div>
			<!-- <p class="f-profile-name">{{user_profile.first_name+' '+user_profile.last_name}}</p> -->
			<div class="col-xs-8 no-padding">
				<span class="f-profile-name">{{user_profile.first_name+' '+user_profile.last_name}}</span>
			</div>
			<!-- <p class="f-profile-email">{{user_profile.email}}</p> -->
		</a>
    </div>
</div>
