<div ng-init="magBrowser = '<?php echo asset('magazine/browser'); ?>'"></div>
<div ui-content-for="navbarLeft">
</div>

<div ui-content-for="navbarRight">
  <!-- <div class="right-category-btns">
    <ui-state default="1" id="activeTab"></ui-state>
    <i class="fa fa-square btn_mag_single_view" ui-class="{'active': activeTab == 1}" ui-set="{'activeTab': 1}"></i>
    <i class="fa fa-th-large btn_mag_grid_view" ui-class="{'active': activeTab == 2}" ui-set="{'activeTab': 2}"></i>
  </div> -->
</div>

<!-- <div ui-content-for="title">
  <a href="{{magBrowser+'/0/'+magID}}" class="magazine-left-button"><i class="fa fa-arrow-left"></i> <span class="f-magazine-lefttitle" ng-bind="magazineTitle"></span></a>
</div> -->

<div class="scrollable">
  <div class="scrollable-content view-magazine-background">
    <div class="owlCarousel magazines-viewer" data-options="{items:1, loop:false ,stagePadding:0}"
    ng-init="magUrl = '<?php echo asset("/api/freyo_image/mag_page");?>'+'/'+((userScreenWidth+50) | number:0); magVidUrl = '<?php echo asset(Config::get('constants.url_file_magazine'));?>'; adsUrl = '<?php echo asset("/api/freyo_image/advertisement");?>'+'/'+((userScreenWidth+100) | number:0); adsVidUrl = '<?php echo asset(Config::get('constants.url_file_banner'));?>';">
        <div class="item magazine-pages owlCarouselItem" ng-repeat="mag in magPages">
            <div class="magazine-page-details" ng-switch="mag.page_type">
                <a href="{{magBrowser+'/0/'+magID}}" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>
                <span ng-switch-when="ads" style="float: right">ADVERTISEMENT</span>
                <span class="m-page-number">{{mag.page_number}}/{{magTotalPage}}</span>
                <span class="m-page-likes {{mag.liked!=null?'i-liked':''}}" ng-switch-default>
                  <!-- <i class="fa fa-paperclip the-unlicker {{mag.liked!=null?'dUnlickAction':''}}"></i>
                 <i class="liked-texts">{{ mag.liked!=null?("you"+(mag.likes>1?(" and "+(mag.likes-1)+" others clipped this"):(" clipped this"))):(mag.likes==0?"Clip this":mag.likes+(mag.likes==1?" other clipped this":" others clipped this"))}}</i> -->
                  <span class="the-unlicker the-liker {{mag.liked!=null?'dUnlickAction':'dLikeAction'}}" style="padding: 6px">
                   
                 <!--  <i class="liked-texts">{{ mag.liked!=null?("you"+(mag.likes>1?(" and "+(mag.likes-1)+" others like this"):(" like this"))):(mag.likes==0?"No like":mag.likes+(mag.likes==1?" other like this":" others like this"))}}</i> -->
                    <!-- <i class="liked-texts">{{ mag.liked!=null?("you"+(mag.likes>1?(" and "+(mag.likes-1)+" others clipped this"):(" clipped this"))):(mag.likes==0?"Clip this":mag.likes+(mag.likes==1?" other clipped this":" others clipped this"))}}</i> -->
                    <span class="liked-texts">{{ mag.likes }}</span>
                    <i class="fa fa-paperclip "></i>
                  </span>
                </span>
                
            </div>
            <div class="magazine-page-container" ng-switch="mag.page_type">
              <div ng-switch-when="image" class="mag-image-page page-to-like" data-page-id="{{mag.page_id}}">
                <div class="{{mag.magazine_content!=''?'wtoucharea':''}}">
                  <div class="is-loading">
                  <img ng-src="{{magUrl+'/'+mag.magazine_page+'/'+mag.date_uploaded}}" class="img-responsive center-block" data-magtype="image" alt="">
                  </div>
                  <!-- <div ng-if="mag.magazine_content != ''" class="read-more-notice text-center">
                    <i class="fa fa-angle-up"></i>
                    <div>Read Content</div>
                  </div> -->
                </div>
                <div ng-if="mag.magazine_content != ''" class="wswipe-up magazine-additional-content hide-overflow dReadMore">
                  <div class="magazine-content" ng-bind-html="mag.magazine_content"></div>
                  <!-- <div class="hide-mag-content"><i class="fa fa-angle-down"></i></div> -->
                  <div class="read-more-notice text-center">
                    <div class="read-more-text">Read more</div>
                  </div>
                </div>
              </div>
              <div ng-switch-when="video" class="page-to-like mag-video-container wtoucharea" data-page-id="{{mag.page_id}}">
                  <div class="is-loading">
                  <img ng-src="{{magUrl+'/'+mag.video_poster+'/'+mag.date_uploaded}}" class="img-responsive center-block" data-magtype="video" alt="">
                  </div>
                  <div class="read-more-notice text-center">
                    <i class="fa fa-angle-up"></i>
                    <div>Watch Video</div>
                  </div>
                  <video class="center-block img-responsive wswipe-up mag-video-player" controls style="display:none;">
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.magazine_page}}" type="video/mp4">
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.video_webm}}" type="video/webm">
                    <source src="{{magVidUrl+'/'+(mag.date_uploaded|date:'yyyy/MM/dd')+'/'+mag.video_ogv}}" type="video/ogg">
                    Device not supported.
                  </video>
              </div>
              <div ng-switch-when="ads" class="magazine-ads-container" data-ads-type="{{mag.magazine_page.ads_type}}" data-ads-id="{{mag.magazine_page.ads_id}}" data-page-num="{{mag.page_number}}" data-mag-id="{{mag.magazine_id}}" data-ads-timer="{{mag.magazine_page.ads_timer}}">
                <div ng-if="mag.magazine_page.ads_type">
                  <a ng-if="isClickable == 'true' && mag.magazine_page.ads_link != '#'" href="{{mag.magazine_page.ads_link}}#@dv3rt1s3m3nt" class="ad-click" target="_blank" data-ads-id="{{mag.magazine_page.ads_id}}" data-mag-id="{{mag.magazine_id}}">
                    <!-- <div class="wtoucharea"> -->
                      <div class="is-loading">

                      <img ng-if="mag.magazine_page.ads_type == 'image'" ng-src="{{adsUrl+'/'+mag.magazine_page.ads_content}}" class="img-responsive center-block" alt="" >
                      </div>
                     <!--  <div class="read-more-notice text-center">
                        <i class="fa fa-angle-up"></i>
                        <div>Click to view advertisement</div>
                      </div> -->
                    <!-- </div> -->
                  </a>

                    <div ng-if="isClickable != 'true' || mag.magazine_page.ads_link == '#'" class="is-loading">

                      <img ng-if="mag.magazine_page.ads_type == 'image'" ng-src="{{adsUrl+'/'+mag.magazine_page.ads_content}}" class="img-responsive center-block" alt="" >
                    </div>
                  <video ng-if="mag.magazine_page.ads_type == 'video'" class="center-block img-responsive" >
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.ads_content}}" type="video/mp4">
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.content_webm}}" type="video/webm">
                    <source src="{{adsVidUrl+'/'+mag.magazine_page.content_ogv}}" type="video/ogg">
                    Video not supported.
                  </video>
                </div>
              </div>
            </div>
            
        </div>
        <div class="item magazine-pages owlCarouselItem" ng-if="magRelated.length > 0 || magOthers.length > 0">
          <div class="magazine-page-details" ng-switch="mag.page_type">
              <a href="{{magBrowser+'/0/'+magID}}" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>
          </div>
          <div class="featured-magazine-container" ng-if="magRelated.length > 0">
            
            <div class="category-items-container">
              <div class="category-item-header">
                  <h4 class="category-title">Related Magazines</h4> <a class="category-all" href="{{magBrowser+'/'+magCatID}}">
                  <span class="category-others">SEE ALL </span>
                  <i class="fa fa-chevron-circle-right"></i></a>
              </div>
              <div class="category-item-body">
                  <div class="magazine-items">
                      <div class="magazine-item-in" ng-repeat="magazine in magRelated" ng-init="magCategory=0; magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/3)+15 | number:0);">
                          <a href="{{magBrowser+'/0/'+magazine.magazine_id}}">
                              <div class="is-loading">
                                  <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                              </div>
                              <p class="magazine-title">{{magazine.title}}</p>
                          </a>
                      </div>
                  </div>
              </div>
            </div>
          </div>

          <div class="featured-magazine-container" ng-if="magOthers.length > 0">
            <div class="category-items-container">
              <div class="category-item-header">
                  <h4 class="category-title">Other Titles</h4> <a class="category-all" href="{{magBrowser+'/0'}}">
                  <span class="category-others">SEE ALL </span>
                  <i class="fa fa-chevron-circle-right"></i></a>
              </div>
              <div class="category-item-body">
                  <div class="magazine-items">
                      <div class="magazine-item-in" ng-repeat="magazine in magOthers" ng-init="magCategory=0; magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/3)+50 | number:0);">
                          <a href="{{magBrowser+'/0/'+magazine.magazine_id}}">
                              <div class="is-loading">
                                  <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                              </div>
                              <p class="magazine-title">{{magazine.title}}</p>
                          </a>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item magazine-pages owlCarouselItem" ng-if="!logged_in && showToRegister">
           <div class="magazine-page-container teaser-logintoview text-center">
            <img ng-src="{{magUrl+'/login-viewmore.jpg/1900-01-01'}}" class="img-responsive center-block">
            <div class="teaser-image-content">
              <h2>There's more to see...</h2>
              <h5>Sign up to browse the full content</h5>
              <a class="teaser-goregister" href="#" ng-click="gobacktoregister()">SIGN UP</a>
              <a class="teaser-gologin" href="#" ng-click="gobacktologin()">Already a member? Log in here.</a>
            </div>
            <img ng-src="{{magVidUrl+'/teaser-freyologo.png'}}" class="teaser-freyologo center-block">
           </div>
        </div>
    </div>
  </div>
</div>
<div class="ticktock-handler"></div>