    <div class="owlCarousel magazines-viewer" data-options="{items:1, loop:false ,stagePadding:0}">
		<div class="item magazine-pages">
			<div class="magazine-page-container">
				<img src='https://placeholdit.imgix.net/~text?txtsize=78&txt=Sample%20Image&w=640&h=830' class="img-responsive center-block">
			</div>
			<div class="magazine-page-details">
				<span class="m-page-likes i-liked">
					<i class="fa fa-heart"></i>
					<i>you and 1225 others like this</i>
				</span>
				<span class="m-page-number">1/5</span>
			</div>
		</div>
		<div class="item magazine-pages">
			<div class="magazine-page-container">
				<img src='https://placeholdit.imgix.net/~text?txtsize=78&txt=Sample%20Image&w=640&h=830' class="img-responsive center-block">
			</div>
			<div class="magazine-page-details">
				<span class="m-page-likes">
					<i class="fa fa-heart"></i>
					<i>122 others like this</i>
				</span>
				<span class="m-page-number">2/5</span>
			</div>
		</div>
		<div class="item magazine-pages">
			<div class="magazine-page-container">
				<img src='https://placeholdit.imgix.net/~text?txtsize=78&txt=Sample%20Image&w=640&h=830' class="img-responsive center-block">
			</div>
			<div class="magazine-page-details">
				<span class="m-page-likes">
					<i class="fa fa-heart"></i>
					<i>12 others like this</i>
				</span>
				<span class="m-page-number">3/5</span>
			</div>
		</div>
		<div class="item magazine-pages">
			<div class="magazine-page-container">
				<img src='https://placeholdit.imgix.net/~text?txtsize=78&txt=Sample%20Image&w=640&h=830' class="img-responsive center-block">
			</div>
			<div class="magazine-page-details">
				<span class="m-page-likes i-liked">
					<i class="fa fa-heart"></i>
					<i>you and 322 others like this</i>
				</span>
				<span class="m-page-number">4/5</span>
			</div>
		</div>
		<div class="item magazine-pages">
			<div class="magazine-page-container">
				<img src='https://placeholdit.imgix.net/~text?txtsize=78&txt=Sample%20Image&w=640&h=830' class="img-responsive center-block">
			</div>
			<div class="magazine-page-details">
				<span class="m-page-likes i-liked">
					<i class="fa fa-heart"></i>
					<i>you and 12255 others like this</i>
				</span>
				<span class="m-page-number">5/5</span>
			</div>
		</div>
	</div>