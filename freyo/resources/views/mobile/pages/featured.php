<!-- ng-repeat featured image -->
<div ng-if="sliderTotal > 1"class="owlCarousel" data-options="{lazyPrefetch:1,lazyLoad:true, loop:true, nav : false, items : 1, autoplay : true, dots : true, dotsEach: true}">
    <div class="item owlCarouselItem" ng-repeat="slider in featuredSlider">
        <img data-src="{{sliderUrl+'/'+slider.featured_image}}" class="img-responsive owl-lazy" alt="">
    </div>
</div>
<div ng-if="sliderTotal == 1" class="owlCarousel" data-options="{nav : false, items : 1, dots: true}">
    <div class="item owlCarouselItem" ng-repeat="slider in featuredSlider">
        <img ng-src="{{sliderUrl+'/'+slider.featured_image}}" class="img-responsive" alt="">
    </div>
</div>

<!-- <div ng-repeat="featured in dashboardFeatured">
    <b>{{ featured.category_id }} - {{featured.category_name}}</b>
    <div ng-repeat="mags in featured.magazines">
        <i>{{ mags }} <br/>WWWWWWWW</i>
    </div>
</div> -->
<div class="col-xs-12 col-p-10 featured-magazine-container feat-category-{{featured.category_id}}" ng-repeat="featured in dashboardFeatured">
    <div class="category-items-container">
        <div class="category-item-header" ng-if="featured.category_name == 'Recommended'">
            <h5 class="category-title">RECOMMENDED</h5> <!-- <a class="category-all" href="{{magBrowser+'/'+featured.category_id}}"> -->
            <!-- <span ng-if="featured.category_id == -1" class="freyo-prime-all">SEE ALL </span>
            <span ng-if="featured.category_id != -1" class="category-others">SEE ALL </span>
            <i class="fa fa-chevron-circle-right"></i></a> -->
        </div>
        <div class="category-item-header" ng-if="featured.category_name != 'Recommended'">
            <h5 class="category-title">MAGAZINE</h5> <!-- <a class="category-all" href="{{magBrowser+'/'+featured.category_id}}"> -->
            <!-- <span ng-if="featured.category_id == -1" class="freyo-prime-all">SEE ALL </span>
            <span ng-if="featured.category_id != -1" class="category-others">SEE ALL </span>
            <i class="fa fa-chevron-circle-right"></i></a> -->
        </div>
        <!-- autoHeight:true, -->
        <!-- ng-if="featured.category_name == 'Recommended'" -->
        <div class="f-recommended" ng-if="featured.category_name == 'Recommended'">
            <div class="category-item-body owlCarousel" ng-if="featured.category_name == 'Recommended' && featured.magazines.length > 0" data-options="{items:3, margin: 10}">
                <div class="magazine-item featured-issue-dashboard owlCarouselItem"  ng-repeat="magazine in featured.magazines">
                    <a href="{{magBrowser+'/'+featured.category_id+'/'+magazine.magazine_id}}">
                        <div class="is-loading">
                            <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                        </div>
                        <!-- <p class="magazine-title">{{magazine.title}}</p> -->
                    </a>
                </div>
                
            </div>
            <div class="category-item-body" ng-if="featured.category_name == 'Recommended' && featured.magazines.length == 0">
                <div class="magazine-item featured-issue-dashboard">
                    <span class="no-comment-msg center">Nothing to recommend.</span>
                </div>
                
            </div>
        </div>
        
        <!--   -->
            <div class="category-item-body" ng-if="featured.category_name != 'Recommended'">
                <div class="magazine-items">
                    <div class="magazine-item featured-issue-dashboard f-magazine-bg" ng-repeat="magazine in featured.magazines">
                        <a href="{{magBrowser+'/'+featured.category_id+'/'+magazine.magazine_id}}">
                            <div class="is-loading">
                                <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
                            </div>
                            <p class="magazine-title">{{magazine.category_name}}</p>
                            <p class="magazine-description">{{ magazine.details }}</p>
                        </a>
                    </div>
                </div>

            </div>

    </div>
</div>