<div ui-content-for="navbarRight">
<!--     <div class="btn">
        <a class="btn_signout" href="#" ui-turn-on="modal1">
            <i class="fa fa-sign-out"></i>
        </a>
    </div> -->
</div>
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <!-- <i class="fa fa-bars target darken" background-check></i> -->
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>
<div class="scrollable">
    <div class="scrollable-content" ui-scroll-bottom="lsAddItems()">
        <div class="f-myprofile-body">
            <!-- <div ng-if="profileType == 'myprofile'" class="ng-scope">
                <div class="f-edit-profile text-center">
                    <p>Please complete your personal information and get
                        <br/>a chance to win a exlusive magazine issue.</p>
                    <a class="btn_edit_profile" href="<?php echo asset('profile/edit');?>">EDIT PROFILE</a>
                </div>
            </div> -->
            <div class="f-myprofile-head">
                <div class="f-myprofile-bg">
                    <div class="blurred-profpic-bg" style="background-color:#f1f1f1"></div>
                    <!-- <img class="blurred-profpic-bg img-responsive" src="<?php echo asset('assets/mobile/images/gintoki_shock.jpg') ?>"> -->
                    <div class="myprof-bg-overlay"></div>
                    <div class="f-myprofile-details text-center">
                        <img class="f-profile-pic img-circle" ng-src="{{url_profile_pic}}">
                        <p class="f-profile-name">{{user_profile.first_name+' '+user_profile.last_name}}</p>
                        <p class="f-profile-email">{{user_profile.email}}</p>
                    </div>
                    <div ng-switch on="profileType">
                        <div ng-switch-when="likes">
                            <ui-state default="1" id="activeTab"></ui-state>
                        </div>
                        <div ng-switch-default>
                            <ui-state default="2" id="activeTab"></ui-state>
                        </div>
                    </div>
                    <div class="btn-group justified nav-tabs profile-btn-group">
                        <a class="btn" ui-class="{'active': activeTab == 1}" ui-set="{'activeTab': 1}"><span id="like_total" data-liketotal="{{ likeTotal }}">{{likeTotal}}</span><br/><small class="thin">{{(likeTotal>1?' CLIPPED':'CLIPPED')}}</small></a>
                        <a class="btn" ui-class="{'active': activeTab == 2}" ui-set="{'activeTab': 2}"><span id="subscriber_total">{{subTotal}}</span><br/><small class="thin">{{(subTotal>1?' SUBSCRIPTIONS':' SUBSCRIPTION')}}</small></a>
                    </div>
                </div>
            </div>
            <div ui-if="activeTab == 1" class="ng-scope" ng-include="likedMagazineUrl()">
            </div>
            <div ui-if="activeTab == 2" class="ng-scope following-magazines" ng-include="subscribedMagazineUrl()">
            </div>
        </div>
    </div>
</div>
