<div ui-content-for="navbarRight"></div>
<div ng-if="!logged_in" ui-content-for="navbarLeft">
</div>
<!-- <div ui-content-for="title">
    <a ng-if="!logged_in" href="<?php echo asset('dashboard');?>" class="magazine-left-button"><i class="fa fa-arrow-left"></i></a>SEARCH
</div> -->
<div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-content-for="navbarLeft">
    <img src="<?php echo asset('assets/mobile/images/freyo-dots.png');?>" class="img-responsive f-dots" style="width: 25px">
</div>

<div class="scrollable">
    <div class="scrollable-content f-search" ui-scroll-bottom="sAddItems()">
        <div class="search-input-box">
            <input type="text" class="col-xs-12" ng-model="searchMag"></input>
            <i class="fa fa-search"></i>
        </div>
        <div class="fmagazines text-center" ng-init="magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/2) | number:0)+'/'; magBrowser = '<?php echo asset('magazine/browser'); ?>'">
            <div class="f-magazine-item col-xs-4" ng-repeat="magazine in (filteredMags = (magazines | filter: searchMag | limitTo: sItemLimit))" ng-init="magCategory=magazine.category_id">
                <a href="{{magBrowser+'/0/'+magazine.magazine_id}}">
                    <div class="is-loading">
                        <img class='magazing-cover' ng-src='{{magCover+magazine.magazine_cover}}'>
                    </div>
                    <p class="magazine-title">{{ magazine.title }}</p>
                    <!-- <div class="magazine-options">
                        <div class="col-xs-12">
                            <i class='fa fa-eye'></i><span>{{magazine.views+(magazine.views>1?' views':' view')}}</span>
                        </div>
                        <div class="col-xs-12">
                            <i class='fa fa-bookmark'></i><span>{{magazine.favorites+(magazine.favorites>1?' subscribers':' subscriber')}}</span>
                        </div>
                    </div>
                    <div class="magazine-rating">
                        <input type="hidden" class="rating" value="{{magazine.rating}}" data-readonly="true" />
                    </div> -->
                </a>
            </div>
        </div>
    </div>
</div>
