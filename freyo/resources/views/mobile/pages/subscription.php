<!-- <div class="fmagazines text-center" ng-init="imgWidth=((userScreenWidth/3) | number:0); imgHeight=((fixImgHeight/fixImgWidth)*imgWidth | number:0); magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/3)+15 | number:0); magBrowser = '<?php echo asset('magazine/browser'); ?>'; lowMagCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/20';"> -->
<div class="fmagazines text-center" ng-init="magCover = '<?php echo asset("/api/freyo_image/mag_cover");?>'+'/'+((userScreenWidth/3)+15 | number:0); magBrowser = '<?php echo asset('magazine/browser'); ?>';">
    <div class="f-magazine-item col-xs-4" ng-repeat="magazine in subsMags | limitTo:subItemLimit" ng-init="magCategory=0">
        <a href="{{magBrowser+'/'+magCategory+'/'+magazine.magazine_id}}">
            <!-- <div class="is-loading" style='width:{{imgWidth}}px; height: {{imgHeight}}px; overflow:hidden; background:#ececec url({{lowMagCover+"/"+magazine.magazine_page}}) no-repeat 0 0 / 100% 100%'> -->
            <div class="is-loading">
                <img class='magazing-cover' ng-src='{{magCover+"/"+magazine.magazine_cover}}'>
            </div>
            <p class="magazine-title">{{ magazine.title }}</p>
            <!-- <div class="magazine-options">
                <div class="col-xs-12">
                    <i class='fa fa-eye'></i><span>{{magazine.views+(magazine.views>1?' views':' view')}}</span>
                </div>
                <div class="col-xs-12">
                    <i class='fa fa-bookmark'></i><span>{{magazine.favorites+(magazine.favorites>1?' subscribers':' subscriber')}}</span>
                </div>
            </div>
            <div class="magazine-rating">
                <input type="hidden" class="rating" value="{{magazine.rating}}" data-readonly="true" />
            </div> -->
        </a>
    </div>
</div>