<!DOCTYPE html>
<html>
<head>
	<title>Wi</title>

	<link href='https://fonts.googleapis.com/css?family=Roboto:500,400,100,300,700,900' rel='stylesheet' type='text/css'>

	 <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/app.css')}}">

	<script type="text/javascript" src="{{asset('assets/mobile/js/angular.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/mobile/js/angular-route.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/mobile/js/angApp.js')}}"></script>

</head>
<body ng-app="freyo">

<div class="nav-slider-w">
	@yield('content')
</div>
	


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>