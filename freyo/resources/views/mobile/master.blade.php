<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
    <title>Freyo</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,400,100,300,700,900' rel='stylesheet' type='text/css'>
    <!-- Latest compiled and minified CSS -->
    {{--
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> --}}
    <link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/validationEngine.jquery.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/mobile-angular-ui-base.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/owl.carousel.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/owl.theme.css') ?>">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/animate.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/library/offline/offline-theme-dark.css') ?>"> -->
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/library/offline/offline-language-english.css') ?>">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/mobile/css/app.css?v=21')}}">
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-73789781-1', 'auto');
      ga('send', 'pageview');
    </script>
     <style type="text/css">
        /* line 3, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui, .offline-ui *, .offline-ui:before, .offline-ui:after, .offline-ui *:before, .offline-ui *:after {
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }

        /* line 6, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui {
          display: none;
          position: fixed;
          background: white;
          z-index: 2000;
        }
        /* line 13, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui .offline-ui-retry {
          display: none;
        }
        /* line 16, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui.offline-ui-up {
          display: none;
        }
        /* line 19, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui.offline-ui-down {
          display: none;
        }

        /* line 8, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui {
          -webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          -moz-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          -webkit-border-radius: 4px 4px 0 0;
          -moz-border-radius: 4px 4px 0 0;
          -ms-border-radius: 4px 4px 0 0;
          -o-border-radius: 4px 4px 0 0;
          border-radius: 4px 4px 0 0;
          font-family: "Helvetica Neue", sans-serif;
          font-weight: 300;
          /*padding: 0em 1em;*/
          padding: 1em;
          background: black;
          color: #cccccc;
          bottom: 0;
          left: 20px;
        }
        /* line 19, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui .offline-ui-content {
          padding-left: 1.5em;
        }
        /* line 22, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui .offline-ui-content:after {
          -webkit-border-radius: 50%;
          -moz-border-radius: 50%;
          -ms-border-radius: 50%;
          -o-border-radius: 50%;
          border-radius: 50%;
          content: " ";
          display: block;
          position: absolute;
          top: 0;
          bottom: 0;
          left: 1em;
          margin: auto;
          height: 0.8em;
          width: 0.8em;
        }
        /* line 36, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui.offline-ui-up .offline-ui-content:after {
          background: #80d580;
        }
        
        /* line 41, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui.offline-ui-down .offline-ui-content:after {
          background: #e24949;
        }
        .offline-ui.offline-ui-down .offline-ui-content::before {
          content: 'Offline' !important;
        }
    </style>
</head>

<body ng-app="myApp">
      <div class='fpreloader'><div class='floader-load'>
      <style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.6);'><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(0deg) translate(0,-50px);transform:rotate(0deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(120deg) translate(0,-50px);transform:rotate(120deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(240deg) translate(0,-50px);transform:rotate(240deg) translate(0,-50px);border-radius:40px;position:absolute;'></div></div>
    </div></div>
    @yield('content')
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script type="text/javascript" src="{{asset('assets/mobile/js/jquery.validationEngine-en.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/mobile/js/jquery.validationEngine.js')}}"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/mobile/js/hammer.min.js?v=3')}}"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/owl.carousel.min.js'); ?>"></script>
    <script src="<? echo asset('assets/mobile/library/offline/offline.min.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/mobile/js/app.js?v=19')}}"></script>
</body>

</html>
