<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- <meta name="viewport" content="width=device-width, user-scalable=no" /> -->
    <!-- <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" /> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimal-ui">
    <meta name="csrf-token" content="<?php echo csrf_token() ?>" />
    <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
    <?php if(isset($_SESSION['fapp_version'])) { ?>
    <meta name="fapp-version" content="<?php echo $_SESSION['fapp_version'] ?>" />
    <?php } ?>
    <?php if(isset($_SESSION['fapp_version']) && $_SESSION['fapp_version'] > 2) { ?>
    <meta name="clickable" content="true" />
    <?php } ?>
    <title>Freyo</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:500,400,100,300,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/mobile-angular-ui-base.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/animate.css') ?>">
    <link href="<? echo asset('assets/mobile/library/bootstrap-rating/css') ?>/star-rating.min.css?w=1" media="all" rel="stylesheet" type="text/css" />
    <link href="<? echo asset('assets/mobile/library/bootstrap-rating/css') ?>/theme-krajee-fa.min.css" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/owl.carousel.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/owl.theme.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/swiper.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/validationEngine.jquery.css') ?>">
    <!-- <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/library/offline/offline-theme-dark.css') ?>"> -->
     <style type="text/css">
        /* line 3, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui, .offline-ui *, .offline-ui:before, .offline-ui:after, .offline-ui *:before, .offline-ui *:after {
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }

        /* line 6, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui {
          display: none;
          position: fixed;
          background: white;
          z-index: 2000;
        }
        /* line 13, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui .offline-ui-retry {
          display: none;
        }
        /* line 16, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui.offline-ui-up {
          display: none;
        }
        /* line 19, ../sass/_offline-theme-base-indicator.sass */
        .offline-ui.offline-ui-down {
          display: none;
        }

        /* line 8, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui {
          -webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          -moz-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.15);
          -webkit-border-radius: 4px 4px 0 0;
          -moz-border-radius: 4px 4px 0 0;
          -ms-border-radius: 4px 4px 0 0;
          -o-border-radius: 4px 4px 0 0;
          border-radius: 4px 4px 0 0;
          font-family: "Helvetica Neue", sans-serif;
          font-weight: 300;
          /*padding: 0em 1em;*/
          padding: 1em;
          background: black;
          color: #cccccc;
          bottom: 0;
          left: 20px;
        }
        /* line 19, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui .offline-ui-content {
          padding-left: 1.5em;
        }
        /* line 22, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui .offline-ui-content:after {
          -webkit-border-radius: 50%;
          -moz-border-radius: 50%;
          -ms-border-radius: 50%;
          -o-border-radius: 50%;
          border-radius: 50%;
          content: " ";
          display: block;
          position: absolute;
          top: 0;
          bottom: 0;
          left: 1em;
          margin: auto;
          height: 0.8em;
          width: 0.8em;
        }
        /* line 36, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui.offline-ui-up .offline-ui-content:after {
          background: #80d580;
        }
        
        /* line 41, ../sass/offline-theme-dark-indicator.sass */
        .offline-ui.offline-ui-down .offline-ui-content:after {
          background: #e24949;
        }
        .offline-ui.offline-ui-down .offline-ui-content::before {
          content: 'Offline' !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/library/offline/offline-language-english.css') ?>">
    <!-- DEVELOPER CSS -->
    <!-- <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/appP.css') ?>"> -->
    <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/app.css?w=42') ?>">   
    <!-- <link rel="stylesheet" type="text/css" href="<? echo asset('assets/mobile/css/app-responsive.css') ?>"> -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      // ga('create', 'UA-73789781-1', 'auto');
      // ga('send', 'pageview');
    </script>
    <!-- jQuery library -->
    <!-- <script src="https://www.google.com/recaptcha/api.js?onload=vcRecaptchaApiLoaded&render=explicit" async defer></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/masonry.pkgd.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/imagesloaded.pkgd.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/jquery.validationEngine-en.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/jquery.validationEngine.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/jquery.lazyload.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/lazysizes.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/owl.carousel.min.js'); ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/swiper.min.js'); ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-route.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/mobile-angular-ui.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/mobile-angular-ui.gestures.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-google-analytics.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-socialshare.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-sanitize.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-animate.min.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/background-check.min.js') ?>"></script>
    <!-- <script type="text/javascript" src="<? echo asset('assets/mobile/js/jquery.timeago.js') ?>"></script> -->
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/moment.js') ?>"></script>
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-moment/angular-moment.min.js') ?>"></script>
    <!-- <script type="text/javascript" src="<? echo asset('assets/mobile/js/ng-text-truncate.js') ?>"></script> -->
    <script src="<? echo asset('assets/mobile/js/readmore.js') ?>" type="text/javascript"></script>
    <!-- <script type="text/javascript" src="<? echo asset('assets/mobile/js/angular-recaptcha.min.js') ?>"></script> -->
    <script src="<? echo asset('assets/mobile/library/offline/offline.min.js') ?>" type="text/javascript"></script>

    <script src="<? echo asset('assets/mobile/library/bootstrap-rating/js') ?>/star-rating.min.js?w=1" type="text/javascript"></script>
    <!-- <script src="<? echo asset('assets/mobile/library/bootstrap-rating/js') ?>/star-rating_locale_en.js"></script> -->
    <!-- developer javascript --> 
    <script type="text/javascript" src="<? echo asset('assets/mobile/js/angApp.js?w=60') ?>"></script> 
</head>
<body ng-app="freyoApp" ng-controller="freyoController" scroll-body >
    <div ui-yield-to="link-css"></div>
    <!-- Sidebars -->
    <div ng-include="sidebarUrl()" class="sidebar sidebar-left"></div>
    <div class="app">
        <div class="sidebar-in-overlay"></div>
        <!--  ui-swipe-right='Ui.turnOn("uiSidebarLeft")'
         ui-swipe-left='Ui.turnOff("uiSidebarLeft")' > -->
        <div id="freyo-header" ng-class="categoryClass()" class="freyo-header navbar navbar-app navbar-absolute-top header-transparent">
            <!-- <div class="navbar-brand navbar-brand-center" ui-yield-to="title">
                DASHBOARD
            </div> -->
            <div class="btn-group pull-left" >
                <div ui-toggle="uiSidebarLeft" class="btn sidebar-toggle" ui-yield-to="navbarLeft">
                    <!-- <i class="fa fa-bars target" background-check></i> -->
                    <img src="<?php echo asset('assets/mobile/images/freyo-dots-white.png');?>" class="img-responsive f-dots" style="width: 25px">
                </div>
            </div>
            <!-- <div class="btn-group pull-right" ui-yield-to="navbarRight">
                <a href="/search">
                <div class="btn">
                    <i class="fa fa-search"></i>
                </div>
                </a>
            </div> -->
        </div>
        <!-- App body -->
        <div class='app-body'>
            <div ng-show="loadingg" class="app-content-loading">
              <i class="loading-spinner">
                  <div class='uil-default-css'>
                      <div class='thedots topdot'></div>
                      <div class='thedots leftdot'></div>
                      <div class='thedots rightdot'></div>
                  </div>
              </i>
            </div>
            <div class='app-content'>
                <ng-view></ng-view>
            </div>
        </div>
    </div>
    <!-- ~ .app -->
    <!-- Modals and Overlays -->
    <div ui-yield-to="modals"></div>
    <div class="modal signout-dialog-box text-center" ui-if="modal1" ui-state='modal1'>
      <div class="modal-backdrop in"></div>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Sign out on Freyo?</h4>
          </div>
          <div class="modal-body">
            <p>You can always access your content by signing back in.</p>
          </div>
          <div class="modal-footer">
            <a ui-turn-off="modal1" href="#" class="">Cancel</a>
            <a ui-turn-off="modal1" href="#" ng-click="signout()" class="sign-out-now">Sign Out</a>
          </div>
        </div>
      </div>
    </div>
    <div class="close-coachmark" style="display:none;"><button>OK</button></div>
</body>
</html>
