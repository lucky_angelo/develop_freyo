<div id="download-now" class="container-fluid download-now">
	<div class="row text-center">
		<h4>Download Now</h4>
		<p>Don’t miss out on this one of a kind magazine experience. <br class="hidden-xs hidden-md"/></p>		
	</div>
	<div class="row phone-stores">
		<div class="col-xs-12 col-sm-6"><a href="https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8"><img src="{{asset('assets/image/appstoreb.png')}}"></a></div>
		<div class="col-xs-12 col-sm-6"><a href="https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo"><img src="{{asset('assets/image/playstoreb.png')}}"></a></div>
	</div>
</div>