@if(Session::has('flash_message_body'))
    <div class="modal fade" id="custom-modal-notification">
        <div class="modal-dialog">
            <div class="modal-content">
                @if(Session::has('flash_message_title'))
                    <div class="modal-header">
                        @if(Session::has('flash_message_close'))
                            @if(Session::pull('flash_message_close') === false)
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            @endif
                        @endif
                        <h4 class="modal-title">{{ Session::pull('flash_message_title') }}</h4>
                    </div>
                    
                @endif
                <div class="modal-body">
                    <p>{{ Session::pull('flash_message_body') }}</p>
                </div>
            </div>
        </div>
    </div>
@endif