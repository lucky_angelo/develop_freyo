<div id="enjoy-unli" class="container enjoy-unli-access">
	<div class="col-xs-12 col-md-8">
		<h4>Enjoy unlimited access to different magazine wherever you are, <br class="hidden-xs hidden-md hidden-lg"/>whenever you like, at <em style="color: black;">no cost</em>.</h4>

		<h5><img src="{{asset('assets/image/personalize.png')}}">PERSONALIZE YOUR FEED</h5>
		<p>Manage all the magazines on your dashboard, choose on the different categories <br class="hidden-xs hidden-md"/>you want to view. Keep up with the latest issues.</p>

		<h5><img src="{{asset('assets/image/discover.png')}}">DISCOVER AMAZING MAGAZINES</h5>
		<p>Explore and enjoy lots of magazines for FREE on topics ranging from fashion, travel, <br class="hidden-xs hidden-md"/>celebrities, food, weddings, sports, and cars. </p>

		<h5><img src="{{asset('assets/image/heart.png')}}">FOLLOW ALL THE TOPICS YOU DESIRED</h5>
		<p>Keep up in the latest issue of magazine. Stay connected with the stories by just following <br class="hidden-xs hidden-md"/>a specific magazine. </p>

		<h5><img src="{{asset('assets/image/share.png')}}">SHARE YOUR FAVORITE MAGAZINES</h5>
		<p>Share your favorite magazines on Facebook, Twitter, Pinterest, LinkedIn, and other social <br class="hidden-xs hidden-md"/>media sites. Let them discover the digital world of magazines.</p>
	</div>

	<div class="col-xs-12 col-md-4 three-phone-container">
<!-- 		<img class="three-phone-1 img-responsive center-block" src="{{asset('assets/image/a_1.png')}}">
		<img class="three-phone-2 img-responsive center-block" style="display:none;" src="{{asset('assets/image/a_2.png')}}">
		<img class="three-phone-3 img-responsive center-block" style="display:none;" src="{{asset('assets/image/a_3.png')}}"> -->
		<img class="three-phone-3 img-responsive center-block" style="" src="{{asset('assets/image/a_3.png')}}">
	</div>
</div>