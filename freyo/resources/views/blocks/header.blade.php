<div class='side-nav-container text-center'>
	<a class='active' data-scroll-nav='0'>&nbsp;</a>
	<a data-scroll-nav='1'>&nbsp;</a>
	<a data-scroll-nav='2'>&nbsp;</a>
	<a data-scroll-nav='3'>&nbsp;</a>
</div>
<header id="top-header" data-scroll-index="0">
	<div data-0p-start="background-position:50% 95%"  data-50p-start="background-position:50% 0%" class="container-fluid freyo-header">
		<div class="container">
			<div class="row logo-container"><img class="center-block" src="{{ asset('assets/image/logo.png') }}"></div>
			<div class="row text-center">
				<h1>The Digital Magazine Revolution</h1>
				<p>Discover stories that impact your life, career and creative path - <br class="hidden-xs"/>hand-picked by people who share your passions.</p>
			</div>
			<div class="row phone-stores">
				<div class="col-xs-12 col-sm-6"><a href="https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8"><img src="{{ asset('assets/image/appstorew.png') }}"></a></div>
				<div class="col-xs-12 col-sm-6"><a href="https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo"><img src="{{ asset('assets/image/playstorew.png') }}"></a></div>
			</div>
			<div class="row big-phone hidden-xs">
				<img class="img-responsive center-block" src="{{ asset('assets/image/iPhone-Mockup(4-19-16).png') }}">
			</div>
		</div>		
	</div>
</header>