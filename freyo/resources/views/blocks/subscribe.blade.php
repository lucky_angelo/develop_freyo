<div data-0p-top="background-position:50% 95%"  data-50p-top="background-position:50% 0%" id="subscribe-now" class="container-fluid subscribe-now" data-scroll-index="3">
	<div class="container text-center">
		<div class="row">
			<h4>Subscribe Now</h4>
			<p>Sign up to our newsletter to be the first to know <br class="hidden-xs hidden-md"/>about the latest features and updates</p>
		</div>
		<div class="row hidden">
			<form id="subscribe-email" method="post">
				<div class="sub-input-group">
					<input name="sub_email" type="email" placeholder="Enter Email Address here..." required/>
					<img class="btn-submit-form" src="{{asset('assets/image/sub_submit.png')}}">
				</div>				
			</form>
		</div>
		

		<div class="row subscription">
		<?php require_once( base_path()."/../public_html/".'panel/config.php'); ?> 
		<?php require_once( base_path()."/../public_html/".'panel/common/session.php'); ?> 
		<?php require_once( base_path()."/../public_html/".'panel/common/css.php'); ?> 
		<?php require_once( base_path()."/../public_html/".'panel/newsletter/home.newsletter.php'); ?> 
		</div>
		

		<div class="row share-smedia">
			<p>Enjoying Freyo? Share the love.</p>
			<div class="col-xs-12 col-sm-4 sm-fb">
				<a class="btn-facebook fb-btn" href="https://www.facebook.com/freyoapp/"><i class="fa fa-facebook"></i> Like us on Facebook</a>
			</div>
			<div class="col-xs-12 col-sm-4 sm-twit">
				<a class="btn-twitter" href="https://twitter.com/freyoapp"><i class="fa fa-twitter"></i> Follow us on Twitter</a>
			</div>
			<div class="col-xs-12 col-sm-4 sm-insta">
				<a class="btn-instagram instagram-btn" href="https://www.instagram.com/freyoapp"><i class="fa fa-instagram"></i> Follow us on Instagram</a>
			</div>
		</div>
	</div>
</div>
<?php //include_once("m.freyo.com/panel/config.php"); 
//include_once($path["docroot"]."common/session.php"); include_once($path["docroot"]."common/css.php"); ?> 
<!-- base_path()."/../public_html/" -->


<?php //require_once( asset('panel/common/session.php')); ?> 
<?php //require_once( asset('panel/common/css.php')); ?> 


<?php //include("../../../public_html/panel/newsletter/home.newsletter.php"); ?> 

<!-- <iframe src="http://m.freyo.com/newsletter.php" frameborder="0" style="height: 500px;width: 700px;"></iframe> -->

<script>
	function transferEmailNewsletter() {
			var email = document.getElementsByName('email');
			email[0].value = this.value;
		}
		var email_address = document.getElementsByName('sub_email');
		email_address[0].addEventListener('input', transferEmailNewsletter, false);


	var img = document.getElementsByClassName("btn-submit-form");
	img[0].addEventListener("click", function(){
		var news = document.getElementsByName("newsletter_submit_btn");
		news[0].click();
	});
</script>