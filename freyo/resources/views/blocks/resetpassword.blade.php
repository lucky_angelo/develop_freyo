@if(isset($showResetForm) && $showResetForm == "showit")
    <!-- <div class="modal fade" id="resetpass-modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
                <div class="container">
                    <h4>Reset Password</h4>
                <!-- </div> 
                <div class="modal-body"> -->
                    <form role="form" class="passwordreset-form" action="<? echo asset('/passwordreset'); ?>" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <input type="hidden" name="email" value="<?php echo $resetEmail; ?>">
                        <input type="hidden" name="resetkey" value="<?php echo $resetKey; ?>">
                        <div class="form-group">
                            <label for="password">Password (Minimum of 6 characters)</label>
                            <input id="password" type="password" name="password" class="validate[required,minSize[6]] form-control" required data-errormessage-value-missing="Password is required!" data-errormessage-range-underflow="Minimum of 6 characters!" data-prompt-position="topLeft">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirm Password</label>
                            <input id="confirm_password" class="validate[required, equals[password]] form-control" type="password" name="confirm_password" required data-errormessage-value-missing="Confirm your password!" data-errormessage-pattern-mismatch="Does not match Password!" data-prompt-position="topLeft"/>
                        </div>
                        <button type="submit" class="btn_reset_password btn btn-primary text-center">Change Password</button>
                    </form>
                </div>
               <!--  </div>
            </div>
        </div>
    </div> -->
@endif