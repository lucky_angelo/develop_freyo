<div id="screenshot-gallery" class="container-fluid screenshot-gallery hide">

	<div class="row text-center">

		<h3>The screenshot gallery</h3>

		<p>Take a look at some of the screens Freyo has to offer.</p>

	</div>

	<div class="row screenshot-container">

		<div class="col-xs-10 col-xs-offset-1 two-screenshot">

			<img class="img-responsive ss-left btn-toggle-ss hidden-xs" src="{{asset('assets/image/ss-left.png')}}">

			<img class="img-responsive ss-right btn-toggle-ss hidden-xs" src="{{asset('assets/image/ss-right.png')}}">

			



			<img class="two-screenshot-1 img-responsive center-block hidden-xs" src="{{asset('assets/image/c_1.png')}}">

			<img style="display:none;" class="two-screenshot-2 img-responsive center-block hidden-xs" src="{{asset('assets/image/c_2.png')}}">





			<img class="img-responsive ss-left btn-toggle-six-ss" src="{{asset('assets/image/ss-left.png')}}">

			<img class="img-responsive ss-right btn-toggle-six-ss" src="{{asset('assets/image/ss-right.png')}}">



			<img class="img-responsive center-block six-ss ss-1" src="{{asset('assets/image/ss_1.png')}}">

			<img style="display:none;" class="img-responsive center-block six-ss ss-2" src="{{asset('assets/image/ss_2.png')}}">

			<img style="display:none;" class="img-responsive center-block six-ss ss-3" src="{{asset('assets/image/ss_3.png')}}">

			<img style="display:none;" class="img-responsive center-block six-ss ss-4" src="{{asset('assets/image/ss_4.png')}}">

			<img style="display:none;" class="img-responsive center-block six-ss ss-5" src="{{asset('assets/image/ss_5.png')}}">

			<img style="display:none;" class="img-responsive center-block six-ss ss-6" src="{{asset('assets/image/ss_6.png')}}">

		</div>

	</div>

</div>