<?
echo "";
$thisfile=$oSystem->getValue("news_pageurl");
$monthArr = array($lang['newspublish']['january'], $lang['newspublish']['february'], $lang['newspublish']['march'], $lang['newspublish']['april'], $lang['newspublish']['may'], $lang['newspublish']['june'], $lang['newspublish']['july'], $lang['newspublish']['august'], $lang['newspublish']['september'], $lang['newspublish']['october'], $lang['newspublish']['november'], $lang['newspublish']['december']);
$sefurl = $oSystem->getValue("news_sefurl");

if ($searchby==""){$searchby="bykey";}
	echo "
	<table border=0 cellpadding=3 cellspacing=0 width=100%>
	<tr><td valign=top><b>".$lang['newspublish']['searchnews']."</b><br><hr class=hrline></td></tr>
	<td valign=top>
		<table border=0 cellpadding=3 cellspacing=0 width=100%>
			<form name=frmSearch action=\"".$thisfile."\" method=post>
			<input type=hidden name=pageaction value=\"searchnews\">
				<tr><td><input type=radio name=searchby value=\"bykey\" ".($searchby=="bykey"?"checked":"").">&nbsp;".$lang['newspublish']['enterkeyword']."<br><input type=text name=searchkey style=\"width:150px\" value=\"".($searchby=="bykey"?stripslashes(htmlentities($searchkey)):"")."\"></td></tr>
				<tr><td><input type=radio name=searchby value=\"bydate\" ".($searchby=="bydate"?"checked":"").">&nbsp;".$lang['newspublish']['enterdatepost']."<br><input type=text name=datesearch style=\"width:150px\" value=\"".($searchby=="bydate"?stripslashes(htmlentities($datesearch)):"")."\"><br>(dd-mm-yyyy)</td></tr>
				<tr><td><input type=button value=\" ".$lang['newspublish']['go']." \" onclick=document.frmSearch.submit();></td></tr>
			</form>
		</table><br>
	</td></tr></table>
	";
	
/***** RSS News Block *****/
if($oSystem->getValue("news_blockrssnews")=="Yes"){
	echo "		
	<table border=0 cellpadding=3 cellspacing=0 width=100%>
	<tr><td><b>".$lang['newspublish']['readnews']."</b><br><hr class=hrline></td></tr>
	<tr><td><a href=\"$thisfile?rss=read\" class=menu-link>".$lang['newspublish']['rssreader']."</a></td></tr>
	</table>
	<br />
	";
}

/***** Categories Block *****/
if($oSystem->getValue("news_blockcat")=="Yes"){
	
	echo "
	<table border=0 cellpadding=3 cellspacing=0 width=100%>
	<tr><td><b>".$lang['newspublish']['menucategory']."</b><br><hr class=hrline></td></tr>
	<tr><td valign=top>
		<table border=0 cellpadding=3 cellspacing=2 width=100%>
	";

	if($parent_id==""){ $parent_id=0; }
	
	echo "<tr><td>";
	echo $oSystem->getValue("news_rss_enable")=="Yes"?"<a href='".$path['webroot']."_files/rss/news/news.xml' class=link><img src=\"image/rss.gif\" border=0></a>":"";
	echo "<a href=\"".$thisfile."\" class=menu-link>".$lang['newspublish']['all']."</a></td></tr>";

	$oNews_Category->data = array("category_id","parent_id","name","descp");
	$oNews_Category->where = "parent_id='0'";
	$oNews_Category->order = "name";		
	$result = $oNews_Category->getList();
	if(mysql_num_rows($result)!=0){	
		while($myrow=mysql_fetch_row($result)){
			$oNews_Category->where = "parent_id=$myrow[0]";
			$myrow[2] = stripslashes($myrow[2]);
			if ($oSystem->getValue("news_showcatdescp")=="Yes" && !empty($myrow[3])){ $myrow[3]="<br><font style=\"font-size:".$oSystem->getValue("news_textsmall")."px\">".stripslashes(nl2br($myrow[3]))."</font><br>"; } else { $myrow[3]=""; }
			echo "<tr><td valign=top>";
			echo $oSystem->getValue("news_rss_enable")=="Yes"?"<a href='".$path['webroot']."_files/rss/news/news_".str_replace(" ", "_", $myrow[2]).".xml' class=link><img src=\"image/rss.gif\" border=0></a>":"";
			if ($sefurl == "Yes"){
				echo "<a href=\"cat-$myrow[0]-$myrow[0].html\" class=menu-link>$myrow[2]</a>$myrow[3]</td></tr>";
			}else{
				echo "<a href=\"".$thisfile."?category_id=$myrow[0]&parent_id=$myrow[0]\" class=menu-link>$myrow[2]</a>$myrow[3]</td></tr>";
			}
		}
		mysql_free_result($result);	
	}else{
		echo "<tr><td>".$lang['newspublish']['nocategories']."</td></tr>";
	}

	echo "
		</table>
	</td></tr>
	</table>
	
	";
}


/***** Archives Block *****/
if($oSystem->getValue("news_blockarch")=="Yes"){

	echo "
	<table border=0 cellpadding=3 cellspacing=0 width=100%>
	<tr><td><br><b>".$lang['newspublish']['menuarchive']."</b><br><hr class=hrline></td></tr>
	<tr><td valign=top>
		<table border=0 cellpadding=3 cellspacing=2 width=100%>
	";

	$localtz = $oSystem->getValue("sys_timezone");
	$localtime = $localtz==""?time():getLocalTime($localtz);
	
	$year = date("Y",$localtime); 
	$month = date("m",$localtime); 
	$day = date("d",$localtime); 
	$currentdate = $year."-".$month."-".$day;
	
	$oNews->data = array("distinct date_format(datepost,'%m %Y')","date_format(datepost,'%Y')","date_format(datepost,'%c')");
	$oNews->where = "newstype='news' and display='Yes' and (dateexpire='0000-00-00' or dateexpire>date_format('$currentdate', '%Y-%m-%d')) and datepost<=date_format('$currentdate', '%Y-%m-%d') and datepost<>'0000-00-00'";	
	$oNews->order = "datepost desc";
	$result = $oNews->getList();
	if(mysql_num_rows($result)!=0){
		while($myrow=mysql_fetch_row($result)){
			$month = substr($myrow[0],0,2)-1;
			$year = substr($myrow[0],3,4);
			if ($sefurl == "Yes"){
				echo "<tr><td><a href=\"arc-$myrow[1]-$myrow[2].html\" class=menu-link>$monthArr[$month] $year</a></td></tr>";
			}else{
				echo "<tr><td><a href=\"".$thisfile."?arcyear=$myrow[1]&arcmonth=$myrow[2]\" class=menu-link>$monthArr[$month] $year</a></td></tr>";
			}			
		}
		mysql_free_result($result);
	}else{
		echo "<tr><td>".$lang['newspublish']['noarchives']."</td></tr>";
	}
	
	echo "
		</table>
	</td></tr>
	</table>
	<br>
	";
}

?>

