<?

if ($oSystem->getValue("news_pageurl") == "")
	$oSystem->setValue("news_pageurl", $path["webroot"]."newspublish/samplenewspublish.php");
	
if ($oSystem->getValue("news_rss_live") == "")
	$oSystem->setValue("news_rss_live", "1");

if($pageaction=="save"){
	$oSystem->setValue("news_pageurl",$frontendurl);
	$oSystem->setValue("news_sefurl",$sefurl);
	$oSystem->setValue("news_showemail",$news_showemail); 
	$oSystem->setValue("news_showprint",$news_showprint); 
	$oSystem->setValue("news_listno",$news_listno);
	$oSystem->setValue("news_newsno",$news_newsno);
	$oSystem->setValue("news_seq",$news_seq);
	$oSystem->setValue("news_blockcat",$news_blockcat);	
	$oSystem->setValue("news_blockarch",$news_blockarch);
	$oSystem->setValue("news_blockrssnews",$news_blockrssnews);		
	$oSystem->setValue("news_tellsubject",$news_tellsubject);
	$oSystem->setValue("news_tellmessage",$news_tellmessage);	 
	$oSystem->setValue("news_titlefont",$news_titlefont);
	$oSystem->setValue("news_titlecolor",$news_titlecolor);
	$oSystem->setValue("news_titlesize",$news_titlesize);
	$oSystem->setValue("news_titlebold",$news_titlebold);
	$oSystem->setValue("news_titleitalic",$news_titleitalic);
	$oSystem->setValue("news_titleunderline",$news_titleunderline);
	$oSystem->setValue("news_titlelink",$news_titlelink);
	$oSystem->setValue("news_titlehover",$news_titlehover);
	$oSystem->setValue("news_textfont",$news_textfont);
	$oSystem->setValue("news_textcolor",$news_textcolor);
	$oSystem->setValue("news_textsize",$news_textsize);
	$oSystem->setValue("news_textsmall",$news_textsmall);
	$oSystem->setValue("news_rss_enable", $news_rss_enable);
	$oSystem->setValue("news_rss_title", $news_rss_title);
	$oSystem->setValue("news_rss_description", $news_rss_description);
	if ($news_rss_logo!=""){
		$ext = strtolower(strrchr($_FILES['news_rss_logo']['name'],"."));
    	if($ext==".jpg" || $ext==".png" || $ext==".gif" || $ext==".jpeg"){
			if(($_FILES['news_rss_logo']['size']<$upload_max || empty($upload_max)) && !empty($_FILES['news_rss_logo']['size'])){
				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
					$location=$path["docroot"]."_files/rss/news/";
					
					if(!file_exists($location)){
						@mkdir ($location, 0777);
						@chmod ($location, 0777);
					}
					
					$oldlogo = $oSystem->getValue("news_rss_logo");
					if (file_exists($path["docroot"]."_files/rss/news/$oldlogo") && $oldlogo!=""){	
						unlink($path["docroot"]."_files/rss/news/$oldlogo");
					}
				  	$news_rss_logo_name = strtolower(str_replace(" ","_",trim($_FILES['news_rss_logo']['name'])));
				  		  		  	
					if($news_rss_logo_name!=""){ 
						$news_rss_logo_name = stripslashes($news_rss_logo_name);
						
						$oSystem->uploadFile($_FILES['news_rss_logo']['tmp_name'],$path["docroot"]."_files/rss/news",$news_rss_logo_name); 
						@chmod ($location, 0666);
						$oSystem->setValue("news_rss_logo", $news_rss_logo_name);
					}
				}
			}else{
				$status_message = "<b>".$lang['news']['status']." :</b> ".$lang['news']['warnexceed']."<br>";
			}
		}else{
			$status_message = "<b>".$lang['news']['status']." :</b> ".$lang['news']['imgallowed']."<br>";
		}
	}
	$oSystem->setValue("news_rss_copyright", $news_rss_copyright);
	$oSystem->setValue("news_rss_editor", $news_rss_editor);
	$oSystem->setValue("news_rss_language", $news_rss_language);
	$oSystem->setValue("news_rss_live", $news_rss_live);
	$oSystem->setValue("news_disstyle", $news_disstyle);
	$oSystem->setValue("news_showpubdate", $news_showpubdate);
	$oSystem->setValue("news_showsummary", $news_showsummary);
	$oSystem->setValue("news_showsummaryart", $news_showsummaryart);
	$oSystem->setValue("news_subcatno", $news_subcatno);
	$oSystem->setValue("news_showcatdescp", $news_showcatdescp);
	$oSystem->setValue("news_dateformat", $news_dateformat);
  
  	$status_message = "<b>". $lang['newspublish']['status'] ." :</b> ".$lang['newspublish']['statusprefersave']."<br>";	  
}

if($pageaction=="default"){
  	$oSystem->setValue("news_pageurl",$path["webroot"]."newspublish/samplenewspublish.php");
  	$oSystem->setValue("news_sefurl","");
  	$oSystem->setValue("news_showemail","Yes"); 
	$oSystem->setValue("news_showprint","Yes"); 
  	$oSystem->setValue("news_listno",20);
  	$oSystem->setValue("news_newsno",5);
	$oSystem->setValue("news_seq","desc");
  	$oSystem->setValue("news_blockcat","Yes");	  
  	$oSystem->setValue("news_blockarch","Yes");
  	$oSystem->setValue("news_blockrssnews","Yes");	  
  	$oSystem->setValue("news_tellsubject","Invitation From Your Friend");
  	$oSystem->setValue("news_tellmessage","Dear [[name]],\r\n\r\nYour friend, [[yourname]] has invited to you to read the following news article.\r\n\r\n[[newsurl]]\r\n\r\nYour Friend\'s Message:\r\n[[message]]\r\n\r\n\r\nThank you,\r\nYour Name\r\n");	
  	$oSystem->setValue("news_titlefont","Arial");
  	$oSystem->setValue("news_titlecolor","#000033");
  	$oSystem->setValue("news_titlesize","17");
  	$oSystem->setValue("news_titlebold","bold");
  	$oSystem->setValue("news_titleitalic","");
  	$oSystem->setValue("news_titleunderline","");
  	$oSystem->setValue("news_titlelink","#0033CC");
  	$oSystem->setValue("news_titlehover","#9999CC");
  	$oSystem->setValue("news_textfont","Arial");
  	$oSystem->setValue("news_textcolor","#000000");
  	$oSystem->setValue("news_textsize","12");
 	$oSystem->setValue("news_textsmall","11");
  	$oSystem->setValue("news_rss_enable", "");
	$oSystem->setValue("news_rss_title", "News");
	$oSystem->setValue("news_rss_description", "News");
	$oldlogo = $oSystem->getValue("news_rss_logo");
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if (file_exists($path["docroot"]."_files/rss/news/$oldlogo") && $oldlogo!=""){
			$location=$path["docroot"]."_files/rss/news/";
			if(!file_exists($location)){
				@mkdir ($location, 0777);
				@chmod ($location, 0777);
			}	
			unlink($path["docroot"]."_files/rss/news/$oldlogo");
			@chmod ($location, 0666);
		}
	}
	$oSystem->setValue("news_rss_logo", "");
	$oSystem->setValue("news_rss_copyright", "");
	$oSystem->setValue("news_rss_editor", "");
	$oSystem->setValue("news_rss_language", "en-us");
	$oSystem->setValue("news_rss_live", "1");
	$oSystem->setValue("news_disstyle", "Same");
	$oSystem->setValue("news_showpubdate", "Yes");
	$oSystem->setValue("news_showsummary", "Yes");
	$oSystem->setValue("news_showsummaryart", "Yes");	
	$oSystem->setValue("news_subcatno", "3");
	$oSystem->setValue("news_showcatdescp", "Yes");
	$oSystem->setValue("news_dateformat", "d-m-Y");

  	$status_message = "<b>". $lang['newspublish']['status'] ." :</b> ".$lang['newspublish']['statuspreferdefault']."<br>";	  
}

if($pageaction=="remove"){
	$oldlogo = $oSystem->getValue("news_rss_logo");
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if (file_exists($path["docroot"]."_files/rss/news/$oldlogo") && $oldlogo!=""){
			$location=$path["docroot"]."_files/rss/news/";
			if(!file_exists($location)){
				@mkdir ($location, 0777);
				@chmod ($location, 0777);
			}	
			unlink($path["docroot"]."_files/rss/news/$oldlogo");
			@chmod ($location, 0666);		
		}
		$oSystem->setValue("news_rss_logo", "");
	}
}

$fontnames = array("Arial", "Courier", "Sans Serif", "Tahoma", "Verdana", "Wingdings");

echo"<table border=0 width=100%><tr><td><b>".$lang['newspublish']['newspublisher']."</b></td><td>";
include("wce.menu.php");
echo"
</td></tr></table>
<hr size=1 color=#606060>$status_message<br>
<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=\"index.php?component=$component&page=$page\" method=post enctype=\"multipart/form-data\">
<input type=hidden name=pageaction>
<tr><td valign=top>
	<table border=0 cellpadding=1 width=100%>
	<tr><td colspan=2><b><u>".$lang['newspublish']['adminsetting']."</u></b><br><br></td></tr>
	<tr><td width=35%>".$lang['newspublish']['listno']."</td><td><input type=text name=\"news_listno\" style=\"width:35px\" value=\"".$oSystem->getValue("news_listno")."\"></td></tr>
	<tr><td colspan=2><br><b><u>".$lang['newspublish']['frontendsetting']."</u></b><br><br></td></tr>
	<tr><td>".$lang['newspublish']['showpubdate']."</td><td><input type=checkbox name=news_showpubdate value=\"Yes\" ".($oSystem->getValue("news_showpubdate")=="Yes"?"checked":"")."></td></tr>
	<tr><td>".$lang['newspublish']['showsummary']."</td><td><input type=checkbox name=news_showsummary value=\"Yes\" ".($oSystem->getValue("news_showsummary")=="Yes"?"checked":"")."></td></tr>
	<tr><td>".$lang['newspublish']['discatdescp']."</td><td><input type=checkbox name=news_showcatdescp value=\"Yes\" ".($oSystem->getValue("news_showcatdescp")=="Yes"?"checked":"")."></td></tr>
	<tr><td>".$lang['newspublish']['showsummaryart']."</td><td><input type=checkbox name=news_showsummaryart value=\"Yes\" ".($oSystem->getValue("news_showsummaryart")=="Yes"?"checked":"")."></td></tr>
	<tr><td>".$lang['newspublish']['showprint']."</td><td><input type=checkbox name=news_showprint value=\"Yes\" ".($oSystem->getValue("news_showprint")=="Yes"?"checked":"")."></td></tr>
	<tr><td>".$lang['newspublish']['newsno']."</td><td><input type=text name=\"news_newsno\" style=\"width:35px\" value=\"".$oSystem->getValue("news_newsno")."\"></td></tr>
	<tr><td>".$lang['newspublish']['subcatno']."</td><td><input type=text name=\"news_subcatno\" style=\"width:35px\" value=\"".$oSystem->getValue("news_subcatno")."\"></td></tr>
	<tr><td>".$lang['newspublish']['sortnews']."</td><td valign=top>
		<select name=news_seq><option value=\"asc\"".($oSystem->getValue("news_seq")=="asc"?" selected":"").">".$lang['newspublish']['sortnewsasc']."</option><option value=\"desc\"".($oSystem->getValue("news_seq")=="desc"?" selected":"").">".$lang['newspublish']['sortnewsdesc']."</option></select>
	</td></tr>
	<tr><td>".$lang['newspublish']['displaystyle']."</td><td>
		<select name=news_disstyle>
		<option value=\"Same\" ".($oSystem->getValue("news_disstyle")=="Same"?"selected":"").">".$lang['newspublish']['samewindow']."</option>
		<option value=\"Pop\" ".($oSystem->getValue("news_disstyle")=="Pop"?"selected":"").">".$lang['newspublish']['popupwindow']."</option>
		</select>
	</td></tr>
	<tr><td>".$lang['newspublish']['dateformat']."</td><td>
		<select name=news_dateformat>
			<option value=\"Y-m-d\" ".($oSystem->getValue("news_dateformat")=="Y-m-d"?"selected":"").">2005-01-28</option>
			<option value=\"m-d-Y\" ".($oSystem->getValue("news_dateformat")=="m-d-Y"?"selected":"").">01-28-2005</option>
			<option value=\"d-m-Y\" ".($oSystem->getValue("news_dateformat")=="d-m-Y"?"selected":"").">28-01-2005</option>
			<option value=\"dbY\" ".($oSystem->getValue("news_dateformat")=="dbY"?"selected":"").">28 Jan 2005</option>
			<option value=\"bdY\" ".($oSystem->getValue("news_dateformat")=="bdY"?"selected":"").">Jan 28, 2005</option>
			<option value=\"dMY\" ".($oSystem->getValue("news_dateformat")=="dMY"?"selected":"").">28 January 2005</option>
			<option value=\"MdY\" ".($oSystem->getValue("news_dateformat")=="MdY"?"selected":"").">January 28, 2005</option>
		</select>
	</td></tr>
	<tr><td>".$lang['newspublish']['menublock']."</td><td>
		<table cellpadding=0 cellspacing=0 width=80%>
		<tr><td><input type=checkbox name=news_blockcat value=\"Yes\" ".($oSystem->getValue("news_blockcat")=="Yes"?"checked":"").">".$lang['newspublish']['menucategory']."</td>
			<td><input type=checkbox name=news_blockarch value=\"Yes\" ".($oSystem->getValue("news_blockarch")=="Yes"?"checked":"").">".$lang['newspublish']['menuarchive']."</td>
			<td><input type=checkbox name=news_blockrssnews value=\"Yes\" ".($oSystem->getValue("news_blockrssnews")=="Yes"?"checked":"").">".$lang['newspublish']['menurssnews']."</td>
			</tr>
		</table>
	</td></tr>	
	<tr><td>".$lang['newspublish']['frontendurl']."</td><td><input type=text name=frontendurl style=\"width:360px\" value=\"".$oSystem->getValue("news_pageurl")."\"></td></tr>
	<tr><td>".$lang['newspublish']['sefurl']."</td><td><input type=checkbox name=sefurl value=\"Yes\" onclick=javascript:showcode(this); ".($oSystem->getValue("news_sefurl")=="Yes"?"checked":"")."> * ".$lang['newspublish']['sefurlnote']."</td></tr>
";
		$pattern="/\/(.[^\/]*\.php)/";
		preg_match($pattern, $oSystem->getValue("news_pageurl"), $feurl);
		$rewritecode = "Options +FollowSymLinks\n"
			."RewriteEngine on\n"
			."RewriteRule ^news-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?.html\$ $feurl[1]?news_id=\$1&start=\$2&category_id=\$3&parent_id=\$4&arcyear=\$5&arcmonth=\$6\n"
			."RewriteRule ^cat-(.[^\/]*)-(.[^\/]*).html\$ $feurl[1]?category_id=\$1&parent_id=\$2\n"
			."RewriteRule ^arc-(.[^\/]*)-(.[^\/]*).html\$ $feurl[1]?arcyear=\$1&arcmonth=\$2\n"
			."RewriteRule ^navigate-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?.html\$ $feurl[1]?start=\$1&category_id=\$2&parent_id=\$3&arcyear=\$4&arcmonth=\$5\n"
			."RewriteRule ^search-(.[^\/]*)-(.[^\/]*).html\$ $feurl[1]?pageaction=\$1&start=\$2&searchby=\$3&searchkey=\$4&datesearch=\$5\n"
			."RewriteRule ^searchnews-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?.html\$ $feurl[1]?news_id=\$1&start=\$2&category_id=\$3&parent_id=\$4&pageaction2=\$5&searchby=\$6&searchkey=\$7&datesearch=\$8\n"
			;	
	echo "<tr><td></td><td><div id=rewritecode ";
	echo $oSystem->getValue("news_sefurl")=="Yes"?"":"style=\"display:none\"";
	echo ">".$lang['newspublish']['htaccess']."<br /><textarea id=rulearea style=\"width:360px;height:150px\" readonly>$rewritecode</textarea></div></td></tr>
	</table><br>	

</td></tr>
<tr><td valign=top>

	&nbsp;<b><u>".$lang['newspublish']['definetellfriend']."</u></b><br><br>
	<table border=0 width=100%>
	<tr><td>".$lang['newspublish']['showemail']."</td><td><input type=checkbox name=news_showemail value=\"Yes\" ".($oSystem->getValue("news_showemail")=="Yes"?"checked":"")."></td></tr>
	<tr><td valign=top width=25%>".$lang['newspublish']['subject']."</td><td><input type=text name=\"news_tellsubject\" style=\"width:400px\" value=\"".stripslashes(htmlentities($oSystem->getValue("news_tellsubject")))."\"></td></tr>
	<tr><td valign=top>".$lang['newspublish']['message']."</td><td><textarea name=\"news_tellmessage\" cols=50 rows=10 style=\"width:400px\">".stripslashes(htmlentities($oSystem->getValue("news_tellmessage")))."</textarea></td></tr>
	</table><br>
</td></tr>
<tr><td>
	&nbsp;<b><u>".$lang['newspublish']['fontsncolor']."</u></b><br><br>
	<table border=0 width=100%>
	<tr><td colspan=8><b>".$lang['newspublish']['newsannouncetitle']."</b></td></tr>
	<tr><td>".$lang['newspublish']['fontface'].":</td><td><select name=news_titlefont>
";
	
	for($i=0; $i<count($fontnames); $i++){	echo "<option value=\"$fontnames[$i]\" " . ($fontnames[$i]==$oSystem->getValue("news_titlefont")?"selected":"") . ">$fontnames[$i]</option>";	}
	
echo"
	</select></td>
		<td>".$lang['newspublish']['color'].":</td><td><input type=text name=news_titlecolor size=8 value=\"".$oSystem->getValue("news_titlecolor")."\">&nbsp;&nbsp;<span id=news_titlecolor style=\"width:15px;height:15px;background-color:".$oSystem->getValue("news_titlecolor")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.news_titlecolor, news_titlecolor);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=absmiddle></a></td>
		<td>".$lang['newspublish']['size'].":</td><td><input type=text name=news_titlesize size=4 value=\"".$oSystem->getValue("news_titlesize")."\"> px </td>
		<td colspan=2><input type=checkbox name=news_titlebold value=\"bold\" ".($oSystem->getValue("news_titlebold")=="bold"?"checked":"").">B 
		<input type=checkbox name=news_titleitalic value=\"italic\" ".($oSystem->getValue("news_titleitalic")=="italic"?"checked":"").">I 
		<input type=checkbox name=news_titleunderline value=\"underline\" ".($oSystem->getValue("news_titleunderline")=="underline"?"checked":"").">U
		</td></tr>
	<tr><td>".$lang['newspublish']['link'].":</td><td><input type=text name=news_titlelink size=8 value=\"".$oSystem->getValue("news_titlelink")."\">&nbsp;&nbsp;<span id=news_titlelink style=\"width:15px;height:15px;background-color:".$oSystem->getValue("news_titlelink")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.news_titlelink, news_titlelink);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=absmiddle></a></td>
		<td>".$lang['newspublish']['hover'].":</td><td colspan=5><input type=text name=news_titlehover size=8 value=\"".$oSystem->getValue("news_titlehover")."\">&nbsp;&nbsp;<span id=news_titlehover style=\"width:15px;height:15px;background-color:".$oSystem->getValue("news_titlehover")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.news_titlehover, news_titlehover);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=absmiddle></a></td></tr>
	<tr><td colspan=8><br><b>".$lang['newspublish']['newsannouncesmall']."</b></td></tr>
	<tr><td>".$lang['newspublish']['fontface'].":</td><td><select name=news_textfont>
";

	for($i=0; $i<count($fontnames); $i++){	echo "<option value=\"$fontnames[$i]\" " . ($fontnames[$i]==$oSystem->getValue("news_textfont")?"selected":"") . ">$fontnames[$i]</option>";	}

echo"	
	</select></td>
		<td>".$lang['newspublish']['color'].":</td><td><input type=text name=news_textcolor size=8 value=\"".$oSystem->getValue("news_textcolor")."\">&nbsp;&nbsp;<span id=news_textcolor style=\"width:15px;height:15px;background-color:".$oSystem->getValue("news_textcolor")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.news_textcolor, news_textcolor);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=absmiddle></a></td>
		<td>".$lang['newspublish']['small'].":</td><td><input type=text name=news_textsmall size=4 value=\"".$oSystem->getValue("news_textsmall")."\"> px </td></tr>
	</table><br>
	</td></tr>
	<tr><td>
	&nbsp;<b><u>".$lang['newspublish']['rsssetting']."</u></b><br><br>	
	<table border=0 width=100%>
	<tr><td valign=top>".$lang['newspublish']['rssenable']."</td><td><input type=checkbox name=news_rss_enable value=\"Yes\" ".($oSystem->getValue("news_rss_enable")=="Yes"?"checked":"")."></td></tr>
	<tr><td valign='top'>".$lang['newspublish']['rsstitle']."</td><td><input type=text name=news_rss_title value=\"".$oSystem->getValue("news_rss_title")."\" style=\"width:300px\"></td>
	<tr><td valign='top'>".$lang['newspublish']['rssdescription']."</td><td><input type=text name=news_rss_description value=\"".$oSystem->getValue("news_rss_description")."\" style=\"width:300px\"></td>
	<tr><td valign='top'>".$lang['newspublish']['rsssitelogo']."</td>
";
		echo (!empty($upload_max)?"<input type=hidden name=MAX_FILE_SIZE value=$upload_max>":"");
		echo "<td><input type=\"file\" name=\"news_rss_logo\" size=\"40\">&nbsp;&nbsp;<a href=\"".$path['webroot']."_files/rss/news/".$oSystem->getValue("news_rss_logo")."\">".$oSystem->getValue("news_rss_logo")."</a>";
		if ($oSystem->getValue("news_rss_logo") !="")
			echo "&nbsp;&nbsp;<a href=\"index.php?component=$component&page=$page&pageaction=remove\">".$lang['newspublish']['remove']."</a>";
		echo "</td></tr>";

echo"
	<tr><td valign=\"top\">".$lang['newspublish']['rsscopyright']."</td><td><input type=text name=news_rss_copyright value=\"".htmlentities($oSystem->getValue("news_rss_copyright"))."\" style=\"width:300px\"></td>
	<tr><td valign=\"top\">".$lang['newspublish']['rsseditor']."</td><td><input type=text name=news_rss_editor value=\"".$oSystem->getValue("news_rss_editor")."\" style=\"width:300px\"></td>
	<tr><td valign=\"top\">".$lang['newspublish']['rsslanguage']."</td><td>
	<select name=news_rss_language style=\"width:100px\">
		<option value=\"en-us\" ".($oSystem->getValue("news_rss_language")=="en-us"?"selected":"").">English
		<option value=\"de-de\" ".($oSystem->getValue("news_rss_language")=="de-de"?"selected":"").">German
		<option value=\"el\" ".($oSystem->getValue("news_rss_language")=="el"?"selected":"").">Greek
		<option value=\"en\" ".($oSystem->getValue("news_rss_language")=="en"?"selected":"").">Malay
		<option value=\"ro\" ".($oSystem->getValue("news_rss_language")=="ro"?"selected":"").">Roman
	</select>
	</td>
	<tr><td valign=\"top\">".$lang['newspublish']['rsslive']."</td><td><input type=text name=news_rss_live value=\"".$oSystem->getValue("news_rss_live")."\" onkeypress=\"return onlyNumber(event);\" style=\"width:100px\" maxlength=2>&nbsp;".$lang['newspublish']['day']."</td>
	</table>
	<br>
</td></tr>
<tr><td colspan=2><br>
	<input type=button value=\"".$lang['newspublish']['save']."\" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
	<input type=button value=\"".$lang['newspublish']['default']."\" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">
</td></tr>
</form></table>
<br>
";

?>

<script language=JavaScript src="common/colorpick/picker.js"></script>
<script type="text/javascript">
function onlyNumber(e)
{
	var keynum;
	var keychar;
	var numcheck;
	if (e.keyCode != 37 && e.keyCode != 38 &&  e.keyCode != 39 && e.keyCode != 40 && e.keyCode != 9 && e.keyCode != 13 && e.keyCode != 8 && e.keyCode != 46){
		if(window.event){ // IE
			keynum = e.keyCode;
		}
		else if(e.which){ // Netscape/Firefox/Opera		
			keynum = e.which;
		}
		keychar = String.fromCharCode(keynum);
		numcheck = /\d/;
		return numcheck.test(keychar);
	}
}
function showcode(x){
	if (x.checked == true){
		document.getElementById('rewritecode').style.display = "";
		document.getElementById('rulearea').select();
	}
	else
		document.getElementById('rewritecode').style.display = "none";
}
</script>