<? include("../config.php"); if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) { include_once($path["docroot"]."common/css.php"); } ?>

<html>
<head><title><? echo $lang['newspublish']['newspublisher'] ?></title></head>
<body onLoad="this.focus()">

<?
	$monthArr = array($lang['newspublish']['january'], $lang['newspublish']['february'], $lang['newspublish']['march'], $lang['newspublish']['april'], $lang['newspublish']['may'], $lang['newspublish']['june'], $lang['newspublish']['july'], $lang['newspublish']['august'], $lang['newspublish']['september'], $lang['newspublish']['october'], $lang['newspublish']['november'], $lang['newspublish']['december']);

	echo "
	<style type=\"text/css\">
		.title{	font-family: ".$oSystem->getValue("news_titlefont")."; color: ".$oSystem->getValue("news_titlecolor")."; font-size: ".$oSystem->getValue("news_titlesize")."; font-weight: ".$oSystem->getValue("news_titlebold")."; font-style: ".$oSystem->getValue("news_titleitalic")."; text-decoration: ".$oSystem->getValue("news_titleunderline")."; }
		.textsmall{	font-family: ".$oSystem->getValue("news_textfont")."; color: ".$oSystem->getValue("news_textcolor")."; font-size: ".$oSystem->getValue("news_textsmall")."; }
		a.titlelink:link { color: ".$oSystem->getValue("news_titlelink").";  }
		a.titlelink:active { color: ".$oSystem->getValue("news_titlelink").";  }
		a.titlelink:visited { color: ".$oSystem->getValue("news_titlelink").";  }
		a.titlelink:hover { color: ".$oSystem->getValue("news_titlehover").";  }
	</style>
	
	<table border=0 width=98% align=center>
	<tr><td align=right>
	";
	
	if ($oSystem->getValue("news_dateformat")=="m-d-Y"){	
		$dateformat="%m-%d-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="d-m-Y"){	
		$dateformat="%d-%m-%Y";
	} else if ($oSystem->getValue("news_dateformat")=="dbY"){	
		$dateformat="%d %b %Y";
	} else if ($oSystem->getValue("news_dateformat")=="bdY"){	
		$dateformat="%b %d, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="dMY"){	
		$dateformat="%d %M, %Y";
	} else if ($oSystem->getValue("news_dateformat")=="MdY"){	
		$dateformat="%M %d, %Y";
	} else {
		$dateformat="%Y-%m-%d";
	}
	
	echo"
		<input type=button value=\" ".$lang['newspublish']['print']." \" onclick=\"window.print();return false;\"> 
		<input type=button value=\" ".$lang['newspublish']['close']." \" onclick=\"window.close()\">
	</td></tr>
	</table>
	<hr size=1 color=#000000 width=98%>
	";


	$oNews->data = array("title", "content", "date_format(datepost,' %d, %Y')", "date_format(datepost,'%m')","date_format(datepost,'$dateformat')","datepost","summary");
	$result = $oNews->getDetail($news_id);
	if($myrow=mysql_fetch_row($result)){
		$title = stripslashes($myrow[0]);
		$content = stripslashes($myrow[1]);
		if ($oSystem->getValue("news_showpubdate")=="Yes" && (!(empty($myrow[5]) || $myrow[5]=="0000-00-00"))){ $posted = "".$lang['newspublish']['postedon']."&nbsp;".$myrow[4]; } else { $posted ="&nbsp;"; }
		$sum = stripslashes($myrow[6]);
	}
	mysql_free_result($result);
			
	echo "
	<table border=0 cellpadding=5 cellspacing=0 width=98% align=center>
	<tr><td class=title>$title</td></tr>
	<tr><td class=textsmall>$posted</td></tr>
	<tr><td>".($oSystem->getValue("news_showsummaryart")=="Yes"?"<br>$sum<br>":"")."<br>$content</td></tr>	
	</table><br><br>
	";
		
?>

</body>
</html>