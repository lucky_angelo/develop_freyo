<?

	$oTopMenu = new TopMenu();
	$oTopMenu->component = "newspublish";

	$oMenu = new Menu();
	$oMenu->label = $lang['newspublish']['addnews'];
	$oMenu->page = "wce.addnews.php";
	$oTopMenu->add($oMenu);
	
	$oMenu = new Menu();
	$oMenu->label = $lang['newspublish']['listnews'];
	$oMenu->page = "wce.listnews.php";
	$oTopMenu->add($oMenu);	
		
	$oMenu = new Menu();
	$oMenu->label = $lang['newspublish']['category'];
	$oMenu->page = "wce.category.php";
	$oTopMenu->add($oMenu);
	
	$oMenu = new Menu();
	$oMenu->label = $lang['newspublish']['subscriberss'];
	$oMenu->page = "wce.subscriberss.php";
	$oTopMenu->add($oMenu);
	
	$oMenu = new Menu();
	$oMenu->label = $lang['newspublish']['preference'];
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newspublish']['prefsetting'];
	$oSubMenu->page = "wce.preference.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['newspublish']['interguide'];
	$oSubMenu->page = "wce.guide.php";
	$oMenu->addSub($oSubMenu);
	$oTopMenu->add($oMenu);	
	
	$oTopMenu->create();

?>