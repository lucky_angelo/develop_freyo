<?php include_once("../config.php"); include_once($path["docroot"]."common/css.php"); ?>
<html>
<head><title>News Publisher Sample Page</title></head>
<body>

<table border=1 cellpadding=0 cellspacing=0 width=780 align=center style="border:1px #000000 solid; border-collapse:collapse">
<tr><td align=center colspan=3 style="border-bottom:1px #000000 solid"><br>News Publisher ~ Front-End Sample<br><br> You can easily integrate the Front-End to your web design page. <br>Please refer to the guidelines below.<br><br></td></tr>
<tr><td width=170 height=300 valign=top align=center style="border-right:1px #000000 solid"><br>

	<?php include($path["docroot"]."newspublish/home.menu.php"); ?>

</td><td valign=top style="border-right:1px #000000 solid"><br>

	<?php 
		if ($rss == "read"){
			include($path["docroot"]."newspublish/home.viewrss.php");
		}else{ include($path["docroot"]."newspublish/home.news.php"); }
	?>

</td><td width=170 height=300 valign=top align=center><br>

	<?php 
		include($path["docroot"]."newspublish/home.announce.php");
	?>

</td></tr>
</table>


<br><br>
<table width=700 align=center>
<tr><td valign=top width=25%>
	<b><u>Front-End Integration Guidelines</u></b><br><br>
	You can easily integrate the front-end with your web design pages.
	<table border=0 cellpadding=5>
	<tr><td align=center><img src=image/sample1.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/arrow.gif border=0></td>
		<td align=center><img src=image/sample2.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/sample4.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/sample3.jpg style="border:1px #B6B6B6 solid"></td></tr>
	<tr><td align=center>Before</td>
		<td>&nbsp;</td>
		<td align=center colspan=3>After Integration</td></tr>
	</table>
	
	<br><br>

</td></tr>
<tr><td valign=top>
	
	<b>Integration Instructions</b><br>
	<table cellpadding=5>
	<tr><td valign=top>1. </td><td>Using a text editor, open the web page file (.php file) that you wish to integrate the News Publisher to, insert the line below to the top of the file, to call the 'config.php' file.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include_once("<font color=#F65A0E>document_path_of_panel_folder</font>/config.php"); include_once($path["docroot"]."common/css.php"); ?&gt; </font></td></tr>
	<tr><td valign=top></td><td valign=top>* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.</td></tr>	
	<tr><td valign=top>2. </td><td>In your .php web page file, put the line below to call the menu area - news categories and archives.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include($path["docroot"]."newspublish/home.menu.php"); ?&gt; </font></td></tr>
	<tr><td valign=top>3. </td><td>Meanwhile, to display the news's content area such as news title, full article page, put the line below.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include($path["docroot"]."newspublish/home.news.php"); ?&gt; </font></td></tr>
	<tr><td valign=top>4. </td><td>Next, use the following code to display the announcement section.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include($path["docroot"]."newspublish/home.announce.php"); ?&gt; </font></td></tr>
	<tr><td valign=top>5. </td><td>For sample integration code, please refer to the source code of this file.  To do this, open the file with your text editor. </td></tr>
	<tr><td valign=top colspan=2>** To find your <font color=#F65A0E>'document_path_of_panel_folder'</font>, check the path['docroot'] value from the panel/config.php file</td></tr>
	</table>	
	

</td></tr>
</table>
<br><br>

</body>
</html>