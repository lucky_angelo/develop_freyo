#News Publisher 4.1

CREATE TABLE news_publish (
  news_id int(10) NOT NULL auto_increment,
  category_id int(10) default NULL,
  newstype varchar(20) default NULL,
  title text,
  datepost date default '0000-00-00',
  dateexpire date default '0000-00-00',
  display varchar(10) default NULL,
  summary text,
  content text,
  thumbnail varchar(50) default NULL,
  exlink_url text,
  seq int(10) default '0',
  PRIMARY KEY  (news_id)
) ENGINE=MyISAM;


CREATE TABLE news_category (
  category_id int(10) NOT NULL auto_increment,
  parent_id int(10) default NULL,
  name varchar(50) default NULL,
  descp text,
  cat_seq int(10) default '0',
  PRIMARY KEY  (category_id)
) ENGINE=MyISAM;

CREATE TABLE news_rss (
  rss_id int(10) NOT NULL auto_increment,
  title varchar(50) default NULL,
  url text,
  type varchar(50) default NULL,
  dateadd date default '0000-00-00',
  PRIMARY KEY  (rss_id)
) ENGINE=MyISAM;

INSERT INTO sys_component VALUES (NULL, 'News Publisher', 'newspublish', 'news', 'Post your latest News Article or Announcement. Set Posted and Expiry Date for your news, HTML editor, Tell-A-Friend and Printer Version. ', '4.1', 'wce.addnews.php', '', 'Show','');
INSERT INTO sys_variable VALUES ('news_listno', '20');
INSERT INTO sys_variable VALUES ('news_newsno', '5');
INSERT INTO sys_variable VALUES ('news_seq', 'desc');
INSERT INTO sys_variable VALUES ('news_blockcat', 'Yes');
INSERT INTO sys_variable VALUES ('news_blockarch', 'Yes');
INSERT INTO sys_variable VALUES ('news_blockrssnews', 'Yes');
INSERT INTO sys_variable VALUES ('news_tellsubject', 'Invitation From Your Friend');
INSERT INTO sys_variable VALUES ('news_tellmessage', 'Dear [[name]],\r\n\r\nYour friend, [[yourname]] has invited to you to read the following news article.\r\n\r\n[[newsurl]]\r\n\r\nYour Friend\'s Message:\r\n[[message]]\r\n\r\n\r\nThank you,\r\nYour Name\r\n');
INSERT INTO sys_variable VALUES ('news_titlefont', 'Arial');
INSERT INTO sys_variable VALUES ('news_titlecolor', '#000033');
INSERT INTO sys_variable VALUES ('news_titlesize', '17');
INSERT INTO sys_variable VALUES ('news_titlebold', 'bold');
INSERT INTO sys_variable VALUES ('news_titleitalic', '');
INSERT INTO sys_variable VALUES ('news_titleunderline', '');
INSERT INTO sys_variable VALUES ('news_titlelink', '#0033CC');
INSERT INTO sys_variable VALUES ('news_titlehover', '#9999CC');
INSERT INTO sys_variable VALUES ('news_textfont', 'Arial');
INSERT INTO sys_variable VALUES ('news_textcolor', '#000000');
INSERT INTO sys_variable VALUES ('news_textsize', '12');
INSERT INTO sys_variable VALUES ('news_textsmall', '11');
INSERT INTO sys_variable VALUES ('news_pageurl', '');
INSERT INTO sys_variable VALUES ('news_showemail', 'Yes');
INSERT INTO sys_variable VALUES ('news_showprint', 'Yes');
INSERT INTO sys_variable VALUES ('news_disstyle', 'Same');
INSERT INTO sys_variable VALUES ('news_showpubdate', 'Yes');
INSERT INTO sys_variable VALUES ('news_showsummary', 'Yes');
INSERT INTO sys_variable VALUES ('news_showsummaryart', 'Yes');
INSERT INTO sys_variable VALUES ('news_subcatno', '3');
INSERT INTO sys_variable VALUES ('news_showcatdescp', 'Yes');
INSERT INTO sys_variable VALUES ('news_dateformat', 'd-m-Y');