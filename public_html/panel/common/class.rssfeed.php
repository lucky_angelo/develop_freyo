<?

class RssFeed
{
	var $feedheader, $feedcontent, $feedimage, $feedfooter;
	
	//Constructor
	function RssFeed(){				
		$this->feedheader 	= "";
		$this->feedcontent 	= "";
		$this->feedimage 	= ""; 
		$this->feedfooter 	= "";
	}
	
	//private do not call
	function feedend(){
		$this->feedfooter = "\t</channel>\n";
		$this->feedfooter .= "</rss>";			
	}
	
	function dirExist($tempdir){
		$location=$tempdir;
		$pattern="/.*(\/.*\/)/";
		preg_match($pattern, $location, $matches);
		if(!file_exists(str_replace($matches[1],"/",$location))){			
			$this->dirExist(str_replace($matches[1],"/",$location));
		}
		
		if(!file_exists($location)){
			@mkdir($location, 0777);
			@chmod($location, 0777);
		}		
	}	
	
	//public
	function feeditem($title, $link, $description, $pubdate=""){
		$title 			= htmlentities($title);
		$link 			= htmlentities($link);
		$description	= htmlentities($description);
				
		$this->feedcontent .= "\t\t<item>\n";
		$this->feedcontent .= "\t\t\t<title>$title</title>\n";
		$this->feedcontent .= "\t\t\t<link>$link</link>\n";
		$this->feedcontent .= "\t\t\t<description>$description</description>\n";
		if ($pubdate != ""){ $this->feedcontent.="\t\t\t<pubDate>$pubdate</pubDate>\n"; }
		$this->feedcontent .= "\t\t</item>\n";
	}
	
	//should only call once
	function createFeed($dir,$file){
		$this->feedend();
		$feedtofile = $this->feedheader;

		if (trim($this->feedimage) != "")
			$feedtofile .= $this->feedimage;
		
		$feedtofile .= $this->feedcontent.$this->feedfooter;
		$tempdir = $dir;
		$this->dirExist($tempdir);
		$rssfile = $dir.$file;
		if(!file_exists($rssfile)){ $newlog = "Yes";	}		
		$fp = @fopen($rssfile, "w");
		@fwrite($fp, $feedtofile);
		@fclose($fp);
		if($newlog=="Yes"){ @chmod($rssfile,0777); }
		@chmod($dir, 0777);
		$this->RssFeed();
	}
	
	function feedtitle($title, $link, $description, $language="", $copyright="", $editor=""){
		$title 			= htmlentities($title);
		$link 			= htmlentities($link);
		$description	= htmlentities($description);
		$copyright 		= htmlentities($copyright);		
		
		if ($language==""){ $language = "us-en"; }
		if ($copyright != ""){ $copyright="\t\t<copyright>$copyright</copyright>\n"; }
		if ($editor != ""){ $editor="\t\t<managingEditor>$editor</managingEditor>\n"; }
		
		$this->feedheader = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
		$this->feedheader .= "<rss version=\"2.0\">\n";
	  	$this->feedheader .= "\t<channel>\n";
		$this->feedheader .= "\t\t<title>$title</title>\n"; 
		$this->feedheader .= "\t\t<link>$link</link>\n";
		$this->feedheader .= "\t\t<description>$description</description>\n";
		$this->feedheader .= "\t\t<language>$language</language>\n";
		$this->feedheader .= "$copyright";
		$this->feedheader .= "$editor";
	}
	
	function feedimage($title, $imgurl, $link, $description){
		$title 			= htmlentities($title);
		$link 			= htmlentities($link);
		$description	= htmlentities($description);
		$imgurl 		= htmlentities($imgurl);		
				
		$this->feedimage = "\t\t<image>\n";
		$this->feedimage .= "\t\t\t<title>$title</title>\n";
		$this->feedimage .= "\t\t\t<url>$imgurl</url>\n";
		$this->feedimage .= "\t\t\t<link>$link</link>\n";
		$this->feedimage .= "\t\t\t<width>88</width>\n";
		$this->feedimage .= "\t\t\t<height>31</height>\n";
		$this->feedimage .= "\t\t\t<description>$description</description>\n";
		$this->feedimage .= "\t\t</image>\n";		    
	}
}
?>