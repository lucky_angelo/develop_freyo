<? 
function fetchUrl($url){
     $ch = curl_init();
     curl_setopt($ch, CURLOPT_URL, $url);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($ch, CURLOPT_TIMEOUT, 20);
 
     $retData = curl_exec($ch);
     curl_close($ch); 
 
     return $retData;
}
?>
<link rel="stylesheet" type="text/css" href="<?=$path["webroot"]?>common/scripts/socialnetworks/box_socialnetworkslide.css" />
<div class="socialContentSlider">
	<div class="socialContent">
    	<div class="socialNavigation">
        	<ul>
            	<li><a href="javascript:void(0)" id="navFacebook">Facebook</a></li>
                <li><a href="javascript:void(0)" id="navTwitter">Twitter</a></li>
            </ul>
        </div>
    </div>
    <!-- facebook -->
    <div class="socialContent ContentFacebook">
    		<? 
				//$profile_id = "112579988761953"; //cavitejobs
				$profile_id = "145991878752363";
				//App Info, needed for Auth
				$app_id = "103737289753798";
				$app_secret = "acaa773e50f55a21d1bd5246742595f0";
				//retrieve auth token
				$authToken = fetchUrl("https://graph.facebook.com/oauth/access_token?type=client_cred&client_id={$app_id}&client_secret={$app_secret}");
				$authToken = end(explode("=", $authToken));
				$authToken = urlencode($authToken);
				//var_dump("https://graph.facebook.com/{$profile_id}/feed?{$authToken}"); die();
				//$data = fetchUrl("https://graph.facebook.com/{$profile_id}/feed?{$authToken}");
				//var_dump("https://graph.facebook.com/{$profile_id}/feed?{$authToken}"); die();
			?>
    		<script type="text/javascript">
				var count = 1;
				
				//$(document).ready(function($) {
							$.getJSON("https://graph.facebook.com/LEENTechNetworkSolutions/posts", 
							   {access_token : "<?=$authToken?>", include_rts : "true", limit : "15"}, 
							   function(json) {
								   
								   $.each(json.data, function(key, value) {
										if (value.message) {
											var postlink = '';
											postlink = value.id.split("_");
											var fbpostlink = 'https://www.facebook.com/'+postlink[0]+'/posts/'+postlink[1];
											
											var splttime = 0;
											splttime = value.created_time.split("+");
											$('#foofb').append('<li><table width="254" cellpadding="0" cellspacing="0" border="0" style="table-layout:fixed;"><col width="100" /><col width="146" /><tr><td align="left" valign="top"><img src="<?=$path["webroot"]?>/common/scripts/socialnetworks/images/tmpprofileimage.png" /></td><td align="left" valign="top"><p class="title"><a href="http://www.facebook.com/LEENTechNetworkSolutions" target="_blank">LEENTech Network Solutions</a></p><p class="content"><a href="'+fbpostlink+'" target="_blank">'+value.message+'</a></p><span><abbr class="timeago" title="'+splttime[0].toUpperCase()+'">'+value.created_time+'</abbr></span></td></tr></table></li>');
										}
								   });
								   //	Scrolled by user interaction
									$('#foofb').carouFredSel({
										prev: '#prevfb',
										next: '#nextfb',
										circular: false,
										infinite: false,
										width: 764,
										auto: false
									});
									$("abbr.timeago").timeago();
							   }
							);
				//});
			</script>
            <div id="samplefb"></div>
    		<div class="list_carousel">
            	
				<ul id="foofb" class="dataFB">
                </ul>
				<div class="clearfix"></div>
			</div>
            <a id="prevfb" class="prev" href="#"></a>
			<a id="nextfb" class="next" href="#"></a>
    </div>
    
    <!-- twitter -->
    <div class="socialContent ContentTwitter">
    		<?
				//$twscreenname = "cavitejobs"; //cavitejobs
				$twscreenname = "leentechsystems";
				$twrss = "https://api.twitter.com/1/statuses/user_timeline.rss?screen_name={$twscreenname}&count=15";
				$twxml = simplexml_load_file($twrss);
				$twxml = $twxml->channel;
				
			?>
            <div class="list_carousel">
				<ul id="footw">
					<?
						if ($twxml) {
							foreach($twxml->item as $item) {
								?>
                                    <li>
                                        <table width="254" cellpadding="0" cellspacing="0" border="0" style="table-layout:fixed;">
                                            <col width="100" />
                                            <col width="146" />
                                            <tr>
                                                <td align="left" valign="top"><img src="<?=$path["webroot"]?>common/scripts/socialnetworks/images/tmpprofileimage.png" /></td>
                                                <td align="left" valign="top">
                                                    <p class="content"><a href="<?=$item->link?>" target="_blank"><?=$item->description?></a></p>
                                                    <span><abbr class="timeago" title="<?=$item->pubDate?>"><?=$item->pubDate?></abbr></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>
                                <?
							}
						}
					?>
				</ul>
				<div class="clearfix"></div>
			</div>
            <a id="prevtw" class="prev" href="#"></a>
			<a id="nexttw" class="next" href="#"></a>
    </div>
    
    <!-- social buttons -->
    <div class="socialContent">
    	<div class="socialButtons">
        	<a href="http://www.facebook.com/LEENTechNetworkSolutions" class="socialIconButtons buttonFB" target="_blank"></a>
            <a href="http://twitter.com/leentechsystems" class="socialIconButtons buttonTW" target="_blank"></a>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
		$(function() {
				//	Scrolled by user interaction
				$('#footw').carouFredSel({
					prev: '#prevtw',
					next: '#nexttw',
					circular: false,
					infinite: false,
					width: 764,
					auto: false
				});
		});
</script>
<script type="text/javascript">
	$(document).ready( function() {
								$('#navFacebook').click();
								});
	$('#navFacebook').click( function() {
									  $('.socialNavigation ul li a').removeClass('current');
									  $('#navFacebook').addClass('current');
									  $('.ContentTwitter').css('display','none');
									  $('.ContentFacebook').css('display','block');
									  $('.buttonTW').css('display','none');
									  $('.buttonFB').css('display','block');
									  });
	$('#navTwitter').click( function() {
									  $('.socialNavigation ul li a').removeClass('current');
									  $('#navTwitter').addClass('current');
									  $('.ContentFacebook').css('display','none');
									  $('.ContentTwitter').css('display','block');
									  $('.buttonFB').css('display','none');
									  $('.buttonTW').css('display','block');
									  });
</script>