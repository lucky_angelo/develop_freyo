<?

if($pageaction=="save"){
	
	if($user_id==1){
	   	$oUser->data = array("email","language","pageeffect");
		$oUser->value = array(addslashes(trim($email)),$language,$pageeffect);
		$oUser->update($user_id);
		$status = $lang['common']['yourinfoupdated'];
		$returnClass = "successMessage";
		$oSystem->setValue("sys_mailtype",$sendmail);
		$oSystem->setValue("sys_mailcharset",trim($mail_charset));
		$oSystem->setValue("sys_smtphost",$smtphost);
		$oSystem->setValue("sys_smtpport",$smtpport);
		$oSystem->setValue("sys_smtpuser",$smtpuser);
		$oSystem->setValue("sys_smtppass",$smtppass);
		$oSystem->setValue("sys_wtstats",$wtstats);
		$oSystem->setValue("sys_skin",$skin);
		//social medias
		//$oSystem->setValue("sys_socialmedia_fb",$sys_socialmedia_fb);
		//$oSystem->setValue("sys_socialmedia_tw",$sys_socialmedia_tw);
		$oSystem->setValue("sys_socialmedia_yt",$sys_socialmedia_yt);
		$oSystem->setValue("sys_socialmedia_fk",$sys_socialmedia_fk);
		$oSystem->setValue("sys_socialmedia_da",$sys_socialmedia_da);
		$oSystem->setValue("sys_socialmedia_tl",$sys_socialmedia_tl);
		$tztype == "user"?$oSystem->setValue("sys_timezone",$timezone):$oSystem->setValue("sys_timezone","");
		/*echo "<script>window.location='index.php?component=common&page=wce.info.php&t=".mktime()."'</script>";*/

	}else{
	   	$oUser->data = array("name","email","language","pageeffect");
		$oUser->value = array(addslashes(trim($name)),addslashes(trim($email)),$language,$pageeffect);
		$oUser->update($user_id);
		$status = $lang['common']['yourinfoupdated'];
		$returnClass = "successMessage";
	}
	
	if($oldpass!="" && $newpass!="" && $chkpass!=""){
		
		$oUser->data = array('username');
		$result = $oUser->getDetail($user_id);
		if($myrow=mysql_fetch_row($result)){ $username = $myrow[0];	}
		mysql_free_result($result);
		
		$oUser->data = array("password");
		$result = $oUser->getDetail($user_id);
		if($myrow=mysql_fetch_row($result)){ $curpass = $myrow[0];	}
		mysql_free_result($result);
		
		$oldpass = setupPassword($username, $oldpass);
		
		if($oldpass==$curpass && $newpass==$chkpass){
			$newpass = setupPassword($username, $newpass);
		   	$oUser->data = array("password");
			$oUser->value = array($newpass);
			$oUser->update($user_id);
			$status = $lang['common']['yourpasswordchange'];
			$returnClass = "successMessage";
		}else{
			$status = $lang['common']['yourpasswordnotmatch'];
			$returnClass = "errorMessage";
		}
		
		
	}
	
	$status_message="<span=\"$returnClass\"><b>".$lang['common']['status'].": </b>$status</span>";

}

if (isset($dc) && $dc!="") {
	if ($dc == "tw") {
		$oSystem->setValue("sys_socialmedia_tw","");
		header("Location: index.php?component=common&page=wce.info.php");
		//$returnClass = "successMessage";
		//$status_message="<span=\"$returnClass\"><b>".$lang['common']['status'].": </b>$status</span>";
	}
	
	if ($dc == "fb") {
		$oSystem->setValue("sys_socialmedia_fb","");
		$oSystem->setValue("sys_socialmedia_fb_at","");
		$oSystem->setValue("sys_socialmedia_fb_pg","");
		?><script type="text/javascript">logoutFacebookUser();</script><?
		//header("Location: index.php?component=common&page=wce.info.php");
	}
}

if (isset($confb) && $confb!="" ) {
	if ($confb == "fb") {
		
	}
}

$oUser->data = array("name","email","username","language","pageeffect");
$result=$oUser->getDetail($_SESSION['UserId']);
if($myrow=mysql_fetch_row($result)){
	$user_id=$_SESSION['UserId'];
	$name=stripslashes(htmlentities($myrow[0]));
	$email=stripslashes(htmlentities($myrow[1]));
	$username=stripslashes(htmlentities($myrow[2]));
	$language=$myrow[3];
	$pageeffect=$myrow[4];
}
mysql_free_result($result);

$mailcharset=$oSystem->getValue("sys_mailcharset");
if(empty($mailcharset)){$mailcharset="iso-8859-1";}

$sendmail = $oSystem->getValue("sys_mailtype");
if($sendmail=="phpmail"){$sendmail_mail="checked";}
if(empty($sendmail) || $sendmail=="smtp"){$sendmail_smtp="checked";}

$timezonevalue = $oSystem->getValue("sys_timezone");
?>

<!--Menu Start-->
<table border=0 cellpadding=0 cellspacing=0 width=100%>
<tr><td><b><? echo $lang['common']['admin'] ?></b></td><td valign=bottom align=right><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Menu End-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=common&page=wce.info.php" method=post>
<input type=hidden name=pageaction value="save">
<input type=hidden name=user_id value="<? echo $user_id ?>">
<tr><td>

	<b><u><? echo $lang['common']['userinfo'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td width=35%><? echo $lang['common']['username'] ?></td><td><input type=text name=username style="width:135px" value="<? echo $username ?>" disabled></td></tr>
	<tr><td><? echo $lang['common']['name'] ?></td><td><input type=text name=name style="width:230px" value="<? echo $name ?>"></td></tr>
	<tr><td><? echo $lang['common']['email'] ?></td><td><input type=text name=email style="width:230px" value="<? echo $email ?>"></td></tr>
	<tr><td></td><td valign=top><? echo $lang['common']['emailtip']?></td></tr>
	</table><br><br>
	
	<b><u><? echo $lang['common']['changepassword'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td width=35%><? echo $lang['common']['oldpassword'] ?></td><td><input type=password name=oldpass style="width:135px"> *</td></tr>	
	<tr><td><? echo $lang['common']['newpassword'] ?></td><td><input type=password name=newpass style="width:135px"> *</td></tr>		
	<tr><td><? echo $lang['common']['confirmpassword'] ?></td><td><input type=password name=chkpass style="width:135px"> *</td></tr>	
	</table><br><br>
	
	<? 	if($user_id==1){ ?>
	<b><u><? echo $lang['common']['sendmailoption'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td><? echo $lang['common']['charset'] ?></td><td><input type=text name=mail_charset style="width:135px" value="<? echo $mailcharset; ?>"></td></tr>
	<tr><td valign=top width=35%><input type=radio name=sendmail value="phpmail" <? echo $sendmail_mail?>><? echo $lang['common']['usephpmail'] ?></td><td></td></tr>
	<tr><td valign=top colspan=2><? echo $lang['common']['phpmailtip']?></td></tr>
	<tr><td colspan=2><br></td></tr>
	<tr><td valign=top><input type=radio name=sendmail value="smtp" <? echo $sendmail_smtp?>><? echo $lang['common']['usesmtp'] ?></td><td></td></tr>
	<tr><td valign=top colspan=2><? echo $lang['common']['smtptip']?></td></tr>
	<tr><td><? echo $lang['common']['smtpport'] ?></td><td><input type=text name=smtpport style="width:135px" value="<?= $oSystem->getValue("sys_smtpport")==""?"25":$oSystem->getValue("sys_smtpport"); ?>"></td></tr>
	<tr><td><? echo $lang['common']['smtphost'] ?></td><td><input type=text name=smtphost style="width:230px" value="<? echo $oSystem->getValue("sys_smtphost") ?>"></td></tr>
	<tr><td><? echo $lang['common']['smtpuser'] ?></td><td><input type=text name=smtpuser style="width:230px" value="<? echo $oSystem->getValue("sys_smtpuser") ?>"></td></tr>
	<tr><td><? echo $lang['common']['smtppass'] ?></td><td><input type=password name=smtppass style="width:230px" value="<? echo $oSystem->getValue("sys_smtppass") ?>"></td></tr>
	</table><br><br>
	
	<b><u><? echo $lang['common']['oneadminskin'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td width=35%><? echo $lang['common']['changeskin'] ?></td><td>
		<select name=skin>
		<?		
			$handle = opendir("./common/theme"); 
			while ($file = readdir($handle)) { 
				if($file!="." && $file!=".."){
					echo "<option value=\"$file\" ".($file==$oSystem->getValue("sys_skin")?"selected":"").">$file</option>";
				}
			}
			closedir($handle);
		?>
		</select>	
	</td></tr>
	</table><br><br>
    
   	<b><u><? echo $lang['common']['wtstatsaccount'] ?></u></b>
    <table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
    <tr><td width=35%><? echo $lang['common']['username'] ?></td><td>
    	<input type="text" name="wtstats" value="<? echo $oSystem->getValue("sys_wtstats") ?>" style="width:230px" />
    </td></tr>
    </table><br><br>
    
    <b><u><? echo $lang['common']['socialmedia_accounts'] ?></u></b>
    <table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
    <tr><td width=35%><? echo $lang['common']['socialmedia_fb'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<? /* 
    	<input type="text" name="sys_socialmedia_fb" value="<? echo $oSystem->getValue("sys_socialmedia_fb") ?>" style="width:230px" />
    	
    	<a href="http://developers.facebook.com/tools/explorer/" target="_blank"><?=$lang['common']['sys_socialmedia_fb_link']?></a>
    	*/ ?>
    	<? if($oSystem->getValue("sys_socialmedia_fb")!="") { ?>
    		<a href="index.php?component=common&page=wce.info.php&dc=fb">	
        		<img src="common/image/bt-fbdisconnect.png" border="0" />
            </a>
    	<? } else { ?>
    		<?
			$f1 = new Facebook_class();
			$f1->loadJsSDK($path_to_library);
			
			$fb_cookie = $f1->getCookie();
			if($fb_cookie!='') {
				$oSystem->setValue("sys_socialmedia_fb", $f1->cookie["user_id"]);
			}
			?>
    		<? /*<a href="common/sociallib/connectfb/connect.php">*/ ?>
    		<a href="javascript:void(0)" onclick="fbActionConnect();">
    			<img src="common/image/bt-fbconnect.png" border="0" />
    		</a>
    	<? } ?>
    </td></tr>
    
    <tr><td width=35%><? echo $lang['common']['socialmedia_tw'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<? /*<input type="text" name="sys_socialmedia_tw" value="<? echo $oSystem->getValue("sys_socialmedia_tw") ?>" style="width:230px" />*/ ?>
        <? if($oSystem->getValue("sys_socialmedia_tw")!="") { ?>
        	<a href="index.php?component=common&page=wce.info.php&dc=tw">	
        		<img src="common/image/bt-twitterdisconnect.png" border="0" />
            </a>
        <? } else { ?>
        	<a href="common/sociallib/connecttw/redirect.php">
		        <img src="common/image/bt-twitterconnect.png" border="0" />
            </a>
        <? } ?>
    </td></tr>
    
    <tr><td width=35%><? echo $lang['common']['socialmedia_yt'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<input type="text" name="sys_socialmedia_yt" value="<? echo $oSystem->getValue("sys_socialmedia_yt") ?>" style="width:230px" />
    </td></tr>
    
    <tr><td width=35%><? echo $lang['common']['socialmedia_fk'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<input type="text" name="sys_socialmedia_fk" value="<? echo $oSystem->getValue("sys_socialmedia_fk") ?>" style="width:230px" />
    </td></tr>
    
    <tr><td width=35%><? echo $lang['common']['socialmedia_da'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<input type="text" name="sys_socialmedia_da" value="<? echo $oSystem->getValue("sys_socialmedia_da") ?>" style="width:230px" />
    </td></tr>
    
    <tr><td width=35%><? echo $lang['common']['socialmedia_tl'] ?> <? echo $lang['common']['socialmedia_id'] ?></td><td>
    	<input type="text" name="sys_socialmedia_tl" value="<? echo $oSystem->getValue("sys_socialmedia_tl") ?>" style="width:230px" />
    </td></tr>
   
    </table><br><br>
    
	<b><u><? echo $lang['common']['inserttz'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td width=35%><input type=radio name=tztype value="default" onclick="toggletz()" <?= $timezonevalue==""?"checked":""; ?>><?= $lang['common']['tzserver'] ?></td><td></td></tr>
	<tr><td valign=top><input type=radio name=tztype value="user" onclick="toggletz()" <?= $timezonevalue!=""?"checked":""; ?>><?= $lang['common']['tzlocal'] ?></td>
	<td valign=bottom><input type=text name=timezone style="width:135px" value="<?= $timezonevalue ?>" maxlength='5'>&nbsp;&nbsp;&nbsp;<?= $lang['common']['timezonenote'] ?></td></tr>
	</table><br><br>
	<? } ?>	
	
	<b><u><? echo $lang['common']['changelanguage'] ?></u></b>
	<table border=0 width=95% cellpadding=2 cellspacing=2 align=center>
	<tr><td width=35%><? echo $lang['common']['selectlanguage'] ?></td><td>
		<select name=language>
		<?		
			$handle = opendir("./common/language"); 
			while ($file = readdir($handle)) { 
				if(getfileextension($file)==".php"){
					$thislanguage = substr($file,0,strrpos($file,"."));
					if($file==$language){	echo "<option value=\"$file\" selected>$thislanguage</option>";	}else{	echo "<option value=\"$file\">$thislanguage</option>";	}
				}
			}
			closedir($handle);
		?>
		</select>	
	</td></tr>
	</table><br><br>
	
	<? if($user_id==1){ echo "<input type=button value=\" ".$lang['common']['save']." \" onclick=\"tzvalidate()\">"; }
	   else {echo "<input type=button value=\" ".$lang['common']['save']." \" onclick=\"document.thisform.submit();\">";}		
	?>
	<input type=button value="<? echo $lang['common']['cancel'] ?>" onclick="window.location='index.php'">

</td></tr>
</form>
</table>
<script language='javascript'>
function toggletz(){
	if (document.thisform.tztype[0].checked){
		document.thisform.timezone.value="";
	}
	else if (document.thisform.tztype[1].checked){
		document.thisform.timezone.focus();
	}
}
function tzvalidate(){
	if (document.thisform.tztype[1].checked && !document.thisform.timezone.value.match(/[+-]{1}\d{4}/)){
		alert ("<? echo $lang['common']['invalidtz']; ?>!");
		document.thisform.timezone.focus();
		document.thisform.timezone.select();
	}else{
		document.thisform.submit();
	}
}
</script>	
