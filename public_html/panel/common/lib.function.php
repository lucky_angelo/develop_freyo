<?

//for clear memcachedcache 2015/10/16
function clearServerMemcached(){
	$thecached = new Memcached();
	$thecached->addServer('127.0.0.1', 11211);
	$thecached->flush();
}

function currencyformat($price) {

	$price = (string)$price;

	if(strlen($price)==0) {

		return "0.00";

	} else {

		$dotpos = strrpos($price,".");

		if(!$dotpos) {

			$price .= ".00";

			return $price;

		} else {

			switch(strlen(substr($price,$dotpos+1))) {

			case 0:		$price .= "00";	return $price;	break;

			case 1:		$price .= "0";	return $price;	break;

			case 2:		return $price;	break;

			default;	return "<b>Error in price ${price}!</b>";	break;

			}

		}

	}

}



function getfileextension($filename) {

    $ext = strrchr($filename,".");

    return $ext;

}



function isexecutable($filename){



	$extension =  getfileextension($filename);

	switch($extension){

		case '.php'		: return TRUE; break;

		case '.asp'		: return TRUE; break;

		case '.aspx'	: return TRUE; break;

		case '.jsp'		: return TRUE; break;

		case '.cgi'		: return TRUE; break;

		case '.exe'		: return TRUE; break;

		case '.js'		: return TRUE; break;

		case '.cfm'		: return TRUE; break;

		case '.dll'		: return TRUE; break;

		default 		: return FALSE; break;

	}

}



function filesizeformat($filesize) {

	$filesize = (string)$filesize;

	if(strlen($filesize)==0) {

		return "0.0";

	} else {

		$dotpos = strrpos($filesize,".");

		if(!$dotpos) {

			$filesize .= ".0";

			return $filesize;

		}else

			return $filesize;

	}

}



function split_sql($sql) {



    $sql = trim($sql);

    $sql = preg_replace("/\n#[^\n]*\n/", "", $sql);

    $buffer = array();

    $ret = array();

    $in_string = false;

    for($i=0; $i<strlen($sql)-1; $i++) {

        if($sql[$i] == ";" && !$in_string) {

            $ret[] = substr($sql, 0, $i);

            $sql = substr($sql, $i + 1);

            $i = 0;

        }

        if($in_string && ($sql[$i] == $in_string) && $buffer[0] != "\\") {

            $in_string = false;

        } elseif(!$in_string && ($sql[$i] == "\"" || $sql[$i] == "'")

                 && (!isset($buffer[0]) || $buffer[0] != "\\")) {

            $in_string = $sql[$i];

        }

        if(isset($buffer[1])) {

            $buffer[0] = $buffer[1];

        }

        $buffer[1] = $sql[$i];

    }

    if(!empty($sql)) {

        $ret[] = $sql;

    }

    return($ret);

}



function exec_sql_lines($sql_file, $old_string = '', $new_string = '') {



    if(!empty($sql_file) && $sql_file != "none") {

        $sql_query = addslashes(fread(fopen($sql_file, "r"), filesize($sql_file)));

        if($old_string != '') {

            $sql_query = preg_replace("#".$old_string."#",$new_string,$sql_query);

        }

    }

    $pieces  = split_sql($sql_query);

    if (count($pieces) == 1 && !empty($pieces[0])) {

       $sql_query = trim($pieces[0]);

       exit;

    }

    for ($i=0; $i<count($pieces); $i++) {

        $pieces[$i] = stripslashes(trim($pieces[$i]));

        if(!empty($pieces[$i]) && $pieces[$i] != "#") {

            $result = mysql_query ($pieces[$i]);

        }

    }

    $sql_query = stripslashes($sql_query);

    return true;

}



function setupcomponentdb($file,$seq){

    if(is_dir($file) && file_exists($file."/database.sql")){

		exec_sql_lines($file."/database.sql");

		$sql = "update sys_component set menuseq='$seq' where shortname='$file'";

		mysql_query($sql);

    }



    if($file=="geotraffic" && file_exists($file."/database.csv")){

		$handle = fopen ($file."/database.csv","r");

		while ($myrow = fgetcsv ($handle, 1000, ",")) {

			$sql = "insert into geotraffic_ip values('$myrow[0]','$myrow[1]','$myrow[2]','$myrow[3]')";

			mysql_query($sql);

		}

		fclose($handle);

    }

}



function recurDir($tmp_dir,$home_dir){

    $dh = opendir($tmp_dir);

	while($file = readdir($dh)) {

		if($file!="." && $file!=".." && strpos($file,".")!=0){

			copy("$tmp_dir/$file","$home_dir/$file");

			chmod("$home_dir/$file",0666);

			unlink("$tmp_dir/$file");

		}else if($file!="." && $file!=".." && strpos($file,".")==0){

			$in_dir = $tmp_dir."/".$file;

			$hin_dir = $home_dir."/".$file;

			if(!file_exists($hin_dir)){  mkdir ($hin_dir, 0777);	}

			recurDir($in_dir,$hin_dir);

		}

	}

	closedir($dh);

}



function recurRmDir($tmp_dir){

    $dh = opendir($tmp_dir);

	while($file = readdir($dh)) {

		if($file!="." && $file!=".."){

			$thisfile = $tmp_dir."/".$file;



			if(is_dir($thisfile)){

				$in_dir = $tmp_dir."/".$file;

				recurRmDir($in_dir);

				if(file_exists($in_dir)){  rmdir ($in_dir);	}

			}else if(is_file($thisfile)){

				unlink($thisfile);

			}

		}

	}

	closedir($dh);

}



function getParamList(){

	global $param_list, $_GET, $_POST, $_COOKIE;

	while( $g = each( $_GET ) ){ $param_list .= $g['key']."=".$g['value']."&"; }

	while( $p = each( $_POST ) ){ $param_list .= $p['key']."=".$p['value']."&"; }

	while( $c = each( $_COOKIE ) ){ $param_list .= $c['key']."=".$c['value']."&"; }

}



function gmtime(){

	return time() - (int) date('Z');

}



function getLocalTime($difftime){

	$gmtime = gmtime();

	$arimethic = substr($difftime,0,1);

	$hourinterval = substr($difftime,1,2);

	$minuteinterval = substr($difftime,3,2);

	if ($arimethic == "+")

		$retime = $gmtime + $hourinterval*60*60 + $minuteinterval*60;

	else

		$retime = $gmtime - ($hourinterval*60*60 + $minuteinterval*60);

	return $retime;

}



function setupPassword($username, $password){

	$username = sha1($username);

	$password = md5($password);



	$hashpassword = substr($username, 0, 4) . substr($password,4,32);



	return $hashpassword;

}



function fetchFBUrl($url){

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_TIMEOUT, 20);



	$retData = curl_exec($ch);

	curl_close($ch);



	return $retData;

}

function toAscii($str, $replace=array(), $delimiter='-') {

	setlocale(LC_ALL, 'en_US.UTF8');

	if( !empty($replace) ) {

		$str = str_replace((array)$replace, ' ', $str);

	}



	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);

	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);

	$clean = strtolower(trim($clean, '-'));

	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);



	return $clean;

}

function randomPassword() {

    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

    $pass = array(); //remember to declare $pass as an array

    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

    for ($i = 0; $i < 8; $i++) {

        $n = rand(0, $alphaLength);

        $pass[] = $alphabet[$n];

    }

    return implode($pass); //turn the array into a string

}

?>