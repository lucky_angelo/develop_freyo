<?

class Validate extends GenericSQL{
	
	function validate($label, $field, $validate){
		global $status_message, $lang;
		switch($validate){
			case "NotEmpty":
				if(trim($field)==""){	
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['cannotbeblank'].".</font><br>";		
				}
				break;
			case "IsEmail":
				if(!preg_match("^[a-z0-9\.\'_-]+@+[a-z0-9\._-]+\.+[a-z]{2,4}$^", $field)) {		
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['notvalidemail'].".</font><br>";		
				}
				break;							
			case "IsInteger":
				if(!preg_match("^[0-9]+$^", $field)) {	
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['notvalidinteger'].".</font><br>";		
				}
				break;	
			case "IsNumeric":
				if(!is_numeric($field)) {	
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['notvalidnumberic'].".</font><br>";		
				}
				break;				
		}
	}
	
	function valCheck($label, $field, $validate){
		global $status_message, $lang;
		switch($validate){
			case "NotEmpty":
				if(count($field)==0){	
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['mustbechecked'].".</font><br>";		
				}
				break;				
		}
	}

	
	function valMatch($label1, $field1, $label2, $field2){
		global $status_message, $lang;
		if(strcmp($field1,$field2)!=0){	
			$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label1 ".$lang['common']['and']." $label2 ".$lang['common']['arenotmatch'].".</font><br>";		
		}
	}
	
	function valEither($label1, $field1, $label2, $field2){
		global $status_message, $lang;
		if(trim($field1)=="" && trim($field2)==""){	
			$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label1 ".$lang['common']['or']." $label2 ".$lang['common']['cannotbeblank'].".</font><br>";	
		}
	}
	
	function valEMail($label,$field){
		global $status_message, $lang;
	    if(preg_match("#^[a-z0-9\.\'_-]+@+[a-z0-9\._-]+\.+[a-z]{2,4}$#", $field)) {
	       	$status_message.="";
	    }else{
			$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $label ".$lang['common']['notemail'].".</font><br>";		
	    }
	}
	
	function valAuthCode($label,$field,$validatenum){
		global $lang;
		
		if(is_null($validatenum) || md5($field) != $validatenum){
			return "<font color=ff0000>".$lang['common']['validcodeerr']."</font><br><br>";	
		}
		else return "";				
	}  	

}

?>