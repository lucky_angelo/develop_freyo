<?

$lang['common']['hdwebsite'] 				= "website";
$lang['common']['hdfront'] 					= "front-end";
$lang['common']['hdadmin'] 					= "admin";
$lang['common']['hdpro'] 					= "pro";
$lang['common']['hdabout'] 					= "about";
$lang['common']['hdlogout'] 				= "logout";
$lang['common']['minfeature'] 				= "Minimize Features List";
$lang['common']['showallfeature']			= "Show All Features";
$lang['common']['confirmlogout']			= "Are you sure you want to log out?";
$lang['common']['visitstore']				= "";
$lang['common']['nocomponent']				= "No module installed";
$lang['common']['trialversion']				= "Trial Version";
$lang['common']['licensenotregistered']		= "Server License Is Not Registered";
$lang['common']['needanyhelp']				= "Need Any Help? Click Here";
$lang['common']['frontendcss']				= "Front-End CSS";
$lang['common']['component']				= "Modules";
$lang['common']['modbuild']					= "CMS Pages";
$lang['common']['thirdparty']				= "3rd Party";


$lang['common']['status']	 				= "Status ";
$lang['common']['yourinfoupdated']			= "Your information has been updated.";
$lang['common']['yourpasswordchange']		= "Your password has been changed.";
$lang['common']['yourpasswordnotmatch']		= "Your passwords are not matched.";
$lang['common']['admin'] 					= "Admin";
$lang['common']['infonsetting']				= "Info & Settings";
$lang['common']['installation']				= "Installation";
$lang['common']['manageuser'] 				= "Manage User";
$lang['common']['menuseq']					= "Menu Sequence";

$lang['common']['userinfo'] 				= "User Information";
$lang['common']['username'] 				= "Username";
$lang['common']['name']		 				= "Name";
$lang['common']['email'] 					= "Email Address";
$lang['common']['smtphost']					= "SMTP Host";
$lang['common']['smtpport']					= "SMTP Port";
$lang['common']['smtpuser']					= "SMTP User";
$lang['common']['smtppass']					= "SMTP Password";
$lang['common']['changepassword']			= "Change Password";
$lang['common']['oldpassword'] 				= "Old Password";
$lang['common']['newpassword'] 				= "New Password";
$lang['common']['confirmpassword']			= "Confirm Password";
$lang['common']['othersetting']				= "Other Settings";
$lang['common']['language']					= "Language";
$lang['common']['selectlanguage']			= "Select Language";
$lang['common']['enablepageblend']			= "Enable Page Blend Effect";
$lang['common']['yes'] 						= "Yes";
$lang['common']['no'] 						= "No";
$lang['common']['save'] 					= "Save";
$lang['common']['cancel'] 					= "Cancel";
$lang['common']['sendmailoption'] 			= "Email Sending Option";
$lang['common']['usephpmail'] 				= "Use PHP mail() function";
$lang['common']['phpmailtip'] 				= "This is useful when connection to your mail server is blocked";
$lang['common']['charset'] 					= "Charset for your email";
$lang['common']['usesmtp'] 					= "Use SMTP";
$lang['common']['smtptip'] 					= "Enter the SMTP information for the above email address.";
$lang['common']['emailtip']					= "It is recommended you use the email account of this domain";
$lang['common']['mailcharset'] 				= "Charset for your email";
$lang['common']['changeskin']				= "Change Theme";
$lang['common']['changelanguage']			= "Change Language";
$lang['common']['oneadminskin']				= "Theme/Appearance";
$lang['common']['wtstatsaccount']			= "LEENTech WTStats Account";
$lang['common']['inserttz']					= "Time Zone";
$lang['common']['timezonenote']				= "Examples: +0000, +0500, -0200";
$lang['common']['tzserver']					= "Use Default (server time zone)";
$lang['common']['tzlocal']					= "Set Local Time Zone (GMT)";
$lang['common']['invalidtz']				= "Invalid time zone format";


$lang['common']['websiteinfo'] 				= "Installation Information";
$lang['common']['webaddress'] 				= "Product Activation URL (Copy and Paste)";
$lang['common']['requestactivation']		= "Request Activation Key";
$lang['common']['componentinstall']			= "Modules Installed";
$lang['common']['hide'] 					= "Hide";
$lang['common']['show']		 				= "Show";
$lang['common']['buynow']	 				= "Now!";
$lang['common']['fullversion'] 				= "Full Version";
$lang['common']['invalidkey'] 				= "Invalid Key";
$lang['common']['serialkey'] 				= "Activation Key";
$lang['common']['moveup'] 					= "Move Up";
$lang['common']['movedown'] 				= "Move Down";
$lang['common']['activate'] 				= "Activate";
$lang['common']['uninstall'] 				= "Uninstall";
$lang['common']['none']		 				= "None";
$lang['common']['componentnotinstall']		= "Modules Not Installed";
$lang['common']['clicktoinstall']			= "Click To Install";
$lang['common']['warnconfirmuninstall']		= "Warning! All data for this module will be deleted. Are you sure you want to remove this module ?";
$lang['common']['install']					= "Proceed to install selected modules?";
$lang['common']['proceedtoinstall']			= "Proceed to install this module ?";
$lang['common']['installsampledata']		= "Would you like to install some sample data for this module ?";


$lang['common']['newuseradded']				= "New user added.";
$lang['common']['edituserdetail']			= "Edit user details.";
$lang['common']['userdetailupdated']		= "User details updated.";
$lang['common']['invalid_password']			= "New Password and Confirm Password didn't match!";
$lang['common']['userdeleted'] 				= "User deleted.";
$lang['common']['modulepermission']			= "Module Permission";
$lang['common']['password'] 				= "Password";
$lang['common']['confirm_password']			= "Confirm Password";
$lang['common']['update'] 					= "Update";
$lang['common']['delete'] 					= "Delete";
$lang['common']['back'] 					= "Back";
$lang['common']['add'] 						= "Add";
$lang['common']['number']	 				= "No";
$lang['common']['permission'] 				= "Permission";
$lang['common']['lastlogin'] 				= "Last Login & IP";
$lang['common']['editthisrecord'] 			= "Edit This Record";
$lang['common']['deletethisrecord']			= "Delete This Record";
$lang['common']['sorryadmindelete']			= "Sorry, the Admin user cannot be deleted.";
$lang['common']['confirmdeleteuser']		= "Are you sure you want to delete this user ?";
$lang['common']['all']						= "ALL";



$lang['common']['plsproceedtologin']		= "Please Proceed With Your Administration Login";
$lang['common']['login'] 					= "Sign in";
$lang['common']['rememberme']				= "Remember me";
$lang['common']['returntowebsite']			= "Return To Web Site";
$lang['common']['forgetpassword']			= "Forget Password";
$lang['common']['errorinvalidlogin']		= "Error: Invalid login";
$lang['common']['errorinvalidlogin_1']		= "Error: You already reach the limit of retries. Please try again after 30 minutes.";
$lang['common']['yourlogininfo']			= "Your Login Information";
$lang['common']['yourloginasbelow']			= "Your login information is as below.";
$lang['common']['dear'] 					= "Dear";
$lang['common']['thankyou']					= "Thank You";
$lang['common']['retrivelogininfo']			= "We will retrieve your login information and send it to your email address shortly.";
$lang['common']['closewindow']				= "Close Window";
$lang['common']['sorry'] 					= "Sorry";
$lang['common']['unabletoretrieve']			= "We are unable to retrieve your login information.";
$lang['common']['forgetloginpassword']		= "Forget Your Login Password?";
$lang['common']['plsenterinfobelow']		= "Please enter both your information below.";
$lang['common']['retrievepass']				= "Retrieve Password";
$lang['common']['plsusername']				= "Please enter your Username.";
$lang['common']['plsemail']					= "Please enter your Email Address.";
$lang['common']['nopermission']				= "Sorry. You have no permission to access this page.";
$lang['common']['pagenofound']				= "Page Not Found";

$lang['common']['error']					= "Error";
$lang['common']['cannotbeblank']			= "cannot be blank";
$lang['common']['notvalidemail']			= "is not a valid email address";
$lang['common']['notvalidinteger']			= "is not a valid integer";
$lang['common']['notvalidnumberic']			= "is not a valid number";
$lang['common']['and']						= "and";
$lang['common']['or']						= "or";
$lang['common']['arenotmatch']				= "are not match";
$lang['common']['notemail']					= "is not a valid email address.";
$lang['common']['mustbechecked']			= "must be checked at lease once.";
$lang['common']['dataexist']				= "already exists.";

$lang['common']['deleteselected']			= "Delete Selected";
$lang['common']['confirmdelete'] 			= "Are you sure you want to delete the selected record(s) ?";
$lang['common']['viewimage'] 				= "View";
$lang['common']['delimage'] 				= "Delete";

$lang['common']['selectedrecorddeleted']	= "Selected record(s) deleted.";
$lang['common']['newrecordadded']			= "New record added.";
$lang['common']['confirmselect']			= "Are you sure you want to delete the selected record(s) ?";
$lang['common']['records']					= "Records";
$lang['common']['of']						= "of";
$lang['common']['prev']						= "Prev";
$lang['common']['next']						= "Next";
$lang['common']['sortasc']					= "Sort In Ascending Order";
$lang['common']['sortdesc']					= "Sort In Descending Order";
$lang['common']['editrecord']				= "Edit This Record";
$lang['common']['confirmdel']				= "Are you sure you want to delete this record ?";
$lang['common']['fileremoved']				= "File removed.";
$lang['common']['recordupdated']			= "Record updated.";
$lang['common']['createatleast']			= "Create at least an Add Record page first.";


$lang['common']['refresh']					= "Refresh";
$lang['common']['validcode']				= "Enter Validation Code";
$lang['common']['validcodeerr']				= "Invalid Validation Code";
$lang['common']['frontendcssdescp']			= "You can enter your Cascading Style Sheet code below as your module's front-end display style.";
$lang['common']['enabelfrontendcss']		= "Enable Front-End CSS";


$lang['common']['socialmedia_return'] 		= "You don't have any social media that is sync. Please go to the admin section and add your social media ids.";
$lang['common']['socialmedia_accounts'] 	= "Social Media IDs";
$lang['common']['socialmedia_id'] 			= "ID";
$lang['common']['socialmedia_fb'] 			= "Facebook";
$lang['common']['socialmedia_tw'] 			= "Twitter";
$lang['common']['socialmedia_ig'] 			= "Instagram";
$lang['common']['socialmedia_yt'] 			= "Youtube";
$lang['common']['socialmedia_fk'] 			= "Flickr";
$lang['common']['socialmedia_da'] 			= "Deviant Art";
$lang['common']['socialmedia_tl'] 			= "Tumblr";
$lang['common']['sys_socialmedia_fb_link'] 	= "Get your Facebook ID here";


$lang['common']['tw_header'] 				= "Profile Summary";
$lang['common']['tw_stat_followers']		= "Followers";
$lang['common']['tw_stat_friends']			= "Friends";
$lang['common']['tw_stat_status'] 			= "Status";


$lang['common']['ds_social_stream']			= "Social Stream";
$lang['common']['ds_social_stats'] 			= "Social Status";
?>