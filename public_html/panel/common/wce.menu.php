<?

	$oTopMenu = new TopMenu();
	$oTopMenu->component = "common";

	$oMenu = new Menu();
	$oMenu->label = $lang['common']['infonsetting'];
	$oMenu->page = "wce.info.php";
	$oTopMenu->add($oMenu);
	
	if($oUser->checkAdmin()){
		
		$oMenu = new Menu();
		$oMenu->label = $lang['common']['installation'];
		$oMenu->page = "wce.install.php";
		$oTopMenu->add($oMenu);

		$oMenu = new Menu();
		$oMenu->label = $lang['common']['menuseq'];
		$oMenu->page = "wce.menuseq.php";
		$oTopMenu->add($oMenu);
		
		$oMenu = new Menu();
		$oMenu->label = $lang['common']['manageuser'];
		$oMenu->page = "wce.user.php";
		$oTopMenu->add($oMenu);
		
		$oMenu = new Menu();
		$oMenu->label = $lang['common']['frontendcss'];
		$oMenu->page = "wce.css.php";
		$oTopMenu->add($oMenu);
			
	}
		
	$oTopMenu->create();
	
?>
