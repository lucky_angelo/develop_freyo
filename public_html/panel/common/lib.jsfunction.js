function setPointer(theRow, thePointerColor)	{
    if (typeof(theRow.style) == 'undefined' || typeof(theRow.cells) == 'undefined') {
        return false;
    }

    var row_cells_cnt = theRow.cells.length;
    for (var c = 0; c < row_cells_cnt; c++) {
        theRow.cells[c].bgColor = thePointerColor;
    }

    return true;
}

function About(){
	iLeft = (screen.width/2) - 250;
	iTop = (screen.height/2) - 150;	
	window.open("index.php?component=common&page=wce.about.php&headfoot=no","Popup","width=300,height=210,top="+iTop+",left="+iLeft+",dependent=yes,titlebar=no,scrollbars=no");		
}

function popImage(imageURL, caption){
  var windowTop = 100; var windowLeft = 100; var defaultWidth = 730;
  var defaultHeight = 532; var onLoseFocusExit = true; var undefined;
  var Options = "width=" + defaultWidth + ",height=" + defaultHeight + ",top=" + windowTop + ",left=" + windowLeft + ",resizable"  
  var myScript = "<html>\n" + 
    "<head>\n" + 
    "<title>" + caption + "\</title>\n" +
    "<script language=\"JavaScript\" type=\"text/javascript\">\n" +
    "function resizewindow () {\n" +
    "  var width = document.myimage.width;\n" + 
    "  var height = document.myimage.height;\n";
  if (navigator.appName.indexOf("Netscape") != -1) {  myScript = myScript +  "  window.innerHeight = height;\n  window.innerWidth = width;\n"
  }else if (navigator.appName.indexOf("Opera") != -1) {   myScript = myScript +  "  window.resizeTo (width+12, height+71);\n"
  }else if (navigator.appName.indexOf("Microsoft") != -1) { myScript = myScript + "  window.resizeTo (width+12, height+71);\n" 
  }else {   myScript = myScript + "  window.resizeTo (width+14, height+34);\n"  }
  myScript = myScript + "}\n" + "window.onload = resizewindow;\n" +
    "</script>\n</head>\n" + "<body ";
  if (onLoseFocusExit) {myScript = myScript + "onblur=\"self.close()\" ";}
  myScript = myScript + "style=\"margin: 0px; padding: 0px;\">\n" +
    "<img src=\"" + imageURL + "\" alt=\"" + caption + "\" title=\"" + caption + "\" name=\"myimage\">\n" + 
    "</body>\n" +  "</html>\n";  
  var imageWindow = window.open ("","imageWin",Options);
  imageWindow.document.write (myScript)
  imageWindow.document.close ();
  if (window.focus) imageWindow.focus();
  return false;
}


var bikky = document.cookie;
function getCookie(name) { 
	var index = bikky.indexOf(name + "=");
	if (index == -1) return null;
	index = bikky.indexOf("=", index) + 1;
	var endstr = bikky.indexOf(";", index);
	if (endstr == -1) endstr = bikky.length;
	return unescape(bikky.substring(index, endstr));
}

function setCookie(name, value) {
   if (value != null && value != "")
     document.cookie=name + "=" + escape(value) + ";";
 bikky= document.cookie;
}

var bikkys = document.cookie;
function getexpirydate(nodays){
	var UTCstring;
	Today = new Date();
	nomilli=Date.parse(Today);
	Today.setTime(nomilli+nodays*24*60*60*1000);
	
	UTCstring = Today.toUTCString();
	return UTCstring;
}

function getcookietime(cookiename) {
	var cookiestring=""+document.cookie;
	var index1=cookiestring.indexOf(cookiename);
	if (index1==-1 || cookiename=="") return ""; 
	 	var index2=cookiestring.indexOf(';',index1);
	if (index2==-1) index2=cookiestring.length; 
		return unescape(cookiestring.substring(index1+cookiename.length+1,index2));
}

function setcookietime(name,value,duration){
	cookiestring=name+"="+escape(value)+";EXPIRES="+getexpirydate(duration);
	document.cookie=cookiestring;
	bikkys= document.cookie;
	if(!getcookietime(name)){
		return false;
	}
	else{
		return true;
	}
}

function getPositionX(element){
	var x=0;
	while(element!=null){
		x += element.offsetLeft-element.scrollLeft;
		element=element.offsetParent;
	}
	return x;
}

function getPositionY(element){
	var y=0;
	while(element!=null){
		y += element.offsetTop-element.scrollTop;
		element=element.offsetParent;
	}
	return y;
}

function sortlist(j,dropdownid,formname){
	//move obj up when j is -1 and down when j=1
	var x = document.getElementById(dropdownid);
	var tempvalue = "";
	var temptext = "";
	var IsLimit = false;
	
	i = x.selectedIndex;		
	
	//make sure hv selected item
	if (i>=0){
		
		if (sAgent.indexOf('msie')!=-1 && sAgent.indexOf('opera')==-1) {
			 var z = formname.elements;
		}else{
			var curnode = "";
			if (j>0) curnode=document.getElementById(x.value);
			else curnode=document.getElementById(x.options[i-1].value);
			var parnode = curnode.parentNode;
			var nextnode = curnode.nextSibling;			
		}
		
		k = i + j;
		if (j<0)
			IsLimit = x.options[0].selected;
		else
			IsLimit = x.options[x.length-1].selected;	
		
		if (IsLimit == false){
			tempvalue = x.options[i].value;
			temptext = x.options[i].text;
			x.options[i].value = x.options[k].value;
			x.options[i].text = x.options[k].text;
			x.options[k].value = tempvalue;
			x.options[k].text = temptext;
			x.options[k].selected = true;
			
			if (sAgent.indexOf('msie')!=-1 && sAgent.indexOf('opera')==-1) {
				tempvalue = z(x.length-i-1).value;
				z(x.length-i-1).value = z(x.length-k-1).value;
				z(x.length-k-1).value = tempvalue;
			}else {						
				parnode.replaceChild(curnode,nextnode);
				parnode.insertBefore(nextnode,curnode);
			}
		}
	}
}

function editnode(j, btn1, btn2, btn3, dropdownid, textid, formname){
	//j=-1 when dblclick and j=1 when update node
	//btn1=add_btn btn2=edit_btn btn3=del_btn
	var oAdd = document.getElementById(btn1);
	var oEdit = document.getElementById(btn2);
	var oDel = document.getElementById(btn3);
	var x = document.getElementById(dropdownid);
	var oText = textid;
	var i = x.selectedIndex;
	
	if (j<0){
		oAdd.style.display = 'none';
		oEdit.style.display = '';
		oDel.style.display = 'none';			
		oText.value = x.value;
		oText.focus();
		oText.select();
		x.disabled = true;
	}else{
		oAdd.style.display = '';
		oEdit.style.display = 'none';
		oDel.style.display = '';

		if (sAgent.indexOf('msie')!=-1 && sAgent.indexOf('opera')==-1) {
			 var z = formname.elements;
			 z(x.length-i-1).value = oText.value;
		}else{
			var curnode = document.getElementById(x.value);
			curnode.value = oText.value;
			curnode.id = oText.value;	
		}
		x.options[i].value = oText.value;
		x.options[i].text = oText.value;	
		
		oText.value = '';
		x.disabled = false;
	}
}

function nextSeq(x, e){
	var txtfield = new Array();
	var count = 0;
	if (e.keyCode==13){
		for (i=0; i<x.form.elements.length; i++){
			if (x.form.elements[i].type == "text"){
				txtfield[count] = i;
				count++;
			}				
		}
		for (i=0;i<txtfield.length;i++){
			if (x.form.elements[txtfield[i]] == x){
				if (i == txtfield.length-1){
					i=0;
				}else{ i = i+1; }
				
				x.form.elements[txtfield[i]].focus();
				x.form.elements[txtfield[i]].select();
				break;
			}
		}
	}
}