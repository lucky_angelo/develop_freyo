<?

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	/***** Header Check *****/
	if($partner_link=="No" || filesize($path["docroot"].$path["skin"]."image/head_logo.gif")=="9928"){ 
		$header_image = $path["webroot"].$path["skin"]."image/head_logo_inside.png"; 
	}else{
		$header_image = $path["webroot"].$path["skin"]."image/head_logo_inside.png";
	}
}

/***** Load User Profile *****/
$oUser->data = array("language","fullmenu","pageeffect");
$result=$oUser->getDetail($_SESSION['UserId']);
if($myrow=mysql_fetch_row($result)){
	$languagefile = $myrow[0];
	$showfullmenu = $myrow[1];
	$showpageeffect = $myrow[2];
}
mysql_free_result($result);

if($pageaction=="save" && $language!="" ){ $languagefile=$language; }
if($languagefile==""){ $languagefile="English.php"; }

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	if(file_exists($path["docroot"]."common/language/$languagefile")){
		include_once($path["docroot"]."common/language/$languagefile"); 
	}
}

$oComponent->data = array("shortname");
$result = $oComponent->includeClass();
while($myrow=mysql_fetch_row($result)){	
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if(file_exists($path["docroot"]."$myrow[0]/language/$languagefile")){
			include_once($path["docroot"]."$myrow[0]/language/$languagefile"); 
		}	
	}
}
mysql_free_result($result);

if(!$oComponent->chkActivate($component,$path["webroot"]) && $component!="common"){	
	if(filesize("common/image/bg_trial.gif")=="1476"){	$bgimage = "common/image/bg_trial.gif";	}else{	$bgimage = "common/image/bg_trial.gif";		}
	if($partner_link!="No"){ $storelink = "<font color=0000ff><b>".$lang['common']['visitstore']."</b></font>"; }
}


/****************************/

$hdfront = $lang['common']['hdfront'];
$hdadmin = $lang['common']['hdadmin'];
$hdpro = $lang['common']['hdpro'];
$hdabout = $lang['common']['hdabout'];
$hdlogout = $lang['common']['hdlogout'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Administration Area</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?=$path["webroot"]."".$path["skin"]?>style.css" />
<link rel="stylesheet" type="text/css" href="<?=$path["webroot"]."".$path["skin"]?>scrollbar.css" />
<link rel="stylesheet" href="<?=$path["webroot"]?>common/scripts/jqtransformplugin/jqtransform.css" type="text/css" media="all" />
<script language="javascript" src="<?=$path["webroot"]?>common/scripts/jquery-1.8.2.js"></script>
<script language="javascript" src="<?=$path["webroot"]?>common/lib.jsfunction.js"></script>
<script language="javascript" src="<?=$path["webroot"]?>common/scripts/jquery.idTabs.min.js"></script>
<script language="javascript" src="<?=$path["webroot"]?>common/scripts/flexmenu/modernizr.custom.js"></script>
</head>

<body topmargin="0" leftmargin="0">

<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center" class="panel">
	
    <tr>
    	<td width="100%" valign="top">
        	<!--Header-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td width="100%" valign=top>
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%" background="<?=$path["webroot"]."".$path["skin"]?>image/head_bg.jpg">
                        	<tr>
                            	<td align="left" valign="middle" height="116">
                                	<a href="<?=$path["webroot"]?>"><img src="<?=$header_image?>" /></a>
                                </td>
                                
                                <td width="50%" align="right" valign="middle">
                                	<table border="0" width="331" height="41" cellpadding="0" cellspacing="0" style="background-color:#000000;" class="table-topCtrl">
                                    	<tr>
                                        <?		
										if ($component=="" || $component=="common" || $component=="modbuild"){
											
										} else {
											?>
                                            <td align="center" valign="middle">
                                            	<a href="javascript:viewFrontEnd('<?=$component?>');" class="topCtrlfrontend">
                                                	<?=$hdfront?>
                                                </a>
                                            </td>
                                            <?
										}
										
										?>
                                        	<td align="center" valign="middle">
                                            	<a href="<?=$path["webroot"]?>index.php?component=common&page=wce.info.php" class="topCtrlinfo">
                                                	<?=$hdadmin?>
                                                </a>
                                            </td>
                                        <?
										if($oUser->checkAdmin()){
											?>
											<td align="center" valign="middle">
                                            	<a href="<?=$path["webroot"]?>index.php?component=modbuild&page=wce.modbuild.php&headfoot=no" class="topCtrlpro">
                                                	<?=$hdpro?>
                                                </a>
                                            </td>
                                            <?
										}
										?>
                                        	<td align="center" valign="middle">
                                            	<a href="<?=$path["webroot"]?>index.php?pageaction=logout" class="topCtrllogout">
                                                	<?=$hdlogout?>
                                                </a>
                                            </td>
                                            
                                        </tr>
                                       
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			<!-- End of Header -->
            
            <?
			$nav_component = "";
			$nav_modbuild = "";
			$nav_3rdparty = "";
			$oSequence->data = array("seq_id","type","module","seq");
			$oSequence->order = "seq";
			$result = $oSequence->getList();
			$countMenuComponent = 0;
			$countMenuModBuild = 0;
			$countMenuThirdParty = 0;
			$countMenuBorder = "";
			$currentMenuClass = "";
			if ($wtstats) $component = "";
				while($myrow = mysql_fetch_row($result)){
					$currentMenuClass = "";
					$display_menu = "No";
					//Components
					if($myrow[1]=="component"){
						if ($countMenuComponent <= 0) $countMenuBorder = "border-left:0;"; 
						$oComponent->data = array("name","menulink","shortname");		
						$result1 = $oComponent->getDetail($myrow[2]);
						if($myrow1 = mysql_fetch_row($result1)){
							if ($oUser->checkPermission($myrow1[2])){
								if($lang[$myrow1[2]]['menutitle']!=""){	$menutitle = $lang[$myrow1[2]]['menutitle']; }else{ $menutitle=$myrow1[0]; }
									if ($myrow1[2] == $component) {
										$iconComponentCurrent = $myrow1[2]."-current.png";
										$currentMenuClass = "currentmnuButtonActive";
									} else {
										$iconComponentCurrent = $myrow1[2].".png";
									}
									
									$menuicon = $path["webroot"].$path["skin"]."icon/$iconComponentCurrent";
									$menulink = $path["webroot"]."index.php?component=$myrow1[2]&page=$myrow1[1]";
									$display_menu = "Yes";
								}					
							}
							
							if($display_menu=="Yes"){
								$menutitleClass = preg_replace('/\s+/', '', $menutitle);
								$nav_component .='
								<div class="main-navigation '.$currentMenuClass.'">
									<a href="'.$menulink.'" target="'.$menutarget.'" class="mnuButton icon'.$menutitleClass.'" style="'.$countMenuBorder.'">
										<img src="'.$menuicon.'" border="0"><br />
										'.$menutitle.'
									</a>
								</div>';    
                            }
                            $countMenuComponent++;  
							$countMenuBorder = "";
							mysql_free_result($result1);
						}
											
						//ModBuild
						if($myrow[1]=="modbuild"){
							if ($countMenuModBuild <= 0) $countMenuBorder = "border-left:0;"; 
							$oModule->data = array("mod_id","mod_name","mod_icon");
							$result1 = $oModule->getDetail($myrow[2]);
							if($myrow1 = mysql_fetch_row($result1)){
								$menuicon = ($myrow1[2]!=""?$path["webroot"]."_files/modbuild/$myrow1[2]":$path["webroot"]."common/image/blank.gif");
								$menutitle = $myrow1[1];
								$menulink = "index.php?component=modbuild&page=wce.backend.php&mod_id=$myrow1[0]";
								$display_menu = "Yes";
							}
													
							if($display_menu=="Yes"){
								$menutitleClass = preg_replace('/\s+/', '', $menutitle);
								$nav_modbuild .='
								<div class="main-navigation">
									<a href="'.$menulink.'" target="'.$menutarget.'" class="mnuButton icon'.$menutitleClass.'" style="'.$countMenuBorder.'">
										<img src="'.$menuicon.'" border="0"><br />
										'.$menutitle.'
									</a>
								</div>';    
                            }
							$countMenuModBuild++;
							$countMenuBorder = "";
							mysql_free_result($result1);
						}
												
						//3rdParty
						if($myrow[1]=="3rdparty"){
							if ($countMenuThirdParty <= 0) $countMenuBorder = "border-left:0;"; 
							$oThirdParty->data = array("app_id","name","url","icon","targetwin");
							$result1 = $oThirdParty->getDetail($myrow[2]);
							if($myrow1 = mysql_fetch_row($result1)){
								$thisuser = $oUser->getUsername();
								$oThirdParty_Param->data = array("app_param","app_value");
								$oThirdParty_Param->where = "app_id='$myrow1[0]' and app_user='$thisuser'";
								$resultparam = $oThirdParty_Param->getList();
								while($myrow1param = mysql_fetch_row($resultparam)){
									$str_param .="$myrow1param[0]=$myrow1param[1]&";
								}
								mysql_free_result($resultparam);
								$menuicon = ($myrow1[3]!=""?$path["webroot"]."_files/modbuild/$myrow1[3]":$path["webroot"]."common/image/blank.gif");
								$menutitle = $myrow1[1];
								$menulink = $myrow1[2]."?".$str_param;
								$menutarget = $myrow1[4];
								$display_menu = "Yes";
							}
													
							if($display_menu=="Yes"){
								$menutitleClass = preg_replace('/\s+/', '', $menutitle);
								$nav_3rdparty .='
								<div class="main-navigation">
									<a href="'.$menulink.'" target="'.$menutarget.'" class="mnuButton icon'.$menutitleClass.'" style="'.$countMenuBorder.'">
										<img src="'.$menuicon.'" border="0"><br />
										'.$menutitle.'
									</a>
								</div>';    
                            }
							$countMenuThirdParty++;
							$countMenuBorder = "";						
							mysql_free_result($result1);
						}		
													
				}
				mysql_free_result($result);
											
			?>
            <!--Menu-->
            <div id="component-nav" class="usual"> 
                        	<ul class="component-newposition">
                            	<? if ($nav_component!="") { ?>
                                	<li><a href="#nav-component" class="selected"><?=$lang['common']['component']?></a></li>
								<? } ?>
                                <? if ($nav_modbuild!="") { ?>
                                	<li><a href="#nav-modbuild"><?=$lang['common']['modbuild']?></a></li>
								<? } ?>
                                <? if ($nav_3rdparty!="") { ?>
                                	<li><a href="#nav-thirdparty"><?=$lang['common']['thirdparty']?></a></li>
								<? } ?>
                            </ul>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
            	<tr>
                	<td width="100%">
                    	
                                
                            <div style="clear:both; height:0; padding:0;"></div>
                            <? if ($nav_component!="") { ?>
                        	   	<div id="nav-component"><?=$nav_component?></div>
							<? } ?>
                            <? if ($nav_modbuild!="") { ?>
                               	<div id="nav-modbuild"><?=$nav_modbuild?></div>
							<? } ?>
                            <? if ($nav_3rdparty!="") { ?>
                               	<div id="nav-thirdparty"><?=$nav_3rdparty?></div>
							<? } ?>  
                        
                        <script type="text/javascript"> 
							$("#component-nav ul").idTabs(); 
						</script>
                    	<div style="clear:both; height:0; padding:0;"></div>        
                	</td>
            	</tr>
            </table>
            </div>
            <script src="<?=$path["webroot"]?>common/scripts/flexmenu/jquery.js"></script>
            <!-- In a production site, it would be a good idea to combine the following three scripts. -->
            <script src="<?=$path["webroot"]?>common/scripts/flexmenu/flexmenu.min.js"></script>
            <script type="text/javascript">
                //$('#mainNavbar').flexMenu();
            </script>
            <script src="<?=$path["webroot"]?>common/scripts/flexmenu/ios-orientationchange-fix.min.js"></script>
            <!-- End of Menu -->
            <!-- Body -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" height="350" background="<?=$bgimage?>">
            	<tr>
                	<td width="100%" valign="top">
                    	<table border="0" cellpadding="0" cellspacing="0" width="100%">
                        	<tr>
                            	<td width="100%" bgcolor="ffffff" align="center">
                                		
                                </td>
                            </tr>					
                        </table>
                    </td>
                </tr>
            	<tr>
                	<td width="100%" valign="top" align="center">
                    	<!--Content-->
                    	<table border="0" cellpadding="0" cellspacing="0" width="98%">
                        	<tr>
                            	<td valign="top">
                                	<br />
                                    <script language=javascript>
									function viewFrontEnd(component){
										window.open("index.php?component="+component+"&page=wce.frontend.php&headfoot=no","FrontEndView","width=800,height=600,top=0,left=10,toolbar=1,menubar=1,location=1,titlebar=yes,scrollbars=yes,resizable=yes");	
									}
								</script>