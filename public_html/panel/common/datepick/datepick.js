// Title: Timestamp picker
// Description: See the demo at url
// URL: http://us.geocities.com/tspicker/
// Script featured on: http://javascriptkit.com/script/script2/timestamp.shtml
// Version: 1.0
// Date: 12-05-2001 (mm-dd-yyyy)
// Author: Denis Gritcyuk <denis@softcomplex.com>; <tspicker@yahoo.com>
// Notes: Permission given to use this script in any kind of applications if
//    header lines are left unchanged. Feel free to contact the author
//    for feature requests and/or donations


function load_calendar(str_target, str_datetime) {
	var arr_months = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"];
	var week_days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
	var n_weekstart = 1; // day week starts from (normally 0 or 1)

	var dt_datetime = (str_datetime == null || str_datetime =="" || str_datetime =="0000-00-00" ?  new Date() : str2dt(str_datetime));

	var dt_prev_month = new Date(dt_datetime);
	dt_prev_month.setMonth(dt_datetime.getMonth()-1);

	var dt_next_month = new Date(dt_datetime);
	dt_next_month.setMonth(dt_datetime.getMonth()+1);
	var dt_firstday = new Date(dt_datetime);
	dt_firstday.setDate(1);

	dt_firstday.setDate(1-(7+dt_firstday.getDay()-n_weekstart)%7);
	var dt_lastday = new Date(dt_next_month);
	dt_lastday.setDate(0);
	

	// html generation (feel free to tune it for your particular application)
	// print calendar header
	var str_buffer = new String (
		"<html>\n"+
		"<head><title>Calendar</title></head>\n"+
		"<body style=\"background-color:transparent\">\n"+
		"<table class=\"clsOTable\" cellspacing=\"0\" cellspadding=\"0\" border=\"0\" width=\"100%\" style=\"position:absolute;left:0px;top:0px\">\n"+
		"<tr><td bgcolor=\"#CD3027\">\n"+
		"<table cellspacing=\"1\" cellpadding=\"3\" border=\"0\" width=\"100%\">\n "+
		"<tr>\n	<td bgcolor=\"#CD3027\"><a href=\"javascript:parent.show_calendar2('"+str_target+"','"+ dt2dtstr(dt_prev_month)+"');\">"+
		"<img src=\"common/datepick/arwprev.gif\" width=\"16\" height=\"16\" border=\"0\""+
		" alt=\"previous month\"></a></td>\n"+
		"	<td bgcolor=\"#CD3027\" colspan=\"5\">"+
		"<font color=\"white\" face=\"tahoma, verdana\" size=\"2\">"
		+arr_months[dt_datetime.getMonth()]+" "+dt_datetime.getFullYear()+"</font></td>\n "+
		"	<td bgcolor=\"#CD3027\" align=\"right\"><a href=\"javascript:parent.show_calendar2('"+str_target+"','"+dt2dtstr(dt_next_month)+"');\">"+
		"<img src=\"common/datepick/arwnext.gif\" width=\"16\" height=\"16\" border=\"0\""+
		" alt=\"next month\"></a></td>\n</tr>\n"
	);

	var dt_current_day = new Date(dt_firstday);
	// print weekdays titles
	str_buffer += "<tr>\n";
	for (var n=0; n<7; n++)
		str_buffer += "	<td bgcolor=\"#E3B0AC\">"+
		"<font color=\"white\" face=\"tahoma, verdana\" size=\"2\">"+
		week_days[(n_weekstart+n)%7]+"</font></td>\n";
	// print calendar table
	str_buffer += "</tr>\n";
	while (dt_current_day.getMonth() == dt_datetime.getMonth() || dt_current_day.getMonth() == dt_firstday.getMonth()) {
		// print row heder
		str_buffer += "<tr>\n";
		for (var n_current_wday=0; n_current_wday<7; n_current_wday++) {
				if (dt_current_day.getDate() == dt_datetime.getDate() && dt_current_day.getMonth() == dt_datetime.getMonth())
					// print current date
					str_buffer += "	<td bgcolor=\"#FFE88C\" align=\"right\">";
				else if (dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
					// weekend days
					str_buffer += "	<td bgcolor=\"#F8D9D7\" align=\"right\">";
				else
					// print working days of current month
					str_buffer += "	<td bgcolor=\"white\" align=\"right\">";

				if (dt_current_day.getMonth() == dt_datetime.getMonth())
					// print days of current month
					str_buffer += "<a href=\"javascript:parent.document.getElementById('"+str_target+
					"').value='"+dt2dtstr(dt_current_day)+"'; parent.hide_calendar();\" style=\"text-decoration:none\">"+
					"<font color=\"black\" face=\"tahoma, verdana\" size=\"2\">";
				else 
					// print days of other months
					str_buffer += "<a href=\"javascript:parent.document.getElementById('"+str_target+
					"').value='"+dt2dtstr(dt_current_day)+"'; parent.hide_calendar();\" style=\"text-decoration:none\">"+
					"<font color=\"gray\" face=\"tahoma, verdana\" size=\"2\">";
				str_buffer += dt_current_day.getDate()+"</font></a></td>\n";
				dt_current_day.setDate(dt_current_day.getDate()+1);
		}
		// print row footer
		str_buffer += "</tr>\n";
	}
	// print calendar footer
	

	return str_buffer;
}

function show_calendar(str_target,str_datetime){
	var str=load_calendar(str_target,str_datetime);
	var iCaldoc = iCal.document;
	iCaldoc.write(str);
	iCaldoc.close();


	el = document.getElementById(str_target);

	xPos = el.offsetLeft;
	tempEl = el.offsetParent;
	while (tempEl != null) {
		xPos += tempEl.offsetLeft;
		tempEl = tempEl.offsetParent;
	}

	yPos = el.offsetTop;
	tempEl = el.offsetParent;
	while (tempEl != null) {
		yPos += tempEl.offsetTop;
		tempEl = tempEl.offsetParent;
	}
	
	if (document.getElementById('iCal').style.left=="-500px"){
		$('#iCal').css('top', yPos + el.offsetHeight + 2);
		$('#iCal').css('left', xPos);
		//document.getElementById('iCal').style.top = yPos + el.offsetHeight + 2;
		//document.getElementById('iCal').style.left = xPos;
	} else {
		hide_calendar();
	}
}

function show_calendar2(str_target,str_datetime){
	var str=load_calendar(str_target,str_datetime);
	var iCaldoc = iCal.document;
	iCaldoc.write(str);
	iCaldoc.close();


	el = document.getElementById(str_target);

	xPos = el.offsetLeft;
	tempEl = el.offsetParent;
	while (tempEl != null) {
		xPos += tempEl.offsetLeft;
		tempEl = tempEl.offsetParent;
	}

	yPos = el.offsetTop;
	tempEl = el.offsetParent;
	while (tempEl != null) {
		yPos += tempEl.offsetTop;
		tempEl = tempEl.offsetParent;
	}
	
	document.getElementById('iCal').style.top = yPos + el.offsetHeight + 2;
	document.getElementById('iCal').style.left = xPos;
}

function hide_calendar(){
	$('#iCal').css('left', -500);
	//document.getElementById('iCal').style.left=-500;
}

// datetime parsing and formatting routimes. modify them if you wish other datetime format
function str2dt (str_datetime) {
	var re_date = /^(\d\d\d\d)\-(\d\d)\-(\d\d)$/;
	if (!re_date.exec(str_datetime))
		return alert("Invalid Datetime format: "+ str_datetime);
	return (new Date (RegExp.$1, RegExp.$2-1, RegExp.$3));
}
function dt2dtstr (dt_datetime) {
	return (new String (
			dt_datetime.getFullYear()+"-"+pd02((dt_datetime.getMonth()+1))+"-"+pd02(dt_datetime.getDate())));
}
function dt2tmstr (dt_datetime) {
	return (new String (
			dt_datetime.getHours()+":"+dt_datetime.getMinutes()+":"+dt_datetime.getSeconds()));
}

function pd02(number){
	number = ""+number;

	if (number.length==0)
		return "00";
	else if (number.length==1)
		return "0" + number;
	else
		return number;
}
