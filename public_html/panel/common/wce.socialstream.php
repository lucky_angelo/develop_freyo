<? if((($oSystem->getValue('sys_socialmedia_tw')!="") || ($oSystem->getValue('sys_socialmedia_fb')!="") || ($oSystem->getValue('sys_socialmedia_yt')!="") || ($oSystem->getValue('sys_socialmedia_fk')!="") || ($oSystem->getValue('sys_socialmedia_da')!="") || ($oSystem->getValue('sys_socialmedia_tl')!="")) || isset($_POST['search_ss'])) { ?>
	<? /*Social Stream*/ ?>
	<script type="text/javascript">
	jQuery(document).ready(function($){
		$('#social-stream').dcSocialStream({
			feeds: {
				<? if (!isset($_POST['search_ss']) || $_POST['search_ss']=="") { ?>
					<? if($oSystem->getValue('sys_socialmedia_tw')!="") { ?>twitter: {
						id: '<?=$oSystem->getValue('sys_socialmedia_tw')?>',
						thumb: true
					}, <? } ?> <? if($oSystem->getValue('sys_socialmedia_fb')!="") { ?>facebook: {
						id: '<?=$oSystem->getValue('sys_socialmedia_fb_pg')?>'
					},<? } ?><? if($oSystem->getValue('sys_socialmedia_yt')!="") { ?>youtube: {
						id: '#<?=$oSystem->getValue('sys_socialmedia_yt')?>'
					},<? } ?><? if($oSystem->getValue('sys_socialmedia_fk')!="") { ?>flickr: {
						id: '<?=$oSystem->getValue('sys_socialmedia_fk')?>'
					},<? } ?><? if($oSystem->getValue('sys_socialmedia_da')!="") { ?>deviantart: {
						id: '<?=$oSystem->getValue('sys_socialmedia_da')?>'
					},<? } ?><? if($oSystem->getValue('sys_socialmedia_tl')!="") { ?>tumblr: {
						id: '<?=$oSystem->getValue('sys_socialmedia_tl')?>',
						thumb: 250
					}
					<? } ?>
				<? } else { ?>
					twitter: {
						id: '<?=$_POST['search_ss']?>',
						thumb: true
					},
					delicious: {
						id: '<?=$_POST['search_ss']?>',
						out: 'intro'
					},
					vimeo: {
						id: '<?=$_POST['search_ss']?>',
						out: 'intro'
					},
					youtube: {
						id: '<?=$_POST['search_ss']?>'
					},
					dribbble: {
						id: '<?=$_POST['search_ss']?>',
						out: 'intro'
					},
					deviantart: {
						id: '<?=$_POST['search_ss']?>',
						out: 'intro'
					}
					<? if (is_int($_POST['search_ss'])) { ?>
					, facebook: {
						id: '<?=$_POST['search_ss']?>'
					}
					<? } ?>
					
				<? } ?>
			},
			rotate: {
				delay: 0
			},
			limit: 50,
			control: false,
			filter: true,
			wall: true,
			order: 'random',
			max: 'limit',
			iconPath: 'common/scripts/socialstream/images/dcsns-light/',
			imagePath: 'common/scripts/socialstream/images/dcsns-light/'
		});
					 
	});
	</script>
	<? /*End of Social Stream*/ ?>
	<table width="100%" cellpaddin="0" cellspacing="0" border="0">
    	<tr>
        	<td align="left">
            	<form method="post" action="index.php?component=common&page=wce.socialstream.php">
                	<input type="text" name="search_ss" value="<?=(($_POST['search_ss'])?$_POST['search_ss']:"")?>" placeholder="Search Twitter, Youtube, Deviant Art, etc." style="width:250px;" /> <input type="submit" name="search_go" value="Go" />
                </form>
            </td>
        </tr>
		<tr>
			<td align="center">
				<? include($path["docroot"]."common/scripts/socialstream/socialstream.php"); ?>
			</td>
		</tr>
	</table>
<? } else { ?>
	<table width="100%" cellpaddin="0" cellspacing="0" border="0">
		<tr>
			<td align="center">
				<?=$lang['common']['socialmedia_return']?>
			</td>
		</tr>
	</table>
<? } ?>