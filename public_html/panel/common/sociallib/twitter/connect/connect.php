<?
include("../include/webzone.php");

if(isset($_GET['status'])) {
	$status = $_GET['status'];
	$t1 = new Twitter_class();
	$result = $t1->updateTwitterStatus(array("status"=>$status));
	$status_id = $result->id;
	$screen_name = $result->user->screen_name;
} elseif($_GET['logout']==1) {
	unset($_SESSION['twitter_access_token']);
	header('Location: ' . $_SERVER['HTTP_REFERER']);
} else {
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	$tok = $connection->getRequestToken(OAUTH_CALLBACK);
	
	$_SESSION['twitter_oauth_token'] = $tok['oauth_token'];
	$_SESSION['twitter_oauth_token_secret'] = $tok['oauth_token_secret'];
	
	$request_link = $connection->getAuthorizeURL($tok['oauth_token']);
	
	//save referer to redirect to it once connected
	$_SESSION['HTTP_REFERER'] = $_SERVER['HTTP_REFERER'];
	
	header('Location: ' . $request_link);
}

?>