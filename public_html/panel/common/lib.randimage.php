<?

	include_once("../config.php");
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		include_once($path["docroot"]."common/session.php");
	}
		
	$alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$rand_str = substr(str_shuffle($alphanum), 0, 5);

	$_SESSION['image_value'] = md5($rand_str);	

	$letter1=substr($rand_str,0,1);
	$letter2=substr($rand_str,1,1);
	$letter3=substr($rand_str,2,1);
	$letter4=substr($rand_str,3,1);
	$letter5=substr($rand_str,4,1);
	
	$image=imagecreatefrompng("image/noise.png");
	
	$angle1 = rand(-20, 20);
	$angle2 = rand(-20, 20);
	$angle3 = rand(-20, 20);
	$angle4 = rand(-20, 20);
	$angle5 = rand(-20, 20);
	
	$font1 = "fonts/2.ttf";
	$font2 = "fonts/2.ttf";
	$font3 = "fonts/3.ttf";
	$font4 = "fonts/5.ttf";
	$font5 = "fonts/10.ttf";
	
	$colors[0]=array(0,130,0);
	$colors[1]=array(0,0,0);
	$colors[2]=array(192,0,192);
	$colors[3]=array(37,1,209);
	$colors[4]=array(240,16,31);
	$colors[5]=array(56,99,169);
	
	$color1=rand(0, 5);
	$color2=rand(0, 5);
	$color3=rand(0, 5);
	$color4=rand(0, 5);
	$color5=rand(0, 5);
	
	$textColor1 = imagecolorallocate ($image, $colors[$color1][0],$colors[$color1][1], $colors[$color1][2]);
	$textColor2 = imagecolorallocate ($image, $colors[$color2][0],$colors[$color2][1], $colors[$color2][2]);
	$textColor3 = imagecolorallocate ($image, $colors[$color3][0],$colors[$color3][1], $colors[$color3][2]);
	$textColor4 = imagecolorallocate ($image, $colors[$color4][0],$colors[$color4][1], $colors[$color4][2]);
	$textColor5 = imagecolorallocate ($image, $colors[$color5][0],$colors[$color5][1], $colors[$color5][2]);
	
	$size = 12;
	imagettftext($image, $size, $angle1, 5, $size+3, $textColor1, $font1, $letter1);
	imagettftext($image, $size, $angle2, 20, $size+3, $textColor2, $font2, $letter2);
	imagettftext($image, $size, $angle3, 35, $size+3, $textColor3, $font3, $letter3);
	imagettftext($image, $size, $angle4, 50, $size+3, $textColor4, $font4, $letter4);
	imagettftext($image, $size, $angle5, 65, $size+3, $textColor5, $font5, $letter5);

	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	header('Content-type: image/jpeg');
	imagejpeg($image);
	imagedestroy($image);
?>

