<?

class System extends Item{
	
	var $db;
	
	function System(){}
	
	function getValue($attribute){
		$sql="select value from sys_variable where attribute='$attribute'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];		}
		mysql_free_result($result);	
	}
	
	function setValue($attribute,$value){
		$sql="select value from sys_variable where attribute='$attribute'";
		$result=mysql_query($sql,$this->db);
		if(mysql_num_rows($result)!=0)
			$sql="update sys_variable set value='$value' where attribute='$attribute'";
		else
			$sql="insert into sys_variable(attribute,value) values('$attribute','$value')";
		mysql_query($sql,$this->db);
	}
	
	function getCompList(){
		$this->getdatalist();	$this->getwherelist();	$this->getorderlist();
		$sql="select $this->datalist from sys_component $this->wherelist $this->orderlist";
		$result=mysql_query($sql,$this->db);
		return $result;				
	}
	
	function getAllKeys(){
		$sql="select shortname,version, serialkey from sys_component where serialkey not like ''";
		$result=mysql_query($sql,$this->db);
		return $result;
	}
	
	function securitycheck($web) {
		$count = 0;
		$banned_date = '';
		$now = date('Y-m-d H:i:s');
		$ip = $_SERVER['REMOTE_ADDR'];
		$report = 0;
		
		$time_interval = 30*60;
		$time_idle = 300*60;
		
		$return = true;
		
		//check and update if the last try to login is last 5 hours
		$sql = "SELECT * FOM `sys_logs` WHERE `log_status` = 'Failed' AND `ip_used` = '".$ip."' AND `notify_to_admin` = 'No' AND `banned` = '0'";
		$result=mysql_query($sql,$this->db);
		if($result) {
			while($row = mysql_fetch_array($result)) {
				if(time() >= strtotime('+ 5 hour', $row['log_date'])) {
					$sql = "UPDATE `sys_logs` SET `banned` = '1' WHERE id = '".$row['id']."'";
					mysql_query($sql,$this->db);
				}
			}
		}
		
		//check how many tries does the ip logged in
		$sql = "SELECT COUNT(*) as `num` FROM `sys_logs` WHERE log_status='Failed' AND 
															   notify_to_admin = 'No' AND 
															   banned = '0' AND 
															   ip_used = '".$ip."'";
		$result=mysql_query($sql,$this->db);
		if($result) list($count) = mysql_fetch_row($result);
		
		//banned ip if it is equal or greater than 3 times
		if($count>=3) {
			$sql = "UPDATE `sys_logs` SET `banned` = '1' WHERE `log_status` = 'Failed' AND `ip_used` = '".$ip."' AND `notify_to_admin` = 'No' AND `banned` = '0'";
			mysql_query($sql,$this->db);
			
			$sql = "INSERT INTO `sys_logs_banned_ip`(`banned_ip`,`banned_date`)VALUES('".$ip."','".$now."')";
			mysql_query($sql,$this->db);
		}
		
		//checked if the banned date is expires
		$sql = "SELECT DATE_ADD(`banned_date`, INTERVAL 30 MINUTE) AS banned_passed FROM `sys_logs_banned_ip` WHERE `banned_ip` = '".$ip."' AND `banned_status` = 'banned'";
		$result2=mysql_query($sql,$this->db);
		if($result2) list($banned_date) = mysql_fetch_row($result2);
		
		if(time() >= strtotime($banned_date)) {
			$sql = "UPDATE `sys_logs_banned_ip` SET `banned_status` = 'passed' WHERE `banned_ip` = '".$ip."' AND `banned_status` = 'banned'";
			mysql_query($sql,$this->db);
			
			$return = false;
		} 
		
		
		$sql = "SELECT COUNT(*) AS report FROM `sys_logs_banned_ip` WHERE `banned_ip` = '".$ip."'";
		$result3=mysql_query($sql,$this->db);
		if($result3) list($report) = mysql_fetch_row($result3);
		
		if($report > 0 && ($report % 2 == 0)) {
			$emailto = "support@leentechsystems.com";
			$subject = "Website Warning Alert.";
			$body = "Please check the logs for more information.";
			$body .= "\r\n";
			$body .= "Website: ".$web."";
			$body .= "\r\n";
			$body .= "IP: ".$_SERVER['REMOTE_ADDR']."";
			$body .= "\r\n";
			$body .= "Date: ".date('Y-m-d H:i:s')."";
			$this->mail($emailto,$subject,$body,$emailto);
		}
		
		return $return;
	}

	function mail($to,$subject,$message,$from,$html="",$attachment="",$customheader=""){
		
		if($this->getValue("sys_mailtype")=="smtp"){

			$charset = $this->getValue("sys_mailcharset");			
			if($charset==""){ $charset = "iso-8859-1"; }	

			$oMail = new PHPMailer();
			$oMail->IsSMTP();                      								
			$oMail->Host = $this->getValue("sys_smtphost");
			$oMail->Port = $this->getValue("sys_smtpport");
			$oMail->CharSet = $charset;
			$oMail->Username = $this->getValue("sys_smtpuser");
			$oMail->Password = $this->getValue("sys_smtppass");		
			$oMail->SMTPAuth = true;
			$oMail->From = $from;
			$oMail->FromName = $from;
			$oMail->AddAddress($to);
			
			//$oMail->WordWrap = 100;  
			if($html!=""){ 
					$oMail->IsHTML(true);
			}
						
			if($attachment!=""){ $oMail->AddAttachment($attachment); }
			
			if($customheader!=""){ 
					$oMail->AddCustomHeader("X-OneAdminService:".$customheader);
			}
	
			$oMail->Subject = $subject;
			$oMail->Body    = $message;
			$oMail->Send();
			
			$oMail->ClearAddresses();
   			if($attachment!=""){ $oMail->ClearAttachments(); }
		
		}else{
						
			$charset = $this->getValue("sys_mailcharset");			
			if($charset==""){ $charset = "iso-8859-1"; }
						
			if($html!=""){
				$mailfrom = "From: $from\r\nReturn-Path: $from\r\nMIME-Version: 1.0\r\nContent-Type: text/html; charset=$charset";	
			}else{
				$mailfrom = "From: $from\r\nReturn-Path: $from";	
			}
						
			mail($to,$subject,$message,$mailfrom);		
		}
	
	}

}

?>