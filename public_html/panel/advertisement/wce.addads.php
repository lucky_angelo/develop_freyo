<? // START Adding of ADS
$submitAction="";
if($pageaction == "add") {
    if(isset($location)){
        $location = implode(',', $location);
    }
    
    if($section_id==0) {
        $section="all";
    }
    $priority=999;
    if($section=="issue" && isset($gender) && isset($age) && isset($location)) {
        $priority=1;
    }
    else if(($section=="issue" && isset($age) && isset($location)) || ($section=="issue" && isset($gender) && isset($location)) || ($section=="issue" && isset($age) && isset($gender))) {
        $priority=2;
    }
    else if(($section=="issue" && isset($gender)) || ($section=="issue" && isset($location)) || ($section=="issue" && isset($age))) {
        $priority=3;
    }
    else if($section=="issue") {
        $priority=4;
    }
    else if($section=="magazine" && isset($gender) && isset($age) && isset($location)) {
        $priority=5;
    }
    else if(($section=="magazine" && isset($age) && isset($location)) || ($section=="magazine" && isset($gender) && isset($location)) || ($section=="magazine" && isset($age) && isset($gender))) {
        $priority=6;
    }
    else if(($section=="magazine" && isset($gender)) || ($section=="magazine" && isset($location)) || ($section=="magazine" && isset($age))) {
        $priority=7;
    }
    else if($section=="magazine") {
        $priority=8;
    }
    else if($section=="category" && isset($gender) && isset($age) && isset($location)) {
        $priority=9;
    }
    else if(($section=="category" && isset($age) && isset($location)) || ($section=="category" && isset($gender) && isset($location)) || ($section=="category" && isset($age) && isset($gender))) {
        $priority=10;
    }
    else if(($section=="category" && isset($gender)) || ($section=="category" && isset($location)) || ($section=="category" && isset($age))) {
        $priority=11;
    }
    else if($section=="category") {
        $priority=12;
    }
    else if(isset($gender) && isset($age) && isset($location)) {
        $priority=13;
    }
    else if((isset($age) && isset($location)) || (isset($gender) && isset($location)) || (isset($age) && isset($gender))) {
        $priority=14;
    }
    else if(isset($gender) || isset($location) || isset($age)) {
        $priority=15;
    }
    else {
        $priority=16;
    }
    $fadsName=strtolower(str_replace(" ", "_", trim($_FILES['ads_content']['name'])));
    if($type == "video"){
        $fadsNameWEBM = strtolower(str_replace(" ", "_", trim($_FILES['ads_webm']['name'])));
        $fadsNameOGV = strtolower(str_replace(" ", "_", trim($_FILES['ads_ogv']['name'])));
    }
    $uniqueid=md5(uniqid(rand(), true));
    $uniqueid=substr($uniqueid, 0, 5);

    if($fadsName!="") {
        $fadsName = $uniqueid."_".$fadsName;
        if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
            $oAds->uploadFile($_FILES['ads_content']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsName);
            if($fadsNameWEBM!=""){
                $fadsNameWEBM=$uniqueid."_".$fadsNameWEBM;
                $oAds->uploadFile($_FILES['ads_webm']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsNameWEBM);
            }
            if($fadsNameOGV!=""){
                $fadsNameOGV=$uniqueid."_".$fadsNameOGV;
                $oAds->uploadFile($_FILES['ads_ogv']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsNameOGV);
            }
        }
    } 
    $oAds->data=array("caption", "type", "section", "section_id", "content", "content_webm", "content_ogv", "priority", "impression", "status", "date_created", "age", "age2", "gender", "location", "ads_timer", "ads_link", "cost_per_view", "cost_per_click");
    $oAds->value=array($caption, $type, $section, $section_id, $fadsName, $fadsNameWEBM, $fadsNameOGV, $priority, $impression, $status, date('Y-m-d H:i:s'), isset($age)?$age:"-1", isset($age2)?$age2:"-1", isset($gender)?$gender:"all", isset($location)?$location:"0", isset($timer)?$timer:"0", $redirect, $costperview, $costperclick);
    // var_dump($oAds->value); die();
    $oAds->add();
    clearServerMemcached();
    $status_message="<b>Status:</b> New advertisement added.<br>";
}

// END Adding ADS
// START Editing ADS
elseif($pageaction=="edit") {
    $oAds->data=array("caption", "type", "section", "section_id", "content", "impression", "status", "age", "age2", "gender", "location", "ads_timer", "content_webm", "content_ogv", "ads_link", "cost_per_view", "cost_per_click");
    $result=$oAds->getDetail($ads_id);
    if($myrow=mysql_fetch_row($result)) {
        $wcaption=stripslashes($myrow[0]);
        $wtype=$myrow[1];
        $wsection=$myrow[2];
        $wsection_id=$myrow[3];
        $dis_content=substr($myrow[4], 6, strlen($myrow[4])-6);
        $dis_content2=substr($myrow[12], 6, strlen($myrow[12])-6);
        $dis_content3=substr($myrow[13], 6, strlen($myrow[13])-6);
        if($myrow[4]!="" && $myrow[1]=="image") {
            $adsContent="$dis_content <a href=\"_files/advertisement/$myrow[4]\" onclick=\"return popImage('_files/advertisement/$myrow[4]','Image')\">View</a>";
        }
        else {
            $adsContent=$dis_content; $adsContentWEBM=$dis_content2; $adsContentOGV=$dis_content3;
        }
        $wimpression=$myrow[5];
        $wstatus=$myrow[6];
        $wage=$myrow[7];
        $wage2=$myrow[8];
        $wgender=$myrow[9];
        $wlocation=$myrow[10];
        $location_id = explode(',', $wlocation);
        $wtimer=$myrow[11];
        $wredirect = $myrow[14];
        $wcostperview = $myrow[15];
        $wcostperclick = $myrow[16];
        if($wsection=="issue") {
            $wwmagazine_id=$wsection_id;
        }
        else {
            $vparent_id=$wsection_id;
        }
    }
    $status_message="<b>Status:</b> Edit advertisement.<br>";
    $submitAction="&start=$next&sortby=$sortby&sortseq=$sortseq&searchtype=$searchtype&s_caption=$s_caption";
}

// END Editing ADS
// START Updating ADS
elseif($pageaction=="update") {
    if(isset($location)){
        $location = implode(',', $location);
    }
    if($section_id==0) {
        $section="all";
    }
    $priority=999;
    if($section=="issue" && isset($gender) && isset($age) && isset($location)) {
        $priority=1;
    }
    else if(($section=="issue" && isset($age) && isset($location)) || ($section=="issue" && isset($gender) && isset($location)) || ($section=="issue" && isset($age) && isset($gender))) {
        $priority=2;
    }
    else if(($section=="issue" && isset($gender)) || ($section=="issue" && isset($location)) || ($section=="issue" && isset($age))) {
        $priority=3;
    }
    else if($section=="issue") {
        $priority=4;
    }
    else if($section=="magazine" && isset($gender) && isset($age) && isset($location)) {
        $priority=5;
    }
    else if(($section=="magazine" && isset($age) && isset($location)) || ($section=="magazine" && isset($gender) && isset($location)) || ($section=="magazine" && isset($age) && isset($gender))) {
        $priority=6;
    }
    else if(($section=="magazine" && isset($gender)) || ($section=="magazine" && isset($location)) || ($section=="magazine" && isset($age))) {
        $priority=7;
    }
    else if($section=="magazine") {
        $priority=8;
    }
    else if($section=="category" && isset($gender) && isset($age) && isset($location)) {
        $priority=9;
    }
    else if(($section=="category" && isset($age) && isset($location)) || ($section=="category" && isset($gender) && isset($location)) || ($section=="category" && isset($age) && isset($gender))) {
        $priority=10;
    }
    else if(($section=="category" && isset($gender)) || ($section=="category" && isset($location)) || ($section=="category" && isset($age))) {
        $priority=11;
    }
    else if($section=="category") {
        $priority=12;
    }
    else if(isset($gender) && isset($age) && isset($location)) {
        $priority=13;
    }
    else if((isset($age) && isset($location)) || (isset($gender) && isset($location)) || (isset($age) && isset($gender))) {
        $priority=14;
    }
    else if(isset($gender) || isset($location) || isset($age)) {
        $priority=15;
    }
    else {
        $priority=16;
    }
    $fadsName=strtolower(str_replace(" ", "_", trim($_FILES['ads_content']['name'])));
    if($type == "video"){
        $fadsNameWEBM = strtolower(str_replace(" ", "_", trim($_FILES['ads_webm']['name'])));
        $fadsNameOGV = strtolower(str_replace(" ", "_", trim($_FILES['ads_ogv']['name'])));
    }
    $uniqueid=md5(uniqid(rand(), true));
    $uniqueid=substr($uniqueid, 0, 5);
    if($fadsName!="" || $fadsNameWEBM!="" || $fadsNameOGV!="") {

        $updateFields = array();
        $updateData = array();
        if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
            if($fadsName!=""){
                $fadsName=$uniqueid."_".$fadsName;
                $oAds->uploadFile($_FILES['ads_content']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsName);
                array_push($updateFields, "content");
                array_push($updateData, $fadsName);
            }
            if($fadsNameWEBM!=""){
                $fadsNameWEBM=$uniqueid."_".$fadsNameWEBM;
                $oAds->uploadFile($_FILES['ads_webm']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsNameWEBM);
                array_push($updateFields, "content_webm");
                array_push($updateData, $fadsNameWEBM);
            }
            if($fadsNameOGV!=""){
                $fadsNameOGV=$uniqueid."_".$fadsNameOGV;
                $oAds->uploadFile($_FILES['ads_ogv']['tmp_name'], $path["docroot"]."_files/advertisement", $fadsNameOGV);
                array_push($updateFields, "content_ogv");
                array_push($updateData, $fadsNameOGV);
            }
        }
        if($type == "image"){
            $updateFields = array("content","content_webm","content_ogv");
            $updateData = array($fadsName, $fadsNameWEBM, $fadsNameOGV);
        }
        $oAds->data=$updateFields;
        $result=$oAds->getDetail($ads_id);
        if($myrow=mysql_fetch_assoc($result)) {
            if($fadsName!=""){
                if(file_exists($path["docroot"]."_files/advertisement/".$myrow['content']) && $myrow['content']!="") {
                    unlink($path["docroot"]."_files/advertisement/".$myrow['content']);
                }
            }
            if($fadsNameWEBM!=""){
                if(file_exists($path["docroot"]."_files/advertisement/".$myrow['content_webm']) && $myrow['content_webm']!="") {
                    unlink($path["docroot"]."_files/advertisement/".$myrow['content_webm']);
                }
            }
            if($fadsNameOGV!=""){
                if(file_exists($path["docroot"]."_files/advertisement/".$myrow['content_ogv']) && $myrow['content_ogv']!="") {
                    unlink($path["docroot"]."_files/advertisement/".$myrow['content_ogv']);
                }
            }
        }
        mysql_free_result($result);

        $oAds->data=$updateFields;
        $oAds->value=$updateData;
        $oAds->update($ads_id);
    }
    $oAds->data=array("caption", "type", "section", "section_id", "priority", "impression", "status", "date_updated", "age", "age2", "gender", "location", "ads_timer", "ads_link","cost_per_view", "cost_per_click");
    $oAds->value=array($caption, $type, $section, $section_id, $priority, $impression, $status, date('Y-m-d H:i:s'), isset($age)?$age:"-1", isset($age2)?$age2:"-1", isset($gender)?$gender:"all", isset($location)?$location:"0", isset($timer)?$timer:"0", $redirect, $costperview, $costperclick);
    $oAds->update($ads_id);
    $oAds->data=array("caption", "type", "section", "section_id", "content", "impression", "status", "age", "age2", "gender", "location", "ads_timer", "content_webm", "content_ogv", "ads_link","cost_per_view", "cost_per_click");
    $result=$oAds->getDetail($ads_id);
    if($myrow=mysql_fetch_row($result)) {
        $wcaption=stripslashes($myrow[0]);
        $wtype=$myrow[1];
        $wsection=$myrow[2];
        $wsection_id=$myrow[3];
        $dis_content=substr($myrow[4], 6, strlen($myrow[4])-6);
        $dis_content2=substr($myrow[12], 6, strlen($myrow[12])-6);
        $dis_content3=substr($myrow[13], 6, strlen($myrow[13])-6);
        if($myrow[4]!="" && $myrow[1]=="image") {
            $adsContent="$dis_content <a href=\"_files/advertisement/$myrow[4]\" onclick=\"return popImage('_files/advertisement/$myrow[4]','Image')\">View</a>";
        }
        else {
            $adsContent=$dis_content; $adsContentWEBM=$dis_content2; $adsContentOGV=$dis_content3;
        }
        $wimpression=$myrow[5];
        $wtimer=$myrow[11];
        $wstatus=$myrow[6];
        $wage=$myrow[7];
        $wage2=$myrow[8];
        $wgender=$myrow[9];
        $wlocation=$myrow[10];
        $wredirect = $myrow[14];
        $wcostperview = $myrow[15];
        $wcostperclick = $myrow[16];
        $location_id = explode(',', $wlocation);
        if($wsection=="issue") {
            $wwmagazine_id=$wsection_id;
        }
        else {
            $vparent_id=$wsection_id;
        }
    }
    // clearServerMemcached();
    $status_message="<b>Status:</b> Advertisement updated.<br>";
    $submitAction="&start=$next&sortby=$sortby&sortseq=$sortseq&searchtype=$searchtype&s_caption=$s_caption";
}
// END Updating ADS
?> <table border=0 width=100%><tr><td><b>Advertisement Manager</b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br> <!--Body Start-->
<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=advertisement&page=wce.addads.php<? echo $submitAction; ?>" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="add">
<input type=hidden name="ads_id" value="<? echo $ads_id; ?>">
<tr><td width="150">Status</td>
<td><select name="status" required> 
<option value="a" <? echo (($wstatus=="a")?"selected='selected'":"")?>>Approved</option>
<option value="p" <? echo (($wstatus=="p")?"selected='selected'":"")?>>Pending</option>
<option value="e" <? echo (($wstatus=="e")?"selected='selected'":"")?>>Expired</option>
</select></td></tr>
<tr><td>Type</td><td><select name="type" id="ads_type" required>
<option value="image" <? echo (($wtype=="image")?"selected='selected'":"")?>>Image/GIF</option>
<option value="video" <? echo (($wtype=="video")?"selected='selected'":"")?>>Video</option>
 <!-- <option value="form" <? echo (($wtype=="form")?"selected='selected'":"")?>>Form</option> --> </select></td></tr>
 <tr><td>Section</td><td>
 <input type="radio" class='bannerSection' name="section" value="category" checked="checked" />
<label for="section_global">Category</label> &nbsp;
<input type="radio" class='bannerSection' name="section" value="magazine" />
<label for="section_global">Magazine</label> &nbsp;
<input type="radio" class='bannerSection' name="section" value="issue" />
<label for="section_global">Issue</label></td></tr>
<tr id="magazine-category"><td>Magazine Category</td>
<td><select id="section_category" name="section_id"><option value="0">All</option><?php print $oMagz_Category->getCategoryTree(0, TRUE)?></select></td></tr>
<tr id="magazine-magazine" style="display:none;"><td>Magazine</td><td><select id="section_magazine" name="section_id"><?php print $oMagz_Category->getCategoryTree(0, FALSE, TRUE)?></select></td></tr>
<tr id="magazine-issue" style="display:none;"><td>Issue</td><td><select id="section_issue" name="section_id"><?php print $oMagz->getMagazineList() ?></select></td></tr>

<tr class="ads_image_row"><td>Advertisement<br/>(1242 x 1606px)</td><td><input class="ads_image" type='file' name='ads_content' <?echo $pageaction=="add"?'required':"";?>><? print $adsContent;?></td></tr>

<tr class="ads_video_row" style="display:none;">
<td>Advertisement</td>
<td>MP4: <input id="ads_video" class="ads_video" type='file' name='ads_content' accept="video/mp4" <?echo $pageaction=="add"?'required':"";?>><? print $adsContent;?></td>
</tr>

<tr class="ads_video_row" style="display:none;">
<td></td>
<td>WEBM: <input id="ads_video2" class="ads_video" type="file" name="ads_webm" style="width:230px" accept="video/webm" <?echo $pageaction=="add"?'required':"";?>> <? echo $adsContentWEBM ?></td>
</tr>

<tr class="ads_video_row" style="display:none;">
<td></td>
<td>OGV: <input id="ads_video3" class="ads_video" type="file" name="ads_ogv" style="width:230px" accept="video/ogg" <?echo $pageaction=="add"?'required':"";?>> <? echo $adsContentOGV ?></td>
</tr>

<tr><td>Caption</td><td><input type="text" name="caption" value="<?print $wcaption;?>" style="" required /> * </td></tr>
<tr><td>Impression</td><td><input type="number" name="impression" value="<? echo isset($wimpression)?$wimpression:"1000"; ?>" style="width:100px;" required> *</td></tr>
<tr><td>Timer</td><td><input type="number" name="timer" min="0" max="30" value="<? echo isset($wtimer)?$wtimer:"0"; ?>" style="width:50px;"> (Max: 30 seconds)</td></tr>
<tr><td>Show only to</td>
<td>Age<input type="checkbox" class="show_age" name="show_age"> &nbsp; 
Gender<input type="checkbox" class="show_gender"> &nbsp; Location<input type="checkbox" class="show_location"></td></tr> 
<tr style="display:none;"><td>Age</td><td><input type="number" name="age" value="<? echo isset($wage)?$wage!='-1'?$wage:'1':'1'; ?>" style="width:60px;" disabled="disabled" required> to <input type="number" name="age2" value="<? echo isset($wage2)?$wage2!='-1'?$wage2:'1':'1'; ?>" style="width:60px;" disabled="disabled" required></td></tr>
<tr style="display:none;"> <td>Gender</td> <td> <select name="gender" disabled="disabled" required> <option value="male" <? echo (($wgender=="male")?"selected='selected'":"")?>>Male</option> <option value="female" <? echo (($wgender=="female")?"selected='selected'":"")?>>Female</option> </select></td></tr>
<tr style="display:none;"> <td>Location</td> <td> <select id="adslocation" class="adslocation" multiple name="location[]" data-placeholder="Select some Provinces.."><? echo $oLoc->getLocation(); ?></select> </td> </tr> 
<tr>
	<td>Advertisement link: </td> 
	<td><input type="url" name="redirect" value="<? print $wredirect ?>" required/> * </td>
</tr>
<tr>
	<td>Cost per view: </td> 
	<td><input type="number" name="costperview" step="0.25" min="0" value="<? print $wcostperview ?>" required/> * </td>
</tr>
<tr>
	<td>Cost per click: </td> 
	<td><input type="number" name="costperclick" step="0.25" min="0" value="<? print $wcostperclick ?>" required/> * </td>
</tr>
<tr><td><br/></td></tr> 
<tr><td colspan=4> <? if($pageaction=="edit" || $pageaction=="update") { ?> 
    <input type=button value="Update" onclick="validate('update')">
    <input type=button value="Delete" onclick="ConfirmDelete(<? echo $ads_id ?>)">
    <input type=button value="Back" onclick="<? echo "window.location='index.php?component=advertisement&page=wce.listads.php&sortby=$sortby&sortseq=$sortseq&start=$next&searchtype=$searchtype&s_caption=$s_caption'"; ?>">
<? } else { ?> 
	<input type=button value="Add" onclick="validate('add')"> 
<? } ?>
</td></tr>
</form></table>
<script language=javascript>
function ConfirmDelete(id) {
    if(confirm('Are you sure you want to delete this record?')) {
        window.location='index.php?component=advertisement&page=wce.listads.php&pageaction=delete&ads_id='+id+'<? echo "&start=$next&sortby=$sortby&sortseq=$sortseq&searchtype=$searchtype&s_caption=$s_caption"; ?>';
    }
}

function validate(submitype) {
    if(document.thisform.caption.value=="") {
        alert("Please fill up the Caption");
        document.thisform.caption.select();
        return false;
    }
    else if(document.thisform.redirect.value=="") {
        alert("Please fill up the Advertisement link");
        document.thisform.redirect.select();
        return false;
    }
    else if((($('input[name="ads_content"]').val()=="" && document.thisform.type.value=="image") || (($('#ads_video').val()=="" || $('#ads_video2').val()=="" || $('#ads_video3').val()=="") && document.thisform.type.value=="video")) && submitype != 'update' ){
        alert("Please upload Advertisement");
        return false;
    }
    else {
        var str = document.thisform.redirect.value;
        if(str != '#') {
            var patt = new RegExp("http://");
            var res = patt.test(str);
            if(!res) {
                // alert("Please put http:// at the beginning of the link.");
                document.thisform.redirect.value = 'http://'+str;
                // document.thisform.redirect.select();
                // return false;
            }
        }
        
        if(document.thisform.show_age.checked){
            if(document.thisform.age.value > document.thisform.age2.value){
                alert("Age1 must not be greater than Age2");
                document.thisform.age.select();
                return false;
            }
        }
        if(submitype == "update"){
            document.thisform.pageaction.value='update';
        }
        $('body').loader('show');
        document.thisform.submit();
    }
    return false;
}
$('#ads_type').change(function(){
    switch($(this).val()) {
        case "video":
            $('.ads_video_row').css("display","table-row");
            $('.ads_video').removeAttr("disabled");
            $('.ads_image').attr("disabled","disabled");
            $('.ads_image_row').css("display","none");
            // $('.redirect-to').css("display","none");
            // $('.redirect-to input[type=url]').attr("disabled", "disabled");
            break;
        default:
            $('.ads_image_row').css("display","table-row");
            $('.ads_image').removeAttr("disabled");
            $('.ads_video').attr("disabled","disabled");
            $('.ads_video_row').css("display","none");
            // $('.redirect-to').css("display","table-row");
            // $('.redirect-to input[type=url]').removeAttr("disabled");
            break;
    }
});
$('select[name="section_id"]').chosen({
    disable_search_threshold: 10, width: "30%"
});
$('input[name="section"]').change(function() {
    switch($(this).val()) {
        case "issue": $('#magazine-issue').show();
        $('#section_issue').removeAttr('disabled');
        $('#magazine-category').hide();
        $('#section_category').attr('disabled', 'disabled');
        $('#magazine-magazine').hide();
        $('#section_magazine').attr('disabled', 'disabled');
        break;
        case "category": $('#magazine-category').show();
        $('#section_category').removeAttr('disabled');
        $('#magazine-issue').hide();
        $('#section_issue').attr('disabled', 'disabled');
        $('#magazine-magazine').hide();
        $('#section_magazine').attr('disabled', 'disabled');
        break;
        case "magazine": $('#magazine-magazine').show();
        $('#section_magazine').removeAttr('disabled');
        $('#magazine-issue').hide();
        $('#section_issue').attr('disabled', 'disabled');
        $('#magazine-category').hide();
        $('#section_category').attr('disabled', 'disabled');
        break;
    }
});

$('.show_age').change(function() {
    if($(this).is(':checked')) {
        $('input[name="age"]').closest('tr').show();
        $('input[name="age"]').removeAttr('disabled');
        $('input[name="age2"]').closest('tr').show();
        $('input[name="age2"]').removeAttr('disabled');
    }
    else {
        $('input[name="age"]').closest('tr').hide();
        $('input[name="age"]').attr('disabled', 'disabled');
        $('input[name="age2"]').closest('tr').hide();
        $('input[name="age2"]').attr('disabled', 'disabled');
    }
});

$('.show_gender').change(function() {
    if($(this).is(':checked')) {
        $('select[name="gender"]').closest('tr').show();
        $('select[name="gender"]').removeAttr('disabled');
    }
    else {
        $('select[name="gender"]').closest('tr').hide();
        $('select[name="gender"]').attr('disabled', 'disabled');
    }
});

$('.show_location').change(function() {
    if($(this).is(':checked')) {
        $('.adslocation').closest('tr').show();
        $('.adslocation').removeAttr('disabled');
        $('.adslocation').chosen({
            disable_search_threshold: 10, width: "30%"
        });
    }
    else {
        $('.adslocation').closest('tr').hide();
        $('.adslocation').attr('disabled', 'disabled');
    }
});

$(document).ready(function() {
    $('#section_issue').attr('disabled', 'disabled');
    $('#section_magazine').attr('disabled', 'disabled');
    var type="<? print $wtype; ?>"
    var jpageaction="<? print $pageaction; ?>";
    if(jpageaction=="edit" || jpageaction=="update") {
        var age="<? print $wage; ?>", age2="<? print $wage2; ?>", gender="<? print $wgender; ?>", location="<? print $wlocation; ?>", section="<? print $wsection; ?>";
        switch(section) {
            case 'magazine': $('input[value="magazine"]').trigger("click");
            break;
            case 'issue': $('input[value="issue"]').trigger("click");
            break;
        }
        if(age !=-1) {
            $('.show_age').trigger("click");
        }
        if(gender !="all") {
            $('.show_gender').trigger("click");
        }
        if(location != 0){
            $('.show_location').trigger("click");
        }
    }
    switch(type) {
        case "video":
            $('.ads_video_row').css("display","table-row");
            $('.ads_video').removeAttr("disabled");
            $('.ads_image').attr("disabled","disabled");
            $('.ads_image_row').css("display","none");
            break;
        default:
            $('.ads_image_row').css("display","table-row");
            $('.ads_image').removeAttr("disabled");
            $('.ads_video').attr("disabled","disabled");
            $('.ads_video_row').css("display","none");
            break;
    }
});
</script>
