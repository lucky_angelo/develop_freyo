<?php

$thisfile=$oSystem->getValue("contact_siteurl");
$localtz = $oSystem->getValue("sys_timezone");
$localtime = $localtz==""?time():getLocalTime($localtz);
$curr_datetime=date("Y-m-d H:i:s",$localtime);

echo "
	<style type=\"text/css\">
		.titletext{	font-family: ".$oSystem->getValue("contact_titlefont")."; font-size: ".$oSystem->getValue("contact_titlesize")."px; color: ".$oSystem->getValue("contact_titlecolor").";  }
		.descptext{	font-family: ".$oSystem->getValue("contact_descpfont")."; font-size: ".$oSystem->getValue("contact_descpsize")."px; color: ".$oSystem->getValue("contact_descpcolor").";  }
	</style>
";
if ($pageaction=="post"){
	if ($oSystem->getValue("contact_enableimage")=="Yes"){
		$status_message = $oSystem->valAuthCode($lang['common']['validcode'],$validationcode,$_SESSION['image_value']);
	}
	
	// addition validation
	$oContact_Field->data = array("fieldtype","fieldrequire","fieldtext","fieldname","valid_type","fileexten","uploadtype");
	$oContact_Field->order = "sequence asc";
	$result = $oContact_Field->getList();
	while($myrow=mysql_fetch_row($result)){
		$myrow[2]=stripslashes($myrow[2]);
		if ($myrow[0]=="textbox" && $$myrow[3]!=""){
			if ($myrow[4]=="Validemail"){
				$oContact_Form->validate($myrow[2],$$myrow[3],"IsEmail");
			} else if ($myrow[4]=="Digit"){
				$oContact_Form->validate($myrow[2],$$myrow[3],"IsNumeric");
			} else if ($myrow[4]=="Letter"){
				if(!preg_match("/^[a-z]+$/", $$myrow[3])){	
					$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> $myrow[2] ".$lang['contactform']['onlyletter'].".</font>";		
				}
			}
		} else if ($myrow[0]=="checkbox" && $myrow[1]=="yes") {
			$multi_str="";  $arr_multi=array();
			$myrow[2]=stripslashes($myrow[2]);
			$count_chk="chk_".$myrow[3];
			$val_count_chk=$$count_chk;
			
			for($i=1; $i<=$val_count_chk;$i++){
				$val_chk=$myrow[3]."_".$i;
				if ($$val_chk!=""){  array_push($arr_multi,$$val_chk); }
			}
			if (@count($arr_multi)>0){
				for($i=0; $i<count($arr_multi)-1;$i++){
					$multi_str .= $arr_multi[$i].",";
				}
				$multi_str .= $arr_multi[$i];
			}
			
			$oContact_Form->validate($myrow[2],$multi_str,"NotEmpty");
		} else if ($myrow[0]=="multiselect" && $myrow[1]=="yes"){
			$multi_str=""; $arr_multi=array();
			if (is_array($$myrow[3])){
				foreach($$myrow[3] as $val){
					if($val!="") { array_push($arr_multi,$val);	}
				}
			}
			if (@count($arr_multi)>0){
				for($i=0; $i<count($arr_multi)-1;$i++){
					$multi_str .= $arr_multi[$i].",";
				}
				$multi_str .= $arr_multi[$i];
			}
			$oContact_Form->validate($myrow[2],$multi_str,"NotEmpty");
		} else if ($myrow[0]=="fileupload") {
			if ($myrow[5]!="All"){
				$arr_filetype=array();
				$arr_filetype=explode(',',$myrow[6]);
				$filename = strtolower(str_replace(" ","_",trim($_FILES[$myrow[3]]['name'])));
				if ($filename!=""){
					$ext = strrchr($filename,".");
					$ext=str_replace(".","",$ext);
					if(!in_array($ext,$arr_filetype)){
						$status_message .="<font color=ff0000><b>".$lang['common']['error']." :</b> '$myrow[2]' - ".$lang['contactform']['invalidfileupload']."&nbsp;(".$lang['contactform']['onlyaccept']." ".$myrow[6].")</font>";
					} 
				}
			}
			
		}
	}
	mysql_free_result($result);	
	
	if($status_message==""){
		$oContact_Form->data = array("datepost");
		$oContact_Form->value = array($curr_datetime);
		
		$message.=$lang['contactform']['datepost']." : $curr_datetime\n";
		$arr_autorespon=array();
		
		$oContact_Field->data = array("fieldname","fieldtype","fieldtext","sendauto");
		$result = $oContact_Field->getList();
		while($myrow=mysql_fetch_row($result)){
			$fieldtext=stripslashes($myrow[2]);
			
			$oContact_Form->data[]= $myrow[0];
			if ($myrow[1]=="checkbox"){
				$multi_str=""; $arr_multi=array();
				$count_chk="chk_".$myrow[0];
				$val_count_chk=$$count_chk;
				
				for($i=1; $i<=$val_count_chk;$i++){
					$val_chk=$myrow[0]."_".$i;
					if ($$val_chk!=""){  array_push($arr_multi,$$val_chk); }
				}
				if (@count($arr_multi)>0){
					for($i=0; $i<count($arr_multi)-1;$i++){
						$multi_str .= $arr_multi[$i].",";
					}
					$multi_str .= $arr_multi[$i];
				}
				
				$oContact_Form->value[]= addslashes($multi_str);
				
				$val_str=stripslashes($multi_str);
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			} else if ($myrow[1]=="multiselect"){
				$multi_str=""; $arr_multi=array();
				if (is_array($$myrow[0])){
					foreach($$myrow[0] as $val){
						if($val!="") { array_push($arr_multi,$val);	}
					}
				}
				if (@count($arr_multi)>0){
					for($i=0; $i<count($arr_multi)-1;$i++){
						$multi_str .= $arr_multi[$i].",";
					}
					$multi_str .= $arr_multi[$i];
				}
				
				$oContact_Form->value[]= addslashes($multi_str);
				
				$val_str=stripslashes($multi_str);
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			} else if ($myrow[1]=="fileupload"){
				$uniqueid = md5(uniqid(rand(), true));	
				$uniqueid=substr($uniqueid,0,3);
				$filename = strtolower(str_replace(" ","_",trim($_FILES[$myrow[0]]['name'])));
				if ($filename!="") {
					$filename2=$uniqueid."_".$filename;
					$oContact_Form->uploadFile($_FILES[$myrow[0]]['tmp_name'],$path["docroot"]."_files/contactform",$filename2); 
				} else {
					$filename2="";
				}
				$oContact_Form->value[]= addslashes($filename2);
				
				$val_str=stripslashes($filename2);
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			} else if ($myrow[1]=="datetype"){
				$fieldday=$myrow[0]."day";
				$fieldmonth=$myrow[0]."month";
				$fieldyear=$myrow[0]."year";
				$val_str=$$fieldday."-".$$fieldmonth."-".$$fieldyear;
				
				$oContact_Form->value[]= addslashes($val_str);
				
				$val_str=stripslashes($val_str);
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			} else if ($myrow[1]=="textbox"){
				$oContact_Form->value[]= addslashes($$myrow[0]);
				$val_str=stripslashes($$myrow[0]);
				if ($myrow[3]=="Yes"){
					array_push($arr_autorespon,$val_str);
				}
				
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			} else {
				$oContact_Form->value[]= addslashes($$myrow[0]);
				$val_str=stripslashes($$myrow[0]);
				
				if ($val_str=="") { $val_str="N/A"; }
				$message.=$fieldtext." : $val_str\n";
			}
		}	
		mysql_free_result($result);
		$oContact_Form->add();
		$lastid=$oContact_Form->getLastID();
		
		//send notification admin email
		if ($oSystem->getValue("contact_emailnotice")=="yes"){
			$emailfrom = $oUser->getAdminEmail();
			$emailto = $oSystem->getValue("contact_adminemail");
			$subject = stripslashes($oSystem->getValue("contact_noticesubject"));
			$subject = str_replace("[[contact_id]]",$lastid,$subject);
			$body = stripslashes($oSystem->getValue("contact_noticecontent"));
			$body = str_replace("[[message]]",$message,$body);
			$body = stripslashes($body);
			
			if(!empty($emailfrom) && !empty($emailto)){
				$oSystem->mail($emailto, $subject, $body, $emailfrom);
			} 
			
			if(!empty($emailfrom)){
				//send extra email
				$arr_email=array();
				$contact_sendextra = $oSystem->getValue("contact_sendextra");
				$arr_email=explode(',',$contact_sendextra);
				foreach($arr_email as $val){
					$oSystem->mail($val, $subject, $body, $emailfrom);
				}
			}
		}
		
		//send auto response email
		if ($oSystem->getValue("contact_autoresponse")=="yes"){
			$emailfrom = $oUser->getAdminEmail();
			$subject = stripslashes($oSystem->getValue("contact_emailsubject"));
			$body = stripslashes($oSystem->getValue("contact_emailcontent"));
			$body = str_replace("[[message]]",$message,$body);
			$body = stripslashes($body);
			
			if(!empty($emailfrom)){
				foreach($arr_autorespon as $val){
					$oSystem->mail($val, $subject, $body, $emailfrom);
				}
			}
		}
		
		echo "
		<table border=\"0\" cellpadding=\"2\" width=\"98%\" align=\"center\"><tr><td>
		".stripslashes($oSystem->getValue("contact_thankyou"))."<br>
		<input type=\"button\" value=\" ".$lang['contactform']['back']." \" onclick=\"window.location='".$thisfile."'\">
		</td></tr></table><br><br>
		";
	} else {	$pageaction="";  }	//img code
}

if($pageaction==""){
/*	echo"<table border=\"0\" cellpadding=\"2\" width=\"98%\" align=\"center\">
		<tr><td class=\"titletext\">".stripslashes($oSystem->getValue("contact_titletext"))."</td></tr>
		<tr><td class=\"descptext\">".stripslashes(nl2br($oSystem->getValue("contact_descptext")))."</td></tr>
		</table><br><hr size=\"1\" color=\"#606060\" width=\"98%\" align=\"center\"><br>
	";*/

	echo "<table border=\"0\" cellpadding=\"2\" width=\"98%\" align=\"center\"><tr><td class=error>$status_message</td></tr><tr><td><b>Note:</b> Fields with <b>(*)</b> are required</td></tr></table>
	<script language=JavaScript>
	var fields = new Array();
	var fieldsrequire = new Array();
	var fieldstype = new Array();
	var fieldstext = new Array();
	var fieldscount = 0;
	</script>

	<table border=\"0\" cellpadding=\"2\" width=\"98%\" align=\"center\">
	<form name=\"frmSend\" action=\"".$thisfile."\" method=\"post\"  enctype=\"multipart/form-data\">
	<input type=\"hidden\" name=\"pageaction\" value=\"post\">
	";
	
	$oContact_Field->data = array("field_id","fieldname","fieldtype","fieldtext","width","height","fieldrequire","default_val","itemrow","fieldsize","fileexten","uploadtype");
	$oContact_Field->order = "sequence asc";
	$result = $oContact_Field->getList();
	while($myrow=mysql_fetch_row($result)){
		$default_val=stripslashes($myrow[7]);
		
		echo "
		<script language=JavaScript>
			fields[fieldscount]=\"$myrow[1]\";
			fieldstype[fieldscount]=\"$myrow[2]\";
			fieldsrequire[fieldscount]=\"$myrow[6]\";
			fieldstext[fieldscount]=\"$myrow[3]\";
			fieldscount++;
		</script>";
		  
		$requiredflag = ($myrow[6]=="yes"?"*":"");
		if ($myrow[2]=="datetype") {
			echo "<tr><td width=\"20%\">".stripslashes($myrow[3])." $requiredflag</td>";
		} else {
			echo "<tr><td valign=\"top\" width=\"20%\">".stripslashes($myrow[3])." $requiredflag</td>";
		}
		
		${$myrow[1]}=stripslashes(${$myrow[1]});
		
		switch($myrow[2]){
		case "textbox":
			echo "<td valign=\"top\"><input type=\"text\" style=\"width:{$myrow[4]}px\" name=\"$myrow[1]\" value=\"".(${$myrow[1]}?${$myrow[1]}:$default_val)."\"></td>";
		break;
		case "select":
			echo "<td valign=\"top\"><select name=\"$myrow[1]\" style=\"width:{$myrow[4]}px\">";
			$oContact_FieldOption->data = array("optionvalue","as_default","use_empty");
			$oContact_FieldOption->where = "field_id=$myrow[0]";
			$oContact_FieldOption->order = "option_id asc";
			$resultOption = $oContact_FieldOption->getList();
			while($myrowOption=mysql_fetch_row($resultOption)){
				$myrowOption[0]=stripslashes($myrowOption[0]);
				if (!isset($$myrow[1]) || $$myrow[1]==""){ 
					$selected=($myrowOption[1]=="Yes"?"selected":"");
				} else {
					$selected=($$myrow[1]==$myrowOption[0]?"selected":"");
				}
				echo "<option value=\"".($myrowOption[2]=="Yes"?"":"$myrowOption[0]")."\" $selected>$myrowOption[0]</option>";
			}
			mysql_free_result($resultOption);
			echo "</select></td>";
		break;
		case "checkbox":
			$itemrow=$myrow[8];
			$count_chk="chk_".$myrow[1];
			echo "<td valign=\"top\"><table border=\"0\" cellpadding=\"2\" width=\"100%\" align=\"center\"><tr>";
			$oContact_FieldOption->data = array("optionvalue","as_default");
			$oContact_FieldOption->where = "field_id=$myrow[0]";
			$oContact_FieldOption->order = "option_id asc";
			$resultOption = $oContact_FieldOption->getList();
			$i_count=0; $item_count=0;
			while($myrowOption=mysql_fetch_row($resultOption)){
				$i_count++; $item_count++;
				$chk_name=$myrow[1]."_".$i_count;
				$myrowOption[0]=stripslashes($myrowOption[0]);
				if (!isset($$chk_name) || $$chk_name==""){ 
					$checked= ($myrowOption[1]=="Yes"?"checked":"");
				} else {
					$checked= ($$chk_name==$myrowOption[0]?"checked":"");
				}
				
				echo "<td><input type=\"checkbox\" name=\"$chk_name\" value=\"$myrowOption[0]\" $checked>$myrowOption[0]</td>";
				if ($item_count>=$itemrow){ $item_count=0; echo"</tr><tr>"; }
			}
			mysql_free_result($resultOption);
			echo "</tr></table><input type=\"hidden\" name=\"$count_chk\" value=\"$i_count\"></td>";
		break;
		case "radio":
			$itemrow=$myrow[8];
			echo "<td valign=\"top\"><table border=\"0\" cellpadding=\"2\" width=\"100%\" align=\"center\"><tr>";
			$oContact_FieldOption->data = array("optionvalue","as_default");
			$oContact_FieldOption->where = "field_id=$myrow[0]";
			$oContact_FieldOption->order = "option_id asc";
			$resultOption = $oContact_FieldOption->getList();
			$i_count=0; $item_count=0;
			while($myrowOption=mysql_fetch_row($resultOption)){
				$i_count++; $item_count++;
				if (!isset($$myrow[1]) || $$myrow[1]==""){ 
					$checked= ($myrowOption[1]=="Yes"?"checked":"");
				} else {
					$checked= ($$myrow[1]==$myrowOption[0]?"checked":"");
				}
				echo "<td style=\"width:{$myrow[4]}px\"><input type=\"radio\" name=\"$myrow[1]\" value=\"$myrowOption[0]\" $checked>$myrowOption[0]</td>";
				if ($item_count>=$itemrow){ $item_count=0; echo"</tr><tr>"; }
			}mysql_free_result($resultOption);
			echo "</tr></table></td>";
		break;
		case "textarea":
			echo "<td valign=\"top\"><textarea style=\"width:{$myrow[4]}px; height:{$myrow[5]}px\" name=\"$myrow[1]\">".(${$myrow[1]}?${$myrow[1]}:$default_val)."</textarea></td>";
		break;
		case "password":
			echo "<td valign=\"top\"><input type=\"password\" style=\"width:{$myrow[4]}px\" name=\"$myrow[1]\" value=\"${$myrow[1]}\"></td>";
		break;
		case "multiselect":
			$fieldsize=$myrow[9];
			$field_name=$myrow[1]."[]";
			echo "<td valign=\"top\"><select size=\"".($fieldsize?$fieldsize:"5")."\" name=\"$field_name\" style=\"width:{$myrow[4]}px;\" multiple>";
			$oContact_FieldOption->data = array("optionvalue","as_default","use_empty");
			$oContact_FieldOption->where = "field_id=$myrow[0]";
			$oContact_FieldOption->order = "option_id asc";
			$resultOption = $oContact_FieldOption->getList();
			while($myrowOption=mysql_fetch_row($resultOption)){
				$myrowOption[0]=stripslashes($myrowOption[0]);
				echo "<option ".($myrowOption[1]=="Yes"?"selected":"")." value=\"".($myrowOption[2]=="Yes"?"":"$myrowOption[0]")."\">$myrowOption[0]</option>";
			}
			mysql_free_result($resultOption);
			echo "</select></td>";		
		break;
		case "fileupload":
			if ($myrow[10]!="All") {$uploadtype="<br>(".$lang['contactform']['onlyaccept']."".$myrow[11].")"; }else{$uploadtype="";}
			echo "<td valign=\"top\"><input type=\"file\" name=\"$myrow[1]\">$uploadtype</td>";
		break;
		case "datetype":
			echo "<td valign=\"top\"><table border=\"0\" cellpadding=\"2\" width=\"100%\" align=\"center\"><tr><td>";
			
			// day
			$oContact_PredefineList->data = array("predefine_name");
			$resultpre=$oContact_PredefineList->getDetail(2);
			if($myrowpre=mysql_fetch_row($resultpre)){
				$predefine_name=stripslashes($myrowpre[0]);
				$fieldday=$myrow[1]."day";
				echo"$predefine_name&nbsp;&nbsp;<select name=\"$fieldday\">";
				$oContact_PredefineItem->data = array("item_name");
	   			$oContact_PredefineItem->where = "predefine_id='2'";  	
			   	$oContact_PredefineItem->order = "predefineitem_id";
			   	$resultitem=$oContact_PredefineItem->getList();
			   	while($myrowitem=mysql_fetch_row($resultitem)){
				   	$myrowitem[0]=stripslashes($myrowitem[0]);
				   	echo"<option value=\"$myrowitem[0]\">$myrowitem[0]</option>";
			   	}
			   	mysql_free_result($resultitem);
			   	echo"</select>&nbsp;&nbsp;";
			}
			mysql_free_result($resultpre);
		
			// month
			$oContact_PredefineList->data = array("predefine_name");
			$resultpre=$oContact_PredefineList->getDetail(3);
			if($myrowpre=mysql_fetch_row($resultpre)){
				$predefine_name=stripslashes($myrowpre[0]);
				$fieldday=$myrow[1]."month";
				echo"$predefine_name&nbsp;&nbsp;<select name=\"$fieldday\">";
				$oContact_PredefineItem->data = array("item_name");
	   			$oContact_PredefineItem->where = "predefine_id='3'";  	
			   	$oContact_PredefineItem->order = "predefineitem_id";
			   	$resultitem=$oContact_PredefineItem->getList();
			   	while($myrowitem=mysql_fetch_row($resultitem)){
				   	$myrowitem[0]=stripslashes($myrowitem[0]);
				   	echo"<option value=\"$myrowitem[0]\">$myrowitem[0]</option>";
			   	}
			   	mysql_free_result($resultitem);
			   	echo"</select>&nbsp;&nbsp;";
			}
			mysql_free_result($resultpre);
			
			// year
			$oContact_PredefineList->data = array("predefine_name");
			$resultpre=$oContact_PredefineList->getDetail(4);
			if($myrowpre=mysql_fetch_row($resultpre)){
				$predefine_name=stripslashes($myrowpre[0]);
				$fieldday=$myrow[1]."year";
				echo"$predefine_name&nbsp;&nbsp;<select name=\"$fieldday\">";
				$oContact_PredefineItem->data = array("item_name");
	   			$oContact_PredefineItem->where = "predefine_id='4'";  	
			   	$oContact_PredefineItem->order = "predefineitem_id";
			   	$resultitem=$oContact_PredefineItem->getList();
			   	while($myrowitem=mysql_fetch_row($resultitem)){
				   	$myrowitem[0]=stripslashes($myrowitem[0]);
				   	echo"<option value=\"$myrowitem[0]\">$myrowitem[0]</option>";
			   	}
			   	mysql_free_result($resultitem);
			   	echo"</select>&nbsp;&nbsp;";
			}
			mysql_free_result($resultpre);
			
			echo"</td></tr></table></td>";
		break;
		}
		echo "</tr>";
	}
	
	if ($oSystem->getValue("contact_enableimage")=="Yes"){
		echo "
			<tr><td>".$lang['common']['validcode']."</td><td><input type=\"text\" name=\"validationcode\" style=\"width:135px\"> *
			<img src=\"".$path["webroot"]."common/lib.randimage.php\"></td></tr>
		";
	}
	
	$buttontext=stripslashes($oSystem->getValue("contact_buttoncaption"));
	echo"
	<tr><td></td><td><br>
		<input type=\"button\" name=\"btnsend\" value=\" ".$buttontext." \" onclick=\"if(checkRequiredField()){document.frmSend.pageaction.value='post'; document.frmSend.submit(); }\"> 
		<input type=\"reset\" name=\"btncancel\" value=\"".$lang['contactform']['reset']."\"> 
	</td></tr>
	</form></table>
	";
}

?>

<script language=JavaScript>

function checkRequiredField(){
	for (i=0; i<fields.length; i++){
		var field = document.frmSend[fields[i]];
		if (fieldsrequire[i]=="yes" && (fieldstype[i] == "textbox" || fieldstype[i] == "textarea" || fieldstype[i] == "password" || fieldstype[i] == "fileupload" || fieldstype[i] == "select")){
			if (!field.value){
				alert("'"+ fieldstext[i] +"' <? echo $lang['contactform']['requiredis'] ?>");
				return false;
			}
		}else if (fieldsrequire[i]=="yes" && fieldstype[i] == "radio"){
			var blnChecked = false;
			for(j=0; j<field.length;j++){
				if (field[j].checked){
					blnChecked = true;
					break;
				}
			}
			if (blnChecked==false) {
				alert("'"+ fieldstext[i] +"' <? echo $lang['contactform']['requiredis'] ?>");
				return false;
			}	
		}
	}
	return true;
}
</script>