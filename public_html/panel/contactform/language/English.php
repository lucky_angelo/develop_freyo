<?php

$lang['contactform']['menutitle'] 					= "Contact Form";
$lang['contactform']['componentdescp']				= "Simple and easy-to-use contact form for your website. Easily fit with web design pages. Customizable form fields, validation and email notification. ";

/*****Top Links *****/
$lang['contactform']['contactform'] 				= "Contact Form";
$lang['contactform']['listmessage'] 				= "List Messages";
$lang['contactform']['managefield'] 				= "Manage Fields";
$lang['contactform']['predefine']					= "Pre-defined List";
$lang['contactform']['preference'] 					= "Preference";
$lang['contactform']['generalsetting']				= "Preference Setting";
$lang['contactform']['presetemail']					= "Preset Emails";
$lang['contactform']['interguide']					= "Integration Guides";


/*****List Message*****/
$lang['contactform']['status'] 						= "Status";
$lang['contactform']['statuscontactdelete'] 		= "Selected message(s) deleted.";
$lang['contactform']['datefrom'] 					= "Date Posted From";
$lang['contactform']['datethrough'] 				= "Through";
$lang['contactform']['search'] 						= "Search";
$lang['contactform']['deleteselected'] 				= "Delete Selected";
$lang['contactform']['showing'] 					= "Showing";
$lang['contactform']['to'] 							= "To";
$lang['contactform']['of'] 							= "Of";
$lang['contactform']['prev'] 						= "Prev";
$lang['contactform']['next'] 						= "Next";
$lang['contactform']['name'] 						= "Name";
$lang['contactform']['email'] 						= "Email";
$lang['contactform']['datepost'] 					= "Date Posted";
$lang['contactform']['confirmdelete'] 				= "Are you sure you want to delete the selected record(s) ?";
$lang['contactform']['searchid']					= "Search # ID";
$lang['contactform']['allmessage']					= "All Message";

/*****Edit Message*****/
$lang['contactform']['statuscontactupdate'] 		= "Message updated.";
$lang['contactform']['back2list'] 					= "Back To List";
$lang['contactform']['number'] 						= "No.";
$lang['contactform']['update'] 						= "Update";
$lang['contactform']['delete'] 						= "Delete";
$lang['contactform']['back'] 						= "Back";
$lang['contactform']['editmessage']					= "Edit Message";

/*****Manage Fields*****/
$lang['contactform']['statusfieldadd'] 				= "New field added.";
$lang['contactform']['statusfieldupdate'] 			= "Field updated.";
$lang['contactform']['statusfielddelete'] 			= "Field deleted.";
$lang['contactform']['fieldtext'] 					= "Field Label";
$lang['contactform']['fieldtype'] 					= "Field Type";
$lang['contactform']['fieldwidth'] 					= "Field Width";
$lang['contactform']['fieldheight']					= "Field Height";
$lang['contactform']['compulsoryfield'] 			= "Compulsory Field";
$lang['contactform']['fieldoptions'] 				= "Field Options";
$lang['contactform']['textbox'] 					= "Text Box";
$lang['contactform']['dropdownselection'] 			= "Drop Down Selection";
$lang['contactform']['checkbox'] 					= "Check Box";
$lang['contactform']['radiobutton'] 				= "Radio Button";
$lang['contactform']['textarea'] 					= "Large Text Area";
$lang['contactform']['required'] 					= "Required";
$lang['contactform']['sequence'] 					= "Sequence";
$lang['contactform']['add'] 						= "Add";
$lang['contactform']['del'] 						= "Del";
$lang['contactform']['sortasc']						= "Sort In Ascending Order";
$lang['contactform']['sortdesc']					= "Sort In Descending Order";
$lang['contactform']['movedown']					= "Move Down";
$lang['contactform']['moveup']						= "Move Up";
$lang['contactform']['editthisrecord']				= "Edit This Record";
$lang['contactform']['delthisrecord']				= "Delete This Record";
$lang['contactform']['yes']							= "Yes";
$lang['contactform']['no']							= "No";
$lang['contactform']['plsfieldtext']				= "Please enter your Field Text.";
$lang['contactform']['mustenteroption']				= "You must enter the Field Options.";
$lang['contactform']['multicheckbox']				= "Multi Checkbox";
$lang['contactform']['password']					= "Password";
$lang['contactform']['multiselect']					= "Multi Select";
$lang['contactform']['fileupload']					= "File Upload";
$lang['contactform']['datetype']					= "Date Type";
$lang['contactform']['defaultvalue']				= "Default Value";
$lang['contactform']['asdefault']					= "Set as Default";
$lang['contactform']['useempty']					= "Use Empty Value";
$lang['contactform']['fieldoptionexist']			= "Field option value exist";
$lang['contactform']['edit']						= "Edit";
$lang['contactform']['plsenteroption']				= "Please enter field options value";
$lang['contactform']['fieldvalidation']				= "Field Validation";
$lang['contactform']['none']						= "None";
$lang['contactform']['validemail']					= "Valid  Email (eg:john@yahoo.com)";
$lang['contactform']['validdigit']					= "Digits Only";
$lang['contactform']['validletter']					= "Letters Only";
$lang['contactform']['sendautoresp']				= "Send auto-responder to this email";
$lang['contactform']['loadfieldoption']				= "Load Field Options";
$lang['contactform']['predefinelist']				= "From Pre-defined List";
$lang['contactform']['optionbelow']					= "From Option List Below";
$lang['contactform']['itemperrow']					= "Options per Row";
$lang['contactform']['fieldsize']					= "Field Size";
$lang['contactform']['fileextension']				= "Allow Upload";
$lang['contactform']['allextensions']				= "All Files Extensions";
$lang['contactform']['uploadfiletype']				= "Only File With Extension";
$lang['contactform']['uploadfiletips']				= "(eg : jpg,gif,zip,bmp,tif,png,jpeg)";
$lang['contactform']['datetypetips']				= "Note : This field will load direct from predefine list for combination Day, Month, Year.";
$lang['contactform']['listupdated']					= "List updated";
$lang['contactform']['plsenterfieldtext']			= "Please enter field text";
$lang['contactform']['plsopt']						= "Please enter at least one options";


/*** Predefine List ***/
$lang['contactform']['id']							= "#ID";
$lang['contactform']['predefine_name']				= "List Name";
$lang['contactform']['predefinesequence']			= "Sequence";
$lang['contactform']['totalitem']					= "Total Item";
$lang['contactform']['viewitem']					= "View Item";
$lang['contactform']['item_name']					= "Item Name";
$lang['contactform']['predefineitem']				= "Items In: ";
$lang['contactform']['predefineseq']				= "Seq";
$lang['contactform']['predefineadd']				= "Predefine list added";
$lang['contactform']['predefineedit']				= "Edit predefine list";
$lang['contactform']['predefineupdate']				= "Predefine list updated";
$lang['contactform']['predefinedelete']				= "Predefine list deleted";
$lang['contactform']['confirmdeletepre']			= "Attention :: Delete this predefine list will delete all the predefine item related with it. Are you sure want to delete selected record(s)?";
$lang['contactform']['predefineitemadd']			= "Predefine item added";
$lang['contactform']['predefineitemedit']			= "Edit predefine item";
$lang['contactform']['predefineitemupdate']			= "Predefine item updated";
$lang['contactform']['predefineitemdelete']			= "Predefine item deleted";
$lang['contactform']['updatelist']					= "Update List";
$lang['contactform']['itemorderupd']				= "Predefine Item List updated";


/*****Preference Settings*****/
$lang['contactform']['statusprefersave'] 			= "Preference settings saved";
$lang['contactform']['statuspreferdefault'] 		= "Preference settings restored to default values.";
$lang['contactform']['general'] 					= "General";
$lang['contactform']['messagelistno'] 				= "No. of Records In List Message";
$lang['contactform']['emailnotification'] 			= "Notification Email";
$lang['contactform']['emailnotificationdesc'] 		= "When a visitor submits form, send a notification to admin.";
$lang['contactform']['enableemailnotification'] 	= "Enable Notification Email";
$lang['contactform']['subject'] 					= "Subject";
$lang['contactform']['message'] 					= "Message";
$lang['contactform']['autoresponder'] 				= "Auto-Responder";
$lang['contactform']['autoresponderdesc'] 			= "Send an auto-responder email to your visitor after he or she posts a message.";
$lang['contactform']['enableautoresponder'] 		= "Enable Auto-Responder";
$lang['contactform']['thankyoumessage'] 			= "Thank You Message";
$lang['contactform']['save'] 						= "Save";
$lang['contactform']['usedefault'] 					= "Use Default";
$lang['contactform']['fontsncolor']					= "Fonts & Color";
$lang['contactform']['contacttitle']				= "Contact Form Title";
$lang['contactform']['contactdescp']				= "Contact Form Description";
$lang['contactform']['fontcolor']					= "Color";
$lang['contactform']['fontsize']					= "Size";
$lang['contactform']['fontface']					= "Font Face";
$lang['contactform']['titletext']					= "Title Text";
$lang['contactform']['descptext']					= "Description Text";
$lang['contactform']['buttoncaption']				= "Button Caption";
$lang['contactform']['imgverify']					= "Enable Image Text Verification";
$lang['contactform']['imgverifynote']				= "Requires both the GD library and FreeType library.";
$lang['contactform']['senderemail']					= "Admin Email";
$lang['contactform']['senderemailtip']				= "It is recommended that you use current's domain email address, and is different from email address in the admin > Info & Settings page.";
$lang['contactform']['sendextra']					= "Send Extra Copy To";
$lang['contactform']['sendextratip']				= "Separate each email with comma, eg:john@yahoo.com,susan@hotmail.com";


/*****Integration Guide*****/
$lang['contactform']['viewsamplepage']				= "Click Here To View Sample Page";
$lang['contactform']['frontinterguide']				= "Front-End Integration Guidelines";
$lang['contactform']['interguide1']					= "You can easily integrate the front-end with your web design pages.";
$lang['contactform']['before']						= "Before";
$lang['contactform']['afterintergration']			= "After Integration";
$lang['contactform']['intergrationins']				= "Integration Instructions";
$lang['contactform']['invalidfrontend']				= "Invalid Front End URL, ensure that the Front-End URL in Preference > Preference Setting page is not blank and valid";
$lang['contactform']['viewfrontend']				= "Front End View";
$lang['contactform']['guide1']						= "Using a text editor, open the web page file (.php file), and put the code below to the top of the file (line 1), to call the 'config.php' file. This will load all the necessary database connection, class declaration files and load session.";
$lang['contactform']['guide2']						= "* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.";
$lang['contactform']['guide3']						= "In your .php web page file, put the line below to call the Contact Form content body.";


/*****Front-End*****/
$lang['contactform']['send'] 						= "Send";
$lang['contactform']['reset'] 						= "Reset";
$lang['contactform']['requiredis']					= "is required.";
$lang['contactform']['frontendurl']					= "Front end URL";
$lang['contactform']['onlyaccept']					= "Accepted file extensions: ";
$lang['contactform']['onlyletter']					= "Letters Only";
$lang['contactform']['invalidfileupload']			= "Invalid file extension";
$lang['contactform']['requiredfield']				= "Required Field";


?>