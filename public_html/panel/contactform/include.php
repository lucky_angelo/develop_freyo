<?

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	include($path["docroot"]."contactform/class.contact.php");
	$oContact_Form = new Contact_Form;
	$oContact_Form->db = $oDb->db;
	
	include($path["docroot"]."contactform/class.field.php");
	$oContact_Field = new Contact_Field;
	$oContact_Field->db = $oDb->db;
	
	include($path["docroot"]."contactform/class.fieldoption.php");
	$oContact_FieldOption = new Contact_FieldOption;
	$oContact_FieldOption->db = $oDb->db;
	
	include($path["docroot"]."contactform/class.predefinelist.php");
	$oContact_PredefineList = new Contact_PredefineList;
	$oContact_PredefineList->db = $oDb->db;
	
	include($path["docroot"]."contactform/class.predefineitem.php");
	$oContact_PredefineItem = new Contact_PredefineItem;
	$oContact_PredefineItem->db = $oDb->db;
}
?>