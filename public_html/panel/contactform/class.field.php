<?

class Contact_Field extends Item{
	
	var $table="contact_field",$primarykey="field_id",$shortname="contactform",$version="4.1";
	var $options=array();
	
	function Contact_Field(){}
	
	function add(){		
		$this->getdatalist();	$this->getvaluelist();
		$sql="insert into $this->table($this->datalist) values($this->valuelist)";
		mysql_query($sql,$this->db);	
	
		$sql = "select max(field_id) from $this->table";
		$result = mysql_query($sql, $this->db);
		if ($myrow = mysql_fetch_row($result)){
			$last_id = $myrow[0];
		}
		$fieldname = "field$last_id";
		$sql = "update $this->table set fieldname = '$fieldname' where field_id='$last_id'";
		mysql_query($sql,$this->db);	
		
		/* add new field to contact */
		for ($i=0; $i<count($this->data); $i++){
			if ($this->data[$i] == 'fieldtype'){
				$fieldtype = $this->value[$i];
				break;
			}
		}
		
		$oContact1 = new Contact_Form;
		$oContact1->db = $this->db;
		$oContact1->fields = array($fieldname);
		$vartype = $this->getVarType($fieldtype);
		$oContact1->types = array($vartype);
		$oContact1->addFields();
			
		$this->logEvent("Add");
	}
	
	function update($id){
		$this->getupdatelist();
		$sql="update $this->table set $this->updatelist where $this->primarykey='$id'";
		mysql_query($sql,$this->db);
		
		/* update field type of contact */
		$this->data = array("fieldname", "fieldtype");
		$result = $this->getDetail($id);
		if ($myrow = mysql_fetch_row($result)){
			$fieldname = $myrow[0];
			$fieldtype = $myrow[1];
		}
		
		$oContact1 = new Contact_Form;
		$oContact1->db = $this->db;
		$oContact1->fields = array($fieldname);
		$vartype = $this->getVarType($fieldtype);
		$oContact1->types = array($vartype);
		$oContact1->updateFields();

		$this->logEvent("Update");
	}	

	function delete($id){
	
		$this->data = array("fieldname");
		$result = $this->getDetail($id);
		if ($myrow = mysql_fetch_row($result)){
			$fieldname = $myrow[0];
		}

		$sql="delete from $this->table where $this->primarykey='$id'";
		mysql_query($sql,$this->db);
		
		/* drop field from contact */
		$oContact1 = new Contact_Form;
		$oContact1->db = $this->db;
		$oContact1->fields = array($fieldname);
		$oContact1->dropFields();

		$this->logEvent("Delete");
	}
				
	function getVarType($fieldtype){
	
		if ($fieldtype == "textbox"){
			$vartype = "text";
		}
		else if ($fieldtype == "select"){
			$vartype = "text";
		}
		else if ($fieldtype == "checkbox"){
			$vartype = "text";
		}
		else if ($fieldtype == "radio"){
			$vartype = "text";
		}
		else if ($fieldtype == "textarea"){
			$vartype = "text";
		}
		else{
			$vartype = "text";
		}
		
		return $vartype;
	}
	
	
	function getLastSequence(){
		$sql="select sequence from $this->table order by sequence desc limit 0,1";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}
	
	function getLastID(){
		$sql="select $this->primarykey from $this->table order by sequence desc limit 0,1";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}
	
	function deleteOptions($field_id){
		$oContact_FieldOption = new Contact_FieldOption;
		$oContact_FieldOption->db = $this->db;
		$sql = "delete from $oContact_FieldOption->table where field_id='$field_id' ";
		mysql_query($sql, $oContact_FieldOption->db);
	}
	
}
		
?>