<?php include_once("../config.php");include_once($path["docroot"]."common/session.php");include_once($path["docroot"]."common/css.php"); ?>
<html>
<head><title>Contact Form Sample Page</title></head>

<body>

<table border=0 cellpadding=0 cellspacing=0 width=780 align=center style="border:1px #000000 solid; border-collapse:collapse">
<tr><td align=center colspan=2 style="border-bottom:1px #000000 solid"><br>Contact Form ~ Front-End Sample<br><br> You can easily integrate the Front-End to your web design page. <br>Please refer to the guidelines below.<br><br></td></tr>
<tr><td width=170 height=300 valign=top align=center style="border-right:1px #000000 solid"><br>

	Left Menu

</td><td valign=top><br>

	<?php 
		include($path["docroot"]."contactform/home.contact.php"); 
	?>

</td></tr>
</table>

<br><br>
<table width=700 align=center>
<tr><td valign=top width=25%>
	<b><u>Front-End Integration Guidelines</u></b><br><br>
	You can easily integrate the front-end with your web design pages.
	<table border=0 cellpadding=5>
	<tr><td align=center><img src=image/sample1.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/arrow.gif border=0></td>
		<td align=center><img src=image/sample2.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/sample4.jpg style="border:1px #B6B6B6 solid"></td>
		<td align=center><img src=image/sample3.jpg style="border:1px #B6B6B6 solid"></td></tr>
	<tr><td align=center>Before</td>
		<td>&nbsp;</td>
		<td align=center colspan=3>After Integration</td></tr>
	</table>
	
	<br><br>

</td></tr>
<tr><td valign=top>
	
	<b>Integration Instructions</b><br>
	<table cellpadding=5>
	<tr><td valign=top>1. </td><td>Using a text editor, open the web page file (.php file), and put the code below to the top of the file (line 1), to call the 'config.php' file. This will load all the necessary database connection, class declaration files and load session.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include_once("<font color=#F65A0E>document_path_of_panel_folder</font>/config.php"); include_once("<font color=#F65A0E>document_path_of_panel_folder</font>/common/session.php"); include_once("<font color=#F65A0E>document_path_of_panel_folder</font>/common/css.php"); ?&gt; </font></td></tr>
	<tr><td valign=top></td><td valign=top>* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.</td></tr>	
	<tr><td valign=top>2. </td><td>In your .php web page file, put the line below to call the Contact Form content body.</td></tr>
	<tr><td valign=top></td><td><font face=tahoma color=15B100>&lt;?php include("<font color=#F65A0E>document_path_of_panel_folder</font>/contactform/home.contact.php"); ?&gt; </font></td></tr>
	<tr><td valign=top>3. </td><td>For sample integration code, please refer to the source code of this file.  To do this, open the file with your text editor. <br><br></td></tr>
	<tr><td valign=top colspan=2>** To find your <font color=#F65A0E>'document_path_of_panel_folder'</font>, check the path['docroot'] value from the panel/config.php file <br><br></td></tr>
</td></tr>
	</table>
</td></tr>
</table>
<br><br>
</body>
</html>