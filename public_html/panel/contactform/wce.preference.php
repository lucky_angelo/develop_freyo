<?

if($pageaction=="save"){
  
  $contact_thankyou = addslashes($FCKeditor);
  $oSystem->setValue("contact_listno",$contact_listno);
  $oSystem->setValue("contact_thankyou",$contact_thankyou); 
  $oSystem->setValue("contact_siteurl",$contact_siteurl);

  $oSystem->setValue("contact_titlefont",$contact_titlefont);
  $oSystem->setValue("contact_titlecolor",$contact_titlecolor);
  $oSystem->setValue("contact_titlesize",$contact_titlesize);
  $oSystem->setValue("contact_titletext",addslashes($contact_titletext));
  $oSystem->setValue("contact_descpfont",$contact_descpfont);
  $oSystem->setValue("contact_descpcolor",$contact_descpcolor);
  $oSystem->setValue("contact_descpsize",$contact_descpsize);
  $oSystem->setValue("contact_descptext",addslashes($contact_descptext));
  $oSystem->setValue("contact_buttoncaption",addslashes($contact_buttoncaption));
  $oSystem->setValue("contact_enableimage",$contact_enableimage);
  
  $status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusprefersave']."<br>";	  
}
if($pageaction=="default"){

  $oSystem->setValue("contact_listno","20");
  $oSystem->setValue("contact_thankyou","<p><font size=3><strong>Thank You</strong></font></p><p>Your message has been sent. We will attend to your inquiry shortly.</p>"); 
  $oSystem->setValue("contact_siteurl","");
  
  $oSystem->setValue("contact_titlefont","Arial");
  $oSystem->setValue("contact_titlecolor","#000000");
  $oSystem->setValue("contact_titlesize","16");
  $oSystem->setValue("contact_titletext","Contact Form");
  $oSystem->setValue("contact_descpfont","Arial");
  $oSystem->setValue("contact_descpcolor","#000000");
  $oSystem->setValue("contact_descpsize","14");
  $oSystem->setValue("contact_descptext","Please submit your contact to us.");
  $oSystem->setValue("contact_buttoncaption","Submit Form");
  $oSystem->setValue("contact_enableimage","Yes");

  $status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statuspreferdefault']."<br>";	  
}

if ($oSystem->getValue("contact_siteurl") == ""){ $oSystem->setValue("contact_siteurl", $path["webroot"]."contactform/samplecontactform.php"); }

$fontnames = array("Arial", "Courier", "Sans Serif", "Tahoma", "Verdana", "Wingdings");

echo"<table border=\"0\" width=\"100%\"><tr><td><b>".$lang['contactform']['contactform']."</b></td><td>";
include("wce.menu.php");
echo"</td></tr></table><hr size=\"1\" color=\"#606060\">";

echo"
	<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"98%\" align=\"center\">
	<form name=\"thisform\" action=\"index.php?component=$component&page=$page\" method=\"post\">
	<input type=\"hidden\" name=\"pageaction\">
	<tr><td valign=\"top\"><b>".$lang['contactform']['generalsetting']."</b><br>$status_message<br></td></tr>

	<tr><td valign=\"top\">
		<table border=\"0\" width=\"100%\">
		<tr><td valign=\"top\" width=\"30%\">".$lang['contactform']['messagelistno']."</td><td><input type=\"text\" name=\"contact_listno\" style=\"width:35px;\" value=\"".$oSystem->getValue("contact_listno")."\"></td></tr>
		<tr><td>".$lang['contactform']['imgverify']."</td><td><input type=\"checkbox\" name=\"contact_enableimage\" value=\"Yes\" ".($oSystem->getValue("contact_enableimage")=="Yes"?"checked":"").">&nbsp;".$lang['contactform']['imgverifynote']."</td></tr>
		<tr><td valign=\"top\">".$lang['contactform']['frontendurl']."</td><td><input type=\"text\" name=\"contact_siteurl\" style=\"width:400px;\" value=\"".$oSystem->getValue("contact_siteurl")."\"></td></tr>
	
		</table><br>
		&nbsp;<b><u>".$lang['contactform']['thankyoumessage']."</u></b><br><br>
		<table border=\"0\" width=\"100%\">
		<tr><td>
";	

		$oFCKeditor = new FCKeditor('FCKeditor') ;
		$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
		$oFCKeditor->ToolbarSet = 'Basic';		
		$oFCKeditor->Width		= '550' ;
		$oFCKeditor->Height		= '150' ;
		$oFCKeditor->Value		= stripslashes($oSystem->getValue("contact_thankyou"));
		$oFCKeditor->Create() ;
	
		
echo"	
		</td></tr>
		</table><br>	
	
	<b><u>".$lang['contactform']['fontsncolor']."</u></b><br><br>
	<table border=\"0\" width=\"100%\">
	<tr><td colspan=\"6\"><b>".$lang['contactform']['contacttitle']."</b></td></tr>
	<tr><td width=\"20%\">".$lang['contactform']['fontface'].":</td><td><select name=\"contact_titlefont\" style=\"width:95px;\">
";

	for($i=0; $i<count($fontnames); $i++){	echo "<option value=\"$fontnames[$i]\" " . ($fontnames[$i]==$oSystem->getValue("contact_titlefont")?"selected":"") . ">$fontnames[$i]</option>";	}
	
echo"
	</select></td>
		<td>".$lang['contactform']['fontcolor'].":</td><td><input type=\"text\" name=\"contact_titlecolor\" size=\"8\" value=\"".$oSystem->getValue("contact_titlecolor")."\">&nbsp;&nbsp;<span id=\"titlecolor\" style=\"width:15px;height:15px;background-color:".$oSystem->getValue("contact_titlecolor")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.contact_titlecolor, titlecolor);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=\"absmiddle\"></a></td>
		<td>".$lang['contactform']['fontsize'].":</td><td><input type=\"text\" name=\"contact_titlesize\" size=\"3\" value=\"".$oSystem->getValue("contact_titlesize")."\"> px </td>
		<tr><td valign=\"top\">".$lang['contactform']['titletext']."</td><td colspan=\"5\"><input type=\"text\" name=\"contact_titletext\" value=\"".stripslashes($oSystem->getValue("contact_titletext"))."\" style=\"width:200px;\"></td></tr>
	</tr>
	</table><br>

	<table border=\"0\" width=\"100%\">
	<tr><td colspan=\"6\"><b>".$lang['contactform']['contactdescp']."</b></td></tr>
	<tr><td width=\"20%\">".$lang['contactform']['fontface'].":</td><td><select name=\"contact_descpfont\" style=\"width:95px;\">
";

	for($i=0; $i<count($fontnames); $i++){	echo "<option value=\"$fontnames[$i]\" " . ($fontnames[$i]==$oSystem->getValue("contact_descpfont")?"selected":"") . ">$fontnames[$i]</option>";	}
	
echo"
	</select></td>
		<td>".$lang['contactform']['fontcolor'].":</td><td><input type=\"text\" name=\"contact_descpcolor\" size=\"8\" value=\"".$oSystem->getValue("contact_descpcolor")."\">&nbsp;&nbsp;<span id=\"descpcolor\" style=\"width:15px;height:15px;background-color:".$oSystem->getValue("contact_descpcolor")."\"></span>&nbsp;<a href=\"javascript:TCP.popup(document.thisform.contact_descpcolor, descpcolor);\"><img src=\"common/colorpick/img/sel.gif\" border=\"0\" align=\"absmiddle\"></a></td>
		<td>".$lang['contactform']['fontsize'].":</td><td><input type=\"text\" name=\"contact_descpsize\" size=\"3\" value=\"".$oSystem->getValue("contact_descpsize")."\"> px </td>
	</tr>
	<tr><td valign=\"top\">".$lang['contactform']['descptext']."</td><td colspan=\"5\"><textarea name=\"contact_descptext\" rows=\"5\" style=\"width:90%\">".stripslashes($oSystem->getValue("contact_descptext"))."</textarea></td></tr>
	<tr><td valign=\"top\">".$lang['contactform']['buttoncaption']."</td><td colspan=\"5\"><input type=\"text\" name=\"contact_buttoncaption\" value=\"".stripslashes($oSystem->getValue("contact_buttoncaption"))."\" style=\"width:150px;\"></td></tr>
	</table><br>	
	
	</td></tr>
	<tr><td colspan=\"2\"><br>
		<input type=\"button\" value=\" ".$lang['contactform']['save']." \" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
		<input type=\"button\" value=\" ".$lang['contactform']['usedefault']." \" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">
	</td></tr>
	</form></table>
	<br>
";
?>
<script language=JavaScript src="common/colorpick/picker.js"></script>

