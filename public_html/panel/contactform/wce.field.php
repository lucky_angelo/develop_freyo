<?

switch($pageaction){
	case "add":
		$arr_valopt=array(); $arr_valdefault=array(); $arr_valempty=array();
		if ($valopt_str!=""){ $arr_valopt=explode('%**%',$valopt_str); }
		if ($arr_valdefault!=""){ $arr_valdefault=explode('%**%',$valdefault_str); }
		if ($arr_valempty!=""){ $arr_valempty=explode('%**%',$valempty_str); }
		$sequence = $oContact_Field->getLastSequence()+1;	
		
		$require = ($require?"yes":"no");
		$oContact_Field->data = array("fieldtype","fieldtext","fieldrequire","sequence","width","height","default_val","valid_type","sendauto","itemrow","fieldsize","fileexten","uploadtype");
		$oContact_Field->value = array($fieldtype,addslashes($fieldtext),$require,$sequence,$fieldwidth,$fieldheight,addslashes($default_val),$valid_type,$sendauto,$itemrow,$fieldsize,$fileexten,$uploadtype);
		$oContact_Field->add();
		$last_field=$oContact_Field->getLastID();
		
		if ($loadfield=="Predefine"){
		  	$oContact_PredefineItem->data = array("predefineitem_id","item_name");
		   	$oContact_PredefineItem->where = "predefine_id='$predefinelist'";  	
		   	$oContact_PredefineItem->order = "item_name";
		   	$result=$oContact_PredefineItem->getList();
		   	while($myrow=mysql_fetch_row($result)){
			   	$item_name=stripslashes($myrow[1]);
			   	$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
				$oContact_FieldOption->value = array($last_field,addslashes($item_name),"No","No");
				$oContact_FieldOption->add();	
		   	} 
   			mysql_free_result($result);
		} else {
			if($fieldtype=="select" || $fieldtype=="radio"){
				$i_def=0;
				foreach($arr_valdefault as $val){
					if ($val=="Yes"){ $i_default=$i_def; }
					$i_def++;
				}
				
				for($i=0;$i<count($arr_valopt);$i++){
					if ($i_default=="$i"){ $val_def="Yes"; } else { $val_def="No"; }
					if ($arr_valempty[$i]=="Yes"){ $use_empty="Yes"; } else { $use_empty="No"; }
					
					$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
					$oContact_FieldOption->value = array($last_field,addslashes($arr_valopt[$i]),$val_def,$use_empty);
					$oContact_FieldOption->add();		
				}
			} else if ($fieldtype=="multiselect" || $fieldtype=="checkbox") {
				for($i=0;$i<count($arr_valopt);$i++){
					if ($arr_valempty[$i]=="Yes"){ $use_empty="Yes"; } else { $use_empty="No"; }
					$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
					$oContact_FieldOption->value = array($last_field,addslashes($arr_valopt[$i]),$arr_valdefault[$i],$use_empty);
					$oContact_FieldOption->add();		
				}
			}
		}
		
		$fieldtype=""; $fieldtext=""; $require=""; $sequence=""; $fieldwidth=""; $fieldheight=""; $default_val=""; $valid_type=""; $sendauto=""; $itemrow=""; $fieldsize=""; $fileexten=""; $uploadtype=""; $arr_valopt=array(); $arr_valdefault=array(); $arr_valempty=array(); $pageaction="";
		$status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusfieldadd']."<br>";
	break;
	case "edit":
		$oContact_Field->data = array("fieldtype","fieldtext","fieldrequire","sequence","width","height","default_val","valid_type","sendauto","itemrow","fieldsize","fileexten","uploadtype");
		$result = $oContact_Field->getDetail($field_id);
		if ($myrow = mysql_fetch_row($result)){
			$fieldtype=$fieldtype?$fieldtype:$myrow[0];
			$fieldtext = stripslashes(htmlentities($myrow[1]));
			$require = $myrow[2];
			$sequence = $myrow[3];
			$fieldwidth = $myrow[4];
			$fieldheight = $myrow[5];
			$default_val = stripslashes(htmlentities($myrow[6]));
			$valid_type = $myrow[7];
			$sendauto = $myrow[8];
			$itemrow = $myrow[9];
			$fieldsize = $myrow[10];
			$fileexten = $myrow[11];
			$uploadtype = $myrow[12];
		}
		mysql_free_result($result);
	break;
	case "update":
		$arr_valopt=array(); $arr_valdefault=array(); $arr_valempty=array();
		if ($valopt_str!=""){ $arr_valopt=explode('%**%',$valopt_str); }
		if ($arr_valdefault!=""){ $arr_valdefault=explode('%**%',$valdefault_str); }
		if ($arr_valempty!=""){ $arr_valempty=explode('%**%',$valempty_str); }
		
		$require = ($require?"yes":"no");
		$oContact_Field->data = array("fieldtype","fieldtext","fieldrequire","width","height","default_val","valid_type","sendauto","itemrow","fieldsize","fileexten","uploadtype");
		$oContact_Field->value = array($fieldtype,addslashes($fieldtext),$require,$fieldwidth,$fieldheight,addslashes($default_val),$valid_type,$sendauto,$itemrow,$fieldsize,$fileexten,$uploadtype);
		$oContact_Field->update($field_id);
		
		$oContact_Field->deleteOptions($field_id); // clear field options
		if ($loadfield=="Predefine"){
		  	$oContact_PredefineItem->data = array("predefineitem_id","item_name");
		   	$oContact_PredefineItem->where = "predefine_id='$predefinelist'";  	
		   	$oContact_PredefineItem->order = "item_name";
		   	$result=$oContact_PredefineItem->getList();
		   	while($myrow=mysql_fetch_row($result)){
			   	$item_name=stripslashes($myrow[1]);
			   	$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
				$oContact_FieldOption->value = array($field_id,addslashes($item_name),"No","No");
				$oContact_FieldOption->add();	
		   	} 
   			mysql_free_result($result);
		} else {
			if($fieldtype=="select" || $fieldtype=="radio"){
				$i_def=0;
				foreach($arr_valdefault as $val){
					if ($val=="Yes"){ $i_default=$i_def; }
					$i_def++;
				}
				
				for($i=0;$i<count($arr_valopt);$i++){
					if ($i_default=="$i"){ $val_def="Yes"; } else { $val_def="No"; }
					if ($arr_valempty[$i]=="Yes"){ $use_empty="Yes"; } else { $use_empty="No"; }
					$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
					$oContact_FieldOption->value = array($field_id,addslashes($arr_valopt[$i]),$val_def,$use_empty);
					$oContact_FieldOption->add();		
				}
			} else if ($fieldtype=="multiselect" || $fieldtype=="checkbox") {
				for($i=0;$i<count($arr_valopt);$i++){
					if ($arr_valempty[$i]=="Yes"){ $use_empty="Yes"; } else { $use_empty="No"; }
					$oContact_FieldOption->data = array("field_id","optionvalue","as_default","use_empty");
					$oContact_FieldOption->value = array($field_id,addslashes($arr_valopt[$i]),$arr_valdefault[$i],$use_empty);
					$oContact_FieldOption->add();		
				}
			}
		}
		
		$fieldtype=""; $fieldtext=""; $require=""; $sequence=""; $fieldwidth=""; $fieldheight=""; $default_val=""; $valid_type=""; $sendauto=""; $itemrow=""; $fieldsize=""; $fileexten=""; $uploadtype=""; $arr_valopt=array(); $arr_valdefault=array(); $arr_valempty=array(); $pageaction="edit";
		$status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusfieldupdate']."<br>";

		$oContact_Field->data = array("fieldtype","fieldtext","fieldrequire","sequence","width","height","default_val","valid_type","sendauto","itemrow","fieldsize","fileexten","uploadtype");
		$result = $oContact_Field->getDetail($field_id);
		if ($myrow = mysql_fetch_row($result)){
			$fieldtype=$fieldtype?$fieldtype:$myrow[0];
			$fieldtext = stripslashes($myrow[1]);
			$require = $myrow[2];
			$sequence = $myrow[3];
			$fieldwidth = $myrow[4];
			$fieldheight = $myrow[5];
			$default_val = stripslashes(htmlentities($myrow[6]));
			$valid_type = $myrow[7];
			$sendauto = $myrow[8];
			$itemrow = $myrow[9];
			$fieldsize = $myrow[10];
			$fileexten = $myrow[11];
			$uploadtype = $myrow[12];
		}
		mysql_free_result($result);
	break;
	case "deleteone":
		$oContact_Field->delete($field_id);	
		$oContact_Field->deleteOptions($field_id); // clear field options
		
		$field_id=""; $pageaction="";
		$status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusfielddelete']."<br>";
	break;
	case "delete":
		if(is_array($delete_id) && @count($delete_id)>0){
			for($i=0; $i<count($delete_id); $i++){ 
				$field_id=$delete_id[$i];
				$oContact_Field->delete($field_id);	
				$oContact_Field->deleteOptions($field_id); // clear field options
			}
			$field_id=""; $pageaction="";
			$status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusfielddelete']."<br>";
		}
	break;
	case "updatelist":
		if(is_array($fieldlisid) && @count($fieldlisid)>0){
			for($i=0; $i<count($fieldlisid); $i++){ 
				$fieldid=$fieldlisid[$i];
				$fieldseq=$fieldseqarr[$fieldid];
				$oContact_Field->data = array("sequence");
				$oContact_Field->value = array($fieldseq);
				$oContact_Field->update($fieldid);
			}
			$pageaction="";
			$status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['listupdated']."<br>";
		}
	break;
}

echo"<table border=\"0\" width=\"100%\"><tr><td><b>".$lang['contactform']['contactform']."</b></td><td>";
include("wce.menu.php");
echo"</td></tr></table><hr size=\"1\" color=\"#606060\">";

if (!isset($fieldtype) || $fieldtype=="") { $fieldtype="textbox"; }

echo"
<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"98%\" align=\"center\">
<form name=\"thisform\" action=\"index.php?component=$component&page=$page\" method=\"post\">
<input type=\"hidden\" name=\"pageaction\" value=\"$pageaction\">
<input type=\"hidden\" name=\"field_id\" value=\"$field_id\">
<input type=\"hidden\" name=\"valopt_str\">
<input type=\"hidden\" name=\"valdefault_str\">
<input type=\"hidden\" name=\"valempty_str\">

<tr><td valign=\"top\" colspan=\"5\"><b>".$lang['contactform']['managefield']."</b><br>$status_message<br></td></tr>
<tr><td valign=\"top\" width=\"20%\">".$lang['contactform']['fieldtype']."</td><td colspan=\"4\">
	<select name=fieldtype onChange=\"document.thisform.submit();\">
		<option value=\"textbox\" ".($fieldtype=="textbox"?"selected":"").">".$lang['contactform']['textbox']."</option>
		<option value=\"select\" ".($fieldtype=="select"?"selected":"").">".$lang['contactform']['dropdownselection']."</option>
		<option value=\"checkbox\" ".($fieldtype=="checkbox"?"selected":"").">".$lang['contactform']['checkbox']."</option>
		<option value=\"radio\" ".($fieldtype=="radio"?"selected":"").">".$lang['contactform']['radiobutton']."</option>
		<option value=\"textarea\" ".($fieldtype=="textarea"?"selected":"").">".$lang['contactform']['textarea']."</option>
		<option value=\"password\" ".($fieldtype=="password"?"selected":"").">".$lang['contactform']['password']."</option>
		<option value=\"multiselect\" ".($fieldtype=="multiselect"?"selected":"").">".$lang['contactform']['multiselect']."</option>
		<option value=\"fileupload\" ".($fieldtype=="fileupload"?"selected":"").">".$lang['contactform']['fileupload']."</option>
		<option value=\"datetype\" ".($fieldtype=="datetype"?"selected":"").">".$lang['contactform']['datetype']."</option>
	</select></td></tr>
";

/*** Loading the relative input layout base on field type ***/
switch ($fieldtype){
	case "textbox" :		include ("fieldprop/fp_textbox.php");		break;
	case "select" :			include ("fieldprop/fp_select.php");		break;
	case "checkbox" :		include ("fieldprop/fp_checkbox.php");		break;
	case "radio" :			include ("fieldprop/fp_radio.php");			break;
	case "textarea" :		include ("fieldprop/fp_textarea.php");		break;
	case "password" :		include ("fieldprop/fp_password.php");		break;
	case "multiselect" :	include ("fieldprop/fp_multiselect.php");	break;
	case "fileupload" :		include ("fieldprop/fp_fileupload.php");	break;
	case "datetype" :		include ("fieldprop/fp_datetype.php");		break;
}

echo"<tr><td>&nbsp;</td><td colspan=\"4\">";

	if($pageaction=="edit" || $pageaction=="update"){
		echo"
			<input type=\"button\" value=\" ".$lang['contactform']['update']." \" onclick=javascript:formBeforeSubmit(\"$fieldtype\",'select_opt','update');>
			<input type=\"button\" value=\" ".$lang['contactform']['delete']." \" onclick=\"ConfirmDelete($field_id)\">
			<input type=\"button\" value=\" ".$lang['contactform']['back']." \" onclick=\"window.location='index.php?component=$component&page=$page'\">
		";
	 }else{
		echo"<input type=\"button\" value=\"  ".$lang['contactform']['add']."  \" onclick=javascript:formBeforeSubmit(\"$fieldtype\",'select_opt','add');>";
	}
	
echo"</td></tr></form></table><br>";

	$sort=array("sequence","field_id","fieldtext");
	if($sortby==""){ $sortby=$sort[0]; } 
	if($sortseq==""){ $sortseq="asc"; }
	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sort[$i]&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['contactform']['sortasc']."\"></a>"; }
	for($i=0;$i<count($sort);$i++){
		if($sortby==$sort[$i]){
		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=desc><img src=\"common/image/sort_desc.gif\" border=0 alt=\"".$lang['contactform']['sortdesc']."\"></a>"; }
		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['contactform']['sortasc']."\"></a>"; }
	}}

	echo "
	<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"98%\" align=\"center\">
			<tr><td><a href=\"javascript:ConfirmDelete();\">".$lang['contactform']['deleteselected']."</a>&nbsp;|&nbsp;<a href=\"javascript:UpdateList()\">".$lang['contactform']['updatelist']."</a></td></tr>
	</table>
		
	<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\" width=\"98%\" align=\"center\">		
	<form name=\"frmList\" action=\"index.php?component=$component&page=$page\" method=\"post\">
	<input type=\"hidden\" name=\"sortby\" value=\"$sortby\">
	<input type=\"hidden\" name=\"sortseq\" value=\"$sortseq\">
	<input type=\"hidden\" name=\"pageaction\">
			
	    <tr bgcolor=\"#E6E6E6\" height=\"24\">
	      <td class=\"gridTitle\" align=\"center\" width=\"25\">&nbsp;<b><input type=\"checkbox\" id=\"checkAll\" onclick=\"selectAll()\"></td>
	      <td class=\"gridTitle\" width=\"45\">&nbsp;<b>".$lang['contactform']['id']."</b> $sortlink[1]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['contactform']['fieldtext']."</b> $sortlink[2]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"80\">&nbsp;<b>".$lang['contactform']['required']."</b> &nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"80\">&nbsp;<b>".$lang['contactform']['sequence']."</b> $sortlink[0]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"30\">&nbsp;</td>
	    </tr> 	
	";
	$delete_id = array(); $fieldseqarr = array(); $fieldlisid=array();
   	$oContact_Field->data = array("field_id","fieldtext","fieldrequire","sequence");
   	$oContact_Field->order = "$sortby $sortseq";	    
   	$result=$oContact_Field->getList();	   	
   	$count=0;	   		
   	while($myrow=mysql_fetch_row($result)){
   		$count++; $fid = $myrow[0];  
   		$myrow[1]=stripslashes($myrow[1]);
   		$myrow[2]=($myrow[2]=="yes"?$lang['contactform']['yes']:$lang['contactform']['no']);
		$sequence = trim($myrow[3]);
   		echo "
   		  <tr id=\"row$count\" style=\"background:#F6F6F6\" height=\"24\"><input type=\"hidden\" name=\"fieldlisid[]\" value=\"$fid\">
   		  <td class=\"gridRow\" align=\"center\"><input type=\"checkbox\" id=\"check$count\" name=\"delete_id[]\" value=\"$myrow[0]\" onclick=\"if(this.checked==true){ selectRow('row$count'); }else{ deselectRow('row$count'); }\"></td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[0]</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[1]</td>
	      <td class=\"gridRow\" align=\"center\">$myrow[2]</td>
	      <td class=\"gridRow\" align=\"center\"><input type=\"text\" name=\"fieldseqarr[$fid]\" value=\"$sequence\" style=\"width:35px;\" onkeypress=\"nextSeq(this, event)\"></td>
		  <td class=\"gridRow\" align=\"center\">&nbsp;<a href=\"index.php?component=$component&page=$page&pageaction=edit&field_id=$myrow[0]&sortby=$sortby&sortseq=$sortseq&start=$start\"><img src=\"common/image/ico_edit.gif\" border=\"0\" alt=\"".$lang['contactform']['editthisrecord']."\"></a>&nbsp;</td>
	    </tr>";
   	}
   	mysql_free_result($result);
	
	echo "</form></table>
		<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"98%\" align=\"center\">
			<tr><td><a href=\"javascript:ConfirmDelete();\">".$lang['contactform']['deleteselected']."</a>&nbsp;|&nbsp;<a href=\"javascript:UpdateList()\">".$lang['contactform']['updatelist']."</a></td></tr>
		</table>
	";
   	
?>    

<script language=javascript>
	var isNS4 = (navigator.appName=="Netscape")?1:0;
	multioption=new Array(); newoption=new Array(); newdefault=new Array(); newempty=new Array(); 
	selectedindex = ""; selecteditem = "";
	
	function ConfirmDeleteOne(id){  
	    if(confirm('<? echo $lang['contactform']['confirmdelete'] ?>')){
	    	window.location='index.php?component=contactform&page=wce.field.php&pageaction=deleteone&field_id='+id;
		}
	}
	
	function ConfirmDelete(){  
	    if(confirm('<? echo $lang['contactform']['confirmdelete'] ?>')){
	    	document.frmList.pageaction.value='delete';
		    document.frmList.submit();
		}
	}
		
	function selectAll(){
		if(document.getElementById("checkAll").checked==true){
			document.getElementById("checkAll").checked=true;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=true; document.getElementById(\"row$i\").style.background='#D6DEEC'; \n"; }	?>
		}else{
			document.getElementById("checkAll").checked=false;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=false; document.getElementById(\"row$i\").style.background='#F6F6F6'; \n"; } ?>		
		}
	}

	function selectRow(row){
		document.getElementById(row).style.background="#D6DEEC";
	}

	function deselectRow(row){
		document.getElementById(row).style.background="#F6F6F6";
	}	

	function UpdateList(){
		document.frmList.pageaction.value='updatelist';
		document.frmList.submit();
	}	

	function addOption(optionvalue,asdefaultid,useemptyid){
		detect_opt="";
		if(newoption.length>0){ 
			for(i=0;i<newoption.length;i++){
				if(optionvalue==newoption[i]) { detect_opt="Yes"; }		
			}
		}	
		
		if (detect_opt==""){	
			if (asdefaultid){ asdefault="Yes"; } else { asdefault="No"; }
			if (useemptyid){ useempty="Yes"; } else { useempty="No"; }
			
			if ((optionvalue.length==0) ||(optionvalue==null)) {
				alert('<? echo $lang['contactform']['plsenteroption'] ?>');
			}else {
				if (optionvalue != ""){
					var opt = new Option(optionvalue,optionvalue);
					document.thisform.select_opt.options.add(opt);
					i= newoption.length;
					newoption[i] = optionvalue;
					newdefault[i] = asdefault;
					newempty[i] = useempty;
				}
			}
			
			document.thisform.as_default.checked=false;
			document.thisform.use_empty.checked=false;
			document.thisform.optionvalue.value="";
			document.thisform.optionvalue.focus();
		}else {
			alert('<? echo $lang['contactform']['fieldoptionexist'] ?>');
			document.thisform.optionvalue.value="";
			document.thisform.optionvalue.focus();
		}
	}

	function deleteOption(){
		var optionlist = document.thisform.select_opt;
		for(var i=0; i<optionlist.options.length; i++) {
			if(optionlist.options[i].selected && optionlist.options[i] != "") {
				optionlist.options[i]= null;
				break;
			}
	   	}
	   	newoption.splice(selecteditem, 1);
	   	newdefault.splice(selecteditem, 1);
	   	newempty.splice(selecteditem, 1);
	}

	function populateItem(index) { 
		selecteditem=index;
	} 
	
	function editnode2(j, btn1, btn2, btn3, dropdownid, defaultid, emptyid, textid){
		//j=-1 when dblclick and j=1 when update node
		//btn1=add_btn btn2=edit_btn btn3=del_btn
		var oAdd = document.getElementById(btn1);
		var oEdit = document.getElementById(btn2);
		var oDel = document.getElementById(btn3);
		var x = document.getElementById(dropdownid);
		var y = document.getElementById(defaultid);
		var z = document.getElementById(emptyid);
		var oText = textid;
		var i = x.selectedIndex;
		
		if (j<0){
			oAdd.style.display = 'none';
			oEdit.style.display = '';
			oDel.style.display = 'none';			
			oText.value = x.value;
			if (newdefault[i]=="Yes"){ y.checked=true; } else { y.checked=false; }
			if (newempty[i]=="Yes"){ z.checked=true; } else { z.checked=false; }
		}else{
			detect_opt="";
			
			if(newoption.length>0){ 
				for(a=0;a<newoption.length;a++){
					if(oText.value==newoption[a] && a!=i) { detect_opt="Yes"; }		
				}
			}	
			
			if (detect_opt==""){
				if (y.checked){ asdefault="Yes"; } else{ asdefault="No"; }
				if (z.checked){ useempty="Yes"; } else{ useempty="No"; }
				
				if ((oText.length==0) ||(oText.value==null)) {
					alert('<? echo $lang['contactform']['plsenteroption'] ?>');
				}else {
					if (oText.value != ""){
						oAdd.style.display = '';
						oEdit.style.display = 'none';
						oDel.style.display = '';
						x.options[i].value = oText.value;
						x.options[i].text = oText.value;	
			
						newoption[i] = oText.value;
						newdefault[i] = asdefault;
						newempty[i] = useempty;
						
						document.thisform.optionvalue.value="";
						document.thisform.optionvalue.focus();
						y.checked=false;
						z.checked=false;
					}
				}
			} else {
				alert('<? echo $lang['contactform']['fieldoptionexist'] ?>');
				document.thisform.optionvalue.value=x.options[i].value;
				document.thisform.optionvalue.focus();
			}
		}
	}
	
	function sortlist2(j, dropdownid, defaultid, emptyid){
		//move obj up when j is -1 and down when j=1
		var x = document.getElementById(dropdownid);
		var y = document.getElementById(defaultid);
		var z = document.getElementById(emptyid);
		var tempvalue = "";
		var temptext = "";
		var tempoption ="";
		var tempdefault ="";
		var tempempty ="";
		var IsLimit = false;
		
		i = x.selectedIndex;		
		
		//make sure hv selected item
		if (i>=0){			
			k = i + j;
			if (j<0) { IsLimit = x.options[0].selected; 
			} else { IsLimit = x.options[x.length-1].selected; }
			
			if (IsLimit == false){
				tempvalue = x.options[i].value;
				temptext = x.options[i].text;
				x.options[i].value = x.options[k].value;
				x.options[i].text = x.options[k].text;
				x.options[k].value = tempvalue;
				x.options[k].text = temptext;
				x.options[k].selected = true;
				
				tempoption = newoption[i];
				newoption[i] = newoption[k];
				newoption[k] = tempoption;
				
				tempdefault = newdefault[i];
				newdefault[i] = newdefault[k];
				newdefault[k] = tempdefault;				
							
				tempempty = newempty[i];
				newempty[i] = newempty[k];
				newempty[k] = tempempty;
			}
		}
	}
	
	function validate(){
		if(document.thisform.fieldtext.value==""){
			alert('<? echo $lang['contactform']['plsfieldtext'] ?>'); document.thisform.fieldtext.focus();
		}else{
			document.thisform.submit()
		}
	}

	function formBeforeSubmit(fieldtype, dropdownid, val_pageaction){
		var count_opt="";
		if (document.thisform.fieldtext.value==""){
			alert('<? echo $lang['contactform']['plsenterfieldtext'] ?>');
			document.thisform.fieldtext.select();
		} else {
			if (fieldtype=="select" || fieldtype=="multiselect" || fieldtype=="radio" || fieldtype=="checkbox"){
				var x = document.getElementById(dropdownid);
				var opt_str="";	var default_str="";	var empty_str="";
				if (document.thisform.loadfield[1].checked){
					if (x.length>0){
						for (j=0;j<x.length-1;j++){
							opt_str=opt_str+x.options[j].value+"%**%";
							default_str=default_str+newdefault[j]+"%**%";
							empty_str=empty_str+newempty[j]+"%**%";
						}
						opt_str=opt_str+x.options[j].value;
						default_str=default_str+newdefault[j];
						
						empty_str=empty_str+newempty[j]; 
						document.thisform.valempty_str.value=empty_str; 
						
						document.thisform.valopt_str.value=opt_str;
						document.thisform.valdefault_str.value=default_str;
					} else { count_opt="No"; }
				}
			}
			
			if (count_opt=="No"){
				alert('<? echo $lang['contactform']['plsopt'] ?>');
			} else {
				document.thisform.pageaction.value=val_pageaction;
				document.thisform.submit();
			} 
		}
	}
	
	<?php	
		if ($pageaction=="edit" || $pageaction=="update"){
			$oContact_FieldOption->data = array("optionvalue","as_default","use_empty");
			$oContact_FieldOption->where = "field_id='$field_id'";	
			$oContact_FieldOption->order = "option_id";
			$result = $oContact_FieldOption->getList();
			while($myrow=mysql_fetch_row($result)){
				if ($myrow[1]=="Yes"){ $default=True; } else { $default=False; }
				if ($myrow[2]=="Yes"){ $empty=True; } else { $empty=False; }
				echo "addOption(\"$myrow[0]\",\"$default\",\"$empty\");";
			}mysql_free_result($result);
		}
	?>
</script>