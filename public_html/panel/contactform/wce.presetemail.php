<?

if($pageaction=="save"){
  
  $contact_thankyou = addslashes($FCKeditor);
  $oSystem->setValue("contact_emailnotice",$contact_emailnotice?"yes":"no"); 
  $oSystem->setValue("contact_noticesubject",addslashes($contact_noticesubject)); 
  $oSystem->setValue("contact_noticecontent",addslashes($contact_noticecontent)); 
  $oSystem->setValue("contact_autoresponse",$contact_autoresponse?"yes":"no"); 
  $oSystem->setValue("contact_emailsubject",addslashes($contact_emailsubject)); 
  $oSystem->setValue("contact_emailcontent",addslashes($contact_emailcontent)); 
  $oSystem->setValue("contact_adminemail",addslashes($contact_adminemail)); 
  $oSystem->setValue("contact_sendextra",addslashes($contact_sendextra)); 

  $status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statusprefersave']."<br>";	  
}
if($pageaction=="default"){

  $oSystem->setValue("contact_emailnotice","yes"); 
  $oSystem->setValue("contact_noticesubject","New Inquiry Posted #ID : [[contact_id]]"); 
  $oSystem->setValue("contact_noticecontent","There was a new Inquiry posted at your website. \r\n\r\n[[message]]\r\n\r\n"); 
  $oSystem->setValue("contact_autoresponse","yes"); 
  $oSystem->setValue("contact_emailsubject","Thank You For Contacting Us"); 
  $oSystem->setValue("contact_emailcontent","This is an acknowledgement for you that your message has  been sent successfully. We will attend to your Inquiry shortly.\r\n\r\n[[message]]\r\n\r\nRegards\r\nYour Company"); 
  $oSystem->setValue("contact_adminemail",""); 
  $oSystem->setValue("contact_sendextra",""); 
  
  $status_message = "<b>".$lang['contactform']['status']." :</b> ".$lang['contactform']['statuspreferdefault']."<br>";	  
}

echo"<table border=\"0\" width=\"100%\"><tr><td><b>".$lang['contactform']['contactform']."</b></td><td>";
include("wce.menu.php");
echo"</td></tr></table><hr size=\"1\" color=\"#606060\">";

echo"
	<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" width=\"98%\" align=\"center\">
	<form name=\"thisform\" action=\"index.php?component=$component&page=$page\" method=\"post\">
	<input type=\"hidden\" name=\"pageaction\">
	
	<tr><td valign=\"top\"><b>".$lang['contactform']['presetemail']."</b><br>$status_message<br></td></tr>
	<tr><td valign=\"top\">
		&nbsp;<b><u>".$lang['contactform']['emailnotification']."</u></b><br><br>
		<table border=\"0\" width=\"100%\" cellpadding=\"2\">
		<tr><td colspan=\"2\">".$lang['contactform']['emailnotificationdesc']."</td></tr>
		<tr><td colspan=\"2\">".$lang['contactform']['enableemailnotification']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"contact_emailnotice\" value=\"yes\" ".($oSystem->getValue("contact_emailnotice")=="yes"?"checked":"")."></td></tr>
		<tr><td width=\"22%\" valign=\"top\">".$lang['contactform']['senderemail']."</td><td><input type=\"text\" name=\"contact_adminemail\" style=\"width:200px;\" value=\"".stripslashes($oSystem->getValue("contact_adminemail"))."\"><br>(".$lang['contactform']['senderemailtip'].")</td></tr>
		<tr><td valign=\"top\">".$lang['contactform']['sendextra']."</td><td><input type=\"text\" name=\"contact_sendextra\" style=\"width:300px\" value=\"".stripslashes(htmlentities($oSystem->getValue("contact_sendextra")))."\"><br>(".$lang['contactform']['sendextratip'].")</td></tr>
		<tr><td>".$lang['contactform']['subject']."</td><td><input type=\"text\" name=\"contact_noticesubject\" style=\"width:300px\" value=\"".stripslashes(htmlentities($oSystem->getValue("contact_noticesubject")))."\"></td></tr>
		<tr><td valign=\"top\">".$lang['contactform']['message']."</td><td><textarea name=\"contact_noticecontent\" cols=\"80\" rows=\"8\">".stripslashes(htmlentities($oSystem->getValue("contact_noticecontent")))."</textarea></td></tr>
		</table><br>	
	
		&nbsp;<b><u>".$lang['contactform']['autoresponder']."</u></b><br><br>
		<table border=\"0\" width=\"100%\" cellpadding=\"2\">
		<tr><td colspan=\"2\">".$lang['contactform']['autoresponderdesc']."</td></tr>
		<tr><td colspan=\"2\">".$lang['contactform']['enableautoresponder']." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"contact_autoresponse\" value=\"yes\" ".($oSystem->getValue("contact_autoresponse")=="yes"?"checked":"")."></td></tr>
		<tr><td width=\"22%\">".$lang['contactform']['subject']."</td><td><input type=\"text\" name=\"contact_emailsubject\" style=\"width:300px\" value=\"".stripslashes(htmlentities($oSystem->getValue("contact_emailsubject")))."\"></td></tr>
		<tr><td valign=\"top\">".$lang['contactform']['message']."</td><td><textarea name=\"contact_emailcontent\" cols=\"80\" rows=\"8\">".stripslashes(htmlentities($oSystem->getValue("contact_emailcontent")))."</textarea></td></tr>
		</table><br>	
	</td></tr>
	<tr><td colspan=\"2\"><br>
		<input type=\"button\" value=\" ".$lang['contactform']['save']." \" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
		<input type=\"button\" value=\" ".$lang['contactform']['usedefault']." \" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">
	</td></tr>
	</form></table>
	<br>
";

?>