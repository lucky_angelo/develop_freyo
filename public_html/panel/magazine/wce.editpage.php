<style type="text/css">

	.saved {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		position: fixed;
		width: 138px;
  		height: 70px;
  		top: 50%;
  		left: 50%;
  		margin-top: -70px;
  		margin-left: -70px;
		padding: 25px 45px;
		border: 1px solid rgba(0,107,14,1);
		-webkit-border-radius: 10px;
		border-radius: 10px;
		font: normal normal bold 16px/1 Arial, Helvetica, sans-serif;
		color: rgba(0,107,14,1);
		-o-text-overflow: ellipsis;
		text-overflow: ellipsis;
		background: rgba(90,232,88,1);
		-webkit-box-shadow: 0 0 8px 4px rgba(0,0,0,0.4) ;
		box-shadow: 0 0 8px 4px rgba(0,0,0,0.4) ;
	}

	.fail {
	    -webkit-box-sizing: border-box;
	    -moz-box-sizing: border-box;
	    box-sizing: border-box;
	    position: fixed;
	    width: 138px;
  		height: 70px;
  		margin-top: -70px;
  		margin-left: -138px;
  		top: 50%;
  		left: 50%;
	  	padding: 25px 29px;
	  	border: 1px solid rgba(130,10,0,1);
	    -webkit-border-radius: 10px;
	    border-radius: 10px;
	  	font: normal normal bold 16px/1 Arial, Helvetica, sans-serif;
	  	color: rgba(130,10,0,1);
	    -o-text-overflow: ellipsis;
	  	text-overflow: ellipsis;
	  	white-space: nowrap;
	  	background: rgba(255,156,147,1);
	    -webkit-box-shadow: 0 0 8px 4px rgba(0,0,0,0.4) ;
	  	box-shadow: 0 0 8px 4px rgba(0,0,0,0.4) ;
	}

	#note {
		color: rgba(0,107,14,1);
		background: rgba(90,232,88,1);
		border-radius: 3px;
		width: 50%;
		margin-bottom: 10px;
		padding: 5px;
	}
</style>
<?

$totalPage = $oMagz_Pages->getLastPage($magazine_id) + 1;

if($pageaction == "edit" || $pageaction == "update"){

	$totalPage--;

}


$fdateUploaded = date('Y-m-d');
switch($pageaction){

	case "add":
		
		if($page_type != "ads"){

			$fimageName = strtolower(str_replace(" ","_",trim($_FILES['magazine_image']['name'])));

			$uniqueid = md5(uniqid(rand(), true));	

			$uniqueid=substr($uniqueid,0,5);

			if($page_type == "video"){
				$fwebmName = strtolower(str_replace(" ","_",trim($_FILES['magazine_webm']['name'])));
				$fogvName = strtolower(str_replace(" ","_",trim($_FILES['magazine_ogv']['name'])));
				$fposterName = strtolower(str_replace(" ","_",trim($_FILES['magazine_poster']['name'])));
			}

			if(($fimageName!="" && $page_type=="image") || ($fposterName!="" && $fimageName!="" && $fwebmName!="" && $fogvName!="" && $page_type=="video")){ 

				$fimageName=$uniqueid."_".$fimageName;

				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {

					$oMagz_Pages->uploadFile($_FILES['magazine_image']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fimageName));
					if($fwebmName!=""){
						$fwebmName=$uniqueid."_".$fwebmName;
						$oMagz_Pages->uploadFile($_FILES['magazine_webm']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fwebmName));
					}
					if($fogvName!=""){
						$fogvName=$uniqueid."_".$fogvName;
						$oMagz_Pages->uploadFile($_FILES['magazine_ogv']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fogvName));
					}
					if($fogvName!=""){
						$fposterName=$uniqueid."_".$fposterName;
						$oMagz_Pages->uploadFile($_FILES['magazine_poster']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fposterName));
					}
				}

			}

		}

		else{

			$fimageName = $magazine_image;

		}

		if($page_number != $totalPage){

			$oMagz_Pages->updatePage($magazine_id, $page_number);

		}



		$oMagz_Pages->data = array("magazine_id","magazine_page","video_webm","video_ogv","video_poster","page_number","page_type","date_uploaded","magazine_content");

		$oMagz_Pages->value = array($magazine_id,str_replace("'", "", $fimageName),str_replace("'", "", $fwebmName),str_replace("'", "", $fogvName),str_replace("'", "", $fposterName),$page_number,$page_type,$fdateUploaded,addslashes($mag_content));

		$oMagz_Pages->add();

		// if($page_type != "ads"){
		// 	$oMagz->data = array('date_updated');
		// 	$oMagz->value = array(date("Y-m-d H:i:s"));
		// 	$oMagz->update($magazine_id);
		// }

		$totalPage++; 



        clearServerMemcached();
		$status_message = "<b>Status :</b> New Magazine Page added.<br>";	

	break;

	

	case "edit":

		$oMagz_Pages->data = array("magazine_page","video_webm","video_ogv","page_number","page_type","date_uploaded","video_poster","magazine_content");

		$result=$oMagz_Pages->getDetail($page_id);

		if($myrow=mysql_fetch_row($result)){

			$dis_thumbnail=substr($myrow[0],6,strlen($myrow[0])-6);
			$dis_thumbnail2=substr($myrow[1],6,strlen($myrow[1])-6);
			$dis_thumbnail3=substr($myrow[2],6,strlen($myrow[2])-6);
			$dis_thumbnail4=substr($myrow[6],6,strlen($myrow[6])-6);

			if($myrow[0]!="" && $myrow[4] != "ads"){ $magPage = "$dis_thumbnail <a href='_files/magazines/".date('Y/m/d', strtotime($myrow[5]))."/$myrow[0]' onclick=\"return popImage('_files/magazines/".date('Y/m/d', strtotime($myrow[5]))."/$myrow[0]','Image')\">View</a>"; }else{ $magPage=""; }

			if($myrow[4] == "video"){$magPage = $dis_thumbnail; $magWEBM = $dis_thumbnail2; $magOGV = $dis_thumbnail3; $magPoster = $dis_thumbnail4;}

			if($myrow[4] == "ads"){$magPage = $myrow[0];}

			$magPageNum=$myrow[3];

			$page_type = $myrow[4];

			$mag_content = stripslashes($myrow[7]);

		}

		$status_message = "<b>Status :</b> Edit Magazine Page.<br>";	

	break;

	

	case "update":
	// var_dump($mag_content);
		if($page_type != "ads"){

			$oMagz_Pages->data = array('date_uploaded');
			$result=$oMagz_Pages->getDetail($page_id);
			if($myrow=mysql_fetch_row($result)){
				$fdateUploaded = $myrow[0];
			}

			$fimageName = strtolower(str_replace(" ","_",trim($_FILES['magazine_image']['name'])));

			$uniqueid = md5(uniqid(rand(), true));	

			$uniqueid=substr($uniqueid,0,5);

			if($page_type == "video"){
				$fwebmName = strtolower(str_replace(" ","_",trim($_FILES['magazine_webm']['name'])));
				$fogvName = strtolower(str_replace(" ","_",trim($_FILES['magazine_ogv']['name'])));
				$fposterName = strtolower(str_replace(" ","_",trim($_FILES['magazine_poster']['name'])));
			}

			if($fimageName!="" || $fwebmName!="" || $fogvName!="" || $fposterName!="" || isset($mag_content)){ 

				$updateFields = array("magazine_content");
				$updateData = array(addslashes($mag_content));
				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {

					if($fimageName!=""){
						$fimageName=$uniqueid."_".$fimageName;
						$oMagz_Pages->uploadFile($_FILES['magazine_image']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d',strtotime($fdateUploaded)),str_replace("'", "", $fimageName));
						array_push($updateFields, "magazine_page");
						array_push($updateData, $fimageName);
					}
					if($fwebmName!=""){
						$fwebmName=$uniqueid."_".$fwebmName;
						$oMagz_Pages->uploadFile($_FILES['magazine_webm']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)), str_replace("'", "", $fwebmName));
						array_push($updateFields, "video_webm");
						array_push($updateData, $fwebmName);
					}
					if($fogvName!=""){
						$fogvName=$uniqueid."_".$fogvName;
						$oMagz_Pages->uploadFile($_FILES['magazine_ogv']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fogvName));
						array_push($updateFields, "video_ogv");
						array_push($updateData, $fogvName);
					}
					if($fposterName!=""){
						$fposterName=$uniqueid."_".$fposterName;
						$oMagz_Pages->uploadFile($_FILES['magazine_poster']['tmp_name'],$path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($fdateUploaded)),str_replace("'", "", $fposterName));
						array_push($updateFields, "video_poster");
						array_push($updateData, $fposterName);
					}
				}
				// var_dump($_FILES);
				// var_dump($fimageName);
				// var_dump($fwebmName);
				// var_dump($fogvName);
				// var_dump($fposterName);

				if($page_type == "image" && $fimageName != ""){
				    $updateFields = array("date_uploaded", "magazine_page","video_webm","video_ogv", "video_poster", "magazine_content");
				    // $updateData = array($fdateUploaded, addslashes($fimageName), addslashes($fwebmName), addslashes($fogvName), addslashes($fposterName, addslashes($mag_content));
				    $updateData = array($fdateUploaded, str_replace("'", "", $fimageName), str_replace("'", "", $fwebmName), str_replace("'", "", $fogvName), str_replace("'", "", $fposterName), addslashes($mag_content));
				}
				// elseif($page_type == "image" && $mag_content != ""){
				// 	$updateFields = array("magazine_content");
				// 	$updateData = array(addslashes($mag_content));
				// }

				$oMagz_Pages->data = $updateFields;
				$result=$oMagz_Pages->getDetail($page_id);

				if($myrow=mysql_fetch_assoc($result)){
					if($fimageName!=""){
						if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['magazine_page']) && $myrow['magazine_page']!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['magazine_page']);
						}
					}
					if($fwebmName!=""){
						if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_webm']) && $myrow['video_webm']!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_webm']);
						}
					}
					if($fogvName!=""){
						if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_ogv']) && $myrow['video_ogv']!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_ogv']);
						}
					}
					if($fposterName!=""){
						if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_poster']) && $myrow['video_poster']!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow['date_uploaded']))."/".$myrow['video_poster']);
						}
					}
					
				}
				mysql_free_result($result);

				$oMagz_Pages->data = $updateFields;
				$oMagz_Pages->value = $updateData;
				// var_dump($updateFields);
				// var_dump($updateFields);
				$oMagz_Pages->update($page_id);

				// $oMagz->data = array('date_updated');
				// $oMagz->value = array(date("Y-m-d H:i:s"));
				// $oMagz->update($magazine_id);
			}

		}

		else{

			$oMagz_Pages->data = array("magazine_page","date_uploaded","video_webm","video_ogv","video_poster");

			$result=$oMagz_Pages->getDetail($page_id);

			if($myrow=mysql_fetch_row($result)){	
				if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[0]") && $myrow[0]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[0]");
				}
				if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[2]") && $myrow[2]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[2]");
				}
				if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[3]") && $myrow[3]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[3]");
				}
				if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[4]") && $myrow[4]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[1]))."/$myrow[4]");
				}
			}

			mysql_free_result($result);

		}



		$oMagz_Pages->updatePage($magazine_id, $page_number, "update", $update_page_number);



		if($page_type == "ads"){

			$oMagz_Pages->data = array("magazine_page", "page_number", "page_type", "date_uploaded");

			$oMagz_Pages->value = array($magazine_image, $page_number, $page_type, $fdateUploaded);

		}else{

			$oMagz_Pages->data = array("page_number", "page_type");

			$oMagz_Pages->value = array($page_number, $page_type);

		}

		$oMagz_Pages->update($page_id);

		

		$oMagz_Pages->data = array("magazine_page","page_number","page_type","date_uploaded","video_webm","video_ogv","video_poster","magazine_content");

		$result=$oMagz_Pages->getDetail($page_id);

		if($myrow=mysql_fetch_row($result)){

			$dis_thumbnail=substr($myrow[0],6,strlen($myrow[0])-6);
			$dis_thumbnail2=substr($myrow[4],6,strlen($myrow[4])-6);
			$dis_thumbnail3=substr($myrow[5],6,strlen($myrow[5])-6);
			$dis_thumbnail3=substr($myrow[5],6,strlen($myrow[5])-6);
			$dis_thumbnail4=substr($myrow[6],6,strlen($myrow[6])-6);

			if($myrow[0]!="" && $myrow[2] == "image"){ $magPage = "$dis_thumbnail <a href='_files/magazines/".date('Y/m/d', strtotime($myrow[3]))."/$myrow[0]' onclick=\"return popImage('_files/magazines/".date('Y/m/d', strtotime($myrow[3]))."/$myrow[0]','Image')\">View</a>"; }else{ $magPage=""; }

			if($myrow[2] == "video"){$magPage = $dis_thumbnail; $magWEBM = $dis_thumbnail2; $magOGV = $dis_thumbnail3; $magPoster = $dis_thumbnail4;}

			if($myrow[2] == "ads"){$magPage = $myrow[0];}



			$magPageNum=$myrow[1];

			$page_type = $myrow[2];

			$mag_content = stripslashes($myrow[7]);
		}

        clearServerMemcached();
		$status_message = "<b>Status :</b> Magazine Page updated.<br>";	

	break;

	

	case "delete":

		$oMagz_Pages->data = array("magazine_page", "page_number", "date_uploaded", "video_webm", "video_ogv","video_poster");

		$result=$oMagz_Pages->getDetail($page_id);

		if($myrow=mysql_fetch_row($result)){	

			if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[0]") && $myrow[0]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[0]"); 	}
			if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[3]") && $myrow[3]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[3]"); 	}
			if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[4]") && $myrow[4]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[4]"); 	}
			if(file_exists($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[5]") && $myrow[5]!=""){ unlink($path["docroot"]."_files/magazines/".date('Y/m/d', strtotime($myrow[2]))."/$myrow[5]"); 	}


			$page_number = $myrow[1];

		}

		mysql_free_result($result);



		if($page_number != $totalPage){

			$oMagz_Pages->updatePage($magazine_id, $page_number, "delete");

		}



		$oMagz_Pages->delete($page_id);



		$totalPage--;

        clearServerMemcached();
		$status_message = "<b>Status :</b> Magazine Page deleted.<br>";	  		

	break;
	case 'ajaxsort':
		header('content-type: application/json');
		ob_clean();
		ob_start();
		mysql_query('BEGIN');
		mysql_query('START TRANSACTION');
		try{
			$indexList = array();
			$il = 0;
			// compare the changes between current and the original order
			foreach($ids as $i => $id) {
				if($ids[$i] != $original_order[$i]) {
					$indexList[$il] = $i; // put the changed indexes in the array
					$il++;
				}
			}

			// initialize the index changed
			$qCount = 0;
			foreach($indexList as $i => $id) {
				$qry="UPDATE magazine_pages SET page_number = ".($id+1)." WHERE page_id= " .$ids[$id]. "";
				mysql_query($qry) or die(mysql_error()); // update the index changed
				$qCount++;
			}
			// $qry2="UPDATE magazine_publisher SET date_updated = NOW() WHERE magazine_id= " .$magazine_id. "";
			// mysql_query($qry2) or die(mysql_error());
			mysql_query('COMMIT');
			echo true;
		} catch(Exception $e) {	
			mysql_query('ROLLBACK');
			echo false;
		}
		die();
	break;

}



?>



<table border=0 width=100%><tr><td><b>Magazine Publisher</b></td><td><? include("wce.menu.php") ?></td></tr></table>

<hr size=1 color=#606060><? echo $status_message ?><br>

<!--Body Start-->



<b style="display: block; padding: 0 0 15px;"><? echo "Editing Pages of ".str_replace('_', ' ', $magTitle); ?></b>

<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>

<form name="thisform" action="index.php?component=magazine&page=wce.editpage.php&<? echo "magazine_id=$magazine_id&magTitle=$magTitle" ?>" method="post" enctype="multipart/form-data">

<input type=hidden name="pageaction">

<input type=hidden name="page_id" value="<? echo $page_id ?>">

<input type=hidden name="update_page_number" value ="<? echo $page_number ?>">

<tr>

<td valign=top width=20%>Magazine Page Type</td>

<td>

		<input type="radio" required="required" name="page_type" id="pt_image" value="image" <?=((!$page_type)?"checked=\"checked\"":($page_type == "image")?"checked=\"checked\"":"")?> />

	    <label for="pt_image">Image</label>

	    <input type="radio" required="required" name="page_type" id="pt_video" value="video" <?=(($page_type == "video")?"checked=\"checked\"":"")?> />

	    <label for="pt_video">Video</label>

	    <input type="radio" required="required" name="page_type" id="pt_ads" value="ads" <?=(($page_type == "ads")?"checked=\"checked\"":"")?> />

	    <label for="pt_ads">Ads</label>

</td></tr>

<tr id="magazine_image_row" class="magazine_image_row">

<td valign=top width=20%>Magazine Image<br/>(768px x 993px)</td>

<td><input id="mag_image" type="file" name="magazine_image" style="width:230px" accept="image/*"> <? echo $magPage ?> </td>

</tr>

<tr class="magazine_image_row"><td colspan=3>Magazine Content</td></tr>
<tr class="magazine_image_row"><td colspan=3>
<table border=0 width=500px cellpadding=0 cellspacing=0 height=300px><tr><td>
	<textarea id="mag_content" name="mag_content"><?php echo $mag_content; ?></textarea>
    <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
        CKEDITOR.replace( 'mag_content', { 
        	toolbar: [ { 
        		name: 'basicStyle', 
        		groups: [ 'list', 'style' ],
        		items: [ 'NumberedList', 'BulletedList', '-', 'Bold', 'Italic', 'Underline', 'Styles', 'Format' ] }
        	],
        	height: '300px',
        	width:'420px' 
        } );
    </script>
</td></tr></table><br>
</td></tr>

<tr class="magazine_video_row" style="display:none;">
<td valign=top width=20%>Magazine Video</td>
<td>GIF: <input id="mag_video" class="mag_video" type="file" name="magazine_poster" style="width:230px" accept="image/gif"> <? echo $magPoster ?></td>
</tr>

<tr class="magazine_video_row" style="display:none;">
<td></td>
<td>MP4: <input id="mag_video1" class="mag_video" type="file" name="magazine_image" style="width:230px" accept="video/mp4"> <? echo $magPage ?></td>
</tr>

<tr class="magazine_video_row" style="display:none;">
<td></td>
<td>WEBM: <input id="mag_video2" class="mag_video" type="file" name="magazine_webm" style="width:230px" accept="video/webm"> <? echo $magWEBM ?></td>
</tr>

<tr class="magazine_video_row" style="display:none;">
<td></td>
<td>OGV: <input id="mag_video3" class="mag_video" type="file" name="magazine_ogv" style="width:230px" accept="video/ogg"> <? echo $magOGV ?></td>
</tr>

<tr id="magazine_ads_row" style="display:none;">

<td valign=top width=20%>Magazine Ads</td>

<td><select id="mag_ads" name="magazine_image" style="width:230px">

<option value="all" <? echo $magPage=='all'?"selected":$magPage==""?"selected":""; ?>>All</option>

<option value="image" <? echo $magPage=='image'?"selected":""; ?>>Image</option>

<option value="video" <? echo $magPage=='video'?"selected":""; ?>>Video</option>

<!-- <option value="form" <? //echo $magPage=='form'?"selected":""; ?>>Form</option> -->

</select></td></tr>





<tr><td valign=top>Page Number</td><td><select name="page_number">

	<? 

	if(isset($magPageNum) && $magPageNum != ""){

		$selected = $magPageNum;

	}

	else{

		$selected = $totalPage;

	}

	for($w = 1; $w <= $totalPage; $w++) {

		echo "<option value='$w' ".($w==$selected?'selected':'').">$w</option>";

	} ?> 

</select></td></tr>

<tr><td valign=top></td><td>

	<? if($pageaction=="edit" || $pageaction=="update"){ ?>

		<input type=button value="Update" onclick="document.thisform.pageaction.value='update';validate('edit')">

		<input type=button value="Delete" onclick="ConfirmDelete(<? echo $page_id ?>)">

		<input type=button value="Back" onclick="<? echo "window.location='index.php?component=magazine&page=wce.editpage.php&magazine_id=$magazine_id&magTitle=".addslashes($magTitle)."'"; ?>">

	<? }else{ ?>

		<input type=button value="Add" onclick="document.thisform.pageaction.value='add';validate()">

	<? } ?>	

</td></tr></form></table><br>





<?

	$sort=array("page_number", "page_type");



	if($psortby==""){ $psortby=$sort[0]; } 

	if($psortseq==""){ $psortseq="asc"; }

	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&psortby=$sort[$i]&psortseq=asc&magazine_id=$magazine_id&magTitle=$magTitle><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort Ascending\"></a>"; }

	for($i=0;$i<count($sort);$i++){

		if($sortby==$sort[$i]){

		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&psortby=$sortby&psortseq=desc&magazine_id=$magazine_id&magTitle=$magTitle><img src=\"common/image/sort_desc.gif\" border=0 alt=\"Sort Descending\"></a>"; }

		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&psortby=$sortby&psortseq=asc&magazine_id=$magazine_id&magTitle=$magTitle><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort Ascending\"></a>"; }

	}}
?>

<script type="text/javascript" src="<?=$path['webroot'] ?>common/scripts/jquery-sortable-min.js"></script>
<script type="text/javascript" src="<?=$path['webroot'] ?>common/scripts/jquery-ui.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// var updatePage = function() {

			// };
			var originalOrder = [];
			var ctr = 0;
	        $('#dragsort').find('tr').each(function(){
	            var id = $(this).attr('id');
	            originalOrder[ctr] = id;
	            ctr++;
	        });
			
			$("#dragsort").sortable({  

		    	update: function() {
		    		var ids = $(this).sortable('toArray');
		    		
		    		$.ajax({
		    			type: 'POST',
		    			// url: '<?php echo $path['webroot'] ?>magazine/sortpage.php',
		    			// data: 'ids='+ids+'&original_order='+originalOrder,
		    			data: {
		    				'ids': ids,
		    				'original_order': originalOrder,
		    				'pageaction': 'ajaxsort',
		    			},
		    			cache: false,
		    		}).done(function(response) {
		    			console.log(response);
		    			if(response) {
		    				$('#message').text('Saved').addClass('saved');
		    				var originalOrder = [];
							var ctr = 0;
					        $('#dragsort').find('tr').each(function(){
					            var id = $(this).attr('id');
					            originalOrder[ctr] = id;
					            ctr++;
					        });
					        var ct = 1;
					        $('#dragsort .page-num').each(function() {
					        	$(this).text(ct);
					        	ct++;
					        });
					        
		    			} else {
		    				$('#message').text('Save failed').addClass('fail');
		    				console.log(response.query+" queries executed.");
		    			}
		    		});
		    		$('#message').fadeOut(1500, function() {
		    			$(this).trigger('hide');
		    		});
		    		$('#message').on('hide', function() {
		    			if($(this).hasClass('saved')) {
		    				$('#message').text('').removeClass('saved').css('display', 'block');
		    			} else if($(this).hasClass('failed')) {
		    				$('#message').text('').removeClass('failed').css('display', 'block');
		    			}
		    		})
		    	}
		    });
		});
	</script>
<?php


	echo "	
	<div id=\"message\"></div>
	<div id=\"note\">Drag and drop rows to sort magazine pages.</div>
	<table id=\"myTable\" border=0 cellpadding=1 cellspacing=0 width=50% align=left>
	
	    <tr bgcolor=\"#E6E6E6\" height=\"24\">

	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>Magazine Page</b>&nbsp;</td>

	      <td class=\"gridTitle\" align=\"center\" width=\"120\">&nbsp;<b>Page Number</b> $sortlink[0]&nbsp;</td>

	      <td class=\"gridTitle\" align=\"center\" width=\"120\">&nbsp;<b>Page Type</b> $sortlink[1]&nbsp;</td>

	      <td class=\"gridTitle\" align=\"center\" width=\"50\">&nbsp;</td>

	    </tr> 
	    <tbody id='dragsort'>	
	";



	$delete_id = array(); $catseqarr = array();

   	$oMagz_Pages->data = array("page_id","magazine_page","page_number","page_type","date_uploaded");

   	$oMagz_Pages->where = "magazine_id = $magazine_id";  	$oMagz_Pages->order = "$psortby $psortseq";

   	$count=0; $result=$oMagz_Pages->getList();



   	while($myrow=mysql_fetch_row($result)){

   		$count++;		$fid = $myrow[0];

   		$dis_thumbnail=substr($myrow[1],6,strlen($myrow[1])-6);

   		if($myrow[1]!="" && $myrow[3] != "ads"){ $magImage = "<a href='_files/magazines/".date('Y/m/d', strtotime($myrow[4]))."/$myrow[1]' onclick=\"return popImage('_files/magazines/".date('Y/m/d', strtotime($myrow[4]))."/$myrow[1]','Image')\"><img src='_files/magazines/".date('Y/m/d', strtotime($myrow[4]))."/$myrow[1]' height=100px width=75px/></a>"; }else{ $magImage=""; }

   		if($myrow[3] == "ads"){$magImage = $myrow[1];}

   		echo "

	    <tr id=\"$fid\" style=\"background:#F6F6F6; cursor: move;\" height=\"24\">

	      <td class=\"gridRow\" align=\"left\">&nbsp;$magImage&nbsp;</td>    

	      <td class=\"gridRow page-num\" align=\"center\">$myrow[2]</td>

	      <td class=\"gridRow\" align=\"center\">$myrow[3]</td>

   		  <td class=\"gridRow\" align=\"center\">

   		  <a href=index.php?component=magazine&page=wce.editpage.php&pageaction=edit&page_id=$myrow[0]&page_number=$myrow[2]&page_type=$myrow[3]&magazine_id=$magazine_id&magTitle=$magTitle><img src=common/image/ico_edit.gif border=0 alt=\"Edit Record\" title='Edit Page'></a>

   		  <a href='javascript:ConfirmDelete($myrow[0])'><img src='common/image/ico_del.gif' border=0 alt=\"Delete Record\" title='Delete Page'></a>

   		  </td>

		</tr>

		";

   	}

   	mysql_free_result($result);

   	

	echo "
	</tbody>
	</table>	

	";

?> 



<script language=javascript>

	function ConfirmDelete(id){  

	    if(confirm('Are you sure you want to delete this record?')){

			window.location="index.php?component=magazine&page=wce.editpage.php&pageaction=delete&page_id="+id+"<? echo "&magazine_id=$magazine_id&magTitle=".addslashes($magTitle); ?>";

		}

	}

	

	function validate(edit){

		var pageType = document.thisform.page_type.value;

		if(edit == undefined || edit == "") {

            edit = "";

        }

		if($('#mag_image').val()=="" && edit != "edit" && pageType == "image"){

			alert('Please upload magazine page.'); return false;

		}else if(($('#mag_video').val()=="" || $('#mag_video1').val()=="" || $('#mag_video2').val()=="" || $('#mag_video3').val()=="") && edit != "edit" && pageType == "video"){

			alert('Please upload magazine video.'); return false;

		}else if($('#mag_ads').val()=="" && pageType == "ads"){

			alert('Please choose ads.'); return false;

		}else{
			
			$('body').loader('show');
			document.thisform.submit();

		}	

		return false;

	}



	$('input[name="page_type"').click(function(){

		switch($(this).val()){

			case 'image':

				$('.magazine_image_row').css("display","table-row");

				$('#mag_image').removeAttr("disabled");

				$('#mag_content').removeAttr("disabled");

				CKEDITOR.instances['mag_content'].setReadOnly(false);

				$('.mag_video').attr("disabled","disabled");

				$('#mag_ads').attr("disabled","disabled");

				$('.magazine_video_row').css("display","none");

				$('#magazine_ads_row').css("display","none");

				break;

			case 'video':

				$('.magazine_video_row').css("display","table-row");

				$('.mag_video').removeAttr("disabled");

				$('#mag_image').attr("disabled","disabled");

				$('#mag_content').attr("disabled","disabled");

				CKEDITOR.instances['mag_content'].setReadOnly(true);

				$('#mag_ads').attr("disabled","disabled");

				$('.magazine_image_row').css("display","none");

				$('#magazine_ads_row').css("display","none");

				break;

			case 'ads':

				$('#magazine_ads_row').css("display","table-row");

				$('#mag_ads').removeAttr("disabled");

				$('.mag_video').attr("disabled","disabled");

				$('#mag_image').attr("disabled","disabled");

				$('#mag_content').attr("disabled","disabled");

				CKEDITOR.instances['mag_content'].setReadOnly(true);

				$('.magazine_video_row').css("display","none");

				$('.magazine_image_row').css("display","none");

				break;

		}

	});



	var pageType = "<? echo $page_type; ?>";

	$(document).ready(function () {

		console.log(pageType);

		switch(pageType){			

			case "video":

				$('.magazine_video_row').css("display","table-row");

				$('.mag_video').removeAttr("disabled");

				$('#mag_image').attr("disabled","disabled");

				$('#mag_content').attr("disabled","disabled");

				// CKEDITOR.instances['mag_content'].setReadOnly(true);

				$('#mag_ads').attr("disabled","disabled");

				$('.magazine_image_row').css("display","none");

				$('#magazine_ads_row').css("display","none");

				break;

			case "ads":

				$('#magazine_ads_row').css("display","table-row");

				$('#mag_ads').removeAttr("disabled");

				$('.mag_video').attr("disabled","disabled");

				$('#mag_image').attr("disabled","disabled");

				$('#mag_content').attr("disabled","disabled");

				// CKEDITOR.instances['mag_content'].setReadOnly(true);

				$('.magazine_video_row').css("display","none");

				$('.magazine_image_row').css("display","none");

				break;

			default:

				$('.magazine_image_row').css("display","table-row");

				$('#mag_image').removeAttr("disabled");

				$('#mag_content').removeAttr("disabled");

				// CKEDITOR.instances['mag_content'].setReadOnly(false);

				$('.mag_video').attr("disabled","disabled");

				$('#mag_ads').attr("disabled","disabled");

				$('.magazine_video_row').css("display","none");

				$('#magazine_ads_row').css("display","none");

				break;

		}

	});

</script>