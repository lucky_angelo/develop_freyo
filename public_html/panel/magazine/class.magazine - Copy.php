<?


class Magazine_Pages extends Item{
	
	var $table="magazine_pages",$primarykey="page_id",$shortname="magazine",$version="1.0";
	var $magazine_id;

	function Magazine_Pages(){}

	function extractMagazipfile(){
		$maxsize = ini_get('upload_max_filesize');
		$upload_max = (!empty($maxsize)?substr($maxsize,0,-1)."000000":"");
		if (strtolower(strrchr($_FILES['magazipfile']['name'],"."))== ".zip"){	// check file extention
			if(($_FILES['magazipfile']['size']<$upload_max || empty($upload_max)) && !empty($_FILES['magazipfile']['size'])){
				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
					//create different folder per uploading
					$uniqueid = md5(uniqid(rand(), true));	
					$uniqueid=substr($uniqueid,0,6);
					$tar_dir = $path["docroot"]."_files/magazines_$uniqueid";
					if(!file_exists($tar_dir)){
						@mkdir ($tar_dir, 0777);
						@chmod ($tar_dir, 0777);
					}
					// $tar_dir = $path["docroot"]."_files/magazines_temp";
					$tar_dir1 = "_files/magazines_$uniqueid";
				
					$zipfilename = str_replace(" ","_",trim($_FILES['magazipfile']['name']));
					if (is_uploaded_file($_FILES['magazipfile']['tmp_name'])) {    $this->uploadFile($_FILES['magazipfile']['tmp_name'], $tar_dir, $zipfilename);	}
				
					include($path["docroot"]."common/class.pclzip.php");
					$oArchive = new PclZip("$tar_dir1/$zipfilename");
					$oArchive->extract($tar_dir1);
					@unlink("$tar_dir/$zipfilename");
					$this->readMagazipfile($tar_dir);
				}
			}
		}
	}

	function readMagazipfile($tar_dir){

	 	$data = $this->getPix($tar_dir);

		foreach($data as $key => $mags) {
			$filename = $filename1 = $mags; 
			// $filename1=$filename;
			// echo "".$lang['photogallery']['filename']."&nbsp;:&nbsp;".$filename;
			if ($this->IsPicture($filename)){	// check whether is picture or not		
				$filename = strtolower(str_replace(" ","_",trim($filename)));
				$uniqueid = md5(uniqid(rand(), true));	
				$uniqueid=substr($uniqueid,0,5);
				$filename2 = $uniqueid."_".$filename;
	
				$this->copyFile($tar_dir."/".$filename1,$path["docroot"]."_files/magazines",$filename2);
	
				$pageNum = $this->getLastPage()+1;	
				$oPhoto_Gallery->data = array("magazine_id","magazine_page","page_number");
				$oPhoto_Gallery->value = array($this->magazine_id,$filename2,$pageNum);
				$oPhoto_Gallery->add();
			}	
		}

		//Remove all files uploaded and folder
		$this->rrmdir($tar_dir);
	}

	function rrmdir($dir) {
	  if (is_dir($dir)) {
	    $objects = scandir($dir);
	    foreach ($objects as $object) {
	      if ($object != "." && $object != "..") {
	        if (filetype($dir."/".$object) == "dir") 
	           $this->rrmdir($dir."/".$object); 
	        else unlink   ($dir."/".$object);
	      }
	    }
	    reset($objects);
	    rmdir($dir);
	  }
	}

	function getLastSequence(){
		$sql="select page_number from $this->table order by sequence desc limit 0,1";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}

	function getPix($tar_dir){
		$filenames=array();
		$handle = opendir($tar_dir); 
		while($file = readdir($handle)){ 
		if (!is_dir($file)){$filenames[] = $file; } 
		}// end while 
		closedir($handle);
		usort($filenames, "cmp"); 
		return $filenames;
	} 
			
	function cmp($a,$b){
		return strcasecmp($a, $b); 
	}

	function IsPicture($file_name){
    	$ext = strtolower(strrchr($file_name,"."));
    	if($ext==".jpg" || $ext==".png" || $ext==".gif" || $ext==".jpeg"){
			return true;				
		}
		else{
			return false;
		}		
	}
}

?>