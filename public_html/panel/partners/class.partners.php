<?

class Partners extends Item {

	var $table="freyo_partners",$primarykey="partners_id",$shortname="partners",$version="1.0";

	var $pathdocroot;
	// var $last_day;

	function Partners() {}

	function getPartners() {
		$sql = "SELECT * FROM $this->table ORDER BY partner_name";

		$result = mysql_query($sql,$this->db);

		return $result;

	}

	function getMagList($partner_id = "false") {
		if($partner_id == "false") {
			$partner_id = $_SESSION['UserId'];

			$sql = "SELECT mp.magazine_id,mp.title,mp.date_updated,mp.views,mp.favorites, mp.publish
			FROM freyo_partner_magazine pm
			JOIN magazine_publisher mp ON mp.magazine_id = pm.magazine_id
			WHERE pm.partner_id = $partner_id";

			$result = mysql_query($sql,$this->db)or die(mysql_errno());

			
		} else {
			$sql = "SELECT mp.magazine_id,mp.title,mp.date_updated,mp.views,mp.favorites, mp.publish
			FROM freyo_partner_magazine pm
			JOIN magazine_publisher mp ON mp.magazine_id = pm.magazine_id
			WHERE pm.partner_id = $partner_id";

			$result = mysql_query($sql,$this->db)or die(mysql_errno());
		}
		return $result;
	}

	function getMagazineList(){

		global $wwmagazine_id;

		$sql="select mp.magazine_id, mc.name, mp.title, fpm.partner_id 
		from magazine_publisher mp
		join magazine_category mc on mc.category_id = mp.category_id
		left join freyo_partner_magazine fpm on fpm.magazine_id = mp.magazine_id
		order by title";

		$result=mysql_query($sql,$this->db);

		return $result;

	}

	function getMagDetails($mag_id) {
		$sql = "SELECT views, favorites, title, magazine_cover FROM magazine_publisher WHERE magazine_id = $mag_id";

		$result = mysql_fetch_row(mysql_query($sql, $this->db));
		return $result;
	}

	function getMagReport($mag_id, $month, $year){
		global $wselected_id;
		$magReport = [];
		$countView = "SELECT COUNT( ar.report_id ) AS total_view, COUNT( ar.report_id ) * cost_per_view AS income, ar.ads_id, DATE_FORMAT( ar.datetime_viewed,  '%Y-%m-%d' ) as day
			FROM ads_report ar
			JOIN ads_item ai ON ar.ads_id = ai.ads_id
			WHERE ar.magazine_id = $mag_id AND MONTH(ar.datetime_viewed) = $month AND YEAR(ar.datetime_viewed) = $year
			GROUP BY DATE_FORMAT( ar.datetime_viewed,  '%Y-%m-%d' ), ads_id";
		$result = mysql_query($countView, $this->db);	
		// $row = mysql_fetch_array($result, MYSQL_ASSOC);
		// $total_view = $row['total_view'];
		$magReport = [];
		$current_day = "";
		$day = "";
		$i = 0;
		$totalPerDay = 0;
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$current_day = $row['day'];

			if($i == 0) {
				$day = $row['day'];
			}
			if($current_day == $day) {
				$totalPerDay += $row['income'];
			} else {
				$magReport[$i]['day'] = $day;
				$magReport[$i]['income'] = $totalPerDay;
				$day = $row['day'];
				$totalPerDay = 0;
				$totalPerDay += $row['income'];
			}
			
			$i++;
			
		}
		$magReport[$i]['day'] = $day;
		$magReport[$i]['income'] = $totalPerDay;
		mysql_free_result($result);

		// $magReport['adsViewReport'] = $viewReport;
		
		
		return $magReport;
	}

	function getClickReport($mag_id, $month, $year) {
		$countClick = "SELECT COUNT( ar.click_id ) AS total_view, COUNT( ar.click_id ) * cost_per_click AS income, ar.ads_id, DATE_FORMAT( ar.datetime_clicked,  '%Y-%m-%d' ) as day
			FROM ads_click_report ar
			JOIN ads_item ai ON ar.ads_id = ai.ads_id
			WHERE ar.magazine_id = $mag_id AND MONTH(ar.datetime_clicked) = $month AND YEAR(ar.datetime_clicked) = $year
			GROUP BY DATE_FORMAT( ar.datetime_clicked,  '%Y-%m-%d' ), ads_id";
		$result = mysql_query($countClick, $this->db);
		$magClickReport = [];
		$current_day = "";
		$day = "";
		$i = 0;
		$totalPerDay = 0;
		while($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$current_day = $row['day'];

			if($i == 0) {
				$day = $row['day'];
			}
			if($current_day == $day) {
				$totalPerDay += $row['income'];
			} else {
				$magClickReport[$i]['day'] = $day;
				$magClickReport[$i]['income'] = $totalPerDay;
				$day = $row['day'];
				$totalPerDay = 0;
				$totalPerDay += $row['income'];
			}
			
			$i++;
			
		}
		$magClickReport[$i]['day'] = $day;
		$magClickReport[$i]['income'] = $totalPerDay;;
		mysql_free_result($result);
		return $magClickReport;
	}

	function years() {
		$years = ["2015", "2016", "2017", "2018", "2019", "2020"];

		return $years;
	}

	function months() {
		$months = [0 => "January", 1 => "February", 2 => "March", 3 => "April", 4 => "May", 5 => "June", 6 => "July", 7 => "August", 8 => "September", 9 => "October", 10 => "November", 11 => "December"];

		return $months;
	}

	
}





?>