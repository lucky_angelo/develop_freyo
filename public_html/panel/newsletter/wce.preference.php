<?

if ($oSystem->getValue("newsletter_page") == "")
	$oSystem->setValue("newsletter_page", $path["webroot"]."newsletter/samplenewsletter.php");

if($pageaction=="save"){
	
  $oSystem->setValue("newsletter_page",$frontendurl);
  $oSystem->setValue("newsletter_enableimageverify",$imgverify);
  $oSystem->setValue("newsletter_emailfrom",$emailfrom);
  $oSystem->setValue("newsletter_emailtest",$emailtest);
  $oSystem->setValue("newsletter_newlistno",$newlistno);
  $oSystem->setValue("newsletter_sublistno",$sublistno);
  $oSystem->setValue("newsletter_pageno",$pageno);
  $oSystem->setValue("newsletter_seq",$seq); 
  $oSystem->setValue("newsletter_enablebrowse",$enablebrowse);  
  $oSystem->setValue("newsletter_popemail",$popemail);
  $oSystem->setValue("newsletter_popserver",$popserver);
  $oSystem->setValue("newsletter_poplogin",$poplogin);
  $oSystem->setValue("newsletter_poppass",$poppass);    
  $oSystem->setValue("newsletter_newstextbody",addslashes($newstextbody));
  $oSystem->setValue("newsletter_newshtmlbody",addslashes($newshtmlbody));
  $oSystem->setValue("newsletter_msgconfirmemail",addslashes($msgconfirmemail));
  $status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['prefsave']."<br>";	  
}
if($pageaction=="default"){
  $oSystem->setValue("newsletter_page",$path["webroot"]."newsletter/samplenewsletter.php");
  $oSystem->setValue("newsletter_enableimageverify","");
  $oSystem->setValue("newsletter_emailtest","Your Testing Email Address");
  $oSystem->setValue("newsletter_newlistno","20");
  $oSystem->setValue("newsletter_sublistno","20");
  $oSystem->setValue("newsletter_pageno","10");
  $oSystem->setValue("newsletter_seq","desc"); 
  $oSystem->setValue("newsletter_enablebrowse","Yes");  
  $oSystem->setValue("newsletter_popemail","");
  $oSystem->setValue("newsletter_popserver","");
  $oSystem->setValue("newsletter_poplogin","");
  $oSystem->setValue("newsletter_poppass","");
  $oSystem->setValue("newsletter_newstextbody", "[[newsletterbody]]\r\n\r\n\r\nClick [[tohtmlurl]] to receive HTML-based newsletter in the future. \r\n\r\nYour are currently subscribed as: [[email]]. To unsubscribe, please click [[unsubscribeurl]].");
  $oSystem->setValue("newsletter_newshtmlbody", "[[newsletterbody]]\r\n\r\n\r\n<font size=1 face=arial>Click [[totexturl]] to receive text-based newsletter in the future. Your are currently subscribed as: [[email]]. To unsubscribe, please click [[unsubscribeurl]] .</font>");
  $oSystem->setValue("newsletter_msgconfirmemail","Dear [[firstname]][[lastname]],\n\nThank you for subscribing. We acknowledge that you are interested in our below newsletter:\n\n[[grouplist]] \n\nPlease click on the link below to activate your Newsletter subscription.\n\n[[activationurl]] \n\nFrom,\nwww.yourdomain.com\n".$website);
  $status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['prefdefault']."<br>";	  
}

if($oSystem->getValue("newsletter_seq")=="asc"){	$newsletter_seqasc="selected"; }
if($oSystem->getValue("newsletter_seq")=="desc"){	$newsletter_seqdesc="selected"; }


echo"<table border=0 width=100%><tr><td><b>".$lang['newsletter']['newsletter']."</b></td><td>";

include("wce.menu.php");

echo"
	</td></tr></table>
	<hr size=1 color=#606060>$status_message<br>
	<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
	<form name=thisform action=\"index.php?component=$component&page=$page\" method=post>
	<input type=hidden name=pageaction>
	<tr><td valign=top colspan=2><b>".$lang['newsletter']['general']."</b></td></tr>
	<tr><td valign=top>".$lang['newsletter']['frontendurl']."</td><td><input type=text name=frontendurl style=\"width:400px\" value=\"".$oSystem->getValue("newsletter_page")."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['imgverify']."</td><td><input type=checkbox name=imgverify value=\"Yes\" ".($oSystem->getValue("newsletter_enableimageverify")=="Yes"?"checked":"")."> * ".$lang['newsletter']['imgverifynote']."</td></tr>
	<tr><td valign=top width=35%>".$lang['newsletter']['listnorecord']."</td><td><input type=text name=newlistno style=\"width:35px\" value=\"".$oSystem->getValue("newsletter_newlistno")."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['subscrno']."</td><td><input type=text name=sublistno style=\"width:35px\" value=\"".$oSystem->getValue("newsletter_sublistno")."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['newslbrowse']."</td><td><select name=\"enablebrowse\"><option value=\"Yes\" ".($oSystem->getValue("newsletter_enablebrowse")=="Yes"?"selected":"").">".$lang['newsletter']['yes']."</option><option value=\"No\" ".($oSystem->getValue("newsletter_enablebrowse")=="No"?"selected":"").">".$lang['newsletter']['no']."</option></select></td></tr>
	<tr><td valign=top>".$lang['newsletter']['browseno']."</td><td><input type=text name=pageno style=\"width:35px\" value=\"".$oSystem->getValue("newsletter_pageno")."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['newslseq']."</td><td><select name=\"seq\"><option value=\"asc\" $newsletter_seqasc>".$lang['newsletter']['ascending']."</option><option value=\"desc\" $newsletter_seqdesc>".$lang['newsletter']['descending']."</option></select></td></tr>
	
	<tr><td colspan=2><br><b>".$lang['newsletter']['sendersetting']."</b></td></tr>
	<tr><td valign=top>".$lang['newsletter']['email']."</td><td><input type=text name=popemail style=\"width:230px\" value=\"".($oSystem->getValue("newsletter_popemail")==""?$oUser->getAdminEmail():$oSystem->getValue("newsletter_popemail"))."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['servername']."</td><td><input type=text name=popserver style=\"width:230px\" value=\"".($oSystem->getValue("newsletter_popserver")==""?$oSystem->getValue("sys_smtphost"):$oSystem->getValue("newsletter_popserver"))."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['loginid']."</td><td><input type=text name=poplogin style=\"width:230px\" value=\"".($oSystem->getValue("newsletter_poplogin")==""?$oSystem->getValue("sys_smtpuser"):$oSystem->getValue("newsletter_poplogin"))."\"></td></tr>
	<tr><td valign=top>".$lang['newsletter']['password']."</td><td><input type=password name=poppass style=\"width:230px\" value=\"".($oSystem->getValue("newsletter_poppass")==""?$oSystem->getValue("sys_smtppass"):$oSystem->getValue("newsletter_poppass"))."\"></td></tr>
	
	<tr><td colspan=2><br><b>".$lang['newsletter']['textnewsl']."</b></td></tr>
	<tr><td colspan=2><textarea name=newstextbody cols=80 rows=10 style=\"width:530px;\">".stripslashes($oSystem->getValue("newsletter_newstextbody"))."</textarea><br><br></td></tr>
	<tr><td colspan=2><b>".$lang['newsletter']['htmlnewsl']."</b></td></tr>
	<tr><td colspan=2><textarea name=newshtmlbody cols=80 rows=10 style=\"width:530px;\">".stripslashes($oSystem->getValue("newsletter_newshtmlbody"))."</textarea><br><br></td></tr>
	<tr><td colspan=2><b>".$lang['newsletter']['confirmemail']."</b></td></tr>
	<tr><td colspan=2><textarea name=msgconfirmemail cols=80 rows=10 style=\"width:530px;\">".stripslashes($oSystem->getValue("newsletter_msgconfirmemail"))."</textarea><br><br></td></tr>
	<tr><td colspan=2><br>
		<input type=button value=\" ".$lang['newsletter']['btnsave']." \" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
		<input type=button value=\" ".$lang['newsletter']['btndefault']." \" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">	
	</td></tr>
	</form></table>
";

?>
