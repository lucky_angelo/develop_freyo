<?

 $style = $path["webroot"].$path["skin"]."style.css"; ?>
<?
	$group_id		= "";
	$letter_id		= "";
	$delivery		= "";
	if (isset($_GET['group_id']) && trim($_GET['group_id']) != ""){	
		$group_id = $_GET['group_id'];
	}
	if (isset($_GET['letter_id']) && trim($_GET['letter_id']) != ""){	
		$letter_id = $_GET['letter_id'];
	}
	if (isset($_GET['delivery']) && trim($_GET['delivery']) != ""){	
		$delivery = $_GET['delivery'];
	}

?>
<html>
<head><title><? echo $lang['newsletter']['bulksending'] ?></title>
<link rel=stylesheet type=text/css href="<? echo $style ?>">
</head>
<script src="common/lib.jsfunction.php" language=javascript></script>

<body topmargin=0 leftmargin=0>
<br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<tr><td valign=top><b><u><? echo $lang['newsletter']['bulksending'] ?></u></b></td></tr>
<tr><td valign=top>

	<table border=0 cellpadding=5 cellspacing=0>    
	<form name=thisform action="index.php?component=newsletter&page=wce.send.php&headfoot=no&pageaction=send&group_id=<?= $group_id ?>&letter_id=<?= $letter_id ?>&delivery=<?= $delivery ?>" method=post>
	<tr><td valign=top colspan=2><? echo $lang['newsletter']['timeout'] ?><br></td></tr>
	<tr><td valign=top><? echo $lang['newsletter']['selectnumber'] ?></td><td>
		<select name=cycleno>
		<?	for($i=5;$i<31;$i=$i+5){	if($i==$cycleno){ echo "<option value=\"$i\" selected>$i</option>";	}else{	echo "<option value=\"$i\">$i</option>"; }}	?>
		</select></td></tr>
	<tr><td valign=top colspan=2><? echo $lang['newsletter']['note'] ?></td></tr>
	<tr><td valign=top colspan=2><input type=button value="<? if($pageaction=="send"){ echo $lang['newsletter']['inprogress']; }else { echo $lang['newsletter']['btnstartsend']; } ?>" onClick="startsend()" <? if($pageaction=="send"){ echo "disabled"; } ?>></td></tr>
	</form></table>
	
</td></tr></table>
<hr size=1 color=#000000 width=98% align=center><br>

<table border=0 cellpadding=5 cellspacing=0 width=98% align=center bgcolor=#FFFFFF>
<tr><td height=200 valign=top>

<?

$today = date("Y-m-d");
$oNewsletter_Delivery->data = array("delivery_id");
$oNewsletter_Delivery->where = "group_id='$group_id' and letter_id='$letter_id' and delivery='$delivery'";
$result = $oNewsletter_Delivery->getList();
$total = mysql_num_rows($result);
mysql_free_result($result);


echo "<div id=\"startsend\" style=\"display:none\"><font color=0000ff><b>".$lang['newsletter']['inprogress']." ($total ".$lang['newsletter']['remaining'].")</b></font></div>";

if($pageaction=="send"){

	$thisfile = $oSystem->getValue("newsletter_page");	
	$emailfrom = ($oSystem->getValue("newsletter_popemail")==""?$oUser->getAdminEmail():$oSystem->getValue("newsletter_popemail"));
	
	if($start==""){ $start=0; }		
	if($timestart==""){ $timestart=date("h:i:s a"); }
	echo "<font color=0000FF><b>".$lang['newsletter']['inprogress']." ($total ".$lang['newsletter']['remaining'].")</b></font><br><br>";
		
	$oNewsletter_Delivery->data = array("delivery_id","delivery","letter_id","subscriber_id");
	$oNewsletter_Delivery->where = "group_id='$group_id' and letter_id='$letter_id' and delivery='$delivery'";
	$oNewsletter_Delivery->order = "delivery_id limit $start,$cycleno";
	$result = $oNewsletter_Delivery->getList();		
	while($myrow=mysql_fetch_row($result)){
	
		$oNewsletter_Letter->data = array("title","bodytext","bodyhtml","attachment","send_attachment");
		$resultsend = $oNewsletter_Letter->getDetail($myrow[2]);
		if($myrowsend = mysql_fetch_row($resultsend)){ 
			$title = stripslashes($myrowsend[0]); 
			$bodytext = stripslashes($myrowsend[1]); 
			$bodyhtml = stripslashes($myrowsend[2]);
			$attachmenturl = ""; 
			$dlattachmenturl="";
			if(trim($myrowsend[3]) != "" && !is_null($myrowsend[3])){
				if ($myrowsend[4] == "Yes"){
					if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
						$attachmenturl = $path["docroot"]."_files/newsletter/".stripslashes($myrowsend[3]);
					}
				}
				$dlattachmenturl = $path["webroot"]."_files/newsletter/".stripslashes($myrowsend[3]);
			} 
		}
		mysql_free_result($resultsend);	
		
		$oNewsletter_Subscriber->data = array("firstname","lastname","company","position","email","lettertype");
		$resultsend=$oNewsletter_Subscriber->getDetail($myrow[3]);
		if($myrowsend = mysql_fetch_row($resultsend)){ 
			$firstname = stripslashes($myrowsend[0]); 
			$lastname = stripslashes($myrowsend[1]); 
			$company = stripslashes($myrowsend[2]); 
			$position = stripslashes($myrowsend[3]); 
			$email = $myrowsend[4];
			$lettertype = $myrowsend[5];
		}		
		mysql_free_result($resultsend);	
		
		$tohtmlurl="$thisfile?pageaction=htmlnewsletter&subscriber_id=$myrow[3]&email=$email";
		$totexturl="<a href=$thisfile?pageaction=textnewsletter&subscriber_id=$myrow[3]&email=$email>".$lang['newsletter']['here']."</a>";
		$unsuburl="$thisfile?pageaction=unsubscribe&subscriber_id=$myrow[3]&email=$email";
		$unsuburlhtml="<a href=$thisfile?pageaction=unsubscribe&subscriber_id=$myrow[3]&email=$email>".$lang['newsletter']['here']."</a>";

		if($lettertype=="Text"){
			$message = stripslashes($oSystem->getValue("newsletter_newstextbody"));
			$message = str_replace("[[tohtmlurl]]",$tohtmlurl,$message);
			$message = str_replace("[[newsletterbody]]",$bodytext,$message);
			$message = str_replace("[[unsubscribeurl]]",$unsuburl,$message);
			$message = str_replace("[[firstname]]",$firstname,$message);
			$message = str_replace("[[lastname]]",$lastname,$message);
			$message = str_replace("[[company]]",$company,$message);
			$message = str_replace("[[position]]",$position,$message);
			$message = str_replace("[[email]]",$email,$message);
			$message = str_replace("[[attachment]]",$dlattachmenturl,$message);
			$oSystem->mail($email,$title,$message,$emailfrom,"",$attachmenturl,"Newsletter");
		}else{
			$message = nl2br(stripslashes($oSystem->getValue("newsletter_newshtmlbody")));
			$message = str_replace("[[totexturl]]",$totexturl,$message);		
			$message = str_replace("[[newsletterbody]]",$bodyhtml,$message);
			$message = str_replace("[[unsubscribeurl]]",$unsuburlhtml,$message);
			$message = str_replace("[[firstname]]",$firstname,$message);
			$message = str_replace("[[lastname]]",$lastname,$message);
			$message = str_replace("[[company]]",$company,$message);
			$message = str_replace("[[position]]",$position,$message);
			$message = str_replace("[[email]]",$email,$message);
			if ($dlattachmenturl != ""){
				$dlattachmenturl = "<a href=\"$dlattachmenturl\">Download File</a>";
			}			
			$message = str_replace("[[attachment]]",$dlattachmenturl,$message);
			$oSystem->mail($email,$title,$message,$emailfrom,"HTML",$attachmenturl,"Newsletter");
		}
		

		echo "$email &nbsp;";
		
		$oNewsletter_Delivery->delete($myrow[0]);		
		$totalsend++;
	}
	
	if(mysql_num_rows($result)!=0 && $start<$total){ 
		echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=newsletter&page=wce.send.php&headfoot=no&pageaction=send&letter_id=$letter_id&group_id=$group_id&delivery=$delivery&cycleno=$cycleno&totalsend=$totalsend&timestart=$timestart\">"; 
	}else{
		echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=newsletter&page=wce.send.php&headfoot=no&pageaction=senddone&letter_id=$letter_id&timestart=$timestart&totalsend=$totalsend\">";
	}
	mysql_free_result($result);

}

if($pageaction=="senddone"){
	
	$timeend = date("h:i:s a");
	echo "
		<font color=0000ff><b>".$lang['newsletter']['sendcomplete']."</b></font><br><br>
		<table border=0 width=100%>
		<tr><td width=35%>".$lang['newsletter']['totalnewsl']."</td><td>: <font color=0000ff>$totalsend</font></td></tr>
		<tr><td>".$lang['newsletter']['timestart']." </td><td>: <font color=0000ff>$timestart</font></td></tr>
		<tr><td>".$lang['newsletter']['timeend']."</td><td>: <font color=0000ff>$timeend</font></td></tr>
		<tr><td colspan=2><br><input type=button value=\"".$lang['newsletter']['closewindow']."\" onclick=\"closepopup()\"></td></tr>
		</table><br><br>
	";

	$oNewsletter_Letter->data = array("datesend");
	$oNewsletter_Letter->value = array(date("Y-m-d"));
	$oNewsletter_Letter->update($letter_id);	
	
}
	
?>

</td></tr></table>
<br><br>

<script language=javascript>
	function startsend(){
		if (!document.getElementById){return;}
		objectID = document.getElementById('startsend');
		objectID.style.display='block';
				
		document.thisform.submit();
	}
	
	function closepopup(){
		window.close();
		opener.location.href="index.php?component=newsletter&page=wce.delivery.php";
	}
</script>
	
</body>
</html>