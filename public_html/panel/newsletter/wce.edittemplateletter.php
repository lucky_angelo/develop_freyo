<?

if($pageaction=="update"){

	$bodyhtml = $FCKeditor;
	$oNewsletter_Template->data = array("letter_name","bodytext","bodyhtml");
	$oNewsletter_Template->value = array(addslashes($title),addslashes($bodytext),addslashes($bodyhtml));
	$oNewsletter_Template->update($letter_id);
	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newslupdate']."<br>";	
}

$oNewsletter_Template->data = array("letter_name","bodytext","bodyhtml");
$result=$oNewsletter_Template->getDetail($letter_id);
if($myrow=mysql_fetch_row($result)){
	$title = stripslashes($myrow[0]);
	$bodytext = stripslashes(htmlentities($myrow[1]));
	$bodyhtml = stripslashes($myrow[2]);
}
mysql_free_result($result);

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<?
	/*****Navigation Calculation*****/
	$oNewsletter_Template->data=array("letter_id");	
	if($vgroup_id!=""){ $oNewsletter_Template->where="group_id='$vgroup_id'"; }
	else if($keyword!=""){ $oNewsletter_Template->where="title like '%$keyword%'"; }
	else{ $oNewsletter_Template->where=""; }	
	$oNewsletter_Template->order="$sortby $sortseq"; $result=$oNewsletter_Template->getList();		
	$count=0; while($myrow=mysql_fetch_row($result)){ if($myrow[0]==$letter_id){ $curpos=$count; }	$count++; }		
	$prevpos=$curpos-1; $nextpos=$curpos+1;	$curpos=$curpos+1;	$total = mysql_num_rows($result);
	if($prevpos>=0){ mysql_data_seek($result,$prevpos);	if($myrow=mysql_fetch_row($result)){ $prev_id=$myrow[0]; }}
	if($nextpos < $total){ mysql_data_seek($result,$nextpos); if($myrow=mysql_fetch_row($result)){ $next_id=$myrow[0]; }}
	if($total==0){ $curpos=0; }	mysql_free_result($result);
	if($prev_id!=""){	$prevlink="<a href=\"index.php?component=newsletter&page=wce.edittemplateletter.php&letter_id=$prev_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['prev']."</a>";	}
	if($next_id!=""){	$nextlink="<a href=\"index.php?component=newsletter&page=wce.edittemplateletter.php&letter_id=$next_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['next']."</a>";	}
	if($prevlink!="" && $nextlink!=""){		$navline="&nbsp;";	}

	echo "
	<table border=0 cellpadding=0 cellspacing=0 width=98% align=center><tr>
	<td width=20%><a href=\"index.php?component=newsletter&page=wce.listtemplateletter.php&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&group_id=$vgroup_id&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['backtolist']."</a></td>
	<td align=center>".$lang['newsletter']['newslno']." ".$curpos." ".$lang['newsletter']['of']." ".$total."</td><td width=23% align=right>$prevlink $navline $nextlink</td></tr>
	</table><br>
	";
?>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newsletter&page=wce.edittemplateletter.php" method=post>
<input type=hidden name=pageaction value="update">
<input type=hidden name=letter_id value="<? echo $letter_id ?>">
<input type=hidden name=sortby value="<? echo $sortby ?>">
<input type=hidden name=sortseq value="<? echo $sortseq ?>">
<input type=hidden name=keyword value="<? echo $keyword ?>">
<input type=hidden name=start value="<? echo $start ?>">

<tr><td valign=top><? echo $lang['newsletter']['newsltempname'] ?></td><td><input type=text name=title style="width:330px" value="<? echo $title ?>"> *</td></tr>
<tr><td valign=top colspan=2><? echo $lang['newsletter']['htmlbased'] ?><br>
	<? 
	$oFCKeditor = new FCKeditor('FCKeditor') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '500' ;
	$oFCKeditor->Value		= $bodyhtml ;
	$oFCKeditor->Create() ;
	?>	
</td></tr>
<tr><td valign=top colspan=2><br><? echo $lang['newsletter']['plaintext'] ?><br><textarea name=bodytext cols=110 rows=23 style="width: 100%"><? echo $bodytext ?></textarea></td></tr>
<tr><td colspan=2><br>
	<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td>
	<input type=button value=" <? echo $lang['newsletter']['btnsave'] ?> " onclick="validate()">
	<input type=button value="<? echo $lang['newsletter']['btndel'] ?>" onclick="ConfirmDelete(<? echo $letter_id ?>)">
	<input type=button value=" <? echo $lang['newsletter']['btnback'] ?> " onclick="<? echo "window.location='index.php?component=newsletter&page=wce.listtemplateletter.php&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&group_id=$vgroup_id&start=$start'"; ?>">
	</td></tr></table>
</td></tr></form></table>
	
<script language=javascript>

	function ConfirmDelete(id){  
	    if(confirm('<? echo $lang['newsletter']['confirmdel'] ?>')){
			window.location='index.php?component=newsletter&page=wce.listtemplateletter.php&pageaction=delete&delete_id[0]='+id+'<? echo "&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&start=$start"; ?>';
		}
	}
		
	function validate(){
		if(document.thisform.title.value==""){
			alert('<? echo $lang['newsletter']['plstitle'] ?>'); document.thisform.title.select();  return false;
		}else{
			document.thisform.submit();
		}	
	}	

</script>
