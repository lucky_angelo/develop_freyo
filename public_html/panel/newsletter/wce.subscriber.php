<?


if ($searchtype != "") $searchtype = stripslashes($searchtype);

if($pageaction=="delete"){	
	for($i=0;$i<count($delete_id);$i++){
		$subscriber_id=$delete_id[$i];
		$oNewsletter_Subscriber->delete($subscriber_id);
	}

	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['subscrdel']."<br>";
	$pageaction = "";
}

if($pageaction=="groupoperation"){
	$wherecondition = "";
	
	if ($group_id != ""){
		$wherecondition = "group_id=$group_id";
	}	
	
	if ($operationtype == "add" || $operationtype == "move"){
		if ($subscrtype == "all"){		//all subscribers
			if ($group_id == ""){
				// get list from subscriber
				$oNewsletter_Subscriber->data = array("distinct(subscriber_id)");
				$result=$oNewsletter_Subscriber->getList();
			}else{
				// get list from subscribe by group id
				$oNewsletter_Subscriber->data = array("distinct(subscriber_id)");
				$oNewsletter_Subscriber->where = $wherecondition;
				$result=$oNewsletter_Subscriber->getSubscribeList();
			}
			while($myrow=mysql_fetch_row($result)){ 
				$subscriber_id = $myrow[0]; 
				//check for duplicate subsribe for each subscribe id
				$oNewsletter_Subscriber->data = array("distinct(subscriber_id)");
				$oNewsletter_Subscriber->where = "group_id=$togroup_id and subscriber_id=$subscriber_id";
				$duplresult=$oNewsletter_Subscriber->getSubscribeList();
				if(mysql_num_rows($duplresult)==0){
					$oNewsletter_Subscriber->data = array("group_id","subscriber_id");
					$oNewsletter_Subscriber->value = array($togroup_id,$subscriber_id);
					$oNewsletter_Subscriber->subscribe();
				}
			}
			
			mysql_free_result($result);
			
		}else{							//selected subscribers
			for($i=0;$i<count($delete_id);$i++){
				$subscriber_id=$delete_id[$i];				
				//check for duplicate subscribe
				$oNewsletter_Subscriber->data = array("distinct(subscriber_id)");
				$oNewsletter_Subscriber->where = "group_id=$togroup_id and subscriber_id=$subscriber_id";
				$result=$oNewsletter_Subscriber->getSubscribeList();
				if(mysql_num_rows($result)==0){
					$oNewsletter_Subscriber->data = array("group_id","subscriber_id");
					$oNewsletter_Subscriber->value = array($togroup_id,$subscriber_id);
					$oNewsletter_Subscriber->subscribe();
				}
			}
		}
	}
	
	if ($operationtype == "remove" || $operationtype == "move"){
		if ($subscrtype == "all"){
			$oNewsletter_Subscriber->where = $wherecondition;
			$oNewsletter_Subscriber->deleteFromGroup();
		}else{
			for($i=0;$i<count($delete_id);$i++){
				$subscriber_id=$delete_id[$i];
				if ($group_id != ""){ $removecondition = $wherecondition." and "; }
				$removecondition = $removecondition."subscriber_id=$subscriber_id";
				$oNewsletter_Subscriber->where = $removecondition;
				$oNewsletter_Subscriber->deleteFromGroup();
			}
		}		
	}	
		
	$oNewsletter_Subscriber->where = "";
	$pageaction = "";
}

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<!--Search Start-->
<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=searchform action="index.php?component=newsletter&page=wce.subscriber.php" method=post>
<tr><td valign=top width=25%>
<? echo $lang['newsletter']['search'] ?></td>
<td><input type="radio" name="searchtype" value="email" <? if ($searchtype == "email" || $searchtype == "") echo checked;?>> <?= $lang['common']['email'] ?>&nbsp;&nbsp;&nbsp;
<input type="radio" name="searchtype" value="concat(firstname, ' ',lastname)" <? if ($searchtype == "concat(firstname, ' ',lastname)") echo checked;?>> <?= $lang['common']['name'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=text name=keyword style="135px" value="<? $keyword ?>" onclick="this.select()">&nbsp;&nbsp;<input type=submit value=" <? echo $lang['newsletter']['btngo'] ?> "></td><td align=right>
</td></tr>
</form>
<form name=thisform action="index.php?component=newsletter&page=wce.subscriber.php" method=post>
<tr><td valign=top><? echo $lang['newsletter']['newslgrp'] ?></td><td valign=top colspan=2>
	<select name=group_id onchange="document.thisform.submit()">
	<option value=""><? echo $lang['newsletter']['suball'] ?></option>
	<?
		$oNewsletter_Group->data = array("group_id","name");
		$oNewsletter_Group->order = "name";
		$result = $oNewsletter_Group->getList();
		$group_name = "All";
		while($myrow=mysql_fetch_row($result)){
			$myrow[1]=stripslashes($myrow[1]);
			if($myrow[0]==$group_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; $group_name = $myrow[1]; }else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>";	}
		}
		mysql_free_result($result);
	?>	
	</select>
</td></tr></form></table><br><hr size=1>
<!--Search End-->
<form name=frmList action="<?= "\"index.php?component=$component&page=$page\"" ?> method=post>
<div id="groupwindow">
<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>
<tr><td valign=top colspan=4><b><?= $lang['newsletter']['subscrgroup'] ?></b><br><br></td><tr>
<tr><td valign=top colspan=4><?= $lang['newsletter']['subscrfromnewslg'].": ".$group_name ?></td></tr>
<tr><td valign=top><input type='radio' name="subscrtype" value="all" checked> <?= $lang['newsletter']['allsubscr'] ?></td>
	<td valign=top colspan=3><input type="radio" name="subscrtype" value="selected"> <?= $lang['newsletter']['selectedsubscr'] ?></td>
</tr>
<tr><td valign=top colspan=4><?= $lang['newsletter']['operation'] ?>: </td></tr>
<tr><td valign=top width=20%><input type='radio' name="operationtype" value="add" checked onclick="toggleWindows('togroup', this)"> <?= $lang['newsletter']['addtogroup'] ?></td>
	<td valign=top width=15%><input type="radio" name="operationtype" value="move" onclick="toggleWindows('togroup', this)" <? if ($group_id=="") echo "disabled" ?>> <?= $lang['newsletter']['movetogroup'] ?></td>
	<td valign=top colspan>
	<div id='togroup' style="display:block;">&nbsp;<? echo $lang['newsletter']['subscrtonewslg'] ?>&nbsp;&nbsp;&nbsp;
	<select name=togroup_id>
	<?
		$oNewsletter_Group->data = array("group_id","name");
		$oNewsletter_Group->order = "name";
		$result = $oNewsletter_Group->getList();
		while($myrow=mysql_fetch_row($result)){
			$myrow[1]=stripslashes($myrow[1]);
			if ($myrow[0]!=$group_id){
				if($myrow[0]==$togroup_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; }
				else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>";	}
			}
		}mysql_free_result($result);
	?>	
	</select></div>
	</td>
</tr>
<tr><td valign=top colspan=3><input type="radio" name="operationtype" value="remove" onclick="toggleWindows('togroup', this)"> <?= $lang['newsletter']['removefromgroup'] ?> -  <?= $group_name?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=button name='submitbtn' value="  <? echo $lang['newsletter']['submit'] ?> " onclick="validation()"></td></tr>
</table>
</div><br />

<?	
	/*****List Subscribers*****/
	$sort=array("firstname","email","lettertype","status");

	if($sortby==""){ $sortby=$sort[0]; } 
	if($sortseq==""){ $sortseq="asc"; }
	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sort[$i]&sortseq=asc&keyword=$keyword&group_id=$group_id&searchtype=$searchtype&start=$start><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['newsletter']['sortasc']."\"></a>";	}
	for($i=0;$i<count($sort);$i++){
		if($sortby==$sort[$i]){
		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=desc&keyword=$keyword&group_id=$group_id&searchtype=$searchtype&start=$start><img src=\"common/image/sort_desc.gif\" border=0 alt=\"".$lang['newsletter']['sortdesc']."\"></a>"; }
		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=asc&keyword=$keyword&group_id=$group_id&searchtype=$searchtype&start=$start><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['newsletter']['sortasc']."\"></a>"; }
	}}

	$listno = $oSystem->getValue("newsletter_sublistno");	
	
	$oNewsletter_Subscriber->data = array("newsletter_subscriber.subscriber_id");
	if($group_id!=""){ 
		$oNewsletter_Subscriber->where="newsletter_subscribe.group_id=$group_id and newsletter_subscriber.subscriber_id=newsletter_subscribe.subscriber_id";
		$result=$oNewsletter_Subscriber->getSubscriberList();	
	}else if($keyword!=""){ 
		$oNewsletter_Subscriber->where = "$searchtype like '%$keyword%'";		
		$result=$oNewsletter_Subscriber->getList();
	}else{ 
		$oNewsletter_Subscriber->where=""; 
		$result=$oNewsletter_Subscriber->getList();		
	}	
		
	if(mysql_num_rows($result)!=0){	$total=mysql_num_rows($result); }else{ $total=0; }	
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$listno; $next=$start+$listno;	$from=$start+1; $to=$listno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }
	if($prev>=0){ $prevlink="<a href=\"index.php?component=newsletter&page=wce.subscriber.php&start=$prev&group_id=$group_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&searchtype=$searchtype\">".$lang['newsletter']['prev']."</a>"; }else{ $prevlink=""; }
	if($next<$total){ $nextlink="<a href=\"index.php?component=newsletter&page=wce.subscriber.php&start=$next&group_id=$group_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&searchtype=$searchtype\">".$lang['newsletter']['next']."</a>"; }else{ $nextlink=""; }

	echo "
	<table border=0 cellpadding=5 cellspacing=0 width=98% align=center>
	<tr><td width=20%><a href=\"javascript:ConfirmDelete()\">".$lang['newsletter']['delselected']."</a></td><td align=center>".$lang['newsletter']['showing']." ".$from." ".$lang['newsletter']['to']." ".$to." ".$lang['newsletter']['of']." ".$total."</td><td width=20% align=right>$prevlink &nbsp;&nbsp; $nextlink</td></tr>
	</table>	
	
	<table border=0 cellpadding=1 cellspacing=0 width=98% align=center>
		<input type=hidden name=pageaction value=\"delete\">
		<input type=hidden name=sortby value=\"$sortby\">
		<input type=hidden name=sortseq value=\"$sortseq\">
		<input type=hidden name=searchtype value=\"$searchtype\">
		<input type=hidden name=keyword value=\"$keyword\">
		<input type=hidden name=group_id value=\"$group_id\">
		<input type=hidden name=start value=\"$start\">
		
	    <tr bgcolor=\"#E6E6E6\" height=\"24\">
	      <td class=\"gridTitle\" align=\"center\" width=\"25\"><input type=checkbox id=\"checkAll\" onclick=\"selectAll()\"></td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['newsletter']['fullname']."</b>&nbsp;$sortlink[0]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['newsletter']['email']."</b>&nbsp;$sortlink[1]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"80\">&nbsp;<b>".$lang['newsletter']['type']."</b>&nbsp;$sortlink[2]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"80\">&nbsp;<b>".$lang['newsletter']['subscrstatus']."</b>&nbsp;$sortlink[3]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"30\">&nbsp;</td>
	    </tr> 
	";

 	$oNewsletter_Subscriber->data = array("newsletter_subscriber.subscriber_id","newsletter_subscriber.firstname","newsletter_subscriber.email","newsletter_subscriber.lettertype","newsletter_subscriber.status","newsletter_subscriber.lastname");
	$oNewsletter_Subscriber->order = "$sortby $sortseq limit $start,$listno";

	if($group_id!=""){ 
		$oNewsletter_Subscriber->where="newsletter_subscribe.group_id=$group_id and newsletter_subscriber.subscriber_id=newsletter_subscribe.subscriber_id";
		$result=$oNewsletter_Subscriber->getSubscriberList();		
	}else if($keyword!=""){ 
		$oNewsletter_Subscriber->where = "$searchtype like '%$keyword%'";
		$result=$oNewsletter_Subscriber->getList();
	}else{ 
		$oNewsletter_Subscriber->where=""; 
		$result=$oNewsletter_Subscriber->getList();		
	}	
	   		
   	$count=0;
   	while($myrow=mysql_fetch_row($result)){
   		$count++;
		$myrow[1]=stripslashes($myrow[1]);
		$fullname = $myrow[1]." ".stripslashes($myrow[5]);
   		echo "
	    <tr id=\"row$count\" style=\"background:#F6F6F6\" height=\"24\">
	      <td class=\"gridRow\" align=\"center\"><input type=checkbox id=\"check$count\" name=\"delete_id[]\" value=\"$myrow[0]\" onclick=\"if(this.checked==true){ selectRow('row$count'); }else{ deselectRow('row$count'); }\"></td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$fullname&nbsp;</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[2]&nbsp;</td>
	      <td class=\"gridRow\" align=\"center\">&nbsp;$myrow[3]&nbsp;</td>
	      <td class=\"gridRow\" align=\"center\">&nbsp;$myrow[4]&nbsp;</td>
   		  <td class=\"gridRow\" align=\"center\"><a href=\"index.php?component=newsletter&page=wce.subscriberedit.php&subscriber_id=$myrow[0]&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&vgroup_id=$group_id&searchtype=$searchtype&start=$start\"><img src=common/image/ico_edit.gif border=0 alt=\"".$lang['newsletter']['editrecord']."\"></a></td>
	    </tr>";  
   	}
	mysql_free_result($result);
	
	echo "</table></form>";	
?>

<script language=javascript>

	function ConfirmDelete(){  
	    if(confirm('<? echo $lang['newsletter']['confirmselected'] ?>')){
	    	document.frmList.submit();
		}
	}
	
	function selectAll(){
		if(document.getElementById("checkAll").checked==true){
			document.getElementById("checkAll").checked=true;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=true; document.getElementById(\"row$i\").style.background='#D6DEEC'; \n"; }	?>
		}else{
			document.getElementById("checkAll").checked=false;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=false; document.getElementById(\"row$i\").style.background='#F6F6F6'; \n"; } ?>		
		}
	}

	function selectRow(row){
		document.getElementById(row).style.background="#D6DEEC";
	}

	function deselectRow(row){
		document.getElementById(row).style.background="#F6F6F6";
	}
	
	function linkbutton(x){
		x.style.cursor = 'pointer';
		x.style.color = 'blue';		
	}
	
	function toggleWindows(windowname, inputobject){		
		var x = document.getElementsByTagName('div')[windowname];
		if (windowname == 'togroup'){
			if (inputobject.value == 'remove'){
				x.style.display='none';
			}				
			else{
				x.style.display='block';
			}	
		}
		else{	
			if (x.style.display == 'block') x.style.display='none'; else x.style.display='block';
		}	
	}
	
	function validation(){				
		/*
		if (document.frmList.operationtype[2].checked){
			toggleWindows('comfirmationwindow', '');
			
		}
		*/
		
		document.frmList.pageaction.value='groupoperation';
		document.frmList.submit();			
	}	
</script>
