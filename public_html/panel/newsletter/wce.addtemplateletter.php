<?

function defaultValue(){

	global $tempfilename, $bodytext, $bodyhtml, $template_id;
	
	$tempfilename = "";
	$bodytext = "";
	$bodyhtml = "";
	$template_id = "0";
}

if($pageaction=="add"){ 	//save template
	
	$bodyhtml = $FCKeditor;
	
	if (trim($tempfilename) == ""){		//if user does not specified template file name
		$tempfilename = "temp_".date("Ymd");
	}

	$oNewsletter_Template->data = array("letter_id");		//check whether template exist or not
	$oNewsletter_Template->where = "letter_name='".addslashes($tempfilename)."'";
	$result=$oNewsletter_Template->getList();
	if(mysql_num_rows($result)>0){
		$oNewsletter_Template->data = array("max(letter_id)");
		$oNewsletter_Template->where = "";
		$result=$oNewsletter_Template->getList();		
		$myrow=mysql_fetch_row($result);
		$tempfilename = $tempfilename."_".($myrow[0]+1);
	}
	$oNewsletter_Template->data = array("letter_name","bodytext","bodyhtml");
	$oNewsletter_Template->value = array(addslashes($tempfilename),addslashes($bodytext),addslashes($bodyhtml));
	$oNewsletter_Template->add();
	
	defaultValue(); 
	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newsltempadded']." <br>";	
	
	
}elseif($pageaction=="loadtemp"){ 	//load template
	
	if ($template_id == "0"){		//blank template
		$bodyhtml = "";
		$bodytext = "";
	}else{
		$oNewsletter_Template->data = array("bodytext","bodyhtml");
		$oNewsletter_Template->where = "letter_id='".addslashes($template_id)."'";
		$result=$oNewsletter_Template->getList();
		if($myrow=mysql_fetch_row($result)){
			$bodyhtml = $myrow[1];
			$bodytext = $myrow[0];
		}
	}
}else{ defaultValue(); }

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center><tr>
<td><b><u><? echo $lang['newsletter']['addtemplate'] ?></u></b></td><td align=right>
</td></tr></table><br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newsletter&page=wce.addtemplateletter.php" method=post>
<input type=hidden name=pageaction value="add">
<input type=hidden name=bodyhtml>
<tr><td valign=top width=25%><? echo $lang['newsletter']['loadtemplate'] ?></td><td><select name=template_id onchange="document.thisform.pageaction.value='loadtemp';document.thisform.submit()">
<option value="0" <? if($template_id == "0") echo selected ?>>Blank</option>
<?	$oNewsletter_Template->data = array("letter_id","letter_name");
	$oNewsletter_Template->where = "";
	$oNewsletter_Template->order = "letter_name";
	$result = $oNewsletter_Template->getList();
	while($myrow=mysql_fetch_row($result)){
		$myrow[1]=stripslashes($myrow[1]);
		if($myrow[0]==$template_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; }else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>"; }
	}
	mysql_free_result($result);
?>
</select></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['newsltempname'] ?></td><td><input type=text name=tempfilename value="<? echo $tempfilename ?>" style="width:330px"> *</td></tr>
<tr><td valign=top colspan=2><? echo $lang['newsletter']['htmlbased'] ?><br>
	<? 
	$oFCKeditor = new FCKeditor('FCKeditor') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/" ;
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '500' ;
	$oFCKeditor->Value		= stripslashes($bodyhtml);
	$oFCKeditor->Create() ;
	?>	
</td></tr>
<tr><td valign=top colspan=2><br><? echo $lang['newsletter']['plaintext'] ?><br><textarea name=bodytext cols=110 rows=23 style="width: 100%"><?= stripslashes(htmlentities($bodytext)) ?></textarea></td></tr>
<tr><td valign=top colspan=2><br>
<input type=submit name='submitbtn' value="  <? echo $lang['newsletter']['btnsave'] ?> ">
</td></tr>
</form></table>
