#!/usr/bin/php -q
<?php $configpath = "/home/printphi/public_html/panel/";  ?>
<?
	
if (!isset($_REQUEST['configpath']) && !isset($_GET['configpath']) && !isset($_POST['configpath'])) {
    include($configpath."config.php");
	
}

$today = date("Y-m-d");
$ttime = date("H:i:s");
$dtCron = date("Y-m-d H:i:s");

$thisfile = $oSystem->getValue("newsletter_page");	
$emailfrom = ($oSystem->getValue("newsletter_popemail")==""?$oUser->getAdminEmail():$oSystem->getValue("newsletter_popemail"));

$oNewsletter_Delivery->data = array("delivery_id","delivery","letter_id","subscriber_id");
$oNewsletter_Delivery->where = " ADDTIME( `delivery` , `delivery_time` ) <= '{$dtCron}'";
$oNewsletter_Delivery->order = "delivery_id limit 0,30";
$result = $oNewsletter_Delivery->getList();		
$countHowMany = 0;
while($myrow=mysql_fetch_row($result)){

	$oNewsletter_Letter->data = array("title","bodytext","bodyhtml","attachment","send_attachment");
	$resultsend = $oNewsletter_Letter->getDetail($myrow[2]);
	if($myrowsend = mysql_fetch_row($resultsend)){ 
		$title = stripslashes($myrowsend[0]); 
		$bodytext = stripslashes($myrowsend[1]); 
		$bodyhtml = stripslashes($myrowsend[2]);
		$attachmenturl = ""; 
			$dlattachmenturl="";
			if(trim($myrowsend[3]) != "" && !is_null($myrowsend[3])){
				if ($myrowsend[4] == "Yes"){
					if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
						$attachmenturl = $path["docroot"]."_files/newsletter/".stripslashes($myrowsend[3]);
					}
				}
				$dlattachmenturl = $path["webroot"]."_files/newsletter/".stripslashes($myrowsend[3]);
			} 
	}
	mysql_free_result($resultsend);	
	
	$oNewsletter_Subscriber->data = array("firstname","lastname","company","position","email","lettertype");
	$resultsend=$oNewsletter_Subscriber->getDetail($myrow[3]);
	if($myrowsend = mysql_fetch_row($resultsend)){ 
		$firstname = stripslashes($myrowsend[0]); 
		$lastname = stripslashes($myrowsend[1]); 
		$company = stripslashes($myrowsend[2]); 
		$position = stripslashes($myrowsend[3]); 
		$email = $myrowsend[4];
		$lettertype = $myrowsend[5];
	}		
	mysql_free_result($resultsend);	
	
	$tohtmlurl="$thisfile?pageaction=htmlnewsletter&subscriber_id=$myrow[3]&email=$email";
	$totexturl="<a href=$thisfile?pageaction=textnewsletter&subscriber_id=$myrow[3]&email=$email>".$lang['newsletter']['here']."</a>";
	$unsuburl="$thisfile?pageaction=unsubscribe&subscriber_id=$myrow[3]&email=$email";
	$unsuburlhtml="<a href=$thisfile?pageaction=unsubscribe&subscriber_id=$myrow[3]&email=$email>".$lang['newsletter']['here']."</a>";

	if($lettertype=="Text"){
		$message = stripslashes($oSystem->getValue("newsletter_newstextbody"));
		$message = str_replace("[[tohtmlurl]]",$tohtmlurl,$message);
		$message = str_replace("[[newsletterbody]]",$bodytext,$message);
		$message = str_replace("[[unsubscribeurl]]",$unsuburl,$message);
		$message = str_replace("[[firstname]]",$firstname,$message);
		$message = str_replace("[[lastname]]",$lastname,$message);
		$message = str_replace("[[company]]",$company,$message);
		$message = str_replace("[[position]]",$position,$message);
		$message = str_replace("[[email]]",$email,$message);
		$message = str_replace("[[attachment]]",$dlattachmenturl,$message);
		$oSystem->mail($email,$title,$message,$emailfrom,"",$attachmenturl,"Newsletter");
	}else{
		$message = nl2br(stripslashes($oSystem->getValue("newsletter_newshtmlbody")));
		$message = str_replace("[[totexturl]]",$totexturl,$message);		
		$message = str_replace("[[newsletterbody]]",$bodyhtml,$message);
		$message = str_replace("[[unsubscribeurl]]",$unsuburlhtml,$message);
		$message = str_replace("[[firstname]]",$firstname,$message);
		$message = str_replace("[[lastname]]",$lastname,$message);
		$message = str_replace("[[company]]",$company,$message);
		$message = str_replace("[[position]]",$position,$message);
		$message = str_replace("[[email]]",$email,$message);
		if ($dlattachmenturl != ""){
				$dlattachmenturl = "<a href=\"$dlattachmenturl\">Download File</a>";
			}			
		$message = str_replace("[[attachment]]",$dlattachmenturl,$message);
		$oSystem->mail($email,$title,$message,$emailfrom,"HTML",$attachmenturl,"Newsletter");
	}	
	$countHowMany = $countHowMany + 1;
	$oNewsletter_Delivery->delete($myrow[0]);		
}

print "Number of sent messages: {$countHowMany} :: Date: ".date("Y-m-d H:i:s")." - ".round($time, 2)." seconds.\n";
?>