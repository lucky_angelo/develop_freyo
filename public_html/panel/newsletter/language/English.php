<?
$lang['newsletter']['menutitle'] 		= "Newsletter";
$lang['newsletter']['componentdescp']	= "Multiple groups, confirmation, activation, unsubscribe, HTML or text-based, past newsletter, personalization, HTML editor, test email, import subscribers, bulk sending tool. ";


/***** Top Links *****/
$lang['newsletter']['newsletter'] 		= "Newsletter";
$lang['newsletter']['managenewsl'] 		= "Manage Newsletter";
$lang['newsletter']['addnewsl'] 		= "Add Newsletter";
$lang['newsletter']['listnewsl'] 		= "List Newsletter";
$lang['newsletter']['editnewsl'] 		= "Edit Newsletter";
$lang['newsletter']['managegrp'] 		= "Manage Group";
$lang['newsletter']['subscriber'] 		= "Subscribers";
$lang['newsletter']['delivery'] 		= "Delivery Schedule";
$lang['newsletter']['preference'] 		= "Preference";
$lang['newsletter']['sendschedule']		= "Send Schedule";
$lang['newsletter']['importsubsc']		= "Import Subscriber";
$lang['newsletter']['sendschedule']		= "Send Schedule";
$lang['newsletter']['listsubscriber']	= "List Subscriber";
$lang['newsletter']['addsubscriber']	= "Add Subscriber";
$lang['newsletter']['newslettergroup']	= "Newsletter Group";
$lang['newsletter']['writenewsletter']	= "Write Newsletter";
$lang['newsletter']['help'] 			= "Help";


/***** Add Newsletter *****/
$lang['newsletter']['status'] 			= "Status";
$lang['newsletter']['newsladded'] 		= "Newsletter added. Go to Delivery Schedule to send the newsletter.";
$lang['newsletter']['choosegrp'] 		= "Choose Group";
$lang['newsletter']['publishdate'] 		= "Publish Date";
$lang['newsletter']['fromemail'] 		= "From Email";
$lang['newsletter']['newslsubject'] 	= "Newsletter Subject";
$lang['newsletter']['newslbody'] 		= "Newsletter Body";
$lang['newsletter']['htmlbased'] 		= "HTML-based Email";
$lang['newsletter']['plaintext'] 		= "Plain Text Email";
$lang['newsletter']['newsltext'] 		= "Text";
$lang['newsletter']['showarchive'] 		= "Show Newsletter In Archive";
$lang['newsletter']['remove'] 			= "Remove";
$lang['newsletter']['btnsave'] 			= "Save";
$lang['newsletter']['plstitle'] 		= "Please enter your Newsletter Subject.";
$lang['newsletter']['warnexceed']		= "Upload file size exceeds ";
$lang['newsletter']['uploadfile']		= "Upload Attachment";
$lang['newsletter']['attachfile']		= "Attach to Email";



/***** List Newsletter *****/
$lang['newsletter']['newsldel'] 		= "Newsletter(s) deleted.";
$lang['newsletter']['newslgrp'] 		= "Newsletter Group";
$lang['newsletter']['showall'] 			= "Show All";
$lang['newsletter']['searchsub'] 		= "Search Subject";
$lang['newsletter']['btngo'] 			= "Go";
$lang['newsletter']['sortasc'] 			= "Sort In Ascending Order";
$lang['newsletter']['sortdesc'] 		= "Sort In Descending Order";
$lang['newsletter']['prev'] 			= "Prev";
$lang['newsletter']['next'] 			= "Next";
$lang['newsletter']['delselected'] 		= "Delete Selected";
$lang['newsletter']['showing'] 			= "Showing";
$lang['newsletter']['to'] 				= "To";
$lang['newsletter']['of'] 				= "Of";
$lang['newsletter']['listnewslsub'] 	= "Newsletter Subject";
$lang['newsletter']['listpublish'] 		= "Publish";
$lang['newsletter']['listsend'] 		= "Send";
$lang['newsletter']['editrecord'] 		= "Edit This Record";
$lang['newsletter']['confirmselected'] 	= "Are you sure you want to delete the selected record(s) ?";


/***** Edit Newsletter *****/
$lang['newsletter']['newslupdate'] 		= "Newsletter updated.";
$lang['newsletter']['here'] 			= "Here";
$lang['newsletter']['newslsend'] 		= "Both HTML and Text newsletter has been sent to your Test email address.";
$lang['newsletter']['backtolist'] 		= "Back To List";
$lang['newsletter']['newslno'] 			= "Newsletter No.";
$lang['newsletter']['btndel'] 			= "Delete";
$lang['newsletter']['btnback'] 			= "Back";
$lang['newsletter']['btntestemail'] 	= "Test Newsletter";
$lang['newsletter']['btnsendnow'] 		= "Send Now";
$lang['newsletter']['confirmdel'] 		= "Are you sure you want to delete this record ?";


/***** Newsletter Group *****/
$lang['newsletter']['grpadd'] 			= "Newsletter group added.";
$lang['newsletter']['grpedit'] 			= "Editing newsletter group.";
$lang['newsletter']['grpupdate'] 		= "Newsletter group updated.";
$lang['newsletter']['grpdel'] 			= "Newsletter group deleted.";
$lang['newsletter']['showpublic'] 		= "Show group for public subscribers";
$lang['newsletter']['btnupdate'] 		= "Update";
$lang['newsletter']['btnadd'] 			= "Add";
$lang['newsletter']['listno'] 			= "No";
$lang['newsletter']['listnewslgrp'] 	= "Newsletter Group";
$lang['newsletter']['listsub'] 			= "Subscribers";
$lang['newsletter']['delrecord'] 		= "Delete This Record";
$lang['newsletter']['plsgroup'] 		= "Please enter your Newsletter Group.";


/***** Subscribers ******/
$lang['newsletter']['fullname'] 		= "Full Name";
$lang['newsletter']['grpid'] 			= "Group ID";
$lang['newsletter']['keyword'] 			= "Keyword";
$lang['newsletter']['alllist'] 			= "All List";
$lang['newsletter']['subscrdel'] 		= "Selected subscriber(s) deleted.";
$lang['newsletter']['searchemail'] 		= "Search Email";
$lang['newsletter']['btnimport'] 		= "Import";
$lang['newsletter']['suball'] 			= "All";
$lang['newsletter']['firstname'] 		= "First Name";
$lang['newsletter']['email'] 			= "Email Address";
$lang['newsletter']['type'] 			= "Type";
$lang['newsletter']['subscrstatus'] 	= "Status";
$lang['newsletter']['subscradd'] 		= "New subscriber added. Click on List to view.";
$lang['newsletter']['addsub'] 			= "Add Subscriber";
$lang['newsletter']['alreadyexist'] 	= "already exist in your mailing list";
$lang['newsletter']['btnlist'] 			= "List";
$lang['newsletter']['lastname'] 		= "Last Name";
$lang['newsletter']['company'] 			= "Company";
$lang['newsletter']['position'] 		= "Position";
$lang['newsletter']['newsltype'] 		= "Newsletter Type";
$lang['newsletter']['subscrupdate'] 	= "Subscriber details updated.";
$lang['newsletter']['active'] 			= "Active";
$lang['newsletter']['verify'] 			= "Verify";
$lang['newsletter']['suspend'] 			= "Suspend";
$lang['newsletter']['subscrgrp'] 		= "Subscribed Group";
$lang['newsletter']['subscrimport'] 	= "subscriber(s) imported. Click on List to view.";
$lang['newsletter']['message1'] 		= "Newsletter subscriber database can be imported from your existing contacts.";
$lang['newsletter']['message2'] 		= "Prepare all your contacts in CSV file with the following format.";
$lang['newsletter']['format'] 			= "Format:";
$lang['newsletter']['example'] 			= "Example:";
$lang['newsletter']['browsecsv'] 		= "Browse for the CSV file";
$lang['newsletter']['checkgrp'] 		= "Check to subscribe to newsletter group below:";
$lang['newsletter']['plsemail'] 		= "Please enter your Subscriber's Email Address.";


/****** Preference Settings ******/
$lang['newsletter']['general']			= "General Settings";
$lang['newsletter']['prefsave'] 		= "Preference settings saved.";
$lang['newsletter']['prefdefault'] 		= "Preference settings restored to default values.";
$lang['newsletter']['fromeamail'] 		= "From Email Address";
$lang['newsletter']['testemail'] 		= "Test Email Address";
$lang['newsletter']['listnorecord'] 	= "No of Records in List Newsletter";
$lang['newsletter']['subscrno'] 		= "No of Records in Subscribers";
$lang['newsletter']['browseno'] 		= "No of Records in Browse Newsletters";
$lang['newsletter']['newslseq'] 		= "Sequence of Newsletter Archives";
$lang['newsletter']['ascending'] 		= "Ascending";
$lang['newsletter']['descending'] 		= "Descending";
$lang['newsletter']['textnewsl'] 		= "Text-based Newsletter Body";
$lang['newsletter']['htmlnewsl'] 		= "HTML-based Newsletter Body";
$lang['newsletter']['confirmemail'] 	= "Confirmation Email";
$lang['newsletter']['btndefault'] 		= "Use Default";
$lang['newsletter']['sendersetting']	= "Sender's Email Settings (POP3)";	//edited by wee
$lang['newsletter']['servername'] 		= "Server Name";
$lang['newsletter']['loginid']	 		= "Login ID";
$lang['newsletter']['password'] 		= "Password";
$lang['newsletter']['newslbrowse'] 		= "Enable Browse Newsletter";
$lang['newsletter']['yes'] 				= "Yes";
$lang['newsletter']['no'] 				= "No";
$lang['newsletter']['prefsetting']		= "Preference Setting";


/*****Integration Guide*****/
$lang['newsletter']['interguide']			= "Integration Guides";
$lang['newsletter']['viewsamplepage']		= "Click Here To View Sample Page";
$lang['newsletter']['frontinterguide']		= "Front-End Integration Guidelines";
$lang['newsletter']['interguide1']			= "You can easily integrate the front-end with your web design pages.";
$lang['newsletter']['before']				= "Before";
$lang['newsletter']['afterintergration']	= "After Integration";
$lang['newsletter']['intergrationins']		= "Integration Instructions";
$lang['newsletter']['invalidfrontend']		= "Invalid Front End URL, ensure that the Front-End URL in Preference > Preference Setting page is not blank and valid";
$lang['newsletter']['viewfrontend']			= "Front End View";
$lang['newsletter']['guide1']				= "Using a text editor, open the web page file (.php file) that you wish to integrate the Newsletter to, insert the line below to the top of the file, to call the 'config.php' file.";
$lang['newsletter']['guide1note']			= "* Note: Always put the above code to the line 1, without any preceding character(s), not even a space.";
$lang['newsletter']['guide2']				= "Meanwhile, to display the newsletter's content area, put the line below.";


/***** Bulk Sending *****/
$lang['newsletter']['bulksending'] 		= "Newsletter Bulk Sending Tool";
$lang['newsletter']['timeout'] 			= "In order to avoid server time out during the process, you may wish to send the newsletter in separate cycles. Smaller number will produce better results.";
$lang['newsletter']['totalsubscr'] 		= "Total subscribers to be sent for this newsletter";
$lang['newsletter']['selectnumber'] 	= "Select number of subscribers to send for each cycle";
$lang['newsletter']['btnstartsend'] 	= "Start Sending";
$lang['newsletter']['inprogress'] 		= "Sending Newsletter In Progress...";
$lang['newsletter']['sendcomplete'] 	= "Send Progress Complete!";
$lang['newsletter']['remaining'] 		= "Remaining";
$lang['newsletter']['totalnewsl'] 		= "Total Newsletter(s) Sent";
$lang['newsletter']['timestart'] 		= "Server Time Start";
$lang['newsletter']['timeend'] 			= "Server Time End";
$lang['newsletter']['closewindow'] 		= "Close Window";


/***** Delivery *****/
$lang['newsletter']['repeatdelivery']	= "Repeated delivery schedule found.";
$lang['newsletter']['deliveryadded']	= "Delivery schedule added.";
$lang['newsletter']['deliverydeleted']	= "Selected delivery schedule(s) deleted.";
$lang['newsletter']['improvesend'] 		= "For improved newsletter sending task, kindly use this feature to manage your newsletter delivery schedule.";
$lang['newsletter']['deliverydate']		= "Delivery Date";
$lang['newsletter']['choosenewsletter']	= "Choose Newsletter";
$lang['newsletter']['choosesubscriber']	= "Choose Subscriber Group";
$lang['newsletter']['addschedule'] 		= "Add To Schedule";
$lang['newsletter']['emailscheduled']	= "Email Addresses Scheduled For Delivery";
$lang['newsletter']['sendnow'] 			= "Send Now!";
$lang['newsletter']['bounceemail'] 		= "Bounce Email";
$lang['newsletter']['cronjob']	 		= "If you wish to run the Delivery Schedule using Cron Job, please read the tutorial in our Knowledge Base.";
$lang['newsletter']['schedule'] 		= "Schedule";
$lang['newsletter']['bouncedeleted'] 	= "Bounced email addresses have been deleted from the subscribers database.";
$lang['newsletter']['undelivered'] 		= "Undelivered Email Addresses (Bounce Back)";
$lang['newsletter']['deliveryschedule']	= "Delivery Schedule";
$lang['newsletter']['total']	 		= "Total";
$lang['newsletter']['invalidpop'] 		= "Invalid POP Login";


/***** Front-End *****/
$lang['newsletter']['hbrowsenewsl'] 	= "Browse Newsletter";
$lang['newsletter']['hnewsltitle'] 		= "Newsletter Title";
$lang['newsletter']['hdate'] 			= "Date";
$lang['newsletter']['hfound'] 			= "No Newsletter Found";
$lang['newsletter']['htitle'] 			= "Title";
$lang['newsletter']['hback'] 			= "Back";
$lang['newsletter']['hsubscrnewsl'] 	= "Subscribe Newsletter";
$lang['newsletter']['hrequired'] 		= "Required Field. Please check at least one newsletter group.";
$lang['newsletter']['hnotes'] 			= "Important notes";
$lang['newsletter']['hunsubscr'] 		= "You may unsubscribe at anytime.";
$lang['newsletter']['hrespond'] 		= "You may have to respond to a confirmation email.";
$lang['newsletter']['hunblock'] 		= "You may have to unblock future emails from bulk mail filters.";
$lang['newsletter']['hprivacy'] 		= "Privacy Policy";
$lang['newsletter']['hreveal'] 			= "We will not reveal any email addresses to third parties for any reasons.";
$lang['newsletter']['hbtnsubscribe'] 	= "Subscribe";
$lang['newsletter']['hplsfirstname']	= "Please enter your First Name.";
$lang['newsletter']['hplslastname']		= "Please enter your Last Name.";
$lang['newsletter']['hplsemail']		= "Please enter your Email Address.";
$lang['newsletter']['hplsvalid']		= "Please make sure that your Email Address is a valid email.";
$lang['newsletter']['hplscheck']		= "Please check at least one Newsletter Group";
$lang['newsletter']['hthanksubscr']		= "Thank you for subscribing to our newsletter. A confirmation email has been sent to your email address.";
$lang['newsletter']['hcheckemail']		= "Please check your email in a short while to complete the subscription process.";
$lang['newsletter']['hreturnhome']		= "Return To Newsletter";

$lang['newsletter']['hsubscribeconf']	= "Newsletter Subscription Confirmation";
$lang['newsletter']['hmsgsorry']		= "We are sorry that your email address is already in our database.";
$lang['newsletter']['hmsgactivate']		= "Activate Newsletter Subscription";
$lang['newsletter']['hmsgthankactive']	= "Thank you for activating your Newsletter subscription. You will be able to receive our next newsletter release.";
$lang['newsletter']['hmsgunsubscribe']	= "Unsubscribe Newsletter";
$lang['newsletter']['hmsgregret']		= "We regret to hear that you have decided to unsubscribe our newsletter. Thank you for using our service.";
$lang['newsletter']['hmsgreceivetxt']	= "Receive Text-based Newsletter";
$lang['newsletter']['hmsgtxtfuture']	= "Thank you for using our service. You will be receiving text-based newsletter in the future.";
$lang['newsletter']['hmsgreceivehtml']	= "Receive HTML-based Newsletter";
$lang['newsletter']['hmsghtmlfuture']	= "Thank you for using our service. You will be receiving HTML-based newsletter in the future.";

/******** Template *************/
$lang['newsletter']['addtemplate'] 		= "Create Template";		
$lang['newsletter']['listtemplate']		= "List Template";
$lang['newsletter']['searchtempl']		= "Search Template";			
$lang['newsletter']['btnsavetemplate'] 	= "Save As Template";		
$lang['newsletter']['loadtemplate'] 	= "Load Template";			
$lang['newsletter']['newsltempname']	= "Template Name";			
$lang['newsletter']['newsltempadded']	= "Template created.";		
$lang['newsletter']['newsltempdel'] 	= "Template(s) deleted.";	
$lang['newsletter']['search'] 			= "Search";					
$lang['newsletter']['subscrgroup']		= "Manage Subscribers Group";
$lang['newsletter']['selectedsubscr']	= "Selected Subscribers"; 	
$lang['newsletter']['allsubscr']		= "All Subscibers";
$lang['newsletter']['subscrfromnewslg']	= "Subscribers From Newsletter Group";
$lang['newsletter']['subscrtonewslg']	= "To Newsletter Group";
$lang['newsletter']['movetogroup']		= "Move";
$lang['newsletter']['addtogroup']		= "Add";
$lang['newsletter']['removefromgroup']	= "Remove From Group";
$lang['newsletter']['operation']		= "Operation";
$lang['newsletter']['submit']			= "Submit";
$lang['newsletter']['subscrtosend']		= "Subscibers Email Address For Delivery";
$lang['newsletter']['bouncemsg']		= "Bounce Mail Message";
$lang['newsletter']['frontendurl']		= "Front-End URL";
$lang['newsletter']['imgverify']		= "Enable Image Text Verification";
$lang['newsletter']['imgverifynote']	= "Requires GD Library with FreeType Support.";
?>