<?

$maxsize = ini_get('upload_max_filesize');
$upload_max = (!empty($maxsize)?substr($maxsize,0,-1)."000000":"");

if($pageaction=="update"){

	$bodyhtml = $FCKeditor;
	$oNewsletter_Letter->data = array("group_id","dateletter","title","bodytext","bodyhtml","showletter","attachment","send_attachment");
	$oNewsletter_Letter->value = array($group_id,$dateletter,addslashes($title),addslashes($bodytext),addslashes($bodyhtml),$showletter,addslashes($attachmentname),$sendmail);
	$oNewsletter_Letter->update($letter_id);
	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newslupdate']."<br>";	
}


if($pageaction=="test"){

	$oNewsletter_Letter->data = array("title","bodytext","bodyhtml","group_id","attachment","send_attachment");
	$result=$oNewsletter_Letter->getDetail($letter_id);
	if($myrow=mysql_fetch_row($result)){ 
		$title=stripslashes($myrow[0]); $bodytext=stripslashes($myrow[1]); $bodyhtml=stripslashes($myrow[2]); $group_id=$myrow[3]; 
		$attachmenturl = ""; 
		$dlattachmenturl="";
		if(trim($myrow[4]) != "" && !is_null($myrow[4])){
			if ($myrow[5] == "Yes"){
				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
					$attachmenturl = $path["docroot"]."_files/newsletter/".stripslashes($myrow[4]);
				}
			}
			$dlattachmenturl = $path["webroot"]."_files/newsletter/".stripslashes($myrow[4]);
		}
	}
	mysql_free_result($result);
	
	$email = ($oSystem->getValue("newsletter_popemail")==""?$oUser->getAdminEmail():$oSystem->getValue("newsletter_popemail"));
	$emailfrom = ($oSystem->getValue("newsletter_popemail")==""?$oUser->getAdminEmail():$oSystem->getValue("newsletter_popemail"));
	$thisfile = $oSystem->getValue("newsletter_page");
	$tohtmlurl="$thisfile?pageaction=htmlnewsletter&subscriber_id=";	
	$totexturl="<a href=$thisfile?pageaction=textnewsletter&subscriber_id=>".$lang['newsletter']['here']."</a>";
	$unsuburl="$thisfile?pageaction=unsubscribe&subscriber_id=";
	$unsuburlhtml="<a href=$thisfile?pageaction=unsubscribe&subscriber_id=>".$lang['newsletter']['here']."</a>";

	$messagetext = stripslashes($oSystem->getValue("newsletter_newstextbody"));	
	$messagetext = str_replace("[[tohtmlurl]]",$tohtmlurl,$messagetext);
	$messagetext = str_replace("[[newsletterbody]]",$bodytext,$messagetext);
	$messagetext = str_replace("[[unsubscribeurl]]",$unsuburl,$messagetext);
	$messagetext = str_replace("[[attachment]]",$dlattachmenturl,$messagetext);

	$messagehtml = nl2br(stripslashes($oSystem->getValue("newsletter_newshtmlbody")));	
	$messagehtml = str_replace("[[totexturl]]",$totexturl,$messagehtml);
	$messagehtml = str_replace("[[newsletterbody]]",$bodyhtml,$messagehtml);
	$messagehtml = str_replace("[[unsubscribeurl]]",$unsuburlhtml,$messagehtml);
	$messagehtml = str_replace("[[attachment]]","<a href=\"$dlattachmenturl\">Download File</a>",$messagehtml);

	$oSystem->mail($email,$title,$messagehtml,$emailfrom,"HTML",$attachmenturl);	
	$oSystem->mail($email,$title,$messagetext,$emailfrom,"",$attachmenturl);	
		
	$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['newslsend']."<br>";		
}

if($pageaction=="uploadfile"){
  	if(($_FILES['attachment']['size']<$upload_max || empty($upload_max)) && !empty($_FILES['attachment']['size'])){

	  	$attachmentname = strtolower(str_replace(" ","_",trim($_FILES['attachment']['name'])));
	  	
	  	//ensure file not replace when have same file name
	  	$counter = "10000";
	  	$attachmentname = substr($counter,1)."_".$attachmentname;	  		  	
	  	while ($counter++){
		  	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		  		if (file_exists($path["docroot"]."_files/newsletter/".$attachmentname)) {
		  			$attachmentname = substr($counter,1)."_".substr($attachmentname,5);
				}else{
					break;	
				}	
			}		
		}
	  	
		if($attachmentname!=""){ 
			$attachmentname = stripslashes($attachmentname);
			if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
				$oNewsletter_Letter->uploadFile($_FILES['attachment']['tmp_name'],$path["docroot"]."_files/newsletter",$attachmentname); 
			}
			$IsUploaded = "true";
			$fileurl = $path["webroot"]."_files/newsletter/$attachmentname";
		}		
	}else{
		$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['warnexceed']."<br>";
	}
	
	$pageaction = "";
}

if($pageaction=="removefile"){
	$attachmentname = stripslashes($attachmentname);
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if (file_exists($path["docroot"]."_files/newsletter/$attachmentname")){	
	  		unlink($path["docroot"]."_files/newsletter/$attachmentname");
		}
	}
	$oNewsletter_Letter->data = array("attachment");
	$oNewsletter_Letter->value = array("");
	$oNewsletter_Letter->update($letter_id);
	$IsUploaded = "";
	$fileurl = "";
	$attachmentname = "";
	$pageaction = "";
}

$oNewsletter_Letter->data = array("group_id","dateletter","title","bodytext","bodyhtml","showletter","attachment","send_attachment");
$result=$oNewsletter_Letter->getDetail($letter_id);
if($myrow=mysql_fetch_row($result)){
	$group_id = $myrow[0];
	$dateletter = $myrow[1];
	$title = stripslashes($myrow[2]);
	$bodytext = stripslashes(htmlentities($myrow[3]));
	$bodyhtml = stripslashes($myrow[4]);
	if($myrow[5]=="Yes"){ $showletter="Yes"; }
	
	if(trim($myrow[6]) != "" && !is_null($myrow[6])){
		$attachmentname = stripslashes($myrow[6]);
		$IsUploaded = "true";
		$fileurl = $path["webroot"]."_files/newsletter/$attachmentname";
	}
	
	if($myrow[7]=="Yes"){ $sendmail="Yes"; }
}
mysql_free_result($result);

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<?
	/*****Navigation Calculation*****/
	$oNewsletter_Letter->data=array("letter_id");	
	if($vgroup_id!=""){ $oNewsletter_Letter->where="group_id='$vgroup_id'"; }
	else if($keyword!=""){ $oNewsletter_Letter->where="title like '%$keyword%'"; }
	else{ $oNewsletter_Letter->where=""; }	
	$oNewsletter_Letter->order="$sortby $sortseq"; $result=$oNewsletter_Letter->getList();		
	$count=0; while($myrow=mysql_fetch_row($result)){ if($myrow[0]==$letter_id){ $curpos=$count; }	$count++; }		
	$prevpos=$curpos-1; $nextpos=$curpos+1;	$curpos=$curpos+1;	$total = mysql_num_rows($result);
	if($prevpos>=0){ mysql_data_seek($result,$prevpos);	if($myrow=mysql_fetch_row($result)){ $prev_id=$myrow[0]; }}
	if($nextpos < $total){ mysql_data_seek($result,$nextpos); if($myrow=mysql_fetch_row($result)){ $next_id=$myrow[0]; }}
	if($total==0){ $curpos=0; }	mysql_free_result($result);
	if($prev_id!=""){	$prevlink="<a href=\"index.php?component=newsletter&page=wce.editletter.php&letter_id=$prev_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&vgroup_id=$vgroup_id&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['prev']."</a>";	}
	if($next_id!=""){	$nextlink="<a href=\"index.php?component=newsletter&page=wce.editletter.php&letter_id=$next_id&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&vgroup_id=$vgroup_id&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['next']."</a>";	}
	if($prevlink!="" && $nextlink!=""){		$navline="&nbsp;";	}

	echo "
	<table border=0 cellpadding=0 cellspacing=0 width=98% align=center><tr>
	<td width=20%><a href=\"index.php?component=newsletter&page=wce.listletter.php&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&group_id=$vgroup_id&start=$start\" style=\"text-decoration:none\">".$lang['newsletter']['backtolist']."</a></td>
	<td align=center>".$lang['newsletter']['newslno']." ".$curpos." ".$lang['newsletter']['of']." ".$total."</td><td width=23% align=right>$prevlink $navline $nextlink</td></tr>
	</table><br>
	";
?>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newsletter&page=wce.editletter.php" method=post enctype="multipart/form-data">
<input type=hidden name=pageaction value="update">
<input type=hidden name=letter_id value="<? echo $letter_id ?>">
<input type=hidden name=sortby value="<? echo $sortby ?>">
<input type=hidden name=sortseq value="<? echo $sortseq ?>">
<input type=hidden name=keyword value="<? echo $keyword ?>">
<input type=hidden name=vgroup_id value="<? echo $vgroup_id ?>">
<input type=hidden name=start value="<? echo $start ?>">
<input type=hidden name=fileurl value="<?= $fileurl ?>">
<input type=hidden name=attachmentname value="<?= $attachmentname ?>">
<input type=hidden name=IsUploaded value="<?= $IsUploaded ?>">

<tr><td valign=top width=25%><? echo $lang['newsletter']['choosegrp'] ?></td><td><select name=group_id>
<?	$oNewsletter_Group->data = array("group_id","name"); $oNewsletter_Group->order = "name";
	$result = $oNewsletter_Group->getList();
	while($myrow=mysql_fetch_row($result)){
		$myrow[1]=stripslashes($myrow[1]);
		if($myrow[0]==$group_id){ echo "<option value=\"$myrow[0]\" selected>$myrow[1]</option>"; }else{ echo "<option value=\"$myrow[0]\">$myrow[1]</option>"; }
	}
	mysql_free_result($result);
?> 
</select></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['publishdate'] ?></td><td><input type=text name=dateletter style="width:135px" value="<? echo $dateletter ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=checkbox name=showletter value="Yes" <? if ($showletter == "Yes") echo "checked";?>> <? echo $lang['newsletter']['showarchive'] ?></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['newslsubject'] ?></td><td><input type=text name=title style="width:330px" value="<? echo $title ?>"> *</td></tr>
<tr><td valign=top><? echo $lang['newsletter']['newslbody'] ?>: <br><br></td></tr>

<tr><td valign=top colspan=2><? echo $lang['newsletter']['htmlbased'] ?><br>
	<? 
	$oFCKeditor = new FCKeditor('FCKeditor') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '500' ;
	$oFCKeditor->Value		= $bodyhtml ;
	$oFCKeditor->Create() ;
	?>	
</td></tr>
<tr><td valign=top colspan=2><br><? echo $lang['newsletter']['plaintext'] ?><br><textarea name=bodytext cols=110 rows=23 style="width: 100%"><? echo $bodytext ?></textarea></td></tr>
<tr><td height="25px" valign=top><? echo $lang['newsletter']['uploadfile'] ?></td>
<? 
	echo (!empty($upload_max)?"<input type=hidden name=MAX_FILE_SIZE value=$upload_max>":"");
	if ($IsUploaded == "true"){
		echo "<td valign=top><input type=\"file\" name=\"attachment\" size=\"40\" onchange=\"document.thisform.pageaction.value='uploadfile'; document.thisform.submit();\"><br><a href=\"$fileurl\" target=\"_blank\">$attachmentname</a>&nbsp;&nbsp;
		<a href=\"javascript:onclick='document.thisform.pageaction.value='removefile'; document.thisform.submit();'\">
		".$lang['newsletter']['remove']."</a><br><input type=checkbox name=sendmail value=\"Yes\" ";
		if ($sendmail=="Yes") echo "checked"; 
		echo "> ".$lang['newsletter']['attachfile']."</td>"; 
	}else{
		echo "<td valign=top><input type=\"file\" name=\"attachment\" size=\"40\" onchange=\"document.thisform.pageaction.value='uploadfile'; document.thisform.submit();\">"; 
	}
?>
</td></tr>
<tr><td colspan=2><br>
	<table border=0 cellpadding=0 cellspacing=0 width=100%><tr><td>
	<input type=button value=" <? echo $lang['newsletter']['btnsave'] ?> " onclick="validate()">
	<input type=button value="<? echo $lang['newsletter']['btndel'] ?>" onclick="ConfirmDelete(<? echo $letter_id ?>)">
	<input type=button value=" <? echo $lang['newsletter']['btnback'] ?> " onclick="<? echo "window.location='index.php?component=newsletter&page=wce.listletter.php&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&group_id=$vgroup_id&start=$start'"; ?>">
	</td><td align=right>
	<input type=button value="<? echo $lang['newsletter']['btntestemail'] ?>" onclick="document.thisform.pageaction.value='test';document.thisform.submit()">
	</td></tr></table>
</td></tr></form></table>
	
<script language=javascript>

	function ConfirmDelete(id){  
	    if(confirm('<? echo $lang['newsletter']['confirmdel'] ?>')){
			window.location='index.php?component=newsletter&page=wce.listletter.php&pageaction=delete&delete_id[0]='+id+'<? echo "&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&group_id=$vgroup_id&start=$start"; ?>';
		}
	}
		
	function validate(){
		if(document.thisform.title.value==""){
			alert('<? echo $lang['newsletter']['plstitle'] ?>'); document.thisform.title.select();  return false;
		}else{
			document.thisform.submit();
		}	
	}	

</script>
