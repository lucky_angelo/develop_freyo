<?


if ($searchtype != "") $searchtype = stripslashes($searchtype);
if($pageaction=="update"){

	//Validation
	$oNewsletter_Subscriber->validate($lang['newsletter']['email'],$email,"NotEmpty");
	$oNewsletter_Subscriber->validate($lang['newsletter']['email'],$email,"IsEmail");
	
	if($status_message==""){	
		//Check if email address already exist
		$oNewsletter_Subscriber->data = array("email");
		$oNewsletter_Subscriber->where = "email='$email' and subscriber_id not in ($subscriber_id)";
		$result=$oNewsletter_Subscriber->getList();
		if(mysql_num_rows($result)==0){	
			$oNewsletter_Subscriber->data = array("firstname","lastname","company","position","email","lettertype","status");
			$oNewsletter_Subscriber->value = array(addslashes($firstname),addslashes($lastname),addslashes($company),addslashes($position),$email,$lettertype,$status);
			$oNewsletter_Subscriber->update($subscriber_id);	
			
			$query = "delete from newsletter_subscribe where subscriber_id='$subscriber_id'";
			mysql_query($query,$oNewsletter_Subscriber->db);
			
			for($i=0;$i<count($group_id);$i++){
				$oNewsletter_Subscriber->data = array("group_id","subscriber_id");
				$oNewsletter_Subscriber->value = array($group_id[$i],$subscriber_id);
				$oNewsletter_Subscriber->subscribe();
			}	
			
			$status_message = "<b>".$lang['newsletter']['status']." :</b> ".$lang['newsletter']['subscrupdate']."<br>";
		}else{$status_message = "<b>".$lang['newsletter']['status']." :</b> ".stripslashes($email)." ".$lang['newsletter']['alreadyexist']."<br>";}
	}
}else{
	
	$oNewsletter_Subscriber->data = array("firstname","lastname","company","position","email","lettertype","status");
	$result=$oNewsletter_Subscriber->getDetail($subscriber_id);
	if($myrow=mysql_fetch_row($result)){
		$firstname = stripslashes($myrow[0]);
		$lastname = stripslashes($myrow[1]);
		$company = stripslashes($myrow[2]);
		$position = stripslashes($myrow[3]);
		$email = $myrow[4];
		$lettertype = $myrow[5];
		$status = $myrow[6];
	}
	mysql_free_result($result);
}
if($status=="Active"){ $statusactive="selected"; }
if($status=="Verify"){ $statusverify="selected"; }
if($status=="Suspend"){ $statussuspend="selected"; }
if($lettertype=="HTML"){ $lettertypehtml="selected"; }
if($lettertype=="Text"){ $lettertypetext="selected"; }

?>

<table border=0 width=100%><tr><td><b><? echo $lang['newsletter']['newsletter'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center><tr>
<td><b><u><? echo $lang['newsletter']['addsub'] ?></u></b></td><td align=right>
</td></tr></table><br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action="index.php?component=newsletter&page=wce.subscriberedit.php" method=post>
<input type=hidden name=pageaction value="update">
<input type=hidden name=subscriber_id value="<? echo $subscriber_id ?>">
<input type=hidden name=sortby value="<? echo $sortby ?>">
<input type=hidden name=sortseq value="<? echo $sortseq ?>">
<input type=hidden name=keyword value="<? echo $keyword ?>">
<input type=hidden name=searchtype value="<? echo $searchtype ?>">
<input type=hidden name=vgroup_id value="<? echo $vgroup_id ?>">
<input type=hidden name=start value="<? echo $start ?>">

<tr><td valign=top width=25%><? echo $lang['newsletter']['firstname'] ?></td><td><input type="text" name="firstname" style="width:135px" value="<? echo $firstname ?>"></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['lastname'] ?></td><td><input type="text" name="lastname" style="width:135px" value="<? echo $lastname ?>"></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['company'] ?></td><td><input type="text" name="company" style="width:230px" value="<? echo $company ?>"></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['position'] ?></td><td><input type="text" name="position" style="width:135px" value="<? echo $position ?>"></td></tr>
<tr><td valign=top><? echo $lang['newsletter']['email'] ?></td><td><input type="text" name="email" style="width:230px" value="<? echo $email ?>"> *</td></tr>
<tr><td valign=top><? echo $lang['newsletter']['newsltype'] ?></td><td>
	<select name="lettertype">
		<option value="HTML" <? echo $lettertypehtml ?>>HTML</option>
		<option value="Text" <? echo $lettertypetext ?>><? echo $lang['newsletter']['newsltext'] ?></option>
	</select>
</td></tr>
<tr><td valign=top><? echo $lang['newsletter']['subscrstatus'] ?></td><td>
	<select name="status">
		<option value="Active" <? echo $statusactive ?>><? echo $lang['newsletter']['active'] ?></option>
		<option value="Verify" <? echo $statusverify ?>><? echo $lang['newsletter']['verify'] ?></option>
		<option value="Suspend" <? echo $statussuspend ?>><? echo $lang['newsletter']['suspend'] ?></option>
	</select>
</td></tr>
<tr><td valign=top><? echo $lang['newsletter']['subscrgrp'] ?></td><td>
	<table border=0 cellpadding=0 cellspacing=0>
	<?	
		$subscribed = array();
		$query = "select group_id from newsletter_subscribe where subscriber_id='$subscriber_id'";
		$result1 = mysql_query($query,$oNewsletter_Subscriber->db);
		while($myrow1 = mysql_fetch_row($result1)){	array_push($subscribed,$myrow1[0]);	}
		mysql_free_result($result1);

		$oNewsletter_Group->data = array("group_id","name");
		$oNewsletter_Group->order = "name";
		$result = $oNewsletter_Group->getList();
		$totalgroup = mysql_num_rows($result)-1;
		while($myrow=mysql_fetch_row($result)){
			$myrow[1]=stripslashes($myrow[1]);
			if(in_array($myrow[0],$subscribed)){
				echo "<tr><td><input type=checkbox name=group_id[] value=\"$myrow[0]\" checked></td><td>$myrow[1]</td></tr>";
			}else{
				echo "<tr><td><input type=checkbox name=group_id[] value=\"$myrow[0]\"></td><td>$myrow[1]</td></tr>";
			}				
		}
		mysql_free_result($result);
	?>
	</table>
</td></tr>
<tr><td colspan=2><br>
	<input type=submit value="<? echo $lang['newsletter']['btnupdate'] ?>">
	<input type=button value="<? echo $lang['newsletter']['btndel'] ?>" onclick="<? echo "ConfirmDelete($subscriber_id)"; ?>">
	<input type=button value=" <? echo $lang['newsletter']['btnback'] ?> " onclick="<? echo "window.location='index.php?component=newsletter&page=wce.subscriber.php&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&searchtype=".addslashes($searchtype)."&group_id=$vgroup_id&start=$start'";?>">
</td></tr></form>
	
</td></tr></table>
		
<script language=javascript>
	function ConfirmDelete(id){  
	    if(confirm('<? echo $lang['newsletter']['confirmdel'] ?>')){
			window.location='index.php?component=newsletter&page=wce.subscriber.php&pageaction=delete&delete_id[0]='+id+'<? echo "&sortby=$sortby&sortseq=$sortseq&keyword=$keyword&searchtype=".addslashes($searchtype)."&category_id=$vcategory_id&start=$start"; ?>';
		}
	}	
</script>
