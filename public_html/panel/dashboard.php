<table width="100%" cellpadding="0" cellspacing="0" border="0" style="table-layout:fixed;">
	<col style="width:370px;" />
    <col style="width:370px;" />
    <col style="width:308px;" />
	<tr>
    	<td valign="top">
			<? if($oSystem->getValue('sys_socialmedia_fb')!="") { ?>
				<? 
					$fbclass = new Facebook_class();
					$fbid = $oSystem->getValue('sys_socialmedia_fb');
					$fbuser = "https://graph.facebook.com/{$fbid}";
					$authToken = fetchFBUrl("https://graph.facebook.com/oauth/access_token?client_id=".FACEBOOK_APP_ID."&client_secret=".FACEBOOK_SECRET."");
					$authToken = end(explode("=", $authToken));
					$authToken = urlencode($authToken);
					//var_dump("https://graph.facebook.com/oauth/access_token?client_id=".FACEBOOK_APP_ID."&client_secret=".FACEBOOK_SECRET.""); die();
					$authToken = $fbclass->getAccessToken();
				?>
            	<table class="fb_table">
                    <tr>
                        <td  valign="baseline" class="fb_table_heading">
                            &nbsp;&nbsp;<img src="<?=$path["webroot"].$path["skin"]?>image/fb/facebook.png" />
                            <span>
                            &nbsp;&nbsp;<?=$lang['common']['tw_header']?>
                            </span>
                        </td>
                    </tr>
					<tr>
						<td valign="middle">
							<table class="account_info" style="table-layout:fixed;">
                            	<col style="width:85px;" />
                                <col style="width:auto;" />
								<tr>
									<td align="center">
										<table class="profile_pic">
											<tr>
												<td align="center">
													<span>
														<img src="https://graph.facebook.com/<?=$fbid?>/picture" width="50px" height="50px"/>
													</span>
												</td>
											</tr>
										</table>
									</td>
									<td>
										<p class="username" id="fbusername">leentechsystems</p>
										<p class="fullname" id="fbname">LEENTech Network Solutions</p>
										<p class="profileurl" id="fbprofile">
											<a href="https://www.facebook.com/LEENTechNetworkSolutions" target="_blank">
												https://www.facebook.com/LEENTechNetworkSolutions
											</a>
										</p>
										<script type="text/javascript">
											$(document).ready( function() {
												$.getJSON("https://graph.facebook.com/<?=$fbid?>", 
													function(json) {
													 	$('#fbusername').html(json.username);
													 	$('#fbname').html(json.name);
													 	$('#fbprofile').html('<a href="https://www.facebook.com/'+json.username+'" target="_blank">https://www.facebook.com/'+json.username+'</a>');
													}
												);
											});
										</script>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr>
						<td>
                        			<?
										function formatFacebookUsersInline($content) {
											for($i=0; $i<count($content['data']); $i++) {
												$id = $content['data'][$i]['id'];
												$name = $content['data'][$i]['name'];
												
												$picture = 'https://graph.facebook.com/'.$id.'/picture?type=square'; //square, small, large
												$url = 'http://www.facebook.com/profile.php?id='.$id;
												
												$users[$i]['id'] = $id;
												$users[$i]['name'] = $name;
												$users[$i]['picture'] = $picture;
												$users[$i]['url'] = $url;
												$users[$i]['likes'] = $likes;
											}
											return $users;
										}
										
										function getFacebookFriendsInline($acctoken, $criteria='') {
											$name = $criteria['name'];
											
											if($name=='') $name = 'me';
											
											$url = 'https://graph.facebook.com/'.$name.'/friends?access_token='.$acctoken;
											$content = @file_get_contents($url,0,null,null);
											$content = json_decode($content,true);
											
											$users = formatFacebookUsersInline($content);
											
											return $users;
										}
										
										function displayFriendCountInline($criteria) {
											$users = $criteria['users'];
											$nb_display = $criteria['nb_display'];
											$width = $criteria['width'];
											
											if($width=='') $width="30";
											
											if($nb_display>count($users) || $nb_display=='') $nb_display=count($users); //display value never bigger than nb users
											
											$display = '';
											for($i=0;$i<$nb_display;$i++) {
												$name = $users[$i]['name'];
												$picture = $users[$i]['picture'];
												$url = $users[$i]['url'];
												
												$display .= count($users);
											}
											return $display;
										}
										
										$fb_friends = getFacebookFriendsInline($oSystem->getValue('sys_socialmedia_fb_at'));
										
										$friends_count = "https://graph.facebook.com/fql?q=SELECT%20friend_count%20FROM%20user%20WHERE%20uid=".$fbid;
										$friends_count = @file_get_contents($friends_count);
										$friends_count = json_decode($friends_count,true);
										
										$fb_friends_Count = $friends_count['data'][0]['friend_count'];
										
										
										
										$page_likes="http://graph.facebook.com/".$oSystem->getValue('sys_socialmedia_fb_pg')."/";
										$page_likes = @file_get_contents($page_likes);
										$page_likes = json_decode($page_likes,true);
										if ($page_likes!="") {
											$likes = $page_likes['likes'];
										} else {
											$likes = 0;
										}
										
										$page_shares = "http://graph.facebook.com/".$page_likes['website'];
										$page_shares = @file_get_contents($page_shares);
										$page_shares = json_decode($page_shares,true);
										
										if ($page_shares!="") {
											$shares = $page_shares["shares"];
										} else {
											$shares = 0;
										}
									?>
							<table class="account_info" cellpadding="0" cellspacing="0" style="border:0;">
								<tr>
									<td valign="top" style="padding:4px; width:193px;">
                                    	<table cellpadding="0" cellspacing="0" width="100%" style="table-layout:fixed;">
                                        	<tr>
                                            	<td>
                                                	<p><a href="<?=$page_likes['link']?>" target="_blank"><?=$page_likes['name']?></a></p>
                                                   
                                                    <span class="description">
                                                        <?=((strlen($page_likes['company_overview'])<=103)?$page_likes['company_overview']:substr($page_likes['company_overview'],0 ,100)."...")?>
                                                        <ul class="FBpages">
                                                        <?	
                                                            /*
                                                            $graph_url = 'https://graph.facebook.com/'.$fbid.'/accounts?access_token='.$oSystem->getValue('sys_socialmedia_fb_at');
                                                            $accounts = json_decode(file_get_contents($graph_url));
                                                            //var_dump($graph_url); die();
                                                            if ($accounts->data) {
                                                                foreach($accounts->data as $account) {
                                                                    ?><li><a href="javascript:void(0)" onclick="displayInfo(<?=$account->id?>)"><?=$account->name?></a></li><?
                                                                }
                                                            }
                                                            */
                                                            
                                                        ?>
                                                        </ul>
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
									</td>
									<td  class="account_status" valign="top">
										<table class="status_info row1" cellpadding="0" cellspacing="0">
											<tr>
												<td class="status_info_a">Friends</td>
												<td class="status_info_b"><?=$fb_friends_Count?></td>
											</tr>
										</table>
										<table class="status_info row2" cellpadding="0" cellspacing="0">
											<tr>
												<td class="status_info_a">Likes</td>
												<td class="status_info_b"><?=$likes?></td>
											</tr>
										</table>
										<table class="status_info row3" cellpadding="0" cellspacing="0">
											<tr>
												<td class="status_info_a">Shares</td>
												<td class="status_info_b"><?=$shares?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
                </table>
			<? } ?>
		</td>
    
		<td valign="top">
			
			
			<? 
				if($oSystem->getValue('sys_socialmedia_tw')!="") { 
					$twscreenname = $oSystem->getValue('sys_socialmedia_tw');
					
					
					
					$twitterConnection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oSystem->getValue('sys_socialmedia_tw_access_token'), $oSystem->getValue('sys_socialmedia_tw_access_secret'));
					
					$twObj = $twitterConnection->get('users/show',array('screen_name' => $twscreenname));
					
					
					//$twuser = "https://api.twitter.com/1.1/users/show.xml?screen_name={$twscreenname}";
					//$twxml = simplexml_load_file($twuser);
					?>
                    <table class="tw_table">
                        <tr>
                            <td  valign="baseline" class="tw_table_heading">
                                &nbsp;&nbsp;<img src="<?=$path["webroot"].$path["skin"]?>image/tw/twitter.png" />
                                <span>
                                &nbsp;&nbsp;<?=$lang['common']['tw_header']?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle">
                                <table class="account_info"  style="table-layout:fixed;">
                                	<col style="width:85px;" />
                                	<col style="width:auto;" />
                                    <tr>
                                        <td align="center">
                                            <table class="profile_pic">
                                                <tr>
                                                    <td align="center">
                                                        <span>
                                                            <img src="<?=$twObj->profile_image_url?>" width="50px" height="50px"/>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <p class="username">@<?=$twObj->screen_name?></p>
                                            <p class="fullname"><?=$twObj->name?></p>
                                            <p class="profileurl">
                                                <a href="http://twitter.com/<?=$twObj->screen_name?>" target="_blank">
                                                    http://twitter.com/<?=$twObj->screen_name?>
                                                </a>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table class="account_info" cellpadding="0" cellspacing="0" style="border:0;">
                                    <tr>
                                        <td valign="top" style="padding:4px;">
                                            <span class="description">
                                                <?=$twObj->description?>
                                            </span>
                                        </td>
                                        <td  class="account_status" valign="top">
                                            <table class="status_info row1" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="status_info_a"><?=$lang['common']['tw_stat_followers']?></td>
                                                    <td class="status_info_b"><?=$twObj->followers_count?></td>
                                                </tr>
                                            </table>
                                            <table class="status_info row2" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="status_info_a"><?=$lang['common']['tw_stat_friends']?></td>
                                                    <td class="status_info_b"><?=$twObj->friends_count?></td>
                                                </tr>
                                            </table>
                                            <table class="status_info row3" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td class="status_info_a"><?=$lang['common']['tw_stat_status']?></td>
                                                    <td class="status_info_b"><?=$twObj->statuses_count?></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
					<?	
			 	} 
			?>
			
		</td>
        
        <td valign="top">
        		<table class="na_table">
                    <tr>
                        <td  valign="baseline" class="na_table_heading">
                            &nbsp;&nbsp;<img src="<?=$path["webroot"].$path["skin"]?>image/na/newsandannouncement.png" />
                            <span>
                            &nbsp;&nbsp;Announcement
                            </span>
                        </td>
                    </tr>
					<tr>
						<td valign="top">
							<table class="account_info" style="table-layout:fixed;">
                            	<tr>
									<td align="left" valign="top">
                                    	<?
										$lnsannouncement = "http://www.leentechsystems.com/demo/panel/rss/";
										$lnsxml = simplexml_load_file($lnsannouncement);
										$lnsxml = $lnsxml->channel;
										
										if ($lnsxml) {
											$lnsxml_count = 1;
											foreach($lnsxml->item as $item){
												if ($lnsxml_count <= 1) {
												?>
                                                	<h3 style="color:#126100; margin-top:0; margin-bottom:5px;"><?=stripslashes($item->title)?></h3>
													<p style="margin:0;"><?=$item->datepost?></p>
                                                    <br />
                                                    <p  style="margin:0;">
                                                    	<?=stripslashes($item->description)?>
                                                    </p>
                                                <?
													$lnsxml_count++;
												}
											}
										}
										?>
										
								  </td>
								</tr>
							</table>
						</td>
					</tr>
                </table>
        </td>
		
	</tr>
</table>
<br />
<? if($oSystem->getValue('sys_wtstats')!="") { ?>
<iframe src="http://wtstats.leentechsystems.com/frame.php?a=stats&u=<?=$oSystem->getValue('sys_wtstats')?>" width="100%" height="750" frameborder="0" align="middle" allowtransparency="true" class="scrollbar"></iframe>
<script type="text/javascript" src="<?=$path["webroot"]?>common/scripts/jquery.scroll.js"></script>
<script type="text/javascript">
	$(document).ready( function() {
		$('.scrollbar').scrollbar();
	});
</script>
<? } ?>