<?



class Photo_Gallery extends Item{
	
	var $table="photo_gallery",$primarykey="photo_id",$shortname="photogallery",$version="4.1",$gdversion="20";

	function Photo_Gallery(){}

	function getList(){
		$this->getdatalist();	$this->getwherelist();	$this->getorderlist();
		$sql="select $this->datalist from photo_gallery,photo_category $this->wherelist $this->orderlist";
		$result=mysql_query($sql,$this->db);
		return $result;			
	}
	
	function getListAccess(){
		$this->getdatalist();	$this->getwherelist();	$this->getorderlist();
		$sql="select $this->datalist from photo_gallery,photo_category,photo_access $this->wherelist $this->orderlist";
		$result=mysql_query($sql,$this->db);
		return $result;			
	}
		
	function delete($id){		
		$sql="delete from $this->table where $this->primarykey='$id'";
		mysql_query($sql,$this->db);

		$this->logEvent("Delete");		
	}
	
	function addRating($photo_id,$rating){
		$sql="insert into photo_rating(photo_id,rating) values('$photo_id','$rating')";
		mysql_query($sql,$this->db);		
	}
	
	function getRating($photo_id){
		$sql="select rating from photo_rating where photo_id='$photo_id'";
		$result=mysql_query($sql,$this->db);
		if(mysql_num_rows($result)!=0){	
			$count=0;
			while($myrow=mysql_fetch_row($result)){
				$count++;
				$total = $total + $myrow[0];
			}
			mysql_free_result($result);	
			$rating = $total/$count;
			return round($rating,1);
		}else{
			return 0;
		}
	}
	
	function resizeImage($src_file,$dest_file,$thumbw,$thumbh,$quality){

    	$ext = strrchr($src_file,".");
    	if($ext==".jpg"){
			if($src_img = @imagecreatefromjpeg($src_file)){
				$dst_img = $this->image_resize($src_img, $thumbw, $thumbh); 
				@imagejpeg($dst_img, $dest_file, $quality);   
				@imagedestroy($src_img);
				@imagedestroy($dst_img);
			}
		}else if($ext==".gif"){
			if($src_img = @imagecreatefromgif($src_file)){
				$dst_img = $this->image_resize($src_img, $thumbw, $thumbh); 
				@imagegif($dst_img, $dest_file, $quality);   
				@imagedestroy($src_img);
				@imagedestroy($dst_img);
			}		
		}

		@chmod($dest_file,0777);		
	}
	
	function copyrightImage($src_file,$text,$fontsize,$x,$y,$red,$green,$blue){
	
    	$ext = strrchr($src_file,".");
    	if($ext==".jpg"){
			if($src_img = @imagecreatefromjpeg($src_file)){
				@imagestring($src_img, $fontsize, $x, $y, $text, @imagecolorallocate($src_img, $red,$green,$blue));
				@imagejpeg($src_img,$src_file,100);
			}
		}else if($ext==".gif"){
			if($src_img = @imagecreatefromgif($src_file)){
				@imagestring($src_img, $fontsize, $x, $y, $text, @imagecolorallocate($src_img, $red,$green,$blue));
				@imagegif($src_img,$src_file,100);
			}
		}			
	}		

	function image_resize($im, $maxWidth, $maxHeight) {  
	    $maxAspect = $maxWidth / $maxHeight; 
	    $origWidth = imagesx($im); 
	    $origHeight = imagesy($im); 
	    $origAspect = $origWidth / $origHeight; 
        if ($origAspect <= $maxAspect) { 
            $newWidth = $maxHeight * $origAspect; 
            $newHeight = $maxHeight;  
        } else { 
            $newWidth = $maxWidth; 
            $newHeight = $maxWidth / $origAspect;  
        }  
        
		if($this->gdversion=="20"){
        $om = @imagecreatetruecolor($newWidth, $newHeight);
        @imagecopyresampled($om, $im, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);        
        }
        if($this->gdversion=="16"){
        $om = @imagecreate($newWidth, $newHeight);
        @imagecopyresized($om, $im, 0, 0, 0, 0, $newWidth, $newHeight, $origWidth, $origHeight);
        }
        
        return($om); 
	}

	function getLastSequence(){
		$sql="select sequence from $this->table order by sequence desc limit 0,1";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}
	
	function IsPicture($file_name){
    	$ext = strtolower(strrchr($file_name,"."));
    	if($ext==".jpg" || $ext==".png" || $ext==".gif" || $ext==".jpeg"){
			return true;				
		}
		else{
			return false;
		}		
	}

	function deleteDir($dir) {
	   if (substr($dir, strlen($dir)-1, 1) != '/')
	       $dir .= '/';
	
	   if ($handle = opendir($dir)) {
	       while ($obj = readdir($handle)) {
	           if ($obj != '.' && $obj != '..') {
	               if (is_dir($dir.$obj)) {
	                   if (!deleteDir($dir.$obj))
	                       return false;
	               } elseif (is_file($dir.$obj)) {
	                   if (!unlink($dir.$obj))
	                       return false;
	               }
	           }
	       }
	       closedir($handle);
	
	       if (!@rmdir($dir))
	           return false;
	       return true;
	   }
	   return false;
	}
	
	function gdwatermark($srcfilename, $watermark) {
	    global $oSystem;
	
	    $imageInfo = getimagesize($srcfilename);
	    $width = $imageInfo[0];
	    $height = $imageInfo[1];
	
	    $logoinfo = getimagesize($watermark);
	    $logowidth = $logoinfo[0];
	    $logoheight = $logoinfo[1];
	
	    if( $oSystem->getValue("photo_wmlocation") == "middle" ) {
	        $horizmargin = ( $width / 2 ) - ( $logowidth / 2 );
	        $vertmargin = ( $height / 2 ) - ( $logoheight / 2 );
	    }
	    elseif( $oSystem->getValue("photo_wmlocation") == "topleft" ) {
	        // top left
	        $horizmargin = 0;
	        $vertmargin = 0;
	    }
	    elseif( $oSystem->getValue("photo_wmlocation") == "topright" ) {
	        // top right
	        $horizmargin = $width - $logowidth;
	        $vertmargin = 0;
	    }
	    elseif( $oSystem->getValue("photo_wmlocation") == "bottomleft" ) {
	        // bottom left
	        $horizmargin = 0;
	        $vertmargin = $height - $logoheight;
	    }    
	    elseif( $oSystem->getValue("photo_wmlocation") == "middletop" ) {
	        // top middle        
	        $horizmargin = ( ( $width - $logowidth ) / 2 );
	        $vertmargin = 0;
	    }
	    elseif( $oSystem->getValue("photo_wmlocation") == "middleright" ) {
	        // middle right
	        $horizmargin = $width - $logowidth;
	        $vertmargin = ( $height / 2 ) - ( $logoheight / 2 );
	    }    
	    elseif( $oSystem->getValue("photo_wmlocation") == "bottommiddle" ) {
	        // bottom middle        
	        $horizmargin = ( ( $width - $logowidth ) / 2 );
	        $vertmargin = $height - $logoheight;
	    }
	    elseif( $oSystem->getValue("photo_wmlocation") == "middleleft" ) {
	        // middle left
	        $horizmargin = 0;
	        $vertmargin = ( $height / 2 ) - ( $logoheight / 2 );
	    } 
	    elseif ( $oSystem->getValue("photo_wmlocation") == "bottomright" ) {
	        // bottom right
	        $horizmargin = $width - $logowidth;
	        $vertmargin = $height - $logoheight;
	    }
	
	    $photoImage = ImageCreateFromJPEG($srcfilename);
	    ImageAlphaBlending($photoImage, true);
	
	    $logoImage = ImageCreateFromPNG($watermark);
	    $logoW = ImageSX($logoImage);
	    $logoH = ImageSY($logoImage);
	
	    ImageCopy($photoImage, $logoImage, $horizmargin, $vertmargin, 0, 0, $logoW, $logoH);
	    ImageJPEG($photoImage, $srcfilename, 90);
	
	    ImageDestroy($photoImage);
	    ImageDestroy($logoImage);
	}
	
	function createWaterMark( $filepath, $cat=500) {
	    global $oSystem,$path;
				
	    if ( $oSystem->getValue("photo_watermark") != "" && $oSystem->getValue("photo_gd") == "20") { 
		    $water_image = $path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark");
		     if ( strstr(strtolower($filepath),".jpg") ) {
	            $retval = $this->gdwatermark( $filepath, $water_image );
	        }
		}
	    return;
	}
	
	function frontendurl($link) {
		if($link) {
			$file = @fopen ("$link", "r");
	    }
	    if($file) {
	    	return true ;
	        @fclose($file);
	    }
	}
}

?>