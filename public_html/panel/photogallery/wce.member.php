<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:46 28.12.2006                  */
/**************************************************/


switch($pageaction){
	case "add":
		if(!$oPhoto_Member->userExists($username)){	
			$oPhoto_Member->data = array("name","email","username","password");
			$oPhoto_Member->value = array($name,$email,$username,$password);
			$oPhoto_Member->add();		
			$lastID = $oPhoto_Member->getLastID();
	
			for($i=0;$i<count($category_id);$i++){	$oPhoto_Member->addAccess($lastID,$category_id[$i]);	}
			$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['memberadded']."<br>";		
		}else{
			$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['userexist']."<br>";	
		}
		$name=""; $email=""; $username=""; $password="";
	break;
	
	case "edit":
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['editmember']."<br>";		
	break;
	
	case "update":
		$oPhoto_Member->data = array("name","email","username","password");
		$oPhoto_Member->value = array($name,$email,$username,$password);
		$oPhoto_Member->update($member_id);
		$name=""; $email=""; $username=""; $password="";
	
		$oPhoto_Member->deleteAccess($member_id);
		for($i=0;$i<count($category_id);$i++){	$oPhoto_Member->addAccess($member_id,$category_id[$i]);	}
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['memberupdated']."<br>";
	break;
	
	case "delete":
		for($i=0;$i<count($delete_id);$i++){
			$member_id=$delete_id[$i];
			$oPhoto_Member->delete($member_id);
		}
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['memberdeleted']."<br>";	  
	break;
		
}

if(!empty($member_id)){
	$oPhoto_Member->data = array("name","email","username","password");
	$result=$oPhoto_Member->getDetail($member_id);
	if($myrow=mysql_fetch_row($result)){
		$vmember_id=$member_id;
		$vname=stripslashes(htmlentities($myrow[0]));
		$vemail=stripslashes(htmlentities($myrow[1]));
		$vusername=stripslashes(htmlentities($myrow[2]));
		$vpassword=stripslashes(htmlentities($myrow[3]));
	}
		
	$access = array();
	$result=$oPhoto_Member->getAccess($member_id);
	while($myrow=mysql_fetch_row($result)){	array_push($access,$myrow[0]);	}
	mysql_free_result($result);	
	$oPhoto_Category->access = $access;	
}

?>

<table border=0 width=100%><tr><td><b><? echo $lang['photogallery']['photogallery'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name="thisform" action="index.php?component=photogallery&page=wce.member.php" method="post">
<input type="hidden" name="pageaction">
<input type=hidden name="member_id" value="<? echo $vmember_id ?>">

<tr><td width=25%><? echo $lang['photogallery']['name'] ?></td><td><input type=text name=name style="width:230px" value="<? echo $vname ?>"> *</td></tr>
<tr><td><? echo $lang['photogallery']['email'] ?></td><td><input type=text name=email style="width:230px" value="<? echo $vemail ?>"></td></tr>
<tr><td><? echo $lang['photogallery']['username'] ?></td><td><input type=text name=username style="width:95px" value="<? echo $vusername ?>"> *</td></tr>
<tr><td><? echo $lang['photogallery']['password'] ?></td><td><input type=text name=password style="width:95px" value="<? echo $vpassword ?>"> *</td></tr>
<tr><td valign=top><? echo $lang['photogallery']['setuseraccess'] ?></td><td><select multiple name=category_id[] size=8 style="width:70%"><?	$oPhoto_Category->getCategoryAccess(0); ?></select></td></tr>
<tr><td valign=top></td><td>
	<? if($pageaction=="edit" || $pageaction=="update" || $pageaction=="remove"){ ?>
		<input type=button value="<? echo $lang['photogallery']['btnupdate'] ?>" onclick="document.thisform.pageaction.value='update';validate()">
		<input type=button value=" <? echo $lang['photogallery']['btnback'] ?> " onclick="window.location='index.php?component=photogallery&page=wce.member.php'">
	<? }else{ ?>
		<input type=button value="  <? echo $lang['photogallery']['add'] ?>  " onclick="document.thisform.pageaction.value='add';validate()">
	<? } ?>	
</td></tr></form></table><br>

<?
	$sort=array("member_id","name","email","username");

	if($sortby==""){ $sortby=$sort[0]; } 
	if($sortseq==""){ $sortseq="desc"; }
	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sort[$i]&sortseq=asc&product=$product><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort In Ascending Order\"></a>";	}
	for($i=0;$i<count($sort);$i++){
		if($sortby==$sort[$i]){
		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=desc&product=$product><img src=\"common/image/sort_desc.gif\" border=0 alt=\"Sort In Descending Order\"></a>"; }
		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=asc&product=$product><img src=\"common/image/sort_asc.gif\" border=0 alt=\"Sort In Ascending Order\"></a>"; }
	}}

	$listno = $oSystem->getValue("photo_listno");	
	$oPhoto_Member->data = array("member_id");
	if($name!=""){ $oPhoto_Member->where="name='$name'"; }
	else if($email!=""){ $oPhoto_Member->where="email='$email'"; }
	else if($username!=""){ $oPhoto_Member->where="username='$username'"; }
	else{ $oPhoto_Member->where=""; }	
	$result=$oPhoto_Member->getList();
	if(mysql_num_rows($result)!=0){	$total=mysql_num_rows($result); }else{ $total=0; }	
	
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$listno; $next=$start+$listno;	$from=$start+1; $to=$listno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }
	if($prev>=0){ $prevlink="<a href=\"index.php?component=photogallery&page=wce.member.php&start=$prev&name=$name&email=$email&username=$username&sortby=$sortby&sortseq=$sortseq\">".$lang['photogallery']['prev']."</a>"; }else{ $prevlink=""; }
	if($next<$total){ $nextlink="<a href=\"index.php?component=photogallery&page=wce.member.php&start=$next&name=$name&email=$email&username=$username&sortby=$sortby&sortseq=$sortseq\">".$lang['photogallery']['next']."</a>"; }else{ $nextlink=""; }
	echo "
	<table border=0 cellpadding=5 cellspacing=0 width=98% align=center>
	<tr><td width=20%><a href=\"javascript:ConfirmDelete()\">".$lang['photogallery']['delseleted']."</a></td><td align=center>".$lang['photogallery']['showing']." $from ".$lang['photogallery']['to']." $to ".$lang['photogallery']['of']." $total</td><td width=20% align=right>$prevlink &nbsp;&nbsp; $nextlink</td></tr>
	</table>

	<table border=0 cellpadding=1 cellspacing=0 width=98% align=center>
		<form name=frmList action=\"index.php?component=$component&page=$page\" method=post>
		<input type=hidden name=pageaction value=\"delete\">
		<input type=hidden name=sortby value=\"$sortby\">
		<input type=hidden name=sortseq value=\"$sortseq\">
		<input type=hidden name=name value=\"$name\">
		<input type=hidden name=email value=\"$email\">
		<input type=hidden name=username value=\"$username\">
		<input type=hidden name=start value=\"$start\">
		
	    <tr bgcolor=\"#E6E6E6\" height=\"24\">
	      <td class=\"gridTitle\" align=\"center\" width=\"25\"><input type=checkbox id=\"checkAll\" onclick=\"selectAll()\"></td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['photogallery']['name']."</b> $sortlink[1]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['photogallery']['email']."</b> $sortlink[2]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"130\">&nbsp;<b>".$lang['photogallery']['username']."</b> $sortlink[3]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"30\">&nbsp;</td>
	    </tr> 
	";
	   
   	$oPhoto_Member->data = array("member_id","name","email","username");
	if($name!=""){ $oPhoto_Member->where="name='$name'"; }
	else if($email!=""){ $oPhoto_Member->where="email='$email'"; }
	else if($username!=""){ $oPhoto_Member->where="username='$username'"; }
	else{ $oPhoto_Member->where=""; }	
   	$oPhoto_Member->order = "$sortby $sortseq limit $start,$listno";	    
   	$result=$oPhoto_Member->getList();	   	
   	$count=0;
   	while($myrow=mysql_fetch_row($result)){
   		$count++;		
   		echo "
	    <tr id=\"row$count\" style=\"background:#F6F6F6\" height=\"24\">
	      <td class=\"gridRow\" align=\"center\"><input type=checkbox id=\"check$count\" name=\"delete_id[]\" value=\"$myrow[0]\" onclick=\"if(this.checked==true){ selectRow('row$count'); }else{ deselectRow('row$count'); }\"></td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[1]&nbsp;</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[2]&nbsp;</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[3]&nbsp;</td>
   		  <td class=\"gridRow\" align=\"center\"><a href=\"index.php?component=photogallery&page=wce.member.php&pageaction=edit&member_id=$myrow[0]&sortby=$sortby&sortseq=$sortseq&vproduct=$product&start=$start\"><img src=\"common/image/ico_edit.gif\" border=0 alt=\"".$lang['photogallery']['editrecord']."\"></a></td>
	   	</tr>";
   	}
   	mysql_free_result($result);
	
	echo "</form></table>";
	
?>

<script language=javascript>

	function ConfirmDelete(){  
	    if(confirm('<? echo $lang['photogallery']['confirmdel'] ?>')){
	    	document.frmList.submit();
		}
	}
	
	function selectAll(){
		if(document.getElementById("checkAll").checked==true){
			document.getElementById("checkAll").checked=true;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=true; document.getElementById(\"row$i\").style.background='#D6DEEC'; \n"; }	?>
		}else{
			document.getElementById("checkAll").checked=false;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=false; document.getElementById(\"row$i\").style.background='#F6F6F6'; \n"; } ?>		
		}
	}

	function selectRow(row){
		document.getElementById(row).style.background="#D6DEEC";
	}

	function deselectRow(row){
		document.getElementById(row).style.background="#F6F6F6";
	}
	
	function validate(){
		if(document.thisform.name.value==""){
			alert("<? echo $lang['photogallery']['plsmembername'] ?>"); document.thisform.name.select();  return false;
		}else if(document.thisform.username.value==""){
			alert("<? echo $lang['photogallery']['plsmemberusername'] ?>"); document.thisform.username.select();  return false;
		}else if(document.thisform.password.value==""){
			alert("<? echo $lang['photogallery']['plsmemberpassword'] ?>"); document.thisform.password.select();  return false;
		}else{
			document.thisform.submit();
		}	
	}	
</script>

<!--Body End-->