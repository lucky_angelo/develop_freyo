<?php

echo "<script language=\"javascript\" src=\"".$path["webroot"]."common/lib.jsfunction.php\"></script>";
$thisfile=$oSystem->getValue("photo_page");
$sefurl = $oSystem->getValue("photo_sefurl");
if($parent_id==""){ $parent_id=0; }
if($oSystem->getValue("photo_submitphoto")=="Yes"){ $submitphoto="<a href=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&pageaction=submitphoto&start=$start\"><b>".$lang['photogallery']['hsubmityour']."</b></a>"; }else{$submitphoto="&nbsp;";}
$localtz = $oSystem->getValue("sys_timezone");
$localtime = $localtz==""?time():getLocalTime($localtz);
	
/*****Photo Gallery Thumbnails*****/
if($photo_id=="" && $pageaction!="searchphoto" && $pageaction!="submitphoto" && $pageaction!="submitphotook" && $pageaction!="editinfo" && $pageaction!="editinfook" && $pageaction!="forgetpass" && $pageaction!="forgetpassok"){

	$photo_seq = $oSystem->getValue("photo_seq");	
	$photo_rowno = $oSystem->getValue("photo_rowno");	
	$photo_pageno = $oSystem->getValue("photo_pageno");

	$oPhoto_Gallery->data = array("distinct photo_id","title","thumbnail","newphoto","photo","photo_gallery.category_id");
	if($category_id!="" && $category_id!=0){ 
		$oPhoto_Gallery->where = "photo_gallery.category_id='$category_id' and photo_gallery.category_id=photo_category.category_id and showphoto='Yes'";
	}else{
		$oPhoto_Gallery->where = "(photo_gallery.category_id=photo_category.category_id) and (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid' or showprivate!='Yes') and showphoto='Yes' AND newphoto='Yes'";
	}
	$result = $oPhoto_Gallery->getListAccess();
	$total = mysql_num_rows($result);
		
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$photo_pageno; $next=$start+$photo_pageno;	$from=$start+1; $to=$photo_pageno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }

	if($parent_id!="0"){	
		$subcat_list=$oSystem->getValue("photo_subcatno");
		$val_width=100/$subcat_list."%";
		$toptitle.="<table border=0 cellpadding=2 cellspacing=2 width=98% align=center valign=\"top\"><tr>";
		
		$countline=0;
		$oPhoto_Category->data=array("distinct photo_category.category_id","photo_category.name","photo_category.descp");
		$resultsub=$oPhoto_Category->getSubCategory($category_id,$cookie_photomemberid);
		while($myrowsub=mysql_fetch_row($resultsub)){
			$myrowsub[1]=stripslashes($myrowsub[1]);	
			if ($oSystem->getValue("photo_showcatdescp")=="Yes"){ $myrowsub[2]=stripslashes(nl2br($myrowsub[2])); } else { $myrowsub[2]=""; }
			
			if ($sefurl == "Yes"){
				$toptitle.="<td valign=top width=\"$val_width\"><a href=\"cat-$myrowsub[0]-$myrowsub[0].html\">$myrowsub[1]&nbsp;</a><br><font size=1>$myrowsub[2]</font></td>";
			}else{
				$toptitle.="<td valign=top width=\"$val_width\"><a href=\"".$thisfile."?category_id=$myrowsub[0]&parent_id=$myrowsub[0]\">$myrowsub[1]&nbsp;</a><br><font size=1>$myrowsub[2]</font></td>";
			}
			
			$countline++; if($countline>$subcat_list-1){ $toptitle.="</tr><tr>"; $countline=0; }
		}mysql_free_result($resultsub);
		$toptitle.="</tr></table>";
	}	
	
	/*****Category Tree*****/
	$oPhoto_Category->getCategoryLink($category_id);
	$CategoryLink = ($CategoryLink!=""?"&nbsp;>&nbsp;<b>$CategoryLink</b>":"");
		
	/*****Previous and Next Navigation. Modify To Match Your Display*****/
	if($prev>=0){
		if ($sefurl == "Yes"){
			$prevlink="<a href=\"navigate-$category_id-$parent_id-$prev.html\"><b>".$lang['photogallery']['prev']."</b></a>";  
		}else{
			$prevlink="<a href=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&start=$prev\"><b>".$lang['photogallery']['prev']."</b></a>";  
		}	
	}
	if($next<$total){
		if ($sefurl == "Yes"){
			$nextlink="<a href=\"navigate-$category_id-$parent_id-$next.html\"><b>".$lang['photogallery']['next']."</b></a>"; 
		}else{
			$nextlink="<a href=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&start=$next\"><b>".$lang['photogallery']['next']."</b></a>"; 
		}
	}
	if($prevlink!="" && $nextlink!=""){		$navline="&nbsp;&nbsp;"; 	}

	echo "<table border=0 width=98% cellpadding=5 align=center><tr><td><a href=\"$thisfile\">".$lang['photogallery']['hbacktop']."</a>$CategoryLink</td><td align=right width=25%>$submitphoto</td></tr></table>$toptitle<hr size=1 color=#000000 width=98%>";
	
	$oPhoto_Gallery->order = "photo_gallery.sequence $photo_seq limit $start,$photo_pageno";
	$result = $oPhoto_Gallery->getListAccess();
	if(mysql_num_rows($result)!=0){
		$colwidth = (int)(100/$photo_rowno)."%";	$count=0;
		echo "<table border=0 width=98% align=center><tr><td width=33%><b>".$lang['photogallery']['showing']." ".$from." - ".$to." ".$lang['photogallery']['of']." ".$total."</b></td><td align=right><b>$prevlink $navline $nextlink</b></td></tr></table>";
		echo "<table border=0 cellpadding=5 width=\"98%\"><tr>";
		while($myrow=mysql_fetch_row($result)){
			if($myrow[1]!=""){	$title=stripslashes($myrow[1]); $caption=$myrow[1];	}else{ $title="";	$caption=""; }		
			if($myrow[2]!=""){	
				$thumbnail="<img src=\"".$path["webroot"]."_files/photogallery/$myrow[2]\" border=0 alt=\"$myrow[1]\">";	
			}else{
				$thumbnail = "<img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" alt=\"".$lang['photogallery']['havailable']."\" border=0>";
			}
			if($myrow[3]=="Yes"){	$newphotoico="<img src=\"".$path["webroot"]."photogallery/image/new.gif\" border=0>";	}else{ $newphotoico=""; }
	
			/*****Modify The Thumbnail Design Here*****/				
			if($oSystem->getValue("photo_popup")!="Yes"){
				echo "<td valign=top align=center width=\"$colwidth\">";
				echo $sefurl=="Yes"?"<a href=\"photo-$myrow[5]-$myrow[5]-$myrow[0]-$start-Yes.html\">$thumbnail<br>$title $newphotoico<br></a>":"<a href=\"".$thisfile."?category_id=$myrow[5]&parent_id=$myrow[5]&photo_id=$myrow[0]&countdisplay=$countdisplay&start=$start\">$thumbnail<br>$title $newphotoico<br></a>";
				echo "</td>";
			}else{
				echo "<td valign=top align=center width=\"$colwidth\"><a href=javascript:popUpImg(\"$myrow[0]\");>$thumbnail<br>$title $newphotoico<br></a></td>";
			}
			$count++;
			if($count>$photo_rowno-1){ echo "</tr><tr>"; $count=0; }
		}mysql_free_result($result);	
	}
	
	echo "</tr></table><br>";
}


/*****View Photo Details*****/
if($photo_id!="" && $pageaction!="submitphoto" && $pageaction!="submitphotook"){

	if ($pageaction=="slideshowplay") {				
		echo"
		<table border=0 cellpadding=0 cellspacing=0 width=98% align=center valign=\"top\" height=\"800\">
		<tr><td valign=top>
			<iframe src =\"".$path["webroot"]."photogallery/home.slidshow.php?category_id=$category_id&parent_id=$parent_id&photo_id=$photo_id&start=$start\" width=\"100%\" height=\"100%\" frameborder=0 scrolling=Auto valign=top></iframe>
		</td></tr></table>
		";
	} else {
		if ($countdisplay!="No"){
			/*****Update Photo Hits*****/
			$oPhoto_Gallery->data = array("hit");
			$result=$oPhoto_Gallery->getDetail($photo_id);
			if($myrow=mysql_fetch_row($result)){ $newhit=$myrow[0]+1; }
			mysql_free_result($result);
			$oPhoto_Gallery->data=array("hit");
			$oPhoto_Gallery->value=array($newhit);
			$oPhoto_Gallery->update($photo_id);
		}
		
		/*****Rate The Photo*****/
		if($pageaction=="rate"){
			if($oSystem->getValue("photo_limitrating")=="Yes"){
				$var = "cookie_photorated$photo_id";		
				if($$var!="Yes"){	$oPhoto_Gallery->addRating($photo_id,$rating);	}else{	$msgrated = "<font color=ff0000>".$lang['photogallery']['halreadyrated']."</font>";	}
				echo "<script>setcookietime(\"cookie_photorated$photo_id\",\"Yes\",\"1\");</script>";
			}else{
				$oPhoto_Gallery->addRating($photo_id,$rating);	
			}
		}
		
		/*****Comment The Photo*****/
		if($pageaction=="comment"){
			$date_poster =  date("Y-m-d",$localtime);
			if ($oSystem->getValue("photo_commentapprove")=="Yes"){
				$emailadmin = $oUser->getAdminEmail();
				$photo_adminemail = $oSystem->getValue("photo_adminemail");
				if(empty($emailadmin)){ $status_message="<font color=\"FF0000\">".$lang['photogallery']['emailfail']."</font><br>"; }
				if(empty($photo_adminemail)){ $status_message.="<font color=\"FF0000\">".$lang['photogallery']['adminemailfail']."</font><br>"; }

				if ($status_message==""){	
					$oPhoto_Gallery->data = array("category_id","title");
					$result=$oPhoto_Gallery->getDetail($photo_id);
					if($myrow=mysql_fetch_row($result)){
						$photoname = stripslashes($myrow[1]);
						
						$oPhoto_Category->data = array("name");
						$result2=$oPhoto_Category->getDetail($myrow[0]);
						if($myrow2=mysql_fetch_row($result2)){
							$categoryname=stripslashes($myrow2[0]);
						}
						mysql_free_result($result2);
					}
					mysql_free_result($result);
					
					$lblcomment="<br>".stripslashes(nl2br($comment));
					$subject = stripslashes($oSystem->getValue("photo_commentsubject"));	
					$body = stripslashes(nl2br($oSystem->getValue("photo_commentbody")));
					$body = str_replace("[[photoid]]",$photo_id,$body);
					$body = str_replace("[[photo]]",$photoname,$body);
					$body = str_replace("[[category]]",$categoryname,$body);
					$body = str_replace("[[datepost]]",$date_poster,$body);
					$body = str_replace("[[author]]",stripslashes($name),$body);
					$body = str_replace("[[comment]]",$lblcomment,$body);
					$oSystem->mail($photo_adminemail,$subject,$body,$emailadmin,"HTML");
				}
				$status="Pending";
				$status_comment= $status_message."".$lang['photogallery']['hthankcomment']."<br><br>";
			} else {
				$status="Active";
				$status_comment=$lang['photogallery']['hthankcomment2']."<br><br>";
			}
			
			$oPhoto_Review->data = array("photo_id","date_poster,author,reviewcontent","status");
			$oPhoto_Review->value = array($photo_id,$date_poster,addslashes($name),addslashes($comment),$status);
			$oPhoto_Review->add();
		}
		
		echo"<table border=0 cellpadding=0 cellspacing=0 width=98% align=center valign=\"top\"><tr><td valign=\"top\">";	
		
		/*****Category Tree*****/
		$oPhoto_Category->getCategoryLink($category_id);
		$CategoryLink = ($CategoryLink!=""?">&nbsp;<b>".$CategoryLink."</b>":"");
			
		$photo_seq = $oSystem->getValue("photo_seq");
		$oPhoto_Gallery->data=array("photo_id");
		$oPhoto_Gallery->data = array("distinct photo_id","title","thumbnail","newphoto","photo","photo_gallery.category_id");
		if($category_id!="" && $category_id!=0){ 
			$oPhoto_Gallery->where = "photo_gallery.category_id='$category_id' and photo_gallery.category_id=photo_category.category_id and showphoto='Yes'";
		}else{
			$oPhoto_Gallery->where = "(photo_gallery.category_id=photo_category.category_id) and (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid' or showprivate!='Yes') and showphoto='Yes'";
		}
		$oPhoto_Gallery->order = "photo_gallery.sequence $photo_seq";	
		$result=$oPhoto_Gallery->getListAccess();	
		
		$count=0; while($myrow=mysql_fetch_row($result)){ if($myrow[0]==$photo_id){ $curpos=$count; }	$count++; }		
		$prevpos=$curpos-1; $nextpos=$curpos+1;	$curpos=$curpos+1;
		$total = mysql_num_rows($result);
		if($prevpos>=0){ mysql_data_seek($result,$prevpos);	if($myrow=mysql_fetch_row($result)){ $prev_id=$myrow[0]; }}
		if($nextpos < $total){ mysql_data_seek($result,$nextpos); if($myrow=mysql_fetch_row($result)){ $next_id=$myrow[0]; }}
		mysql_free_result($result);
	
		$oPhoto_Gallery->data = array("category_id");
		$result=$oPhoto_Gallery->getDetail($photo_id);
		if($myrow=mysql_fetch_row($result)){
			$catid = $myrow[0];					
		}
		mysql_free_result($result);

		/*****Slide Show*****/
		if($oSystem->getValue("photo_slideshow")=="Yes"){
			if ($sefurl == "Yes"){
				$slideshow="<a href=\"slideshowplay-$catid-$catid-$photo_id-$start.html\">".$lang['photogallery']['hslideshow']."</a>";
			}else{
				$slideshow="<a href=\"".$thisfile."?category_id=$catid&parent_id=$catid&photo_id=$photo_id&pageaction=slideshowplay&start=$start\">".$lang['photogallery']['hslideshow']."</a>";
			}
		}else{
			$slideshow="";
		}
				
		/*****Previous and Next Navigation. Modify To Match Your Display*****/
		if($prev_id!=""){
			if ($sefurl == "Yes"){
				$prevlink="<a href=\"photo-$category_id-$parent_id-$prev_id-$start-Yes.html\">".$lang['photogallery']['prev']."</a>";	
			}else{
				$prevlink="<a href=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&photo_id=$prev_id&start=$start\">".$lang['photogallery']['prev']."</a>";	
			}
		}
		if($next_id!=""){
			if ($sefurl == "Yes"){
				$nextlink="<a href=\"photo-$category_id-$parent_id-$next_id-$start-Yes.html\">".$lang['photogallery']['next']."</a>";	
			}else{
				$nextlink="<a href=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&photo_id=$next_id&start=$start\">".$lang['photogallery']['next']."</a>";	
			}
		}
		if($prevlink!="" && $nextlink!=""){		$navline="&nbsp;&nbsp;";	}
		
		echo "<table border=0 width=98% align=center><tr><td><a href=\"$thisfile\">".$lang['photogallery']['hbacktop']."$CategoryLink</td><td align=right width=25%><b>$submitphoto</b></td></tr></table><hr size=1 color=#000000 width=98%>";
		echo "<table border=0 width=98% align=center>";
		
		
		if ($frompage=="searchphoto"){ 	echo $sefurl=="Yes"?"<tr><td colspan=3><b><a href=\"search-$frompage-$start-$searchby-$seachcat-$searchkey.html\">".$lang['photogallery']['backtosearch']."</a></b></td></tr>":"<tr><td colspan=3><b><a href=\"".$thisfile."?pageaction=$frompage&start=$start&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey\">".$lang['photogallery']['backtosearch']."</a></b></td></tr>"; }
		echo "<tr><td width=25% align=left><b>$slideshow</b></td><td align=center><b>".$lang['photogallery']['no']." ".$curpos." ".$lang['photogallery']['of']." ".$total." ".$newphoto."</b></td>";
		echo "<td width=25% align=right><b>$prevlink $navline $nextlink</b></td></tr></table><br>";
		
		/*****Public Category*****/
		$publiccat = array();
		$query = "select distinct photo_category.category_id from photo_category inner join photo_access on (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid') or (showprivate!='Yes')";
		$result = mysql_query($query,$oPhoto_Category->db);
		while($myrow=mysql_fetch_row($result)){	array_push($publiccat,$myrow[0]);	}
		mysql_free_result($result);
		
		/*****Photo Details. Modify To Match Your Display*****/
		$oPhoto_Gallery->data = array("title","date_format(publish,'%d-%m-%Y')","descp","descpmore","photo","downloadfile","newphoto","hit","category_id","author");
		$result=$oPhoto_Gallery->getDetail($photo_id);
		if($myrow=mysql_fetch_row($result)){
			if(in_array($myrow[8],$publiccat)){
						
				$title=""; $publish=""; $descp=""; $descpmore=""; 
				$photo="<img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" border=0 alt=\"".$lang['photogallery']['havailable']."\"></a>"; 
				$downloadfile=""; $newphotoico=""; $hit="<b>".$lang['photogallery']['hhit']." :</b> 0<br><br>"; 
	
				if($myrow[6]=="Yes"){				$newphotoico="<img src=\"".$path["webroot"]."photogallery/image/new.gif\" border=0>"; }else{$newphotoico="";}			
				if($myrow[0]!=""){					$title=stripslashes($myrow[0])."&nbsp;".$newphotoico."<br><br>";	}else{$title="".$newphotoico."<br><br>";}
				if($myrow[1]!="0000-00-00" && $oSystem->getValue("photo_showpubdate")=="Yes"){		$publish="<b>".$lang['photogallery']['publishdate']." :</b> $myrow[1]<br><br>";   }else {$publish="";}		 
				if($myrow[2]!=""){					$descp=stripslashes("$myrow[2]<br><br>"); }else{$descp="";}		
				if($myrow[3]!=""){					$descpmore=stripslashes("<br>$myrow[3]<br>"); }else{$descpmore="";}			
				if($myrow[4]!=""){					$photo="<img src=\"".$path["webroot"]."_files/photogallery/$myrow[4]\" border=0 alt=\"$myrow[0]\" style=\"border:1px #333333 solid\">"; }
				if($myrow[5]!=""){					$downloadfile="<a href=\"".$path["webroot"]."_files/photogallery/$myrow[5]\" target=_blank>".$lang['photogallery']['hdownload']."</a><br><br>"; }else{$downloadfile="";}				
				if($oSystem->getValue("photo_showhits")=="Yes"){if($myrow[7]!=0 || $myrow[7]!=""){	$hit="<b>".$lang['photogallery']['hhit']." :</b> $myrow[7]<br><br>"; }else{$hit="<b>".$lang['photogallery']['hhit']." :</b> 0<br><br>"; }} else {$hit="";}
				if($myrow[9]!=""){					$author="<b>".$lang['photogallery']['author']." :</b> $myrow[9]<br><br>";   }else {$author="";}		
				if($oSystem->getValue("photo_showrating")=="Yes"){
					$thisrate = (double)$oPhoto_Gallery->getRating($photo_id);		
					if($thisrate!=0 || $thisrate!=""){	$rating="<b>".$lang['photogallery']['hrating']." :</b> ".$thisrate." ".$lang['photogallery']['of']." 5<br><br>"; }else{	$rating="<b>".$lang['photogallery']['hrating']." :</b> ".$lang['photogallery']['hnotrated']."<br><br>"; }
				}else{ $rating="";	}
			
				switch($oSystem->getValue("photo_style")){
				
					/*****Side Details Style*****/
					case "1":
						echo "<table border=0 cellpadding=5 width=98% align=center>";
						echo "<tr><td valign=top align=center width=70%>$photo</td><td valign=top>$title $publish $author $hit $rating $downloadfile $descp</td></tr>";
						echo "<tr><td colspan=2>$descpmore</td></tr></table>";
						break;
						
					/*****Bottom Details Style*****/
					case "2":
						echo "<table border=0 cellpadding=5 width=98% align=center>";
						echo "<tr><td valign=top align=center colspan=4>$photo</td></tr>";
						echo "<tr><td colspan=4 align=center>$title $downloadfile</td></tr>";
						echo "<tr>".(!empty($publish)?"<td>$publish</td>":"").(!empty($author)?"<td>$author</td>":"").(!empty($hit)?"<td>$hit</td>":"").(!empty($rating)?"<td>$rating</td>":"")."</tr>";
						echo "<tr><td colspan=4>$descp $descpmore</td></tr></table>";
						break;	
				}
				
				if ($status_comment!=""){
					echo"
					<table border=0 cellspacing=2 cellpadding=2 width=98% align=center>
						<tr><td colspan=2><b>".$lang['photogallery']['status']."</b>&nbsp;:&nbsp;$status_comment</td></tr>
					</table>
					";
				}
				
				/*****Visitor Comment*****/
				if($oSystem->getValue("photo_showcomment")=="Yes"){
					$count_review=$oSystem->getValue("photo_reviewlist");
					if ($oPhoto_Review->getCountReview($photo_id)){
						echo "
							<table border=0 cellspacing=2 cellpadding=2 width=98% align=center>
								<tr><td colspan=2><b><u>".$lang['photogallery']['hvisitorcomm']."</u></b></td></tr>
						";
						
						$oPhoto_Review->data = array("review_id","author","reviewcontent","date_format(date_poster,'%d-%m-%Y')");
						$oPhoto_Review->where = "photo_id='$photo_id' and status='Active'";	    
			   			$oPhoto_Review->order = "date_poster DESC,review_id DESC limit 0,$count_review";	    
			   			$result_review=$oPhoto_Review->getList();
			   			while($myrow_review=mysql_fetch_row($result_review)){	   
			   				$myrow_review[1]=stripslashes($myrow_review[1]);
			   				$myrow_review[2]=stripslashes(nl2br($myrow_review[2]));	$dtpost = $myrow_review[3];
			   				echo"<tr><td width=15%>".$lang['photogallery']['authorname']."</td><td>&nbsp;:&nbsp;$myrow_review[1]</td></tr><tr><td>".$lang['photogallery']['dateposter']."</td><td>&nbsp;:&nbsp;$dtpost</td></tr><tr><td colspan=2>$myrow_review[2]<br><hr size=1 color=#C0C0C0 width=100%></td></tr>";
			   			} mysql_free_result($result_review);	   			
						echo"</table><br>";
					}
				}
				
				/*****Rate The Photo*****/
				if($oSystem->getValue("photo_showrating")=="Yes"){
					echo "<table border=0 width=98% align=center>";
					echo "<form name=frmRating action=\"".$thisfile."\" method=post>";
					echo "<input type=hidden name=photo_id value=\"$photo_id\">";
					echo "<input type=hidden name=category_id value=\"$category_id\">";
					echo "<input type=hidden name=parent_id value=\"$parent_id\">";
					echo "<input type=hidden name=start value=\"$start\">";
					echo "<input type=hidden name=pageaction value=\"rate\">";
					echo "<tr><td width=25%><b>".$lang['photogallery']['hratethis']."</b></td><td width=50>";
					echo "<select name=rating><option value=\"5\">5 - ".$lang['photogallery']['hbest']."</option><option value=\"4\">4</option><option value=\"3\">3</option><option value=\"2\">2</option><option value=\"1\">1</option></select>";
					echo "</td><td><input type=submit value=\"".$lang['photogallery']['nbtnrate']."\" style=\"font-size:11px\"></td>";
					echo "<td>$msgrated</td></tr></form></table><br>";
				}
				
				if($oSystem->getValue("photo_showcomment")=="Yes"){
					echo "<table border=0 width=98% align=center>";
					echo "<form name=frmComment action=\"".$thisfile."\" method=post>";
					echo "<input type=hidden name=photo_id value=\"$photo_id\">";
					echo "<input type=hidden name=category_id value=\"$category_id\">";
					echo "<input type=hidden name=parent_id value=\"$parent_id\">";
					echo "<input type=hidden name=start value=\"$start\">";
					echo "<input type=hidden name=pageaction value=\"comment\">";
					echo"<tr><td colspan=2><b><u>".$lang['photogallery']['addyourcomm']."</u></b></td></tr>";
					echo "<tr><td valign=top width=25%><b>".$lang['photogallery']['hyourname']."</b></td><td><input type=text name=name> *</td></tr>";
					echo "<tr><td valign=top><b>".$lang['photogallery']['hyourcomm']."</b></td><td><textarea name=comment cols=40 rows=3></textarea> *</td></tr>";
					echo "<tr><td></td><td><input type=button value=\"".$lang['photogallery']['haddcomm']."\" style=\"font-size:11px\" onclick=\"validate()\"></td></tr></form>";
					echo "</table><br>";
					echo "
					<script language=javascript>
						function validate(){
							if(document.frmComment.name.value==\"\"){	alert(\"".$lang['photogallery']['hplsname']."\"); document.frmComment.name.focus(); return false;	}
							else if(document.frmComment.comment.value==\"\"){	alert(\"".$lang['photogallery']['hplscomment']."\"); document.frmComment.comment.focus(); return false;	}
							else{	document.frmComment.submit();	}
						}
					</script>
					";
				}
			}
		}mysql_free_result($result);
		echo "<table border=0 width=98% align=center><tr>
			  <td colspan=2></td>
			  <td width=25% align=right><b>$prevlink $navline $nextlink</b></td></tr></table>";
		echo"</td></tr></table><br>";
	}
} 

if($pageaction=="submitphoto" || $pageaction=="submitphotook"){		
	if($pageaction=="submitphotook"){
		$oPhoto_Gallery->validate($lang['photogallery']['hyourname'],$name,"NotEmpty");
		$oPhoto_Gallery->validate($lang['photogallery']['hemail'],$email,"NotEmpty");
		$oPhoto_Gallery->validate($lang['photogallery']['htitle'],$title,"NotEmpty");
		$oPhoto_Gallery->validate($lang['photogallery']['hphotofile'],$photo,"NotEmpty");
		$oPhoto_Gallery->validate($lang['photogallery']['hemail'],$email,"IsEmail");
		
		/* img code */
		if ($oSystem->getValue("photo_enableimageverify")=="Yes"){
			$status_message.= $oPhoto_Gallery->valAuthCode("Validation code",$validationcode,$_SESSION['image_value']);
		}
		
		if($status_message==""){
			$publish =  date("Y-m-d",$localtime);
			/* end img code */		
			$photoname = strtolower(str_replace(" ","_",trim($_FILES['photo']['name'])));
			$uniqueid = md5(uniqid(rand(), true));	
			$uniqueid=substr($uniqueid,0,5);
	
			if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
				if($photoname!=""){ 
					$photoname2=$uniqueid."_".$photoname; 
					$oPhoto_Gallery->uploadFile($_FILES['photo']['tmp_name'],$path["docroot"]."_files/photogallery",$photoname2);
				}
		
				if($photoname!="" && $oSystem->getValue("photo_gd")!="None"){
					$ext = strrchr($photoname,".");
					if ($oSystem->getValue("photo_gd")=="20"){
						if($ext==".jpg" || $ext==".gif"){$valid_ext="yes"; } else { $valid_ext="no";}
					} else if ($oSystem->getValue("photo_gd")=="16") {
						if($ext==".jpg"){$valid_ext="yes";} else {$valid_ext="no";	}
					} else {$valid_ext="no";}
					
					if (!extension_loaded('gd')) {   $valid_ext="no";	}
					if($valid_ext=="yes"){	
						$src_file = $path["docroot"]."_files/photogallery/".$photoname2;
						$dest_file = $path["docroot"]."_files/photogallery/".$uniqueid."_t_".$photoname;
						$thumbw = $oSystem->getValue("photo_thumbsize");
						$thumbh = $oSystem->getValue("photo_thumbsize");
						$oPhoto_Gallery->resizeImage($src_file,$dest_file,$thumbw,$thumbh,85);
						$thumbnailname = $uniqueid."_t_".$photoname;		
			
						$src_file = $path["docroot"]."_files/photogallery/".$photoname2;
						$dest_file = $path["docroot"]."_files/photogallery/".$photoname2;
						$resizew = $oSystem->getValue("photo_resize");
						$resizeh = $oSystem->getValue("photo_resize");		
						$oPhoto_Gallery->resizeImage($src_file,$dest_file,$resizew,$resizeh,85);
					}	
				}
			}
	
			
			if ($oSystem->getValue("photo_photoapprove")=="Yes"){
				$photo_status="Pending";
			} else {
				$photo_status="Yes";
			}
			
			$briefdescp="<br>".stripslashes(nl2br($descp));
			$descp = str_replace("\r\n","<br>",$descp);
			$descp.="<br>".$lang['photogallery']['hsubmittedby']." <a href=mailto:$email>$name</a>";
			$sequence = $oPhoto_Gallery->getLastSequence()+1;
			$oPhoto_Gallery->data = array("category_id","title","publish","descp","thumbnail","photo","showphoto","newphoto","sequence","descpmore");
			$oPhoto_Gallery->value = array($category_id,addslashes($title),$publish,trim(addslashes($descp)),$thumbnailname,$photoname2,$photo_status,"Yes",$sequence,addslashes($descpmore));
			$oPhoto_Gallery->add();	
			
			$lastID=$oPhoto_Gallery->getLastID();
			if ($oSystem->getValue("photo_photoapprove")=="Yes"){
				$emailadmin = $oUser->getAdminEmail();
				$photo_adminemail = $oSystem->getValue("photo_adminemail");
				if(empty($emailadmin)){ $status_message="<font color=\"FF0000\">".$lang['photogallery']['emailfail']."</font><br>"; }
				if(empty($photo_adminemail)){ $status_message.="<font color=\"FF0000\">".$lang['photogallery']['adminemailfail']."</font><br>"; }

				if ($status_message==""){
					$oPhoto_Category->data = array("name");
					$result=$oPhoto_Category->getDetail($category_id);
					if($myrow=mysql_fetch_row($result)){
						$category_name=stripslashes($myrow[0]);
					}
					if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
						if(file_exists($path["docroot"]."_files/photogallery/".$photoname2) && $photoname2!=""){ $photolink="<a href=\"".$path["webroot"]."_files/photogallery/$photoname2\" target=\"_blank\">".$photoname."<a>"; 	}
					}
					
					$moredescp="<br>".stripslashes($descpmore);
					$subject = stripslashes($oSystem->getValue("photo_photosubject"));	
					$body = stripslashes(nl2br($oSystem->getValue("photo_photobody")));
					$body = str_replace("[[photoid]]",$lastID,$body);
					$body = str_replace("[[name]]",stripslashes($name),$body);
					$body = str_replace("[[email]]",stripslashes($email),$body);
					$body = str_replace("[[datepost]]",$publish,$body);
					$body = str_replace("[[category]]",stripslashes($category_name),$body);
					$body = str_replace("[[title]]",stripslashes($title),$body);
					$body = str_replace("[[briefdescp]]",$briefdescp,$body);
					$body = str_replace("[[moredescp]]",$moredescp,$body);
					$body = str_replace("[[photolink]]",$photolink,$body);
					$oSystem->mail($photo_adminemail,$subject,$body,$emailadmin,"HTML");
				$status_message.= "<br>".$lang['photogallery']['hthanksubmit']."<br><br>";
				}
			} else {
				$status_message.= "<br>".$lang['photogallery']['hthanksubmit2']."<br><br>";
			}

			$name=""; $email=""; $category_id=""; $title=""; $descp=""; $descpmore="";
		} else {
			$name=stripslashes($name);
			$email=stripslashes($email);
			$title=stripslashes(htmlentities($title));
			$descp=stripslashes($descp);
			$descpmore=stripslashes($descpmore);
		}
	}
	
	$publiccat = array();
	$query = "select distinct photo_category.category_id from photo_category inner join photo_access on (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid') or (showprivate!='Yes')";
	$result = mysql_query($query,$oPhoto_Category->db);
	while($myrow=mysql_fetch_row($result)){	array_push($publiccat,$myrow[0]);	}
	mysql_free_result($result);

	echo "<table border=0 width=98% align=center><tr><td>$status_message</td></tr></table>";
	echo "<table border=0 width=98% align=center cellpadding=3>";
	echo "<form name=\"frmSubmit\" action=\"".$thisfile."?category_id=$category_id&parent_id=$parent_id&start=$start\" method=post enctype=\"multipart/form-data\">";
	echo "<input type=hidden name=pageaction value=\"submitphotook\">";
	echo "<tr><td colspan=2><a href=\"$thisfile\">".$lang['photogallery']['hbacktop']." </a>> <b>".$lang['photogallery']['hsubmittous']."</b><br><br></td></tr>";
	echo "<tr><td>".$lang['photogallery']['hyourname']."</td><td><input type=text name=name style=\"width:230px\" value=\"$name\"> *</td></tr>";
	echo "<tr><td>".$lang['photogallery']['hemail']."</td><td><input type=text name=email style=\"width:230px\" value=\"$email\"> *</td></tr>";
	echo "<tr><td>".$lang['photogallery']['hcategory']."</td><td><select name=category_id>";
	
	$oPhoto_Category->getCategoryOption2(0,$category_id);
	
	echo "
		</select></td></tr>
		<tr><td>".$lang['photogallery']['htitle']."</td><td><input type=text name=title style=\"width:330px\" value=\"$title\"> *</td></tr>
		<tr><td>".$lang['photogallery']['hphotofile']."</td><td><input type=file name=photo size=50\"> *</td></tr>
		<tr><td valign=top>".$lang['photogallery']['briefdescp']."</td><td><textarea name=descp rows=5 cols=80 style=\"width:450px\">$descp</textarea></td></tr>
		<tr><td valign=top colspan=2>".$lang['photogallery']['moredescp']."</td></tr>
		<tr><td valign=top colspan=2>
		<table border=0 width=100% cellpadding=0 cellspacing=0><tr><td>
	";
	
	$oFCKeditor = new FCKeditor('descpmore') ;
	$oFCKeditor->BasePath	= $path["webroot"]."common/richtext/";
	$oFCKeditor->Width		= '100%' ;
	$oFCKeditor->Height		= '250' ;
	$oFCKeditor->Value		= $descpmore ;
	$oFCKeditor->Create() ;

	echo"</td></tr></table></td></tr>";

	echo "";
	if ($oSystem->getValue("photo_enableimageverify")=="Yes"){
		echo "<!-- img code -->
				<tr><td>".$lang['common']['validcode']."</td><td><input type=text name=validationcode style=\"width:135px\"> *
				<img src=\"".$path["webroot"]."/common/lib.randimage.php\" name='validationcode'></td></tr>
				<!-- end img code -->";
	}
	echo "<tr><td colspan=2><br><input type=submit value=\"".$lang['photogallery']['hbtnmyphoto']."\"></td></tr>";
	echo "</form></table><br><br>";
}

if($pageaction=="editinfo" || $pageaction=="editinfook"){

	if($pageaction=="editinfook"){
	   	$oPhoto_Member->data = array("name","email");
		$oPhoto_Member->value = array($name,$email);
		$oPhoto_Member->update($cookie_photomemberid);

		$status = $lang['photogallery']['haccupdated']."<br><br>";
					
		if($oldpass!="" && $newpass!="" && $chkpass!=""){		
		
			$oPhoto_Member->data = array("password");
			$result = $oPhoto_Member->getDetail($cookie_photomemberid);
			if($myrow=mysql_fetch_row($result)){ $curpass = $myrow[0];	}
			mysql_free_result($result);
			
			if($oldpass==$curpass && $newpass==$chkpass){
			   	$oPhoto_Member->data = array("password");
				$oPhoto_Member->value = array($newpass);
				$oPhoto_Member->update($cookie_photomemberid);
				$status = $lang['photogallery']['hpassupdated']."<br><br>";
			}else{
				$status = $lang['photogallery']['hpassnotmatch']."<br><br>";
			}		
		}
	}

	$oPhoto_Member->data = array("name","email","username","password");
	$result=$oPhoto_Member->getDetail($cookie_photomemberid);
	if($myrow=mysql_fetch_row($result)){
		$name = $myrow[0];
		$email = $myrow[1];
		$username = $myrow[2];
	}
	mysql_free_result($result);	

	echo "<table border=0 width=98% align=center><tr><td><b>".$lang['photogallery']['heditaccount']."</b></td></tr></table>";
	echo "<hr size=1 color=#000000 width=98%><br>";
	echo "<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>";
	echo "<form name=frmSignup action=\"".$thisfile."\" method=post>";
	echo "<input type=hidden name=pageaction value=\"editinfook\">";
	echo "<tr><td colspan=2>$status</td></tr>";
	echo "<tr><td width=25%>".$lang['photogallery']['hname']."</td><td><input type=text name=name size=20 value=\"$name\"></td></tr>";
	echo "<tr><td>".$lang['photogallery']['hemail']."</td><td><input type=text name=email size=35 value=\"$email\"></td></tr>";
	echo "<tr><td>".$lang['photogallery']['husername']."</td><td><input type=text name=username size=20 value=\"$username\" disabled></td></tr>";
	echo "<tr><td><br><b>".$lang['photogallery']['hchangepass']."</b></td><td></td></tr>";
	echo "<tr><td>".$lang['photogallery']['holdpass']."</td><td><input type=password name=oldpass size=20 value=\"\"></td></tr>";
	echo "<tr><td>".$lang['photogallery']['hnewpass']."</td><td><input type=password name=newpass size=20 value=\"\"></td></tr>";
	echo "<tr><td>".$lang['photogallery']['hconpass']."</td><td><input type=password name=chkpass size=20 value=\"\"></td></tr>";
	echo "<tr><td></td><td><input type=submit value=\"".$lang['photogallery']['hupdate']."\"></td></tr>";
	echo "</form></table><br><br>";
}

if($pageaction=="forgetpass"){

	echo "<table border=0 width=98% align=center><tr><td><a href=\"$thisfile\">".$lang['photogallery']['hbacktop']."</a> > <b>".$lang['photogallery']['hforgetpass']."</b></td></tr></table>";
	echo "<hr size=1 color=#000000 width=98%>";
	echo "<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>";
	echo "<form name=frmForgetPass action=\"".$thisfile."\" method=post>";
	echo "<input type=hidden name=pageaction value=\"forgetpassok\">";
	echo "<tr><td colspan=4>".$lang['photogallery']['hsearchinfo']."<br><br></td></tr>";
	echo "<tr><td width=20%>".$lang['photogallery']['husername']."</td><td><input type=text name=username size=20> &nbsp;&nbsp;&nbsp;   ".$lang['photogallery']['hor']." </td></tr>";
	echo "<tr><td>".$lang['photogallery']['hemail']."</td><td><input type=text name=email size=20></td></tr>";
	echo "<tr><td></td><td><input type=submit value=\"".$lang['photogallery']['hsend']."\"></td></tr>";
	echo "</form></table><br><br>";
}

if($pageaction=="forgetpassok"){

	$oPhoto_Member->data = array("name","email","username","password");
	$oPhoto_Member->where = "BINARY username='$username' or BINARY email='$email'"; 
	$result = $oPhoto_Member->getList();
	if($myrow=mysql_fetch_row($result)){		
		$emailfrom = $oUser->getAdminEmail();
		if(empty($emailfrom)){ $status_message="<font color=\"FF0000\">".$lang['photogallery']['emailfail']."</font><br>"; }

		if ($status_message==""){
			$subject = $oSystem->getValue("photo_forgetsubject");
			$message = $oSystem->getValue("photo_forgetbody");
			$message = str_replace("[[name]]",$myrow[0],$message);
			$message = str_replace("[[username]]",$myrow[2],$message);
			$message = str_replace("[[password]]",$myrow[3],$message);
			$oSystem->mail($myrow[1],$subject,$message,$emailfrom);	
			$status_message=$lang['photogallery']['hforgetsend']."<br><br>".$lang['photogallery']['hforgetsent']." <br>";
		}
		
		echo "<table border=0 width=98% align=center><tr><td><b>".$lang['photogallery']['hforgetpass']."</b></td></tr></table>";
		echo "<hr size=1 color=#000000 width=98%>";
		echo "<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>";
		echo "<tr><td>$status_message<br><br><a href=\"".$thisfile."?pageaction=forgetpass\">".$lang['photogallery']['hforgetback']."</a></td></tr>";
		echo "</table><br><br>";
				
	}else{
		echo "<table border=0 width=98% align=center><tr><td><b>".$lang['photogallery']['hforgetpass']."</b></td></tr></table>";
		echo "<hr size=1 color=#000000 width=98%>";
		echo "<table border=0 cellpadding=3 cellspacing=0 width=98% align=center>";
		echo "<tr><td>".$lang['photogallery']['hforgetsorry']."<br><br><br><a href=\"".$thisfile."?pageaction=forgetpass\">".$lang['photogallery']['hforgetback']."</a></td></tr>";
		echo "</table><br><br>";
	}
}

if ($pageaction=="searchphoto"){
	$searchresultkey = array();
	$searchkey = str_replace("+"," ",$searchkey);
	
	$vsearchkey=addslashes($searchkey);
	$vsearchkey=addslashes($vsearchkey);
	
	if ($searchby=="bycategory"){
		$oPhoto_Gallery->data = array("distinct photo_id");
		$oPhoto_Gallery->where="photo_gallery.category_id='$seachcat' AND showphoto='Yes'"; 	
	   	$oPhoto_Gallery->order = "title";	    
	   	$result=$oPhoto_Gallery->getList();
	   	while($myrow=mysql_fetch_row($result)){
	   		if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }
   		}
   		mysql_free_result($result);
	} else {
		if ($searchkey!=""){
			$array_stopword = array("a","an","are","you","and","because","begin","bf","both","but","can","do");
			$special = array("~","!","@","#","$","%","^","&","*","(",")",",","�","-","+","{","[","}","]",":",".","<",">","|","/","?","'");
		
			if (!in_array($vsearchkey,$special)){
				if (!in_array($vsearchkey,$array_stopword)){
					$oPhoto_Gallery->data = array("distinct photo_id");
					$oPhoto_Gallery->where="(photo_gallery.category_id=photo_category.category_id) AND ((photo_category.category_id=photo_access.category_id AND member_id='$cookie_photomemberid') OR showprivate!='Yes') AND showphoto='Yes' AND (photo_gallery.title like '%$vsearchkey%' OR photo_gallery.descp like '%$vsearchkey%')";
					$oPhoto_Gallery->order = "title ASC";
					$result=$oPhoto_Gallery->getListAccess();
					while($myrow=mysql_fetch_row($result)){	if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }}
					mysql_free_result($result);	
				}
			}
			
			$searchkey = preg_replace('/\s+/',' ',$searchkey);
			$searchkey=trim($searchkey);
			$searchresultkeylist = explode(" ",$searchkey);	
			for($k=0;$k<count($searchresultkeylist);$k++){	
				if (!in_array($searchresultkeylist[$k],$special)){
					if (!in_array($searchresultkeylist[$k],$array_stopword)){
						$vsearchkey=addslashes($searchresultkeylist[$k]);
						$vsearchkey=addslashes($vsearchkey);
						$oPhoto_Gallery->data = array("distinct photo_id");
						$oPhoto_Gallery->where="(photo_gallery.category_id=photo_category.category_id) AND ((photo_category.category_id=photo_access.category_id AND member_id='$cookie_photomemberid') OR showprivate!='Yes') AND showphoto='Yes' AND (photo_gallery.title like '%$vsearchkey%' OR photo_gallery.descp like '%$vsearchkey%')";
						$result=$oPhoto_Gallery->getListAccess();
						while($myrow=mysql_fetch_row($result)){	if(!in_array($myrow[0],$searchresultkey)){ array_push($searchresultkey,$myrow[0]); }}
						mysql_free_result($result);	
					}
				}
			}
		}
	}
	
	if ($searchby==""){$searchby="bycategory";}
	
	$publiccat = array();
	$query = "select distinct photo_category.category_id from photo_category inner join photo_access on (photo_category.category_id=photo_access.category_id and member_id='$cookie_photomemberid') or (showprivate!='Yes')";
	$result = mysql_query($query,$oPhoto_Category->db);
	while($myrow=mysql_fetch_row($result)){	array_push($publiccat,$myrow[0]);	}
	mysql_free_result($result);

	echo"
		<table border=0 cellpadding=2 cellspacing=2 width=98% align=center>
			<tr><td align=left colspan=3><a href=\"$thisfile\">".$lang['photogallery']['hbacktop']."</a> > <b>".$lang['photogallery']['searchphoto']."</b><br><br></td></tr>
			<form name=frmSearchT action=\"".$thisfile."\" method=post>
			<input type=hidden name=pageaction value=\"searchphoto\">
				<tr><td><input type=radio name=searchby value=\"bycategory\" ".($searchby=="bycategory"?"checked":"")."></td><td width=\"20%\">".$lang['photogallery']['bycategory']."&nbsp;</td><td><select name=seachcat>
	";
	$oPhoto_Category->getCategoryOption2(0,$seachcat);
	echo"
			</select></td></tr>
			<tr><td width=3%><input type=radio name=searchby value=\"bytitle\" ".($searchby=="bytitle"?"checked":"")."></td><td>".$lang['photogallery']['bykeyword']."&nbsp;</td><td><input type=text name=searchkey style=\"width:200px\" value=\"$searchkey\">&nbsp;<input type=button value=\" ".$lang['photogallery']['go']." \" onclick=document.frmSearchT.submit();></td></tr>
			</form>
		</table><br>
	";		
	
	$searchno = $oSystem->getValue("photo_searchno");
	$total= count($searchresultkey);
	
	if($start=="" || $start==0){ $start=0; }
	$prev=$start-$searchno; $next=$start+$searchno;	$from=$start+1; $to=$searchno+$from-1;
	if($to>=$total){ $to=$total; }if($to<$from){ $from=0; }
	
	$searchkey1 = stripslashes(htmlentities(str_replace(" ","+",$searchkey)));
	
	if($prev>=0){ 
		if ($sefurl=="Yes"){
			$prevlink="<a href=\"search-$pageaction-$prev-$searchby-$seachcat-$searchkey1.html\"><b>".$lang['photogallery']['prev']."</b></a>";
		} else {
			$prevlink="<a href=\"".$thisfile."?pageaction=$pageaction&start=$prev&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey1\"><b>".$lang['photogallery']['prev']."</b></a>";	
		}
	} else { $prevlink=""; }
	if($next<$total){ 
		if ($sefurl=="Yes"){
			$nextlink="<a href=\"search-$pageaction-$next-$searchby-$seachcat-$searchkey1.html\"><b>".$lang['photogallery']['next']."</b></a>";
		} else {
			$nextlink="<a href=\"".$thisfile."?pageaction=$pageaction&start=$next&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey1\"><b>".$lang['photogallery']['next']."</b></a>";
		}
	}  else { $nextlink=""; }
	
	if($prevlink!="" && $nextlink!=""){ $navline="&nbsp;|&nbsp;"; }
	
	echo "<table border=0 cellpadding=0 cellspacing=0 width=98% align=center>";
	$lblsearchkey=stripslashes(htmlentities($searchkey));
	
	if ($searchby=="bycategory"){
		$searchcatname=stripslashes($oPhoto_Category->getCatName($seachcat));
		echo"<tr><td valign=top colspan=3>".$lang['photogallery']['searchbycat']."&nbsp;:&nbsp;".$lang['photogallery']['searchfound']."'<b>$searchcatname</b>'<br><br></td></tr>";
	} else {	
		echo"<tr><td valign=top colspan=3>".$lang['photogallery']['searchbykeyword']."&nbsp;:&nbsp;".$lang['photogallery']['searchfound']."'<b>$lblsearchkey</b>'<br><br></td></tr>";
	} 
		
	echo"
		<tr>
			<td width=20%>&nbsp;</td><td valign=top align=center>".$lang['photogallery']['showing']." ".$from."-".$to." ".$lang['photogallery']['of']." ".$total."</td>
			<td align=right width=20% valign=top>&nbsp;$prevlink $navline $nextlink&nbsp;</td>
		</tr>
		</table>
		<hr size=1 color=#B6B6B6 width=98%>
		<table border=0 width=98% align=center>
	";
	
	if(count($searchresultkey)!=0){
		echo "
			<tr><td valign=top>
				<table border=0 cellpadding=2 cellspacing=2 width=100% align=center>
		";
		for($k=0;$k<count($searchresultkey);$k++){
				$oPhoto_Gallery->data = array("category_id","title","descp","thumbnail");
				$result=$oPhoto_Gallery->getDetail($searchresultkey[$k]);
				if($myrow=mysql_fetch_row($result)){	
								
					if($k >= $start && $k < $start+$searchno){
						if ($sefurl=="Yes"){
							$categoryname="<a href=navigate-$myrow[0]-$myrow[0]-0.html>".stripslashes($oPhoto_Category->getCatName($myrow[0]))."</a>";
						} else {
							$categoryname="<a href=\"$thisfile?category_id=$myrow[0]&parent_id=$myrow[0]\">".stripslashes($oPhoto_Category->getCatName($myrow[0]))."</a>";
						}
						$myrow[1] = stripslashes($myrow[1]);
						if ($myrow[2]!=""){$myrow[2]="<td valign=top>".$lang['photogallery']['hlbldescp']."&nbsp;:&nbsp;</td><td>".stripslashes(nl2br($myrow[2]))."</td>";} else {$myrow[2]="<td colspan=2>&nbsp;</td>";}
						
						if($oSystem->getValue("photo_popup")!="Yes"){
							if ($sefurl=="Yes"){
								$myrow[1]="<a href=\"photo-$myrow[0]-$myrow[0]-$searchresultkey[$k]-Yes.html\">$myrow[1]</a>";
								if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
									if(file_exists($path["docroot"]."_files/photogallery/$myrow[3]") && $myrow[3]!=""){
										$thumbnail ="<a href=\"photosearch-$myrow[0]-$myrow[0]-$searchresultkey[$k]-Yes-$pageaction-$start-$searchby-$seachcat-$searchkey1.html\"><img src=\"".$path["webroot"]."_files/photogallery/$myrow[3]\" width=75 border=0 align=left hspace=5></a>";
									} else {
										$thumbnail ="<a href=\"photosearch-$myrow[0]-$myrow[0]-$searchresultkey[$k]-Yes-$pageaction-$start-$searchby-$seachcat-$searchkey1.html\"><img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" border=0 align=left hspace=5></a>";
									}
								}
							} else{
								$myrow[1]="<a href=\"".$thisfile."?category_id=$myrow[0]&parent_id=$myrow[0]&photo_id=$searchresultkey[$k]&frompage=$pageaction&start=$start&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey1\">$myrow[1]</a>";
								if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
									if(file_exists($path["docroot"]."_files/photogallery/$myrow[3]") && $myrow[3]!=""){
										$thumbnail ="<a href=\"".$thisfile."?category_id=$myrow[0]&parent_id=$myrow[0]&photo_id=$searchresultkey[$k]&frompage=$pageaction&start=$start&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey1\"><img src=\"".$path["webroot"]."_files/photogallery/$myrow[3]\" width=75 border=0 align=left hspace=5></a>";
									} else {
										$thumbnail ="<a href=\"".$thisfile."?category_id=$myrow[0]&parent_id=$myrow[0]&photo_id=$searchresultkey[$k]&frompage=$pageaction&start=$start&searchby=$searchby&seachcat=$seachcat&searchkey=$searchkey1\"><img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" border=0 align=left hspace=5></a>";
									}
								}
							}
						}else{
							$myrow[1]="<a href=javascript:popUpImg(\"$searchresultkey[$k]\");>$myrow[1]</a>";
							if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
								if(file_exists($path["docroot"]."_files/photogallery/$myrow[3]") && $myrow[3]!=""){
									$thumbnail ="<a href=javascript:popUpImg(\"$searchresultkey[$k]\");><img src=\"".$path["webroot"]."_files/photogallery/$myrow[3]\" width=75 border=0 align=left hspace=5></a>";
								} else {
									$thumbnail ="<a href=javascript:popUpImg(\"$searchresultkey[$k]\");><img src=\"".$path["webroot"]."photogallery/image/nopic.gif\" width=75 border=0 align=left hspace=5></a>";
								}
							}
						}
						
						echo "<tr><td valign=top width=15%>$thumbnail</td><td valign=top><table border=0 cellpadding=2 cellspacing=2 width=100% align=center><tr><td valign=top width=20%>".$lang['photogallery']['hlbltitle']." :</td><td valign=top>$myrow[1]</td></tr><tr><tr><td valign=top>".$lang['photogallery']['hlblcategory']." :</td><td valign=top>$categoryname</td></tr><tr>$myrow[2]</tr></table></td></tr>";
					}
				} mysql_free_result($result);	
		}
		echo"		
				</table></td></tr>
		";
	}else{	
		echo "<tr><td><br><br><center><font color=ff0000>".$lang['photogallery']['hnofound']."</font></center></td></tr>";	
	}
	echo "</table>".(!(empty($prevlink) && empty($nextlink))?"<br><table border=0 cellpadding=0 cellspacing=0 width=95% align=center><tr><td align=right valign=top>&nbsp;$prevlink $navline $nextlink&nbsp;</td></tr></table>":"")."";
}

?>

<script language=javascript>
function popUpImg(id){
	window.open("<? echo $path["webroot"] ?>photogallery/home.popupimg.php?photo_id="+id,"Popup","width=800,height=600,top=50,left=100,dependent=yes,titlebar=no,scrollbars=yes,resizable=yes");	
}
</script>

