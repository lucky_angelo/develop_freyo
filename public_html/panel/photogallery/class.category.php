<?



class Photo_Category extends Item{
	
	var $table="photo_category",$primarykey="category_id",$shortname="photogallery",$version="4.1";
	var $access = array();

	function Photo_Category(){}

	function delete($id){
		$sql="select parent_id from $this->table where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	$thisparentid=$myrow[0];	}
		mysql_free_result($result);
			
		$sql="select category_id from $this->table where parent_id='$id'";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			$sql="update $this->table set parent_id='$thisparentid' where category_id='$myrow[0]'";
			mysql_query($sql,$this->db);			
		}
		mysql_free_result($result);
		
		$sql="select photo_id from photo_gallery where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			$sql="update photo_gallery set category_id='$thisparentid' where photo_id='$myrow[0]'";
			mysql_query($sql,$this->db);			
		}
		mysql_free_result($result);
		
		$sql="delete from $this->table where category_id='$id'";
		mysql_query($sql,$this->db);
		
		$this->logEvent("Delete");		
	}	
	
	function getCategoryOption($parent_id,$select_id="",$hide_id="",$round=""){
		$space=""; if(!isset($round)){$round=0;}
		$sql = "select category_id, name, parent_id from $this->table where parent_id='$parent_id' order by name";
		$rst = mysql_query($sql,$this->db);
		if(mysql_num_rows($rst)>0){
			if($parent_id!=0){$round++; for($i=0;$i<$round;$i++){$space.="&nbsp;&nbsp;";}}
			while($myr = mysql_fetch_row($rst)){
				$catid = $myr[0];	$catname = stripslashes($myr[1]);	$selected = ($catid==$select_id?" selected":"");				
				if($catid!=$hide_id){
					if($parent_id==0){echo "<option value=\"$catid\"$selected>$catname</option>";}
					else{ echo "<option value=\"$catid\"$selected>$space-- $catname</option>";	}
				}
				$this->getCategoryOption($catid,$select_id,$hide_id,$round);
			}mysql_free_result($rst);
		}else{ $round=0;}
	}
	
	function getCategoryOption2($parent_id,$select_id="",$hide_id="",$round=""){
		Global $publiccat;
		$space=""; if(!isset($round)){$round=0;}
		$sql = "select category_id, name, parent_id from $this->table where parent_id='$parent_id' order by name";
		$rst = mysql_query($sql,$this->db);
		if(mysql_num_rows($rst)>0){
			if($parent_id!=0){$round++; for($i=0;$i<$round;$i++){$space.="&nbsp;&nbsp;";}}
			while($myr = mysql_fetch_row($rst)){
				$catid = $myr[0];	$catname = stripslashes($myr[1]);	$selected = ($catid==$select_id?" selected":"");				
				if(in_array($catid,$publiccat)){
					if($parent_id==0){echo "<option value=\"$catid\"$selected>$catname</option>";}
					else{ echo "<option value=\"$catid\"$selected>$space-- $catname</option>";	}
				}
				$this->getCategoryOption2($catid,$select_id,$hide_id,$round);
			}mysql_free_result($rst);
		}else{ $round=0;}
	}

	function getCategoryAccess($parent_id,$select_id="",$hide_id="",$round=""){
		$space=""; if(!isset($round)){$round=0;}
		$sql = "select category_id, name, parent_id from $this->table where parent_id='$parent_id' order by name";
		$rst = mysql_query($sql,$this->db);
		if(mysql_num_rows($rst)>0){
			if($parent_id!=0){$round++; for($i=0;$i<$round;$i++){$space.="&nbsp;&nbsp;";}}
			while($myr = mysql_fetch_row($rst)){
				$catid = $myr[0];
				$catname = stripslashes($myr[1]);	
				if(in_array($catid,$this->access)){	
					$selected = "selected";				
				} else {
					$selected = "";
				}
				if($catid!=$hide_id){
					if($parent_id==0){echo "<option value=\"$catid\"$selected>$catname</option>";}
					else{ echo "<option value=\"$catid\"$selected>$space-- $catname</option>";	}
				}
				$this->getCategoryAccess($catid,$select_id,$hide_id,$round);
			}mysql_free_result($rst);
		}else{ $round=0;}
	}
	
	function getCatAccessOpt($parent_id){
		global $cattree_top,$vcategory_id,$vvcategory_id,$vparent_id,$category_id,$CategoryOption;

		$sql="select category_id,name,parent_id from $this->table where parent_id='$parent_id' order by name";
		echo "$sql";
		$result=mysql_query($sql,$this->db);
		while($myrow=mysql_fetch_row($result)){
			if($parent_id!=0){ $cattree_top = $cattree_top." > ".stripslashes($myrow[1]); }else{ $cattree_top = stripslashes($myrow[1]); }
			if($myrow[0]!=$category_id){ 
				if(in_array($myrow[0],$this->access)){
					$CategoryOption .="<option value=$myrow[0] selected>$cattree_top</option>";
				}else{
					$CategoryOption .="<option value=$myrow[0]>$cattree_top</option>";
				}
			}
			$sql1="select category_id from $this->table where parent_id='$myrow[0]'";
			$result1=mysql_query($sql1,$this->db);
			if(mysql_num_rows($result1)!=0){	$this->getCatAccessOpt($myrow[0]);		}
			mysql_free_result($result1);
			
			$cattree_top = substr($cattree_top,0,strrpos($cattree_top,">")-1);
		}
		mysql_free_result($result);
	}

	function getCategoryLink($id){
		global $CategoryLink,$oSystem;
		
		$thisfile=$oSystem->getValue("photo_page");
		$sefurl = $oSystem->getValue("photo_sefurl");
		
		$sql="select parent_id,name from $this->table where category_id='$id'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){
			
			$sql="select parent_id,name from $this->table where parent_id='$id'";
			$resultchild=mysql_query($sql,$this->db);
			if(mysql_num_rows($resultchild)!=0){	
				if ($sefurl == "Yes"){
					$CategoryLink = "<a href=\"cat-$id-$id.html\" style=\"text-decoration:none\"><b>".stripslashes($myrow[1])."</b></a>"." > ".$CategoryLink;
				}else{
					$CategoryLink = "<a href=\"".$thisfile."?category_id=$id&parent_id=$id\" style=\"text-decoration:none\"><b>".stripslashes($myrow[1])."</b></a>"." > ".$CategoryLink;
				}										
			}else{
				if ($sefurl == "Yes"){
					$CategoryLink = "<a href=\"cat-$id-$myrow[0].html\" style=\"text-decoration:none\"><b>".stripslashes($myrow[1])."</b></a>"." > ".$CategoryLink;
				}else{
					$CategoryLink = "<a href=\"".$thisfile."?category_id=$id&parent_id=$myrow[0]\" style=\"text-decoration:none\"><b>".stripslashes($myrow[1])."</b></a>"." > ".$CategoryLink;
				}	
			}
			mysql_free_result($resultchild);

			$this->getCategoryLink($myrow[0]);
			mysql_free_result($result);
		}else{
			$CategoryLink = substr($CategoryLink,0,strrpos($CategoryLink, ">"));
		}

	}
	
	function getLastSequence(){
		$sql="select sequence from $this->table order by sequence desc limit 0,1";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}	
	
	function getSubCategory($id,$cookie_photomemberid){
		$this->getdatalist(); $this->getdatalist();
		$sql="select $this->datalist from photo_category LEFT JOIN photo_access ON photo_category.category_id=photo_access.category_id where photo_category.parent_id='$id' AND (photo_access.member_id='$cookie_photomemberid' OR photo_category.showprivate!='Yes') order by sequence asc";
		$result=mysql_query($sql,$this->db);
		return $result;			
	}
	
	function getCatName($category_id){
		$sql="select name from $this->table where category_id='$category_id'";
		$result=mysql_query($sql,$this->db);
		if($myrow=mysql_fetch_row($result)){	return $myrow[0];	}
		mysql_free_result($result);
	}	
}

?>