<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:39 28.12.2006                  */
/**************************************************/


echo "<table border=0 width=100%><tr><td><b>".$lang['photogallery']['photogallery']."</b></td><td>";
include("wce.menu.php");
echo "</td></tr></table><hr size=1 color=#606060>".$status_message."<br>";
//Body Start
echo "
<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=index.php?component=photogallery&page=wce.bulkupload.php method=post enctype=\"multipart/form-data\">
<input type=hidden name=pageaction>
<tr><td colspan=2>".$lang['photogallery']['bulkuploadtip']."<br><br></td></tr>
<tr><td valign=top>
	<b><u>".$lang['photogallery']['defaultdetail']."</u></b><br><br>	
	<table border=0 cellpadding=2 width=98%>
	<tr><td width=5%>1. </td><td valign=top>".$lang['photogallery']['setcategory']."</td><td valign=top><select name=\"category_id\">";
	$oPhoto_Category->getCategoryOption(0,$category_id);
	echo "</select></td></tr>
	<tr><td>2. </td><td>".$lang['photogallery']['setpublish']."</td><td><input type=\"text\" name=\"setdate\" value=\"".date("Y-m-d")."\" style=\"width:80px\"></td></tr>
	<tr><td>3. </td><td width=\"30%\">".$lang['photogallery']['setfilename']."</td><td><input type=\"checkbox\" name=\"settitle\" value=\"Yes\" checked></td></tr>
	<tr><td>4. </td><td>".$lang['photogallery']['setexif']."</td><td><input type=\"checkbox\" name=\"setexif\" value=\"Yes\" ".$setexifcheck."></td></tr>
	<tr><td></td><td colspan=2>* ".$lang['photogallery']['exifmessage']."
	<tr><td>5. </td><td>".$lang['photogallery']['setnew']."</td><td><input type=\"checkbox\" name=\"setnew\" value=\"Yes\" ".$setnewcheck."></td></tr>
	<tr><td>6. </td><td>".$lang['photogallery']['setthumbnail']."</td><td><input type=\"checkbox\" name=\"setthumbnail\" value=\"Yes\" checked></td></tr>
	<tr><td>7. </td><td>".$lang['photogallery']['setresize']."</td><td><input type=\"checkbox\" name=\"setresize\" value=\"Yes\" checked></td></tr>
	<tr><td>8. </td><td>".$lang['photogallery']['setcopyright']."</td><td><input type=\"checkbox\" name=\"setcopyright\" value=\"Yes\" ".$setcopyrightcheck."></td></tr>
	<tr><td>9. </td><td>".$lang['photogallery']['setwatermark']."</td><td><input type=\"checkbox\" name=\"setwatermark\" value=\"Yes\" ".$setwatermarkcheck."></td></tr>
	<tr><td></td><td colspan=2>* ".$lang['photogallery']['watermarkmsg']."
	</table><br>
</td></tr>
<tr><td valign=top>
	<b><u>". $lang['photogallery']['uploadzip']."<u></b><br><br>
	<table border=0 cellpadding=2 width=98%>
	<tr><td valign=top>".$lang['photogallery']['uploadziptip']."</td></tr>
	<tr><td valign=top><br><input type=button value=\"".$lang['photogallery']['btnstartupload']."\" onclick=javascript:uploadImage();></td></tr>
	</table><br>
</td></tr>

<tr><td valign=top>
	<b><u>".$lang['photogallery']['ftpupload']."</u></b><br><br>	
	<table border=0 cellpadding=3 width=98%>
	<tr><td>".$lang['photogallery']['ftpnote']."</td></tr>
	<tr><td>".$lang['photogallery']['ftpnote1']."</td></tr>
	<tr><td>".$lang['photogallery']['ftpnote2']."</td></tr>
	<tr><td>".$lang['photogallery']['ftpnote3']."</td></tr>
	<tr><td>".$lang['photogallery']['ftpnote4']."</td></tr>
	<tr><td><br><input type=button value=\"".$lang['photogallery']['btnstar']."\" onclick=javascript:FTPupload();></td></tr>
	</table><br>
</td></tr>
</form></table>
";
?>
<script language=javascript>
function uploadImage(){
	var category_id = document.thisform.category_id.value;
	var setdate = document.thisform.setdate.value;
	if(document.thisform.settitle.checked){	var settitle="Yes";	}else{var settitle="No";}
	if(document.thisform.setexif.checked){	var setexif="Yes";	}else{var setexif="No";}
	if(document.thisform.setnew.checked){	var setnew="Yes";	}else{var setnew="No";}
	if(document.thisform.setthumbnail.checked){	var setthumbnail="Yes";	}else{var setthumbnail="No";}
	if(document.thisform.setresize.checked){	var setresize="Yes";	}else{var setresize="No";}
	if(document.thisform.setcopyright.checked){	var setcopyright="Yes";	}else{var setcopyright="No";}
	if(document.thisform.setwatermark.checked){	var setwatermark="Yes";	}else{var setwatermark="No";}
	window.open("index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&frompage=uploadzip&category_id="+category_id+"&setdate="+setdate+"&settitle="+settitle+"&setexif="+setexif+"&setnew="+setnew+"&setthumbnail="+setthumbnail+"&setresize="+setresize+"&setcopyright="+setcopyright+"&setwatermark="+setwatermark,"Popup","width=650,height=550,top=20,left=50,dependent=yes,titlebar=no,scrollbars=yes,resizable=yes");
}

function FTPupload(){
	var category_id = document.thisform.category_id.value;
	var setdate = document.thisform.setdate.value;
	if(document.thisform.settitle.checked){	var settitle="Yes";	}else{var settitle="No";}
	if(document.thisform.setexif.checked){	var setexif="Yes";	}else{var setexif="No";}
	if(document.thisform.setnew.checked){	var setnew="Yes";	}else{var setnew="No";}
	if(document.thisform.setthumbnail.checked){	var setthumbnail="Yes";	}else{var setthumbnail="No";}
	if(document.thisform.setresize.checked){	var setresize="Yes";	}else{var setresize="No";}
	if(document.thisform.setcopyright.checked){	var setcopyright="Yes";	}else{var setcopyright="No";}
	if(document.thisform.setwatermark.checked){	var setwatermark="Yes";	}else{var setwatermark="No";}
	window.open("index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&frompage=ftpupload&category_id="+category_id+"&setdate="+setdate+"&settitle="+settitle+"&setexif="+setexif+"&setnew="+setnew+"&setthumbnail="+setthumbnail+"&setresize="+setresize+"&setcopyright="+setcopyright+"&setwatermark="+setwatermark,"Popup","width=650,height=550,top=20,left=50,dependent=yes,titlebar=no,scrollbars=yes,resizable=yes");
}
</script>

