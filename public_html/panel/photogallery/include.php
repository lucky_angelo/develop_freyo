<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:33 28.12.2006                  */
/**************************************************/

if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	include($path["docroot"]."photogallery/class.gallery.php");
	$oPhoto_Gallery = new Photo_Gallery;
	$oPhoto_Gallery->db = $oDb->db;
	
	include($path["docroot"]."photogallery/class.category.php");
	$oPhoto_Category = new Photo_Category;
	$oPhoto_Category->db = $oDb->db;
	
	include($path["docroot"]."photogallery/class.member.php");
	$oPhoto_Member = new Photo_Member;
	$oPhoto_Member->db = $oDb->db;
	
	include($path["docroot"]."photogallery/class.review.php");
	$oPhoto_Review = new Photo_Review;
	$oPhoto_Review->db = $oDb->db;
	
	$oPhoto_Gallery->gdversion = $oSystem->getValue("photo_gd");
}
?>