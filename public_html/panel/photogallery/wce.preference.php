<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:47 28.12.2006                  */
/**************************************************/


if($pageaction=="removes"){
	if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
		if(file_exists($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")) && $oSystem->getValue("photo_watermark")!=""){ @unlink($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")); 	}
		$oSystem->setValue("photo_watermark",""); 
	}
}

if ($oSystem->getValue("photo_page") == "")
	$oSystem->setValue("photo_page", $path["webroot"]."photogallery/samplephotogallery.php");

if($pageaction=="save"){
  $oSystem->setValue("photo_page",$frontendurl);
  $oSystem->setValue("photo_enableimageverify",$imgverify);
  $oSystem->setValue("photo_sefurl",$sefurl);
  $oSystem->setValue("photo_pageno",$photo_pageno);
  $oSystem->setValue("photo_rowno",$photo_rowno);
  $oSystem->setValue("photo_seq",$photo_seq);
  $oSystem->setValue("photo_style",$photo_style);
  $oSystem->setValue("photo_listno",$photo_listno);
  $oSystem->setValue("photo_popup",$photo_popup);
  $oSystem->setValue("photo_showrating",$photo_showrating);
  $oSystem->setValue("photo_limitrating",$photo_limitrating);
  $oSystem->setValue("photo_showcomment",$photo_showcomment); 
  $oSystem->setValue("photo_thumbsize",$photo_thumbsize); 
  $oSystem->setValue("photo_resize",$photo_resize); 
  $oSystem->setValue("photo_slideshow",$photo_slideshow); 
  $oSystem->setValue("photo_slideshowtime",$photo_slideshowtime); 
  $oSystem->setValue("photo_latestblock",$photo_latestblock); 
  $oSystem->setValue("photo_latestno",$photo_latestno); 
  $oSystem->setValue("photo_randomblock",$photo_randomblock); 
  $oSystem->setValue("photo_randomno",$photo_randomno); 
  $oSystem->setValue("photo_submitphoto",$photo_submitphoto); 
  $oSystem->setValue("photo_memberlogin",$photo_memberlogin);   
  $oSystem->setValue("photo_copyright",$photo_copyright); 
  $oSystem->setValue("photo_red",$photo_red); 
  $oSystem->setValue("photo_green",$photo_green); 
  $oSystem->setValue("photo_blue",$photo_blue);       
  $oSystem->setValue("photo_gd",$photo_gd);  
  $oSystem->setValue("photo_showpubdate",$photo_showpubdate); 
  $oSystem->setValue("photo_showhits",$photo_showhits); 
  $oSystem->setValue("photo_subcatno",$photo_subcatno);
  $oSystem->setValue("photo_searchno",$photo_searchno); 
  $oSystem->setValue("photo_showcatdescp",$photo_showcatdescp); 
  $oSystem->setValue("photo_wmlocation",$photo_wmlocation); 
  $oSystem->setValue("photo_reviewlist",$photo_reviewlist);  
  
  $photo_watermark = strtolower(str_replace(" ","_",trim($_FILES['photowatermark']['name'])));
  $ext = strrchr($photo_watermark,".");
  if ($photo_watermark!=""){
	if($ext==".png"){
		if($photo_watermark!=""){ 
			if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			  	if(file_exists($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")) && $oSystem->getValue("photo_watermark")!=""){ @unlink($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")); 	}
				$oPhoto_Gallery->uploadFile($_FILES['photowatermark']['tmp_name'],$path["docroot"]."_files/photogallery",$photo_watermark);
				$oSystem->setValue("photo_watermark",$photo_watermark);
			}
		}	
	} else {
		echo"<script>alert(\"".$lang['photogallery']['extwarning4']."\");</script>";
	}
  }
  
  $status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['prefsave']."<br>";	  
}
if($pageaction=="default"){
  $oSystem->setValue("photo_page",$path["webroot"]."photogallery/samplephotogallery.php");
  $oSystem->setValue("photo_enableimageverify","");
  $oSystem->setValue("photo_sefurl","");
  $oSystem->setValue("photo_pageno","9");
  $oSystem->setValue("photo_rowno","3");
  $oSystem->setValue("photo_seq","desc");
  $oSystem->setValue("photo_style","1");
  $oSystem->setValue("photo_listno","10");
  $oSystem->setValue("photo_popup","No");  
  $oSystem->setValue("photo_showrating","Yes");
  $oSystem->setValue("photo_limitrating","Yes");
  $oSystem->setValue("photo_showcomment","Yes");  
  $oSystem->setValue("photo_thumbsize","120"); 
  $oSystem->setValue("photo_resize","400");  
  $oSystem->setValue("photo_slideshow","Yes"); 
  $oSystem->setValue("photo_slideshowtime","3"); 
  $oSystem->setValue("photo_latestblock","Yes"); 
  $oSystem->setValue("photo_latestno","1"); 
  $oSystem->setValue("photo_randomblock","Yes"); 
  $oSystem->setValue("photo_randomno","1"); 
  $oSystem->setValue("photo_submitphoto","Yes"); 
  $oSystem->setValue("photo_memberlogin","Yes");   
  $oSystem->setValue("photo_copyright","Copyrighted by Your Company Name"); 
  $oSystem->setValue("photo_red","0"); 
  $oSystem->setValue("photo_green","0"); 
  $oSystem->setValue("photo_blue","0"); 
  $oSystem->setValue("photo_gd","20"); 
  $oSystem->setValue("photo_showpubdate","Yes"); 
  $oSystem->setValue("photo_showhits","Yes"); 
  $oSystem->setValue("photo_subcatno","3"); 
  $oSystem->setValue("photo_showcatdescp","Yes");
  $oSystem->setValue("photo_searchno","10");
 
  if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
	  if(file_exists($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")) && $oSystem->getValue("photo_watermark")!=""){ @unlink($path["docroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")); 	}
  }
  $oSystem->setValue("photo_watermark",""); 
  $oSystem->setValue("photo_wmlocation","middle"); 
  
  $oSystem->setValue("photo_reviewlist","5");
   
  $status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['prefrestore']."<br>";	  
}

if($oSystem->getValue("photo_watermark")!=""){ $vphoto_watermark = "<a href=\"".$path["webroot"]."_files/photogallery/".$oSystem->getValue("photo_watermark")."\" onclick=\"return popImage('_files/photogallery/".$oSystem->getValue("photo_watermark")."','Image')\">".$lang['photogallery']['viewimage']."</a> | <a href=javascript:RemoveS();>".$lang['photogallery']['delimage']."</a>";}
echo"<table border=0 width=100%><tr><td><b>".$lang['photogallery']['photogallery']."</b></td><td>";
include("wce.menu.php");
echo"
	</td></tr></table>
	<hr size=1 color=#606060>$status_message<br>

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=\"index.php?component=$component&page=$page\" method=post enctype=\"multipart/form-data\">
<input type=hidden name=pageaction>
<tr><td valign=top>
	<table border=0 width=100%>
	<tr><td colspan=4><b><u>".$lang['photogallery']['adminsetting']."</u></b></td></tr>
	<tr><td width=\"38%\">".$lang['photogallery']['listno']."</td><td><input type=text name=\"photo_listno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_listno")."\"></td></tr>
	<tr><td colspan=4><br><b><u>".$lang['photogallery']['frontendsetting']."</u></b></td></tr>
	<tr><td>".$lang['photogallery']['showrandblock']."</td><td><input type=checkbox name=\"photo_randomblock\" value=\"Yes\" ".($oSystem->getValue("photo_randomblock")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['randno']."</td><td><input type=text name=\"photo_randomno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_randomno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['showlatestblock']."</td><td><input type=checkbox name=\"photo_latestblock\" value=\"Yes\" ".($oSystem->getValue("photo_latestblock")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['latestno']."</td><td><input type=text name=\"photo_latestno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_latestno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['memberlogin']."</td><td><input type=checkbox name=\"photo_memberlogin\" value=\"Yes\" ".($oSystem->getValue("photo_memberlogin")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['galleryseq']."</td><td><select name=\"photo_seq\"><option value=\"asc\" ".($oSystem->getValue("photo_seq")=="asc"?"selected":"").">".$lang['photogallery']['ascending']."</option><option value=\"desc\" ".($oSystem->getValue("photo_seq")=="desc"?"selected":"").">".$lang['photogallery']['descending']."</option></select></td></tr>
	<tr><td>".$lang['photogallery']['enablesubmit']."</td><td><input type=checkbox name=\"photo_submitphoto\" value=\"Yes\" ".($oSystem->getValue("photo_submitphoto")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['pageno']."</td><td><input type=text name=\"photo_pageno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_pageno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['popup']."</td><td><input type=checkbox name=\"photo_popup\" value=\"Yes\" ".($oSystem->getValue("photo_popup")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['rowno']."</td><td><input type=text name=\"photo_rowno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_rowno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['enableslide']."</td><td><input type=checkbox name=\"photo_slideshow\" value=\"Yes\" ".($oSystem->getValue("photo_slideshow")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['interval']."</td><td><input type=text name=\"photo_slideshowtime\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_slideshowtime")."\"> ".$lang['photogallery']['second']."</td></tr>
	<tr><td>".$lang['photogallery']['showcatdescp']."</td><td><input type=checkbox name=\"photo_showcatdescp\" value=\"Yes\" ".($oSystem->getValue("photo_showcatdescp")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['subcatno']."</td><td><input type=text name=\"photo_subcatno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_subcatno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['enablecomm']."</td><td><input type=checkbox name=\"photo_showcomment\" value=\"Yes\" ".($oSystem->getValue("photo_showcomment")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['reviewno']."</td><td><input type=text name=\"photo_reviewlist\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_reviewlist")."\"></td></tr>
	<tr><td>".$lang['photogallery']['showhits']."</td><td><input type=checkbox name=\"photo_showhits\" value=\"Yes\" ".($oSystem->getValue("photo_showhits")=="Yes"?"checked":"")."></td><td>".$lang['photogallery']['searchno']."</td><td><input type=text name=\"photo_searchno\" style=\"width:35px\" value=\"".$oSystem->getValue("photo_searchno")."\"></td></tr>
	<tr><td>".$lang['photogallery']['enablerate']."</td><td><input type=checkbox name=\"photo_showrating\" value=\"Yes\" ".($oSystem->getValue("photo_showrating")=="Yes"?"checked":"")."></td><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>".$lang['photogallery']['limitrate']."</td><td><input type=checkbox name=\"photo_limitrating\" value=\"Yes\" ".($oSystem->getValue("photo_limitrating")=="Yes"?"checked":"")."></td><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr><td>".$lang['photogallery']['showpubdate']."</td><td><input type=checkbox name=\"photo_showpubdate\" value=\"Yes\" ".($oSystem->getValue("photo_showpubdate")=="Yes"?"checked":"")."></td><td>&nbsp;</td><td>&nbsp;</td></tr>
	</table><br>	
</td></tr>
<tr><td valign=top>
	<table border=0 width=100%>
	<tr><td width=30%>".$lang['photogallery']['frontendurl']."</td><td><input type=text name=frontendurl style=\"width:400px\" value=\"".$oSystem->getValue("photo_page")."\"></td></tr>
	<tr><td>".$lang['photogallery']['imgverify']."</td><td><input type=checkbox name=imgverify value=\"Yes\" ".($oSystem->getValue("photo_enableimageverify")=="Yes"?"checked":"")."> * ".$lang['photogallery']['imgverifynote']."</td></tr>
	<tr><td>".$lang['photogallery']['sefurl']."</td><td><input type=checkbox name=sefurl value=\"Yes\" onclick=\"javascript:showcode(this);\" ".($oSystem->getValue("photo_sefurl")=="Yes"?"checked":"")."> * ".$lang['photogallery']['sefurlnote']."</td></tr>
	";
		$pattern="/\/(.[^\/]*\.php)/";
		preg_match($pattern, $oSystem->getValue("photo_page"), $feurl);
		$rewritecode = "Options +FollowSymLinks\n"
			."RewriteEngine on\n"
			."RewriteRule ^cat-(.[^\/]*)?-(.[^\/]*)?.html\$ $feurl[1]?category_id=\$1&parent_id=\$2\n"
			."RewriteRule ^photo-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)?.html\$ $feurl[1]?category_id=\$1&parent_id=\$2&photo_id=\$3&start=\$4&countdisplay=\$5\n"
			."RewriteRule ^slideshowplay-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)-(.[^\/]*)?.html\$ $feurl[1]?category_id=\$1&parent_id=\$2&photo_id=\$3&pageaction=slideshowplay&start=\$4\n"
			."RewriteRule ^navigate-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?.html\$ $feurl[1]?category_id=\$1&parent_id=\$2&start=\$3\n"
			."RewriteRule ^search-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)?.html\$ $feurl[1]?pageaction=\$1&start=\$2&searchby=\$3&seachcat=\$4&searchkey=\$5\n"
			."RewriteRule ^photosearch-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)?-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)-(.[^\/]*)?.html\$ $feurl[1]?category_id=\$1&parent_id=\$2&photo_id=\$3&countdisplay=\$4&frompage=\$5&start=\$6&searchby=\$7&seachcat=\$8&searchkey=\$9\n"
			;	
		echo "<tr><td></td><td><div id=rewritecode ";
		echo $oSystem->getValue("photo_sefurl")=="Yes"?"":"style=\"display:none\"";
		echo ">".$lang['photogallery']['htaccess']."<br /><textarea id=rulearea style=\"width:400px;height:150px\" readonly>$rewritecode</textarea></div></td></tr>";
	
	echo"
	</table><br>	
</td></tr>
<tr><td valign=top><b><u>".$lang['photogallery']['photodetail']."</u></b>
	<table border=0 cellpadding=5 width=70%>
	<tr><td align=center><img src=\"photogallery/image/style1.gif\" style=\"border: 1px solid\"></td><td align=center><img src=\"photogallery/image/style2.gif\" style=\"border: 1px solid\"></td></tr>
	<tr><td align=center><input type=radio name=\"photo_style\" value=\"1\" ".($oSystem->getValue("photo_style")=="1"?"checked":"")."> ".$lang['photogallery']['sidestyle']."</td><td align=center><input type=radio name=\"photo_style\" value=\"2\" ".($oSystem->getValue("photo_style")=="2"?"checked":"")."> ".$lang['photogallery']['bottomstyle']."</td></tr>
	</table><br>
	
</td></tr>
<tr><td valign=top>
	&nbsp;<b><u>".$lang['photogallery']['imagesetting']."</u></b><br>
	<table border=0 width=50% cellpadding=3>
	<tr><td>".$lang['photogallery']['thumbsize']."</td><td><input type=text name=\"photo_thumbsize\" style=\"width:40px\" value=\"".$oSystem->getValue("photo_thumbsize")."\"> ".$lang['photogallery']['pixels']."</td></tr>
	<tr><td>".$lang['photogallery']['enlargesize']."</td><td><input type=text name=\"photo_resize\" style=\"width:40px\" value=\"".$oSystem->getValue("photo_resize")."\"> ".$lang['photogallery']['pixels']."</td></tr>
	</table><br>

	&nbsp;<b><u>".$lang['photogallery']['gdsetting']."</u></b><br>
	<table border=0 width=100% cellpadding=3>
	<tr><td width=30%>".$lang['photogallery']['gdversion']."</td><td><select name=\"photo_gd\"><option value=\"20\" ".($oSystem->getValue("photo_gd")=="20"?"selected":"").">".$lang['photogallery']['v20']."</option><option value=\"16\" ".($oSystem->getValue("photo_gd")=="16"?"selected":"").">".$lang['photogallery']['v162']."</option><option value=\"None\" ".($oSystem->getValue("photo_gd")=="None"?"selected":"").">".$lang['photogallery']['vnone']."</option></select></td></tr>
	<tr><td colspan=2>* ".$lang['photogallery']['gdmessage']."</td></tr>
	</table><br>
	
	&nbsp;<b><u>".$lang['photogallery']['copysetting']."</u></b><br>
	<table border=0 width=100% cellpadding=3>
	<tr><td valign=top>".$lang['photogallery']['copytext']."&nbsp;&nbsp;<input type=text name=\"photo_copyright\" style=\"width:230px\" value=\"".stripslashes(htmlentities($oSystem->getValue("photo_copyright")))."\"></td>
		<td valign=top align=right>".$lang['photogallery']['copycolor'].": ".$lang['photogallery']['copyred']." <input type=text name=photo_red style=\"width:30px\" value=\"".$oSystem->getValue("photo_red")."\">
		".$lang['photogallery']['copygreen']." <input type=text name=photo_green style=\"width:30px\" value=\"".$oSystem->getValue("photo_green")."\">
		".$lang['photogallery']['copyblue']." <input type=text name=photo_blue style=\"width:30px\" value=\"".$oSystem->getValue("photo_blue")."\"></td>
	</tr>	
	</table><br>
	
	&nbsp;<b><u>".$lang['photogallery']['wmsetting']."</u></b><br>
	<table border=0 width=100% cellpadding=3>
	<tr><td width=30%>".$lang['photogallery']['wmlocation']."</td><td><select name=\"photo_wmlocation\"><option value=\"middle\" ".($oSystem->getValue("photo_wmlocation")=="middle"?"selected":"").">".$lang['photogallery']['middle']."</option>
	<option value=\"topleft\" ".($oSystem->getValue("photo_wmlocation")=="topleft"?"selected":"").">".$lang['photogallery']['topleft']."</option>
	<option value=\"topright\" ".($oSystem->getValue("photo_wmlocation")=="topright"?"selected":"").">".$lang['photogallery']['topright']."</option>
	<option value=\"middleright\" ".($oSystem->getValue("photo_wmlocation")=="middleright"?"selected":"").">".$lang['photogallery']['middleright']."</option>
	<option value=\"middleleft\" ".($oSystem->getValue("photo_wmlocation")=="middleleft"?"selected":"").">".$lang['photogallery']['middleleft']."</option>
	<option value=\"bottomright\" ".($oSystem->getValue("photo_wmlocation")=="bottomright"?"selected":"").">".$lang['photogallery']['bottomright']."</option>
	<option value=\"bottomleft\" ".($oSystem->getValue("photo_wmlocation")=="bottomleft"?"selected":"").">".$lang['photogallery']['bottomleft']."</option>
	<option value=\"middletop\" ".($oSystem->getValue("photo_wmlocation")=="middletop"?"selected":"").">".$lang['photogallery']['middletop']."</option>
	<option value=\"bottommiddle\" ".($oSystem->getValue("photo_wmlocation")=="bottommiddle"?"selected":"").">".$lang['photogallery']['bottommiddle']."</option>
	</select></td></tr>
	<tr><td >".$lang['photogallery']['wmphoto']."</td><td><input type=file name=\"photowatermark\" style=\"width:250px;\">&nbsp;&nbsp;".$vphoto_watermark."&nbsp;&nbsp;</td></tr>
	<tr><td colspan=2>".$lang['photogallery']['watermarktips']."</td></tr>	
	</table><br>
</td></tr>
<tr><td valign=top>	
	<input type=button value=\" ".$lang['photogallery']['btnsave']." \" onclick=\"document.thisform.pageaction.value='save';document.thisform.submit();\">
	<input type=button value=\"".$lang['photogallery']['btndefault']."\" onclick=\"document.thisform.pageaction.value='default';document.thisform.submit();\">
</td></tr>
</form></table>
";

?>

<script type="text/javascript">
	function showcode(x){
		if (x.checked == true){
			document.getElementById('rewritecode').style.display = "";
			document.getElementById('rulearea').select();
		}
		else
			document.getElementById('rewritecode').style.display = "none";
	}
	function RemoveS(){
		window.location='index.php?component=photogallery&page=wce.preference.php&pageaction=removes';
	}
</script>