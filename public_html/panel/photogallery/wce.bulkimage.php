<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:36 28.12.2006                  */
/**************************************************/


$maxsize = ini_get('upload_max_filesize');
$upload_max = (!empty($maxsize)?substr($maxsize,0,-1)."000000":"");

function getPix($tar_dir){
	$filenames=array();
	$handle = opendir($tar_dir); 
	while($file = readdir($handle)){ 
	if (!is_dir($file)){$filenames[] = $file; } 
	}// end while 
	closedir($handle);
	usort($filenames, "cmp"); 
	return $filenames;
} 
		
function cmp($a,$b){
	return strcasecmp($a, $b); 
}
		
echo"
	<html>
	<head><title>".$lang['photogallery']['uploadtool']."</title>
	<link rel=stylesheet type=text/css href=\"".$path["webroot"].$path["skin"]."style.css\">
	</head>
	<script src=\"common/lib.jsfunction.php\" language=javascript></script>
	<script src=\"common/lib.function.php\" language=javascript></script>
	<body topmargin=0 leftmargin=0>
	<br>
";

if ($frompage=="uploadzip"){
	echo"
		<table border=0 cellspacing=2 cellpadding=2 width=95% align=center>
			<tr><td valign=top colspan=2><b><u>".$lang['photogallery']['browsezip']."</u></b></td></tr>
			<tr><td valign=top colspan=2>".$lang['photogallery']['uploadtooltip']."<br><br></td></tr>
			<form name=thisform action=\"index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no\" method=post enctype=\"multipart/form-data\">
			<input type=hidden name=pageaction value=uploadzip>
			<input type=hidden name=category_id value=\"$category_id\">
			<input type=hidden name=setdate value=\"$setdate\">
			<input type=hidden name=settitle value=\"$settitle\">
			<input type=hidden name=setexif value=\"$setexif\">
			<input type=hidden name=setnew value=\"$setnew\">
			<input type=hidden name=setthumbnail value=\"$setthumbnail\">
			<input type=hidden name=setresize value=\"$setresize\">
			<input type=hidden name=setcopyright value=\"$setcopyright\">
			<input type=hidden name=setwatermark value=\"$setwatermark\">
			
			<tr><td valign=top width=20%>Browse for ZIP File</td><td valign=top>".(!empty($upload_max)?"<input type=hidden name=MAX_FILE_SIZE value=$upload_max>":"")."<input type=file name=zipfile class=txtNormal size=35></td></tr>
			<tr><td></td><td valign=top>". $lang['photogallery']['zipmessage']." ".(!empty($upload_max)?"<br>* ".$lang['photogallery']['setnew']." $upload_max":"")."</td></tr>
			<tr><td valign=top colspan=2><br><input type=button value=\"".$lang['photogallery']['btnuploadzip']."\" onclick=javascript:startCopy();></td></tr>
		</form>
		</table><br><br>
	";
} elseif ($frompage=="ftpupload"){
	echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=ftpreadfile&category_id=$category_id&setdate=$setdate&settitle=$settitle&setexif=$setexif&setnew=$setnew&setthumbnail=$setthumbnail&setresize=$setresize&setcopyright=$setcopyright&setwatermark=$setwatermark\">";
}

if ($frompage==""){
	if ($pageaction=="uploadzip" || $pageaction=="readfile"){
		$uploadtitle=$lang['photogallery']['uploadzip'];
	} else if ($pageaction=="ftpreadfile"){
		$uploadtitle=$lang['photogallery']['ftpupload'];
	}
	echo"
	";
	if ($pageaction=="uploaddone" || $pageaction=="ftpuploaddone"){
	} else { 
		echo"
			<table border=0 cellspacing=2 cellpadding=2 width=95% align=center><tr><td><b>$uploadtitle</b>&nbsp;</td></tr></table>
			<hr size=1 color=#000000 width=95% align=center><br>
			<table border=0 cellspacing=2 cellpadding=2 width=95% align=center><tr><td height=150 valign=top>
			<font color=0000ff><b>".$lang['photogallery']['uploadprogress']."</b></font><br><br>
		"; 
	}
}

switch($pageaction){
	case "uploadzip":
		if (strtolower(strrchr($_FILES['zipfile']['name'],"."))== ".zip"){	// check file extention
			if(($_FILES['zipfile']['size']<$upload_max || empty($upload_max)) && !empty($_FILES['zipfile']['size'])){
				if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
					$tar_dir = $path["docroot"]."_files/photogallery_temp";
					$tar_dir1 = "_files/photogallery_temp";
				
					$zipfilename = str_replace(" ","_",trim($_FILES['zipfile']['name']));
					if (is_uploaded_file($_FILES['zipfile']['tmp_name'])) {    $oPhoto_Gallery->uploadFile($_FILES['zipfile']['tmp_name'], $tar_dir, $zipfilename);	}
				
					include($path["docroot"]."common/class.pclzip.php");
					$oArchive = new PclZip("$tar_dir1/$zipfilename");
					$oArchive->extract($tar_dir1);
					@unlink("$tar_dir/$zipfilename");
					
					echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=readfile&category_id=$category_id&setdate=$setdate&settitle=$settitle&setexif=$setexif&setnew=$setnew&setthumbnail=$setthumbnail&setresize=$setresize&setcopyright=$setcopyright&setwatermark=$setwatermark\">"; 
				}
			}
		}
	break;
	case "readfile":
	case "ftpreadfile":
		if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			if ($pageaction=="readfile"){
				$tar_dir = $path["docroot"]."_files/photogallery_temp";
			} else {
				$tar_dir = $path["docroot"]."_files/photogallery_ftp";
			}
			
		 	if(!$tar_dir)$tar_dir="."; 
		 	$data = getPix($tar_dir);
		
			if (!$cycle) $cycle = 0; $next = $cycle+1;
			if ($cycle >=0 && $cycle < sizeof($data)) {
				$filename = $data[$cycle]; 
				$filename1=$filename;
				echo "".$lang['photogallery']['filename']."&nbsp;:&nbsp;".$filename;
				if ($oPhoto_Gallery->IsPicture($filename)){	// check whether is picture or not		
					if($settitle=="Yes"){ $title=substr($filename,0,strrpos($filename,"."));  }else{ $title=""; }
		
					$filename = strtolower(str_replace(" ","_",trim($filename)));
					$uniqueid = md5(uniqid(rand(), true));	
					$uniqueid=substr($uniqueid,0,5);
					$filename2 = $uniqueid."_".$filename;
		
					$oPhoto_Gallery->copyFile($tar_dir."/".$filename1,$path["docroot"]."_files/photogallery",$filename2);
					$publish = $setdate;
		
					if($setnew=="Yes"){ $newphoto="Yes"; }else{ $newphoto=""; }
		
					if($setexif=="Yes"){ 
						$ext = strrchr($filename2,".");
						if($ext==".jpg"){			
							$exif = @read_exif_data($path["docroot"]."_files/photogallery/$filename2");
							$descpmore = "";
							while(list($k,$v)=each($exif)) {   $descpmore .= "$k: $v<br>\n";	 }
						}
					}
				
					$ext = strrchr($filename,".");
					if ($oSystem->getValue("photo_gd")=="20"){
						if($ext==".jpg" || $ext==".gif"){$valid_ext="yes"; } else { $valid_ext="no";}
					} else if ($oSystem->getValue("photo_gd")=="16") {
						if($ext==".jpg"){$valid_ext="yes";} else {$valid_ext="no";	}
					} else {$valid_ext="no";}
						
					if($setthumbnail=="Yes" && $oSystem->getValue("photo_gd")!="None"){ 
						if($valid_ext=="yes"){	
							$thumbnail2=$uniqueid."_t_".$filename;
							$src_file = $path["docroot"]."_files/photogallery/".$filename2;
							$dest_file = $path["docroot"]."_files/photogallery/".$thumbnail2;
							$thumbw = $oSystem->getValue("photo_thumbsize");
							$thumbh = $oSystem->getValue("photo_thumbsize");
							$oPhoto_Gallery->resizeImage($src_file,$dest_file,$thumbw,$thumbh,"85");
						}
					}
		
					if($setresize=="Yes" && $oSystem->getValue("photo_gd")!="None"){
						if($valid_ext=="yes"){	
							$src_file = $path["docroot"]."_files/photogallery/".$filename2;
							$dest_file = $path["docroot"]."_files/photogallery/".$filename2;
							$resizew = $oSystem->getValue("photo_resize");
							$resizeh = $oSystem->getValue("photo_resize");
							if($setcopyright!="Yes"){
								$oPhoto_Gallery->resizeImage($src_file,$dest_file,$resizew,$resizeh,"85");
							}else{
								$oPhoto_Gallery->resizeImage($src_file,$dest_file,$resizew,$resizeh,"100");
							}
						}
					}
					
					/*** water mark ***/
					if($setwatermark=="Yes" && $filename2!="" && $oSystem->getValue("photo_gd")=="20" && extension_loaded('gd')){
						$ext = strrchr($filename2,".");
						if($ext==".jpg"){
							if ($oSystem->getValue("photo_watermark")!=""){
								$src_file = $path["docroot"]."_files/photogallery/".$filename2;
								$oPhoto_Gallery->createWaterMark($src_file);	
							} 
						}
					}
		
					if($setcopyright=="Yes" && $oSystem->getValue("photo_gd")!="None"){
						if($valid_ext=="yes"){
							$src_file = $path["docroot"]."_files/photogallery/".$filename2;				
							$text = $oSystem->getValue("photo_copyright");
							$fontsize = 15; $x = 10; $y = 20; 
							$red = $oSystem->getValue("photo_red");
							$green = $oSystem->getValue("photo_green");
							$blue = $oSystem->getValue("photo_blue");		
							$oPhoto_Gallery->copyrightImage($src_file,$text,$fontsize,$x,$y,$red,$green,$blue);	
						}
					}	
		
					$descp = str_replace("\r\n","<br>",$descp);
					$descpmore = str_replace("\r\n","<br>",$descpmore);
		
					$sequence = $oPhoto_Gallery->getLastSequence()+1;	
					$publish = $publish==""?"0000-00-00":$publish;
					$oPhoto_Gallery->data = array("category_id","title","publish","descp","descpmore","thumbnail","photo","downloadfile","showphoto","newphoto","hit","sequence");
					$oPhoto_Gallery->value = array($category_id,addslashes($title),$publish,addslashes($descp),addslashes($descpmore),$thumbnail2,$filename2,$downloadfile,"Yes",$newphoto,"0",$sequence);
					$oPhoto_Gallery->add();
				}
				
				if ($pageaction=="readfile"){
					echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=readfile&category_id=$category_id&setdate=$setdate&settitle=$settitle&setexif=$setexif&setnew=$setnew&setthumbnail=$setthumbnail&setresize=$setresize&setcopyright=$setcopyright&setwatermark=$setwatermark&cycle=$next&tar_dir=$tar_dir\">";
				} else {
					echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=ftpreadfile&category_id=$category_id&setdate=$setdate&settitle=$settitle&setexif=$setexif&setnew=$setnew&setthumbnail=$setthumbnail&setresize=$setresize&setcopyright=$setcopyright&setwatermark=$setwatermark&cycle=$next&tar_dir=$tar_dir\">"; 		
				}
			} else {
				if ($pageaction=="readfile"){
					echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=uploaddone\">"; 
				} else {
					echo "<meta http-equiv=\"Refresh\" Content=\"0; URL=index.php?component=photogallery&page=wce.bulkimage.php&headfoot=no&pageaction=ftpuploaddone\">"; 
				}
			}	
		}	
	break;
	case "uploaddone":
	case "ftpuploaddone":
		if (!isset($_REQUEST['path']) && !isset($_GET['path']) && !isset($_POST['path'])) {
			if ($pageaction=="uploaddone"){
				$tar_dir = $path["docroot"]."_files/photogallery_temp";
				$uploadtitle=$lang['photogallery']['uploadzip'];
			} else {
				$tar_dir = $path["docroot"]."_files/photogallery_ftp";
				$uploadtitle=$lang['photogallery']['ftpupload'];
			}
					
			echo "
				<table border=0 cellspacing=2 cellpadding=2 width=95% align=center><tr><td><b>$uploadtitle</b>&nbsp;</td></tr></table>
				<hr size=1 color=#000000 width=95% align=center><br>
				<table border=0 cellspacing=2 cellpadding=2 width=95% align=center><tr><td valign=top>
			";
			
			// Log file to record which items file successfully upload to server.
			echo"
				<table border=0 cellspacing=0 cellpadding=0 width=100% align=center>
				<tr><td colspan=2>
					<table border=0 cellspacing=2 cellpadding=2 width=98% align=center>
					".($pageaction=="uploaddone"?"<tr><td colspan=2><font color=0000ff><b>".$lang['photogallery']['uploadcomplete']."</b></font><br><br></td></tr>":"<tr><td colspan=2><font color=0000ff><b>".$lang['photogallery']['ftpcomplete']."</b></font><br><br></td></tr>")."
					<tr><td><b>".$lang['photogallery']['file']."<b></td><td><b>".$lang['photogallery']['status']."</b></td></tr>
			";
			$successcount = 0; $failcount = 0;
			$mydir = @opendir($tar_dir) ; 
			while($filename = @readdir($mydir)){
				if ($filename != "." && $filename != ".." && !is_dir($tar_dir."/".$filename)){
					  if ($oPhoto_Gallery->IsPicture($filename)){	// check whether is picture or not
						echo"<tr><td>$filename</td><td>".$lang['photogallery']['success']."</td></tr>";	
						$successcount++;
						
						@unlink($tar_dir."/".$filename);
				  	  }else {@unlink($tar_dir."/".$filename);	echo"<tr><td>$filename</td><td><font color=red>".$lang['photogallery']['fail']."</font></td></tr>"; $failcount++; }	// else add action when file is not picture		
				}elseif (is_dir($tar_dir."/".$filename) && $filename != "." && $filename != ".." ){
					echo"<tr><td>".$lang['photogallery']['directory']."&nbsp;$filename</td><td><font color=red>".$lang['photogallery']['fail']."</font></td></tr>";
					$failcount++;
					$oPhoto_Gallery->deleteDir($tar_dir."/".$filename);
				} 
			}
			@closedir($mydir);
			
			echo"
				<tr><td colspan=2>&nbsp;<br><br></td></tr>
				<tr><td valign=top colspan=2>".$lang['photogallery']['totalsuccess']."&nbsp;:&nbsp;$successcount</td></tr>	
				<tr><td valign=top colspan=2>".$lang['photogallery']['totalfail']."&nbsp;:&nbsp;$failcount<br></td></tr>	
				<tr><td valign=top colspan=2>".$lang['photogallery']['datetime']."&nbsp;:&nbsp;".date("Y-m-d h:m:s")."</td></tr>	
			";
			echo"</table></td></tr></table></td></tr><tr><td valign=top>&nbsp;";
		}
	break;
}

if ($frompage==""){ echo"</td></tr></table>"; }

echo "<table border=0 cellspacing=2 cellpadding=2 width=95% align=center><tr><td align=right><input type=button value=\" ".$lang['photogallery']['close']." \" onclick=\"window.close()\">&nbsp;</td></tr></table>";
?>
<br><br>
<script language=javascript>
	function startCopy(){
		if (document.thisform.zipfile.value==""){
			alert('<?echo $lang['photogallery']['plsuploadfile'] ?>');
			return;
		}
		document.thisform.submit();
	}
</script>
</body>
</html>