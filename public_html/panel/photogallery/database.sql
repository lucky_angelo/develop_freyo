#Photo Gallery 4.1

CREATE TABLE photo_category (
  category_id int(10) NOT NULL auto_increment,
  parent_id int(10) default NULL,
  name varchar(200) default NULL,
  descp text,
  showprivate varchar(5) default NULL,
  sequence int(5),  
  PRIMARY KEY  (category_id)
) ENGINE=MyISAM;

INSERT INTO photo_category VALUES(NULL,0,'Category A','Description for Category A','','1');
INSERT INTO photo_category VALUES(NULL,0,'Category B','Description for Category B','','2');
INSERT INTO photo_category VALUES(NULL,0,'Category C','Description for Category C','','3');

CREATE TABLE photo_gallery (
  photo_id int(10) NOT NULL auto_increment,
  category_id int(10) default NULL,
  title text,
  publish date default '0000-00-00',
  descp text,
  descpmore text,
  author varchar(200) default NULL,
  thumbnail varchar(100) default NULL,
  photo varchar(100) default NULL,
  downloadfile varchar(50) default NULL,
  showphoto varchar(20) default NULL,
  newphoto varchar(5) default NULL,
  hit int(10) default '0',
  status varchar(30) default NULL,
  sequence int(5),
  PRIMARY KEY  (photo_id)
) ENGINE=MyISAM;

CREATE TABLE photo_rating (
  photo_id int(10) default NULL,
  rating int(10) default NULL
) ENGINE=MyISAM;

CREATE TABLE photo_member (
  member_id int(10) NOT NULL auto_increment,
  name varchar(200) default NULL,
  email varchar(200) default NULL,
  username varchar(30) default NULL,
  password varchar(30) default NULL,
  PRIMARY KEY  (member_id)
) ENGINE=MyISAM;

CREATE TABLE photo_access (
  member_id int(10) default NULL,
  category_id int(10) default NULL
) ENGINE=MyISAM;

CREATE TABLE photo_review (
  review_id int(10) NOT NULL auto_increment,
  photo_id int(10) default NULL,
  date_poster date default '0000-00-00',
  author varchar(50) default NULL,
  reviewcontent text,
  status varchar(10) default NULL,
  PRIMARY KEY  (review_id)
) ENGINE=MyISAM;

INSERT INTO photo_access VALUES('','');

INSERT INTO sys_component VALUES (NULL, 'Photo Gallery', 'photogallery', 'photo', 'Multi level categories, navigation bar, enlarge photo, hits, ratings, comments, auto creation of thumbnails, auto resize, bulk uploads with ZIP file or upload form. ', '4.1', 'wce.addphoto.php', '', 'Show','');
INSERT INTO sys_variable VALUES ('photo_listno', '10');
INSERT INTO sys_variable VALUES ('photo_pageno', '9');
INSERT INTO sys_variable VALUES ('photo_rowno', '3');
INSERT INTO sys_variable VALUES ('photo_seq', 'desc');
INSERT INTO sys_variable VALUES ('photo_searchno', '10');
INSERT INTO sys_variable VALUES ('photo_subcatno', '3');
INSERT INTO sys_variable VALUES ('photo_showcatdescp', 'Yes');
INSERT INTO sys_variable VALUES ('photo_style', '1');
INSERT INTO sys_variable VALUES ('photo_popup', 'No');
INSERT INTO sys_variable VALUES ('photo_showrating', 'Yes');
INSERT INTO sys_variable VALUES ('photo_showcomment', 'Yes');
INSERT INTO sys_variable VALUES ('photo_limitrating', 'Yes');
INSERT INTO sys_variable VALUES ('photo_thumbsize', '120');
INSERT INTO sys_variable VALUES ('photo_resize', '400');
INSERT INTO sys_variable VALUES ('photo_slideshow', 'Yes');
INSERT INTO sys_variable VALUES ('photo_slideshowtime', '3');
INSERT INTO sys_variable VALUES ('photo_latestblock', 'Yes');
INSERT INTO sys_variable VALUES ('photo_latestno', '1');
INSERT INTO sys_variable VALUES ('photo_randomblock', 'Yes');
INSERT INTO sys_variable VALUES ('photo_randomno', '1');
INSERT INTO sys_variable VALUES ('photo_memberlogin', 'Yes');
INSERT INTO sys_variable VALUES ('photo_submitphoto', 'Yes');
INSERT INTO sys_variable VALUES ('photo_copyright', 'Copyrighted by Your Company Name');
INSERT INTO sys_variable VALUES ('photo_red', '0');
INSERT INTO sys_variable VALUES ('photo_green', '0');
INSERT INTO sys_variable VALUES ('photo_blue', '0');
INSERT INTO sys_variable VALUES ('photo_gd', '20');
INSERT INTO sys_variable VALUES ('photo_forgetsubject', 'Your login information at YourDomain.com');
INSERT INTO sys_variable VALUES ('photo_forgetbody', 'Dear [[name]],\n\nThank you for contacting us. \n\nBelow are your account information:\n\nUsername : [[username]]\nPassword : [[password]]\n\nPlease keep us informed if you need any of our assistance.\n\n\nRegards\nYourDomain.com\n');
INSERT INTO sys_variable VALUES ('photo_commentapprove', 'Yes');
INSERT INTO sys_variable VALUES ('photo_commentsubject', 'New Comment Posted To Your Photo');
INSERT INTO sys_variable VALUES ('photo_commentbody', 'We would like to notify you about new submission comment approval by you. \n\nBelow is the comment information:\n\nID: [[photoid]]\nPhoto: [[photo]]\nCategory: [[category]]\nDate Posted: [[datepost]]\nAuthor Name : [[author]]\nComment : [[comment]]\n\nRegards,\nYourDomain.com\n');
INSERT INTO sys_variable VALUES ('photo_photoapprove', 'Yes');
INSERT INTO sys_variable VALUES ('photo_photosubject', 'New Photo Submitted To Your Gallery');
INSERT INTO sys_variable VALUES ('photo_photobody', 'We would like to notify you about new photo submission.\n\nBelow is the new photo information:\n\nID: [[photoid]]\nName : [[name]]\nEmail : [[email]]\nDate Posted: [[datepost]]\nTitle : [[title]]\nCategory : [[category]]\nBrief Description : [[briefdescp]]\nDetail Description : [[moredescp]]\nPhoto : [[photolink]]\n\nRegards,\nYourDomain.com\n');
INSERT INTO sys_variable VALUES ('photo_showpubdate', 'Yes');
INSERT INTO sys_variable VALUES ('photo_showhits', 'Yes');
INSERT INTO sys_variable VALUES ('photo_watermark', '');
INSERT INTO sys_variable VALUES ('photo_wmlocation', 'middle');
INSERT INTO sys_variable VALUES ('photo_reviewlist', '5');
INSERT INTO sys_variable VALUES ('photo_adminemail', '');