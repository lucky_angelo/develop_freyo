Photo Gallery 4.1
========================

You can choose to download only the Photo Gallery component or the complete Panel Admin Suite. 
Before installing, we recommend you to take a look at the System Requirements.

System Requirements
-------------------

Script Type 			: PHP Server Side Scripting 

Operating System 		: Linux or Windows Server 

Web Server 			: Apache or IIS 

PHP Version 			: PHP 4.2 or above 

PHP Configuration 		: ZLib library *
				: GD 1.6.2 , GD 2.0 library or higher *
				: Enable EXIF * 
						
Database 			: MySQL 

Web Browser 			: Internet Explorer 5 or above, Firefox, Mozilla, Javascript and cookie enable 

Computer Hardware 		: PC and Mac 


* ZLib library is required to decrypt our encrypted codes. We recommend PHP 4.2 and above as the lower 
  versions will results in buffer handling error.

* GD library is required for Auto Thumbnail Creation and Auto Image Resize features. GIF format is now 
  supported in GD 2.0.28, but previous versions only supports JPEG.

* Enable EXIF is not necessary, but only if you wish to use the image EXIF data during Bulk Uploading.



Installation Guides
-------------------

1. Download the Photo Gallery distribution ZIP file. 

2. Extract and upload the entire folder to your website under your domain name. 

	Example : http://your_domain_name.com/panel/ 
		
 
3. For Linux system, set the permission for the _files folder and config.php file to Read, Write, 
   Execute for all parties.


	Linux command example : chmod 777 _files , chmod 777 config.php
		
		
4. To access the Installation Wizard, visit the URL with your web browser. Follow the instructions in 
   the Installation Wizard. 

   * For Windows Server, you access to Panel Admin using http://your_domain_name.com/panel/index.php
   
5. Once the installation is completed, reset the file permission for 'config.php' file to 644.

6. You may proceed to login to the Administration area. 



Integration Guides
------------------

1. Using a text editor, open your web page file (.php file), and put the code below to the top of the file (line 1), to call the 'config.php' file. This will load all the necessary database connection, class declaration files and load session.

 	<?php include_once("document_path_of_panel_folder/config.php"); include_once($path["docroot"]."common/session.php"); include_once($path["docroot"]."common/css.php"); ?>

 	* Note: Always insert the above code to the line 1, without any preceding character(s), not even a space.


2. To display the dynamic title, put the line below between the title tag.

 	<?php include($path["docroot"]."photogallery/home.title.php"); ?>


3. In your HTML code, put the line below to call the menu area such as photo search, photo categories, random photo, etc.

 	<?php include($path["docroot"]."photogallery/home.category.php"); ?>


4. In the body content of your .php web page file, put the line below to show the gallery's content.

 	<?php include($path["docroot"]."photogallery/home.gallery.php") ?>


5. For sample integration code, please refer to the source of this file. To do this, open it with your text editor.

** To find your 'document_path_of_panel_folder', check the path['docroot'] value from the panel/config.php file


6. Finally, as additional tip, you can put your front-end file in any location you like, but put the call to 
   the Panel Admin folder using the relative path. 
   
   Example, if your front-end file is at   http://yourdomain.com/gallery.php and the Panel Admin folder is located 
   at http://yourdomain.com/panel/ , use the code below. 
   
 	<?php include_once("panel/config.php") ?> 

	<?php include_once($path["docroot"]."common/session.php"); ?>

	<?php include_once($path["docroot"]."common/css.php"); ?>

	<?php include($path["docroot"]."photogallery/home.title.php"); ?>
 		
 	<?php include_once($path["docroot"]."photogallery/home.category.php") ?>  
 		
 	<?php include_once($path["docroot"]."photogallery/home.gallery.php") ?>  

