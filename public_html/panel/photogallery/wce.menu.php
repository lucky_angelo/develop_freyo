<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:47 28.12.2006                  */
/**************************************************/


	$oTopMenu = new TopMenu();
	$oTopMenu->component = "photogallery";

	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['managephoto'];
	
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['photogallery']['addphoto'];
	$oSubMenu->page = "wce.addphoto.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['photogallery']['listphoto'];
	$oSubMenu->page = "wce.listphoto.php";
	$oMenu->addSub($oSubMenu);
	$oTopMenu->add($oMenu);

	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['managecategory'];
	$oMenu->page = "wce.category.php";
	$oTopMenu->add($oMenu);	
		
	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['bulkupload'];
	$oMenu->page = "wce.bulkupload.php";
	$oTopMenu->add($oMenu);	
	
	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['member'];
	$oMenu->page = "wce.member.php";
	$oTopMenu->add($oMenu);	
	
	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['approval'];
	$oMenu->page = "wce.approve.php";
	$oTopMenu->add($oMenu);	
	
	$oMenu = new Menu();
	$oMenu->label = $lang['photogallery']['preference'];
	
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['photogallery']['generalsetting'];
	$oSubMenu->page = "wce.preference.php";
	$oMenu->addSub($oSubMenu);

	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['photogallery']['presetemails'];
	$oSubMenu->page = "wce.presetemail.php";
	$oMenu->addSub($oSubMenu);
						
	$oSubMenu = new SubMenu();
	$oSubMenu->label = $lang['photogallery']['integrateguide'];
	$oSubMenu->page = "wce.guide.php";
	$oMenu->addSub($oSubMenu);	
	$oTopMenu->add($oMenu);	
	
	
	$oTopMenu->create();

?>