<?
/**************************************************/
/*        SourceCop Decoder v. 1.0.4              */
/*            by DGT                     */
/*           18:53:40 28.12.2006                  */
/**************************************************/


switch($pageaction){
	case "add":
		$oPhoto_Category->data = array("parent_id","name","descp","showprivate","sequence");
		$oPhoto_Category->value = array($parent_id,addslashes($name),addslashes($descp),$showprivate,$sequence);
		$oPhoto_Category->add();
		$category_id=$parent_id;
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catadd']."<br>";		
	break;
	
	case "edit":
		$oPhoto_Category->data = array("parent_id","name","descp","showprivate","sequence");
		$result=$oPhoto_Category->getDetail($category_id);
		if($myrow=mysql_fetch_row($result)){
			$vvcategory_id=$category_id;
			$vparent_id=$myrow[0];
			$vname=stripslashes(htmlentities($myrow[1]));
			$vdescp=stripslashes(htmlentities($myrow[2]));
			$vshowprivate=$myrow[3];
			$sequence=$myrow[4];
		}
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catedit']."<br>";	
	break;
	
	case "update":
		$oPhoto_Category->data = array("parent_id","name","descp","showprivate","sequence");
		$oPhoto_Category->value = array($parent_id,addslashes($name),addslashes($descp),$showprivate,$sequence);
		$oPhoto_Category->update($category_id);
		
		$oPhoto_Category->data = array("parent_id","name","descp","showprivate","sequence");
		$result=$oPhoto_Category->getDetail($category_id);
		if($myrow=mysql_fetch_row($result)){
			$vvcategory_id=$category_id;
			$vparent_id=$myrow[0];
			$vname=stripslashes(htmlentities($myrow[1]));
			$vdescp=stripslashes(htmlentities($myrow[2]));
			$vshowprivate=$myrow[3];
			$sequence=$myrow[4];
		}
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catupdate']."<br>";	
	break;
	
	case "delete":
		if(is_array($cat_id) && @count($cat_id)){
			foreach($cat_id as $id){ $oPhoto_Category->delete($id);	}
			$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catdel']."<br>";	  				
		}	
	break;
	
	case "deleteone":
		$oPhoto_Category->delete($category_id);
		$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catdel']."<br>";
	break;
	
	case "updateseq":
		if(is_array($cat_seq) && @count($cat_seq)){
			foreach($cat_seq as $catid=>$seq){
				$oPhoto_Category->data = array("sequence");
				$oPhoto_Category->value = array($seq);
				$oPhoto_Category->update($catid);
			}
			$status_message = "<b>".$lang['photogallery']['status']." :</b> ".$lang['photogallery']['catorderupd']."<br>";
		}     
	break;
}//end switch;

if(!isset($sequence)){
	$oPhoto_Category->data = array("max(sequence)");
	$rst = $oPhoto_Category->getList();
	$myr = @mysql_fetch_row($rst);	$sequence =  $myr[0] + 1;	mysql_free_result($rst);
}

?>

<table border=0 width=100%><tr><td><b><? echo $lang['photogallery']['photogallery'] ?></b></td><td><? include("wce.menu.php") ?></td></tr></table>
<hr size=1 color=#606060><? echo $status_message ?><br>
<!--Body Start-->

<table border=0 cellpadding=2 cellspacing=0 width=98% align=center>
<form name=thisform action=index.php?component=photogallery&page=wce.category.php method=post>
<input type=hidden name="pageaction">
<input type=hidden name="category_id" value="<? echo $vvcategory_id ?>">
<input type=hidden name="sortby" value="<? echo $sortby ?>">
<input type=hidden name="sortseq" value="<? echo $sortseq ?>">

<tr><td valign=top width=20%><? echo $lang['photogallery']['catname'] ?></td><td><input type=text name=name style="width:230px" value="<? echo $vname ?>"> *</td></tr>
<tr><td valign=top><? echo $lang['photogallery']['parentcat'] ?></td><td><select name=parent_id><option value="0"><? echo $lang['photogallery']['topcat'] ?></option><?	$oPhoto_Category->getCategoryOption(0,$vparent_id,$vvcategory_id);?></select></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['catdescp'] ?></td><td><textarea name=descp cols=80 rows=3 style="width:430px"><? echo $vdescp ?></textarea></td></tr>
<tr><td valign=top><? echo $lang['photogallery']['sequence'] ?></td><td><input type=text name=sequence style="width:45px" value="<? echo $sequence ?>"></td></tr>
<tr><td><? echo $lang['photogallery']['privateaccess'] ?></td><td><input type=checkbox name=showprivate value="Yes" <? echo ($vshowprivate=="Yes"?"checked":"")?>></td></tr>
<tr><td valign=top></td><td>
	<? if($pageaction=="edit" || $pageaction=="update"){ ?>
		<input type=button value="<? echo $lang['photogallery']['btnupdate'] ?>" onclick="document.thisform.pageaction.value='update';validate()">
		<input type=button value="<? echo $lang['photogallery']['btndel'] ?>" onclick="ConfirmDeleteOne()">
		<input type=button value=" <? echo $lang['photogallery']['btnback'] ?> " onclick="window.location='index.php?component=photogallery&page=wce.category.php'">
	<? }else{ ?>
		<input type=button value=" <? echo $lang['photogallery']['add'] ?>  " onclick="document.thisform.pageaction.value='add';validate()">
	<? } ?>	
</td></tr></form></table><br>

<?
	$sort=array("parent_id","name","sequence");

	if($sortby==""){ $sortby=$sort[0]; } 
	if($sortseq==""){ $sortseq="asc"; }
	for($i=0;$i<count($sort);$i++){	$sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sort[$i]&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['photogallery']['sortasc']."\"></a>"; }
	for($i=0;$i<count($sort);$i++){
		if($sortby==$sort[$i]){
		if($sortseq=="asc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=desc><img src=\"common/image/sort_desc.gif\" border=0 alt=\"".$lang['photogallery']['sortdesc']."\"></a>"; }
		if($sortseq=="desc"){ $sortlink[$i]="<a href=index.php?component=$component&page=$page&sortby=$sortby&sortseq=asc><img src=\"common/image/sort_asc.gif\" border=0 alt=\"".$lang['photogallery']['sortasc']."\"></a>"; }
	}}

	echo "
	<table border=0 cellpadding=5 cellspacing=0 width=98% align=center>
		<tr><td><a href=\"javascript:ConfirmDelete()\">".$lang['photogallery']['delseleted']."</a>&nbsp;&nbsp;| <a href=\"javascript:UpdateSeq()\">".$lang['photogallery']['updateseq']."</a></td></tr>
	</table>
	<table border=0 cellpadding=1 cellspacing=0 width=98% align=center>		
	<form name=frmList action=\"index.php?component=$component&page=$page&sortby=$sortby&sortseq=$sortseq\" method=post>
	<input type=hidden name=\"pageaction\">
		<tr bgcolor=\"#E6E6E6\" height=\"24\">
	      <td class=\"gridTitle\" align=\"center\" width=\"25\"><input type=checkbox id=\"checkAll\" onclick=\"selectAll()\"></td>
	      <td class=\"gridTitle\" align=\"left\" width=\"40\">&nbsp;<b>".$lang['photogallery']['catid']."</b></td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['photogallery']['parentcat']."</b> $sortlink[0]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"left\" width=\"\">&nbsp;<b>".$lang['photogallery']['subcat']."</b> $sortlink[1]&nbsp;</td>
	      <td class=\"gridTitle\" align=\"center\" width=\"60\">&nbsp;<b>".$lang['photogallery']['private']."</b></td>	      
	      <td class=\"gridTitle\" align=\"center\" width=\"60\">&nbsp;<b>".$lang['photogallery']['seq']."</b> $sortlink[2]&nbsp;</td>	      
	      <td class=\"gridTitle\" align=\"center\" width=\"30\">&nbsp;</td>
	    </tr> 	
	";
	$cat_seq = array();	$cat_id = array();
   	$oPhoto_Category->data = array("category_id","parent_id","name","showprivate","sequence");
   	$oPhoto_Category->where = "";  	$oPhoto_Category->order = "$sortby,name $sortseq";
   	$count=0; $result=$oPhoto_Category->getList();
   	while($myrow=mysql_fetch_row($result)){
   		$count++;   		$cid = $myrow[0];	
   		$resultparent=$oPhoto_Category->getDetail($myrow[1]);
   		if($myrowparent=mysql_fetch_row($resultparent)){ $parentname=stripslashes($myrowparent[2]);	}else{ $parentname="Top"; }
   		mysql_free_result($resultparent);
   		$myrow[2]=stripslashes($myrow[2]);
   		$myrow[3]=($myrow[3]=="Yes"?$lang['photogallery']['showphotoyes']:$lang['photogallery']['showphotono']);
   		
   		echo "
	    <tr id=\"row$count\" style=\"background:#F6F6F6\" height=\"24\">
	      <td class=\"gridRow\" align=\"center\"><input type=checkbox id=\"check$count\" name=\"cat_id[]\" value=\"$myrow[0]\" onclick=\"if(this.checked==true){ selectRow('row$count'); }else{ deselectRow('row$count'); }\"></td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[0]</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$parentname</td>
	      <td class=\"gridRow\" align=\"left\">&nbsp;$myrow[2]</td>
	      <td class=\"gridRow\" align=\"center\">&nbsp;$myrow[3]</td>
	      <td class=\"gridRow\" align=\"center\"><input type=text name=cat_seq[$cid] value=\"$myrow[4]\" style=\"width:40px;\" onkeypress=\"nextSeq(this, event)\"></td>
   		  <td class=\"gridRow\" align=\"center\"><a href=index.php?component=photogallery&page=wce.category.php&pageaction=edit&category_id=$myrow[0]><img src=common/image/ico_edit.gif border=0 alt=\"".$lang['photogallery']['editrecord']."\"></a></td>
		</tr>
		";
   	}
   	mysql_free_result($result);
   	
	echo "
		</form></table>
		<table border=0 cellpadding=5 cellspacing=0 width=98% align=center>
			<tr><td><a href=\"javascript:ConfirmDelete()\">".$lang['photogallery']['delseleted']."</a>&nbsp;&nbsp;| <a href=\"javascript:UpdateSeq()\">".$lang['photogallery']['updateseq']."</a></td></tr>
		</table>
	";
?> 

<script language=javascript>
	function ConfirmDelete(){  
	    if(confirm('<? echo $lang['photogallery']['suredelete'] ?>')){
	    	document.frmList.pageaction.value='delete';
		    document.frmList.submit();
		}
	}
	
	function ConfirmDeleteOne(){  
	    if(confirm('<? echo $lang['photogallery']['suredelete'] ?>')){
	    	document.thisform.pageaction.value='deleteone';
		    document.thisform.submit();
		}
	}
	
	function UpdateSeq(){
    	document.frmList.pageaction.value='updateseq';
	    document.frmList.submit();
	}	
	
	function selectAll(){
		if(document.getElementById("checkAll").checked==true){
			document.getElementById("checkAll").checked=true;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=true; document.getElementById(\"row$i\").style.background='#D6DEEC'; \n"; }	?>
		}else{
			document.getElementById("checkAll").checked=false;
			<?	for($i=1; $i<=$count; $i++){ echo "document.getElementById(\"check$i\").checked=false; document.getElementById(\"row$i\").style.background='#F6F6F6'; \n"; } ?>		
		}
	}

	function selectRow(row){
		document.getElementById(row).style.background="#D6DEEC";
	}

	function deselectRow(row){
		document.getElementById(row).style.background="#F6F6F6";
	}	
	
	function validate(){
		if(document.thisform.name.value==""){
			alert('<? echo $lang['photogallery']['plscategory'] ?>'); document.thisform.name.select();  return false;
		}else{
			document.thisform.submit();
		}	
	}	
</script>