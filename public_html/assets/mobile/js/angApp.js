(function(){
	var console = {};
	console.log = function(){};
	$.ajaxSetup({
		headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	var setCookie = function(cname, cvalue, exdays) {
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	};
	var getCookie = function(cname) {
	    var name = cname + "=";
	    var ca = document.cookie.split(';');
	    for(var i=0; i<ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1);
	        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	    }
	    return "";
	};
	var checkCookie = function(cname) {
	    var username=getCookie(cname);
	    if (username!="") {
	        return true;
	    }else{
	        return false;
	    }
	};

	var loaderhtml="<div class='floader'><div class='floader-load'>\
		<style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.6);'><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(0deg) translate(0,-50px);transform:rotate(0deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(120deg) translate(0,-50px);transform:rotate(120deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(240deg) translate(0,-50px);transform:rotate(240deg) translate(0,-50px);border-radius:40px;position:absolute;'></div></div>\
	</div></div>";
	var showLoader = function(){
		console.log('show');
		$('body').append(loaderhtml);
	};
	var hideLoader = function(){
		console.log('hide');
		$('.floader').remove();
	};

	// Offline JS
	var run = function(){
      Offline.check();
    }
    window.onbeforeunload = function(ev) {
        showLoader();
    };
    setInterval(run, 10000);
    Offline.on('down', disableAction);
    Offline.on('up', enableAction);
    function disableAction() {
      // showLoader();
      window.onbeforeunload = function(ev) {
        if(Offline.state == 'down') {
          ev.stopPropagation();
          return "Please stay on this page. Any data you have entered may not be saved.";
        }
      };
    }
    function enableAction() {
      // hideLoader();
      window.onbeforeunload = function(ev) {
        // showLoader();
      };
    }
    // END OFFLINE JS
	var app = angular.module('freyoApp',
		[
			"ngRoute",
		  	"mobile-angular-ui",

		  	// touch/drag feature: this is from 'mobile-angular-ui.gestures.js'
			// it is at a very beginning stage, so please be careful if you like to use
			// in production. This is intended to provide a flexible, integrated and and
			// easy to use alternative to other 3rd party libs like hammer.js, with the
			// final pourpose to integrate gestures into default ui interactions like
			// opening sidebars, turning switches on/off ..
			'mobile-angular-ui.gestures',

			'angular-google-analytics',
			'720kb.socialshare',
			'ngSanitize',
			'ngAnimate',
			'angularMoment',
			// 'vcRecaptcha',
		]).filter('fromNow', function() {
	  return function(date) {
	    return moment(date).fromNow();
	  }
	});
	app.config(function (AnalyticsProvider) {
	  	AnalyticsProvider.setAccount('UA-73789781-1');
	});

	app.run(function($rootScope, $location, $window, UserProfile){
		// $rootScope.$on( "$routeChangeStart", function(event, next, current) {
	 //      if ( $rootScope.loggedUser == null ) {
	 //        // no logged user, we should be going to #login
	 //        if ( next.templateUrl == "partials/login.html" ) {
	 //          // already going to #login, no redirect needed
	 //        } else {
	 //          // not going to #login, we should redirect now
	 //          $location.path( "/login" );
	 //        }
	 //      }
	 //    });
	 	$rootScope.timeOut = '';
		$rootScope.userScreenWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		$rootScope.userScreenWidth = $rootScope.userScreenWidth * 2;
		$rootScope.userScreenHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		$rootScope.fixImgHeight = 993;
		$rootScope.fixImgWidth = 768;
		console.log($rootScope.userScreenWidth);
		$window.ga('create', 'UA-73789781-1', 'auto');
		console.log('GA INIT!!');
		$rootScope
        .$on('$routeChangeSuccess',
            function(event){

                if (!$window.ga)
                    return;
 				console.log('GA SEND!!');
                $window.ga('send', 'pageview', { page: $location.path() });
        });
		$rootScope.logged_in = false;
		// $rootScope.logged_in = true;
		if($('meta[name="clickable"]')){
			$rootScope.isClickable = $('meta[name="clickable"]').attr('content');
			console.log($rootScope.isClickable);
		}
		var fapp_version = $('meta[name="fapp-version"]').attr('content');
		console.log(fapp_version);
		var urlfapp_version = "";
		if(fapp_version == 3){
			urlfapp_version = "&version="+fapp_version;
		}else{
			urlfapp_version = "&version="+fapp_version;
			var updated_version_link = "#";
			var userAgent = navigator.userAgent || navigator.vendor || window.opera;
			if( userAgent.match( /Android/i ) ){
				updated_version_link = "https://play.google.com/store/apps/details?id=com.freyo&rdid=com.freyo";
			}
			else{
				updated_version_link = "https://itunes.apple.com/us/app/freyo/id1098542052?ls=1&mt=8&google.com";
			}
			var update_app_modal = '<div id="update-app-modal" class="modal signout-dialog-box text-center">\
			  <div class="modal-backdrop in"></div>\
			  <div class="modal-dialog">\
			    <div class="modal-content">\
			      <div class="modal-header">\
			        <h4 class="modal-title">New Update</h4>\
			      </div>\
			      <div class="modal-body">\
			        <p>Please update to new version for better user experience.</p>\
			      </div>\
			      <div class="modal-footer">\
			        <a href="#" class="close-update-modal" style="color:#666;">Later&nbsp;&nbsp;</a>\
			        <a href="'+updated_version_link+'" class="sign-out-now">Update&nbsp;&nbsp;</a>\
			      </div>\
			    </div>\
			  </div>\
			</div>';
			$('body').append(update_app_modal);
			$('.close-update-modal').click(function(){
				$('#update-app-modal').remove();
			});
		}
		console.log(urlfapp_version); 
		
		$rootScope.signout = function(){
			window.location.replace("/signout?app=banana"+urlfapp_version);
		};
		$rootScope.gobacktoregister = function(){
			var theBaseURL = location.protocol + '//' + location.host;
			window.location.replace(theBaseURL+"?redirectto=signup&app=banana"+urlfapp_version);
		};
		$rootScope.gobacktologin = function(){
			var theBaseURL = location.protocol + '//' + location.host;
			window.location.replace(theBaseURL+"?redirectto=login&app=banana"+urlfapp_version);
		};
		UserProfile.getProfile()
        .success(function(data) {
        	console.log(data);
        	if(!$.isEmptyObject(data)){
	        	if(data[0].phone_number == 0){
	        		data[0].phone_number = "";
	        	}
	        	if(data[0].birthdate == "0000-00-00"){
	        		data[0].birthdate = "";
	        	}
	        	if(data[0].email !== ""){
	        		$rootScope.logged_in = true;
	        		ga('set', 'userId', data[0].user_id); // Set the user ID using signed-in user_id.
	        	}else{
	        		$rootScope.logged_in = false;
	        	}
	        	console.log($rootScope.logged_in);
	            $rootScope.user_profile = data[0];

	            UserProfile.getProfilePicture(data[0].profile_picture)
		    	.success(function(data){
		    		$rootScope.url_profile_pic = data;
		    	});
		    }
		    else{
		    	console.log("empty");
		    }
		    if(data[0].interests == "" || data[0].interests == null) {
		    	var update_app_modal = '<div id="update-interest-modal" class="modal signout-dialog-box text-center">\
				  <div class="modal-backdrop in"></div>\
				  <div class="modal-dialog">\
				    <div class="modal-content">\
				      <div class="modal-header">\
				        <h4 class="modal-title">Interests</h4>\
				      </div>\
				      <div class="modal-body">\
				        <p>Please add interest so we can recommended magazines you may want.</p>\
				      </div>\
				      <div class="modal-footer">\
				        <a href="#" class="redirect-to-interest">OK&nbsp;&nbsp;</a>\
				      </div>\
				    </div>\
				  </div>\
				</div>';
				$('body').append(update_app_modal);
				$('.redirect-to-interest').click(function(){
					$location.path('/interest');
					$('#update-interest-modal').remove();
				});
		    }
        });
	});

	//FACTORY
	app.factory('Magazines', function($http){
		return {
        // get all Magazine Details
        getDetails : function(catID, count, fpick, featured) {
        	if(catID !== undefined && count !== undefined && fpick !== undefined && featured !== undefined){
        		return $http.get('/api/magazine/'+catID+'/'+count+'/'+fpick+'/'+featured);
        	}
        	else if(catID !== undefined && count !== undefined && fpick !== undefined){
        		return $http.get('/api/magazine/'+catID+'/'+count+'/'+fpick);
        	}
        	else if(catID !== undefined && count !== undefined){
        		return $http.get('/api/magazine/'+catID+'/'+count);
        	}
        	else if(catID !== undefined){
           		return $http.get('/api/magazine/'+catID);
        	}
        	else{
        		return $http.get('/api/magazine');
        	}
        },

        getDetailsSingle: function(magID) {
        	return $http.get('/api/magazinesingle/'+magID);
        },

        getPages : function(magID) {
        	return $http.get('/api/pages/'+magID);
        },

        getSlider : function(catID, count) {
    		if(catID !== undefined && count !== undefined){
    			return $http.get('/api/featuredslider/'+catID+'/'+count);
    		}
    		else if(catID !== undefined){
    	   		return $http.get('/api/featuredslider/'+catID);
    		}
    		else{
    			return $http.get('/api/featuredslider');
    		}
        },

        getFeatured : function(){
        	return $http.get('/api/featuredmagazines');
        },

        getLiked : function(){
        	return $http.get('/api/likedmagazines');
        },

        getSubscribed : function(){
        	return $http.get('/api/subscribedmagazines');
        },

        

    }
	});

	app.factory('UserProfile', function($http){
		return {
        // get all Magazine Details
        getProfile : function() {
        	return $http.get('/api/user_profile');
        },

        updateProfile : function() {
        	return $http.get('/api/pages/'+magID);
        },

        getProfilePicture : function(profPic) {
        	return $http.get('/api/get_profile_pic/'+profPic);
        },

        isLoggedIn : function() {
        	return $http.get('/api/checkifloggedin');
        }
    }
	});
	app.factory('freyoCacher', function($cacheFactory){
		return $cacheFactory('freyo-cache');
	});
	app.factory('Comments', function($http, $q) {
		return {
			getComments: function(magID) {
				return $http.get('/api/get_comments/'+magID+'/1');
			},
			getCommentsPoll: function(magID) {
				var temp = {};
				var defer = $q.defer();
				$http.get('/api/get_comments/'+magID).success(function(data) {
					// console.log(data)
					temp = data;
					defer.resolve(data);
				});
				return defer.promise;
			}
		}
	});
	app.factory('Interests', function($http) {
		return {
			getInterests: function() {
				return $http.get('/api/get_interests');
			},
			getUserInterests: function() {
				return $http.get('/api/user_interests');
			}
		}
	})
	//END FACTORY

	// CONTROLLERS
	app.controller('freyoController', function($rootScope, $scope, $routeParams, $route){
		// Needed for the loading screen
		console.log(' time I run.')
		$rootScope.$on('$routeChangeStart', function(){
		    $rootScope.loadingg = true;
		});

		$rootScope.$on('$routeChangeSuccess', function(){
			// var csrf_token = <?php echo csrf_token(); ?>
			// $('meta[name="csrf-token"]').attr('content', '');
		    $rootScope.loadingg = false;
		    var fviewport = $('meta[name="viewport"]').attr('content');
		    if($route.current.templateUrl == "/pages/magazine"){
		    	$('meta[name="viewport"]').attr('content','width=device-width, initial-scale=1.0, maximum-scale=3.0, minimal-ui');
		    }
		    else{
		    	$('meta[name="viewport"]').attr('content','width=device-width, initial-scale=1.0, maximum-scale=1.0, minimal-ui');
		    }
		    // $('meta[name="csrf-token"]').attr('content')
		});

		$scope.categoryClass = function(){
			switch($routeParams.category){
				case 'fashion':
					return "mc-fashion";
				case 'travel':
					return "mc-travel";
				case 'cars':
					return "mc-cars";
				case 'weddings':
					return "mc-weddings";
				case 'celebrity':
					return "mc-celebrity";
				case 'sports':
					return "mc-sports";
				default:
					return "";
			}
		};

		$scope.sidebarUrl = function() {
	       	return "/pages/sidebar";
	    };
	    $scope.featuredUrl = function() {
	       	return "/pages/featured";
	    };
	    $scope.newReleaseUrl = function() {
	       	return "/pages/new_release";
	    };
	    $scope.topChartsUrl = function() {
	       	return "/pages/top_charts";
	    };
	});

	app.controller('editProfileController', function($rootScope, UserProfile){
		// UserProfile.getProfile()
  //       .success(function(data) {
  //       	console.log(data);
  //       	if(data[0].phone_number == 0){
  //       		data[0].phone_number = "";
  //       	}
  //       	if(data[0].birthdate == "0000-00-00"){
  //       		data[0].birthdate = "";
  //       	}
  //           $scope.user_profile = data[0];

  //           UserProfile.getProfilePicture(data[0].profile_picture)
	 //    	.success(function(data){
	 //    		$scope.url_profile_pic = data;
	 //    		$('.fsu-input-box').each(function(){
		//     		if($(this).val() !== ""){
		//     			$(this).parent('.fsu-input').addClass('fsu-active');
		//     		}
		//     	});
	 //    	});
  //       });
		setTimeout(function() {
			$('.fsu-input-box').each(function(){
				if($(this).attr('type') == 'date'){
					$(this).val($rootScope.user_profile.birthdate);
				}
	    		if($(this).val() !== ""){
	    			$(this).parent('.fsu-input').addClass('fsu-active');
	    		}

	    	});
		}, 1000);
	});

	app.controller('dashboardController', function($scope, SharedState, featuredMagazines, isUserLoggedIn, allMagazines, magazineSliders){
		console.log('dashboardController');
		console.log(isUserLoggedIn.data);
		$scope.userisloggedin = isUserLoggedIn.data;
		// if($rootScope.user_profile.email !== ""){
		// 	$rootScope.logged_in = true;
		// }else{
		// 	$rootScope.logged_in = false;
		// }
		$scope.tItemLimit = 10;
		$scope.nrItemLimit = 10;
		$scope.tnrAddItems = function() {
			console.log('bottomscroll');
			//Top Charts
			if(SharedState.get('activeTab') == 2){
				$scope.tItemLimit += 10;
			}
			//New Release
			else if(SharedState.get('activeTab') == 3){
				$scope.nrItemLimit += 10;
			}
		};
		$scope.topfilter = 'rating';
		// $($window).scroll(function() {
		// 	console.log('a');
		// })
		console.log('magazineSliders');
		console.log(magazineSliders.data);
		$scope.featuredSlider  = magazineSliders.data;
		$scope.sliderTotal = magazineSliders.data.length;

		console.log("All Magazines");
		console.log(allMagazines);
		$scope.magazines = allMagazines.data;

		//Freyo Picks & Featured Magazines(User's Favorites)
		console.log('Freyo Picks & Featured');
		console.log(featuredMagazines);
		$scope.dashboardFeatured = featuredMagazines.data;
	});

	app.controller('profileController', function($scope, $routeParams, SharedState, Magazines){
		$scope.profileType = $routeParams.profile;
		// $scope.followClass = "follow-this";
		// $scope.followText = "Follow";
		// $scope.followClick = function(){
		// 	if($scope.followClass === "follow-this")
		// 	{
		// 		$scope.followClass = "unfollow-this";
		// 		$scope.followText = "Unfollow";
		// 	}
		// 	else
		// 	{
		// 		$scope.followClass = "follow-this";
		// 		$scope.followText = "Follow";
		// 	}
		// };
		$scope.likedMagazineUrl = function() {
	       	return "/pages/liked";
	    };
		$scope.subscribedMagazineUrl = function() {
	       	return "/pages/subscription";
	    };
	    $scope.lItemLimit = 50;
	    $scope.subItemLimit = 12;
	    $scope.lsAddItems = function(){
	    	if(SharedState.get('activeTab') == 1){
	    		$scope.lItemLimit += 6;
	    	}
	    	else{
	    		$scope.subItemLimit += 6;
	    	}
	    }
		Magazines.getLiked()
		.success(function(data){
			console.log("Liked");
			console.log(data);
			$scope.likeTotal = data.length;
			$scope.likedMags = data;
		});

		Magazines.getSubscribed()
		.success(function(data){
			console.log("Subscribed");
			console.log(data);
			$scope.subTotal = data.length;
			$scope.subsMags = data;
		});

	});

	app.controller('browserController', function($rootScope, $scope, $routeParams, allMagazines, UserProfile){
		if(!checkCookie('coach_browser')){
			setTimeout(function(){
				$('body').addClass('coach');
				setTimeout(function(){
					$('body').removeClass('coach');
				}, 15000);
				setCookie('coach_browser', 'ok', 1825);
			}, 10);
		}
		function arrayObjectIndexOf(myArray, searchTerm, property) {
		    for(var i = 0, len = myArray.length; i < len; i++) {
		        if (myArray[i][property] == searchTerm) return i;
		    }
		    return -1;
		}
		$scope.objectIndexOf = function(theArray, theSearch, theProperty){
			return arrayObjectIndexOf(theArray, theSearch, theProperty);
		};
		// $(function() {
		// 	$('.app-body').removeClass('extend-top');
		// 	$('.freyo-header').removeClass('hide-to-top header-transparent');
		// });
		
		
		
		setTimeout(function() {
			$('.freyo-header').removeClass('hide-to-top');
		}, 3000);
		
		$(document).off();
		$(document).click(function(e) {
			if(!$('.dropdown-btn').is(e.target)) {
				// console.log('asdf');
				if($('.owl-item.active .dropdown-content').hasClass('dropdown-show')) {
					$('.owl-item.active .dropdown-content').removeClass('dropdown-show');
				}
			}
		});
		
		// console.log(allIssues.data);
		console.log("Browser Magazine Category: "+$routeParams.category);
		console.log(allMagazines.data);
		
		$scope.activeItem = arrayObjectIndexOf(allMagazines.data, $routeParams.magid, 'magazine_id');
		$scope.browserMagazines = allMagazines.data;

		if($routeParams.category == -1){
			$scope.browserTitle = "Recommended"
			$scope.magCat = allMagazines.data[0].category_name;
		}else if($routeParams.category == 0){
			$scope.browserTitle = "All Magazine"
			$scope.magCat = allMagazines.data[0].category_name;
		}else {
			$scope.browserTitle = allMagazines.data[0].category_name; // 9-27-16
			$scope.magCat = allMagazines.data[0].category_name;
			// $scope.browserTitle = $scope.browserMagazines.category_name; 
		}

		$scope.reviewerPic = function(reviewer_pic){
			setTimeout(function(){
				UserProfile.getProfilePicture(reviewer_pic)
				.success(function(data){
					// $scope.revPic = data;
					// console.log($scope.revPic);
					console.log(data);
					return data;
				});
			}, 10);
		};
		console.log('revPic');
		console.log($scope.reviewerPic());
	});

	app.controller('categoryController', function($scope, $routeParams){
		$scope.category = $routeParams.category;
		$scope.tabCategoryClass = function(){
			switch($routeParams.category){
				case 'fashion':
					return "tab-cat-fashion";
				case 'travel':
					return "tab-cat-travel";
				case 'cars':
					return "tab-cat-cars";
				case 'weddings':
					return "tab-cat-weddings";
				case 'celebrity':
					return "tab-cat-celebrity";
				case 'sports':
					return "tab-cat-sports";
				default:
					return "";
			}
		};
	});

	app.controller('magazineController', function($rootScope, $scope, $routeParams, magazinePages, $timeout, $interval, $sce, $window){
		if(!checkCookie('coach_magazine')){
			setTimeout(function(){
				$('body').addClass('coach');
				setTimeout(function(){
					$('body').removeClass('coach');
				}, 15000);
				setCookie('coach_magazine', 'ok', 1825);
			}, 10);
		}
		console.log(magazinePages.data);
		$('.app-body').addClass('extend-top');
		$('.freyo-header').addClass('hide-to-top');
		
		var timeOut;
		// $(document).off().on('click', function() {
		// 	console.log('cl');
			
		// 	$('.freyo-header').removeClass('hide-to-top');
		// 	timeOut = setTimeout(function() {
		//         $('.freyo-header').addClass('hide-to-top');
		// 		console.log('hidden');
		//     }, 3000);
		// });
		// $('.magazine-left-button').click(function() { 
		// 	clearTimeout(timeOut);
		// 	$('.app-body').removeClass('extend-top');
		// 	$('.freyo-header').removeClass('hide-to-top header-transparent');
		// 	console.log('exit');
		// });

		// $scope.$on('$destroy', function() {
		// 	console.log('destroy');
		// 	$('.app-body').removeClass('extend-top');
		// 	$('.freyo-header').removeClass('hide-to-top header-transparent');
		// 	setTimeout(function() {
		// 		$('.freyo-header').removeClass('hide-to-top');
		// 	}, 3000);
		// 	$(document).off();
		// 	console.log(timeOut);
		// 	clearTimeout(timeOut);
		// });
		$scope.trustAsHtml = function(string) {
			return $sce.trustAsHtml(string);
		};
        $scope.magPages = magazinePages.data;
        $scope.magID = magazinePages.data[0].magazine_id;
        $scope.magCatID = magazinePages.data[0].category_id;
        $scope.magazineTitle= magazinePages.data[0].title;
        $scope.magTotalPage = magazinePages.data.length;
        $scope.magRelated = magazinePages.data[0].related_magazines;
        $scope.magOthers = magazinePages.data[0].other_magazines;
	});

	app.controller('searchController', function($scope, Magazines){
		$scope.sItemLimit = 9;
		$scope.sItemPagination = 9;
		$scope.sAddItems = function(){
			$scope.sItemLimit += $scope.sItemPagination;
		}
		Magazines.getDetails()
		.success(function(data){
			console.log("All Magazines");
			console.log(data);
			$scope.magazines = data;
		});
	});
	app.controller('commentsController', function($scope, $timeout, $http, allComments, Comments) {
		if(!checkCookie('coach_browser')){
			setTimeout(function(){
				$('body').addClass('coach');
				setTimeout(function(){
					$('body').removeClass('coach');
				}, 15000);
				setCookie('coach_browser', 'ok', 1825);
			}, 10);
		}
		$scope.comments = allComments.data;
		console.log(allComments.data)
		console.log(allComments.data[0].magazine_id);
		$scope.magazineID = allComments.data[0].magazine_id;
		$scope.a = 0;
		var poll = function() {
			var timeOut = $timeout(function() {
				$http.get('/api/get_comments/'+$scope.magazineID).success(function(data) {
					$scope.comments = data;
					// console.log(data)
					// temp = data;
					// defer.resolve(data);
				});
				// $scope.comments = Comments.getCommentsPoll(allComments.data[0].magazine_id);
				console.log($scope.comments);
				poll();
			}, 5000);
			$scope.$on('$destroy', function(){
			  	$timeout.cancel(timeOut);
			});
		};
		poll();
		
	});
	app.controller('interestController', function($rootScope, $scope, $timeout, $http, allInterests, userInterests) {
		if(!checkCookie('coach_browser')){
			setTimeout(function(){
				$('body').addClass('coach');
				setTimeout(function(){
					$('body').removeClass('coach');
				}, 15000);
				setCookie('coach_browser', 'ok', 1825);
			}, 10);
		}
		console.log($rootScope.user_profile);
		console.log(allInterests.data);
		console.log(userInterests.data);
		$scope.interests = allInterests.data;
		$scope.userInterests = userInterests.data;
		var poll = function() {
			var timeOut = $timeout(function() {
				$http.get('/api/get_interests').success(function(data) {
					$scope.interests = data;
				});
				$http.get('/api/user_interests').success(function(data) {
					$scope.userInterests = data;
				});
				poll();
			}, 3000);
			$scope.$on('$destroy', function(){
			  	$timeout.cancel(timeOut);
			});
		};
		poll();
	});
	// END CONTROLLERS

	// configure our routes
    app.config(function($routeProvider, $locationProvider) {
        $routeProvider
            // route for the home page
            .when('/dashboard', {
                templateUrl : "/pages/dashboard",
                controller: 'dashboardController',
                reloadOnSearch: false,
                resolve: {
                    isUserLoggedIn: function(UserProfile){
                      return UserProfile.isLoggedIn();
                    },
                    magazineSliders: function(Magazines){
                      return Magazines.getSlider(0, 3);
                    },
                    allMagazines: function(Magazines){
                      return Magazines.getDetails();
                    },
                    featuredMagazines: function(Magazines){
                      return Magazines.getFeatured();
                    },
                },
            })
            .when('/top_charts', {
                templateUrl : "/pages/top_charts_new_release",
                controller: 'dashboardController',
                reloadOnSearch: false,
                resolve: {
                    isUserLoggedIn: function(UserProfile){
                      return UserProfile.isLoggedIn();
                    },
                    magazineSliders: function(Magazines){
                      return Magazines.getSlider(0, 3);
                    },
                    allMagazines: function(Magazines){
                      return Magazines.getDetails();
                    },
                    featuredMagazines: function(Magazines){
                      return Magazines.getFeatured();
                    },
                },
            })
            // route for the magazine page
            .when('/magazine/view/:magid', {
                templateUrl : "/pages/magazine",
                controller : 'magazineController',
                resolve: {
                    magazinePages: function(Magazines, $route){
                      return Magazines.getPages($route.current.params.magid);
                    },
                },
                reloadOnSearch: false,
            })
            // route for the magazine category page
            .when('/magazine/browser/:category/:magid?', {
                templateUrl : "/pages/magazine_browser",
                controller : 'browserController',
                resolve: {
                    allMagazines: function(Magazines, $route){
                    	// console.log($route.current.params.category);
                      	// return Magazines.getDetails($route.current.params.category);
                      	return Magazines.getDetailsSingle($route.current.params.magid)
                    },
                },
                reloadOnSearch: false,
            })
            // routes for comments
            .when('/comments/:magid', {
            	templateUrl: "/pages/comments",
            	controller: "commentsController",
            	resolve: {
            		allComments: function(Comments, $route) {
            			console.log($route.current.params.magid);
            			return Comments.getComments($route.current.params.magid);
            		}
            	},
            	reloadOnSearch: false,
            }) 
            // route for the magazine category page
            .when('/magazine/category/:category?', {
                templateUrl : "/pages/category",
                controller : 'categoryController',
                reloadOnSearch: false,
            })
            // route for the search page
            .when('/search', {
                templateUrl : "/pages/search",
            	controller  : 'searchController',
                reloadOnSearch: false,
            })
			// route for the support page
            .when('/support', {
                templateUrl : "/pages/support",
            	// controller  : 'supportController',
                reloadOnSearch: false,
            })
            .when('/profile/edit', {
            	templateUrl : "/pages/editprofile",
            	controller  : 'editProfileController',
                reloadOnSearch: false,
            })
            // route for the profile page following & favorites
            .when('/profile/:profile', {
                templateUrl : "/pages/myprofile",
                controller  : 'profileController',
                reloadOnSearch: false,
            })
            // route for the magazine page
            .when('/freyoswipe/:magid', {
                templateUrl : "/pages/swiperjs",
                controller : 'magazineController',
                reloadOnSearch: false,
            })
            .when('/settings', {
            	templateUrl: "/pages/settings",
            	reloadOnSearch: false,
            })
            .when('/interest', {
            	templateUrl: "/pages/interest",
            	controller: "interestController",
            	resolve: {
            		allInterests: function(Interests) {
            			// console.log(Interests.getInterests());
            			return Interests.getInterests();
            		},
            		userInterests: function(Interests) {
            			return Interests.getUserInterests();
            		}
            	},
            	reloadOnSearch: false,
            })
            .otherwise({
            	 redirectTo: '/dashboard',
            	 controller: 'dashboardController'
            });

    	$locationProvider.html5Mode({
    	    enabled: true,
    	    requireBase: false
    	});
    });
	//END ROUTES

	//DIRECTIVES
	
	// added 10-10-16
	app.directive('browserSlider', function($timeout) {
		return {
			restrict: 'A',
			link: function(scope) {
				console.log($('.magazine-cover-container').height());
			}
		}
	});
	// end

	// added 10-11-16
	app.directive('ratingMagazine', function() {
		return {
			restrict: 'C',
			link: function(scope) {
				console.log('rating magazine');
				$('.rating').click(function() {
					console.log('clicked');
					$('.submit-review').show();
				});
			}
		}
	});
	// end

	app.directive('dropdownBtn', function($timeout) {
		return {
			restrict: 'C',
			link: function(scope, element) {
				console.log('dropdown');
				$(element).click(function(e) {
					e.stopPropagation();
					console.log('clicked');
					$('.owl-item.active .dropdown-content').addClass('dropdown-show');
				});
			}
		}
	});
	
	// app.directive('backgroundCheck', function($timeout) {
	// 	return {
	// 		restrict: 'AC',
	// 		link: function(scope, element) {
	// 			$timeout(function() {
	// 				var img = $('img');
	// 				BackgroundCheck.init({
	// 				    targets: '.target',
	// 				    images: img
	// 				});
	// 			});
				
	// 		}
	// 	}
	// });
	app.directive('owlCarousel', function($timeout, $interval){
		return {
			restrict: 'C',
			link: function (scope) {
				scope.initCarousel = function(element){
					console.log(element);
					console.log('owl-carousel');
					var options = scope.$eval($(element).attr('data-options'));
	            	$timeout(function(){
	            		$(element).owlCarousel(options);
	            	});
				};
				
				
				scope.logThis = function(current) {
					console.log('top');
					// scope.normalInitCarousel($('div.owlCarousel.magazines-viewer'), current);
				}
				
				scope.scrollElem = $('#scroll-elem').controller('scrollableContent');
				scope.currentItem = 0;
				$('.magazines-viewer').on('changed.owl.carousel', function(event){
					scope.currentItem = event.item.index;
					$('.ads-ticktock').remove();
					$('.ticktock-handler').hide();
					$timeout(function() {
						var $ads_container = $('.magazines-viewer').find('.active .magazine-ads-container');
						var ads_id = $ads_container.attr('data-ads-id');
						var ads_type = $ads_container.attr('data-ads-type');
						var mag_id = null;
						var page_num = null;
						var ads_timer = null;
						var ads_ticktocker = null;
						if(ads_id !== undefined && ads_id !== 0){
							mag_id = $ads_container.attr('data-mag-id');
							page_num = $ads_container.attr('data-page-num');
							ads_timer = $ads_container.attr('data-ads-timer');
							if(ads_timer!=0){
								ads_ticktocker = setInterval(function(){
									$('.ads-ticktock').html(--ads_timer);
									if(ads_timer==0){
										clearInterval(ads_ticktocker);

										$('.ads-is-viewed').remove();
										$('body').append('<div style="border: medium none; right:19px; z-index:2;" class="ads-ticktock">&nbsp;SKIP</div>');
										// $('.ads-ticktock').html('SKIP');
										// $('.ads-ticktock').css({border:'none'});
										// $('.ads-is-viewed').css({position:'absolute',left:'initial',bottom:'initial',right:'6px'});
										$('.magazine-left-button').click(function(){
											$('.ads-ticktock').remove();
											$('.ticktock-handler').hide();
											$(this).unbind('click');
										});
										$('.ads-ticktock').click(function(e){
											e.stopPropagation();
											$(this).remove();
											$('.ticktock-handler').hide();
											// $('.freyo-header').addClass('hide-to-top');
											console.log('clicked ads-ticktock');
											$('.magazines-viewer').trigger('next.owl.carousel');
											$(this).unbind('click');
										});
									}
								},1000);
								$('.ticktock-handler').show();
								$('body').append('<div class="ads-is-viewed"><div class="block-all-action"><div class="ads-ticktock">'+ads_timer+'</div></div></div>');
							}
						}
						if(ads_id !== undefined && ads_id !== 0 && ads_type !== 'video'){
							$.ajax({
							    type: "POST",
							    url: "/api/adsviewed",
							    data: {
							    	ads_id : ads_id, 
							    	mag_id : mag_id,
							    	page_num : page_num
							    },
							    success: function(ads_data) {
							      	$('.magazines-viewer').find('.magazine-ads-container[data-ads-id='+ads_id+']').removeAttr('data-ads-id');
							    }
							});
						} else if(ads_id !== undefined && ads_id !== 0 && ads_type == 'video') {
							// $('body').append('<div class="video-is-playing"><div class="block-all-action"></div></div>');
							console.log('body');
							var theVideo = $('.magazines-viewer').find('.active .magazine-ads-container video');
							if(ads_id !== undefined && ads_id !== 0){
								theVideo.trigger('play');
								$.ajax({
								    type: "POST",
								    url: "/api/adsviewed",
								    data: {
								    	ads_id : ads_id, 
								    	mag_id : mag_id,
								    	page_num : page_num
								    },
								    success: function(ads_data) {
								      	$('.magazines-viewer').find('[data-ads-id='+ads_id+']').removeAttr('data-ads-id');
								      	setTimeout(function(){
								      		theVideo.attr('controls', '');
											$('.video-is-playing').remove();
								      	}, 5000);
								    }
								});
							}
							// theVideo.on('ended', function(){
							// 	$('.video-is-playing').remove();
							// });
						}
					});
				});

				// Added: 7-13-16
				// var $clickable = $('.magazines-viewer').find('.active .magazine-ads-container .ad-click');

				$('body').off('click', '.ad-click').on('click', '.ad-click', function() {
					// console.log($clickable);
					var fapp_version = $('meta[name="fapp-version"]').attr('content');
					if(fapp_version == 3) {
						var $ads_container = $('.magazines-viewer').find('.active .magazine-ads-container .ad-click');
						var ads_id = $ads_container.attr('data-ads-id');
						if(ads_id !== undefined && ads_id !== 0){
							var mag_id = $ads_container.attr('data-mag-id');
							console.log(ads_id);
							$.ajax({ 
								type: 'POST',
								url: '/api/adsclicked',
								data: {
									ads_id: ads_id,
									mag_id: mag_id,
									// elem: $clickable,
								}
							}).done(function(ads_data) {
								$('.magazines-viewer').find('.active .magazine-ads-container .ad-click').removeAttr('data-ads-id');
								$('.magazines-viewer').find('.active .magazine-ads-container .ad-click').removeClass('ad-click');
							});
						}
					} else {

					}
					
				});
				// EndAdded:
				$('.magazines-viewer').on('change.owl.carousel', function(){
					$(this).find('.active video').trigger('pause');
					$(this).find('.active .magazine-additional-content').addClass('hide-overflow');
					var text = $('.active .read-more-text').text();
	      	  		console.log(text);
	      	  		if(text == 'Hide') {
						$('.active .read-more-text').text('Read more');
	      	  		} 
				});
				// $('.magazines-browser-slider').on('change.owl.carousel', function(){
				// 	console.log('')
				// });
	        }
		};
	}).directive('owlCarouselItem', function($touch, $timeout, $rootScope, $window){
		return {
	        restrict: 'C',
	        transclude: false,
	        link: function(scope, element) {
	          	// wait for the last item in the ng-repeat then call init
	          	scope.parent = element.parent();
	            if(scope.$last) {
	            	var parent = element.parent();
	            	scope.initCarousel(element.parent());
	            	scope.$parent.showToRegister = true;

	            	if($(element).hasClass('magazine-pages')){
		            	$.fn.extend({
				    	    animateCss: function (animationName, hide) {
				    	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				    	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
				    	            $(this).removeClass(animationName);
				    	            if(hide){
				    	            	$(this).hide();
				    	            }
				    	        });
				    	    }
				    	});
				      
				      	$timeout(function(){
					      	var canSwipeDown = false;
				      		console.log('bind tocuh');
				      		var thisScrollTop = 0;
				      		
					      	// $touch.bind('.wtoucharea', {
						      //   end: function(touch) {
						      //   	console.log(touch);
						      //   	if(touch.direction == 'TOP' && touch.distance > 100){
						      //   		if($('.active img').attr('data-magtype') == 'video'){
						      //   			var userAgent = navigator.userAgent || navigator.vendor || window.opera;
						      //   			if( userAgent.match( /Android/i ) )
						      //   			{
												// window.open($('.active video')[0].currentSrc+'#playvideo#0');
						      //   			}
						      //   			$('.active video').trigger('play');
						      //   		}else{
						      //   			if(!$('.active .wswipe-up').is(':visible')) {
						      //   				// scope.reInitCarousel(parent, scope.currentItem);
						      //   				// $window.scrollTo($(".magazine-page-container").height());
							     //    			// parent.unbind('drag');
							     //    			console.log('height: '+$(".magazine-page-container").height());
								    //     		// $('.active .wswipe-up').animateCss('slideInDown', false);
								    //     		$('.active .wswipe-up').animateCss('fadeInUp', false);
								    //     		$('.active .wswipe-up').show();
								    //     		// $(window).scrollTop($('.magazine-additional-content').offsetTop - 50);
												// // $('html, body').animate({
												// // 	scrollTop: $(".magazine-page-container").height()
												// // }, 1000, function() {
												// // 	console.log('animated');
												// // });
												// // window.location.assign('#mag-content');
												// // $window.pageYOffset = $(".magazine-page-container").height();

								    //     		console.log($('.active .wswipe-up').scrollTop());
								    //     		// if($('.active .wswipe-up').scrollTop() == 0){
								    //     		// 	canSwipeDown = true;
								    //     		// }
						      //   			}
						        			
							     //    	}
						      //   	}

						      //   	if(touch.direction == 'BOTTOM' && touch.distance > 100) {
						      //   		if($('.active .wswipe-up').is(':visible')) {

						      //   			$('.active .wswipe-up').animateCss('fadeOutDown', true);
						      //   		}
						      //   	}
						      //   }
					      	// });
					      	// $touch.bind('.wswipe-up', {
					      	// 	start: function(touch) {
					      	// 	  thisScrollTop = $('.active .wswipe-up').scrollTop();
					      	// 	},

					      	// 	move: function(touch) {
					      	// 		var toZeroOnly = thisScrollTop+(touch.distanceY * -1);
					      	// 		toZeroOnly = (toZeroOnly<0) ? 0 : toZeroOnly;
					      	// 		console.log(toZeroOnly);
					      	// 	  $('.active .wswipe-up').scrollTop(toZeroOnly);
					      	// 	  console.log($('.active .wswipe-up').scrollTop());
					      	// 	},

						      //   end: function(touch) {
						      //   	console.log(touch);
						      //   	thisScrollTop = $('.active .wswipe-up').scrollTop();
						      //   	if(touch.direction == 'BOTTOM' && touch.distance > 100 && canSwipeDown){
						      //   		$('.active .wswipe-up').animateCss('fadeOutDown', true);
						      //   	}
						      //   }
					      	// });
				     //  		$('.read-more-notice').click(function(){
				     //  		  	if($('.active img').attr('data-magtype') == 'video'){
						   //      	var userAgent = navigator.userAgent || navigator.vendor || window.opera;
				     //    			if( userAgent.match( /Android/i ) )
				     //    			{
									// 	window.open($('.active video')[0].currentSrc+'#playvideo#0');
				     //    			}
				     //    			$('.active video').trigger('play');
				     //    		}else{
					    //     		$('.active .wswipe-up').animateCss('slideInUp', false);
					    //     		$('.active .wswipe-up').show();
					    //     		$('html, body').animate({
									// 	scrollTop: $(".magazine-page-container").height()
									// }, 1000, function() {
									// 	console.log('animated');
									// });
					    //     		console.log($('.active .wswipe-up').scrollTop());
					    //     		// if($('.active .wswipe-up').scrollTop() == 0){
					    //     		// 	canSwipeDown = true;
					    //     		// }
					    //     	}
				     //  		});
				      		$('.hide-mag-content').click(function(){
				      			$('.active .wswipe-up').animateCss('slideOutDown', true);
				      		});
				      	   
				      	  	// $(window).bind(
				      	  	//   'touchmove',
				      	  	//    function(e) {
				      	  	//     e.preventDefault();
				      	  	//   }
				      	  	// );
				      	  	$('.read-more-notice').click(function(e) {
				      	  		e.stopPropagation();
				      	  		console.log('clicked');
				      	  		$('.active .magazine-additional-content').toggleClass('hide-overflow');
				      	  		var text = $('.active .read-more-text').text();
				      	  		console.log(text);
				      	  		if(text == 'Read more') {
									$('.active .read-more-text').text('Hide');
				      	  		} else {
				      	  			$('.active .read-more-text').text('Read more');
				      	  		}
				      	  	});

  	  		      	  		var touchtime = 0;
  	  		      	  		$('body').on('click', '.dLikeAction', function(e){
  	  		      	  			e.stopPropagation();
  	  		      	  			// if(touchtime == 0){
  	  		      	  			// 	touchtime = new Date().getTime();
  	  		      	  			// }else{
  	  		      	  			// 	if(((new Date().getTime())-touchtime) < 500) {
  	  		      	  				 	console.log(scope.magazineTitle);
  	  		      	  					// showLoader();
  	  		      	  					var page = $(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id');
  	  		      	  					console.log(page);

  	  		      	  					if(page == undefined || page == 0 || !$rootScope.logged_in){
  	  		      	  						// hideLoader();
  	  		      	  						return false;
  	  		      	  					}
  	  		      	  					var page_likes = $('.owl-item.active .m-page-likes');
  	  		      	  					var liked_texts = $('.owl-item.active .liked-texts');
  	  		      	  					var unlicker = $('.owl-item.active .the-unlicker');
  	  		      	  					// $(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id', 0);
  	  		      	  					$(this).removeClass('dLikeAction');
  	  		      	  					$(unlicker).addClass('dUnlickAction');			      	  					

  	  		      	  					// var like_heart = '<div class="liked-response-heart">\
  	  		      	  					//   <i class="fa fa-heart animated bounceIn"></i>\
  	  		      	  					// </div>';
  	  		      	  					var like_heart = '<div class="liked-response-heart">\
  	  		      	  					  <i class="fa fa-paperclip animated bounceIn"></i>\
  	  		      	  					</div>';
  	  		      	  					page_likes.addClass('i-liked');
  	  		      	  					var user_like_texts = parseInt(liked_texts.html());
  	  		      	  					// if(user_like_texts=="Clip this"){
  	  		      	  					// 	user_like_texts = "you clipped this";
  	  		      	  					// }
  	  		      	  					// else{
  	  		      	  						user_like_texts = user_like_texts+1;
  	  		      	  					// }
  	  		      	  					liked_texts.html(user_like_texts);
  	  		      	  					 $('body').append(like_heart);
  	  		      	  					 setTimeout(function(){
  	  		      	  					 	$('.liked-response-heart').remove();
  	  		      	  					 }, 1000);
		  	  		      	  	        $.ajax({
		  	  		      	  	            type: "POST",
		  	  		      	  	            url: "/api/likemagazine",
		  	  		      	  	            data: {page_id: page},
		  	  		      	  	            success: function(like_data) {
		  	  		      	  	              console.log(like_data);
		  	  		      	  	              var like_message = like_data['message'];

		  	  		      	  	              // $('.close-closest-modal').on('click', function(){
		  	  		      	  	              // 	$('.close-closest-modal').closest('.modal').remove();
		  	  		      	  	              // });
		  	  		      	  	              // hideLoader();
		  	  		      	  	            },
		  	  		      	  	            error: function(error) {
		  	  		      	  	            	console.log(error);
		  	  		      	  	            	// hideLoader();
		  	  		      	  	            }
		  	  		      	  	        });
  	  		      	  			// 	 	touchtime = 0;
  	  		      	  			// 	}else{
  	  		      	  			// 	 	touchtime = new Date().getTime();
  	  		      	  			// 	}
  	  		      	  			// }
  	  		      	  		});

  	  	      	  			$('body').on('click', '.dUnlickAction', function(e){
  	  	      	  				e.stopPropagation();
  	  	      	  			 	console.log(scope.magazineTitle);
  	  	      	  				// showLoader();
  	  	      	  				var page = $(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id');
  	  	      	  				console.log(page);

  	  	      	  				if(page == undefined || page == 0 || !$rootScope.logged_in){
  	  	      	  					// hideLoader();
  	  	      	  					return false;
  	  	      	  				}
  	  	      	  				var page_likes = $('.owl-item.active .m-page-likes');
  	  	      	  				var liked_texts = $('.owl-item.active .liked-texts');
  	  	      	  				var the_liker = $('.owl-item.active .the-liker');

  	  	      	  				// $(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id', 0);
  	  	      	  				$(this).removeClass('dUnlickAction');
  	  	      	  				$(the_liker).addClass('dLikeAction');

  	  	      	  				var like_heart = '<div class="unliked-response-heart">\
  	  	      	  				  <i class="fa fa-paperclip animated bounceIn"></i>\
  	  	      	  				</div>';

  	  	      	  				page_likes.removeClass('i-liked');
  	  	      	  				var user_like_texts = parseInt(liked_texts.html());
  	  	      	  				// if(user_like_texts=="you clipped this"){
  	  	      	  				// 	user_like_texts = "Clip this";
  	  	      	  				// }
  	  	      	  				// else{
  	  	      	  				// 	user_like_texts = user_like_texts.replace("you and ", "");
  	  	      	  				// }
  	  	      	  				user_like_texts = user_like_texts-1;
  	  	      	  				liked_texts.html(user_like_texts);
  	  	      	  				$('body').append(like_heart);
  	  	      	  				setTimeout(function(){
  	  	      	  					$('.unliked-response-heart').remove();
  	  	      	  				}, 1000);
  	  	      	  		        $.ajax({
  	  	      	  		            type: "POST",
  	  	      	  		            url: "/api/unlikemagazine",
  	  	      	  		            data: {page_id: page},
  	  	      	  		            success: function(like_data) {
  	  	      	  		              console.log(like_data);
  	  	      	  		              var like_message = like_data['message'];

  	  	      	  		              // $('.close-closest-modal').on('click', function(){
  	  	      	  		              // 	$('.close-closest-modal').closest('.modal').remove();
  	  	      	  		              // });
  	  	      	  		              // hideLoader();
  	  	      	  		            },
  	  	      	  		            error: function(error) {
  	  	      	  		            	console.log(error);
  	  	      	  		            	// hideLoader();
  	  	      	  		            }
  	  	      	  		        });
  	  	      	  			});
				      	});
				    } 
	            }
	        }
	    };
	}).directive('arrowupOverlay', function() {
		return {
			restrict: 'C',
			link: function(scope, element) {
				console.log(element);
				$(element).click(function(e) {
					e.stopPropagation();
					if($('.owl-item.active .magazine-information-container').hasClass('content-slide-up')){
						$('.owl-item.active .magazine-information-container').removeClass('content-slide-up');
						$('.owl-item.active .magazine-information-container .reviews-comments').hide();
					} else {
						$('.owl-item.active .magazine-information-container .reviews-comments').show();
						$('.owl-item.active .magazine-information-container').addClass('content-slide-up');
						// console.log(scope.parent);
	            		scope.initCarousel($('.owl-item.active .owlCarouselOther'));
					}
				});
			},
		}
	});;
	
	app.directive('thisScroll', function ($window) {
	    return {
	    	restrict :'EAC',
	    	link: function(scope, attrs, element) {
	    		console.log('bind scroll');
	    		console.log($window);
	    		console.log(angular.element($window));
	    		canSwipeDown = false;
		        angular.element($window).on('scroll', function() {
		        	console.log('bind scroll');
		            console.log(element.scrollTop());
		            canSwipeDown = element.scrollTop() === 0
		            scope.$apply();
		        });
		        console.log(canSwipeDown);
		    }
	    };
	});

	// app.directive('wtoucharea', function($touch, $timeout){
	//   // Runs during compile
	//   return {
	//     restrict: 'C',
	//     link: function($scope, elem) {
	//     	console.log($scope);
	    	
	//     }
	//   };
	// });

	app.directive('rating', function(){
		return {
			restrict: 'C',
			link: function (scope, element, attrs) {
				var options = scope.$eval($(element).attr('data-options'));
				setTimeout(function() {
					console.log("rating");
					$(element).rating({
						'showCaption':false,
						'size':'xs',
						'theme':'krajee-fa',
						// 'emptyStar':'<i class="fa fa-star"></i>' 10-5-16,
						'emptyStar':'<i class="fa fa-star-o"></i>',
						'filledStar':'<i class="fa fa-star"></i>',
					});
				}, 10);
	        }
		}
	});

	app.directive('dSelectAddress', function(){
		var linkfunction = function(scope, element, attributes) {
			function getPHLocation() {
				var getPH = $.get('/ajax/getphilippines', function(data){
					ph_location = data;
					var provinceOption = "<option value='0'>Province</option>";
					var cityOption = "<option value='0'>City/Municipality</option>";
					$.each(data, function(key, value){
						if(value.id==attributes.provinceId){
							provinceOption += "<option data-key='"+key+"' value='"+value.id+"' selected>"+value.name+"</option>";
							$.each(value.cities, function(key, value){
								if(value.id==attributes.cityId){
									cityOption += "<option value='"+value.id+"' selected>"+value.name+"</option>";
								}
								else{
									cityOption += "<option value='"+value.id+"'>"+value.name+"</option>";
								}
							});
						}else{
							provinceOption += "<option data-key='"+key+"' value='"+value.id+"'>"+value.name+"</option>";
						}
					});
					$(element).find('.ph-provinces').html(provinceOption);
					$(element).find('.ph-cities').html(cityOption);
				});
			}
			getPHLocation();

			$(element).find('.ph-provinces').on('change', function(){
				$(element).find('.ph-provinces option:selected').each(function() {
					var provinceKey = $(this).attr('data-key');
					var cityOption = "<option value='0'>City/Municipality</option>";
					$.each(ph_location[provinceKey].cities, function(key, value){
						cityOption += "<option value='"+value.id+"'>"+value.name+"</option>";
					})
					$(element).find('.ph-cities').html(cityOption);
				});
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dToggleFollow', function(){
		var linkfunction = function(scope, element, attributes) {
			// console.log("hi");
			$(element).on("click", function(){
				if($(this).hasClass('follow-this')){
					$(this).removeClass('follow-this')
					.addClass('unfollow-this');
				}
				else{
					$(this).removeClass('unfollow-this')
					.addClass('follow-this');
				}
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dInputEvent', function($timeout){
		var linkfunction = function(scope, element, attributes) {
			$(element).find('.fsu-input-box').on("focus", function(){
				$(this).parent('.fsu-input').addClass('fsu-active');
			});
			$(element).find('.fsu-input-box').on("blur", function(){
				if($(this).parent('.fsu-input').hasClass('fsu-active') && !$(this).val()){
					$(this).parent('.fsu-input').removeClass('fsu-active');
				}
			});

			// $(element).find('.fsu-input-box').each(function(){
			// 	console.log("fsu-active");
			// 	console.log($(this).val());
			// 	if($(this).val() !== ""){
			// 		$(this).parent('.fsu-input').addClass('fsu-active');
			// 	}
			// });

		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	// app.directive('dGenderEvent', function(){
	// 	var linkfunction = function(scope, element, attributes) {
	// 		$(element).on("click", function(){
	// 			console.log($(this).attr('data-gender'));
	// 			$('.dGenderEvent').each(function(){
	// 				$(this).removeClass('gChosen');
	// 			});
	// 			$(this).addClass('gChosen')
	// 		});
	// 	}

	// 	return {
	// 		restrict: 'C',
	// 		link: linkfunction
	// 	}
	// });

	app.directive('fMagazineItem', function($timeout){
		return {
	        restrict: 'C',
	        transclude: false,
	        link: function(scope, element) {
	          // wait for the last item in the ng-repeat
	            if(scope.$last) {
	            	var option = { itemSelector: '.f-magazine-item' , transitionDuration: '0.8s'};
    	          	var grid = $(element).parent().masonry(option);
	            	console.log('masonry layout');
	            	$timeout(function(){
	            		grid.imagesLoaded().progress( function() {
									  grid.masonry('layout');
									});
	            	});
	            	setInterval(function(){
									//grid.masonry(option);
									grid.masonry();
									grid.masonry('reloadItems');
									console.log('Reload Masonry');
								},800);
	            }
	        }
	    };
	});

	// app.directive('dReadMore', function() {
	// 	return {
	// 		restrict: 'C',
	// 		link: function(scope, element) {
	// 			console.log('read init');
	// 			console.log(element);
	// 			$(element).readmore();
	// 		}
	// 	}
	// });

	app.directive('dChangePic', function($rootScope, UserProfile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('change', function(){
				var filepic = $(this).val();
				filepic = filepic.replace("C:\\fakepath\\", "");
				console.log(filepic);
				switch(filepic.substring(filepic.lastIndexOf('.')+1).toLowerCase()){
					case 'gif':
					case 'jpeg':
					case 'jpg':
					case 'png':
						showLoader();
						var profpic_data = new FormData();
						var theToken = $('.editprofpic-form').find('input[name="_token"]').val();
						profpic_data.append("_token", theToken);
						profpic_data.append('profile_pic', $(this)[0].files[0]);
						// profpic_data.append('profile_pic');
						console.log(profpic_data);
						$.ajax({
							type: "POST",
							url: "/api/update_profile_pic",
							contentType: false,
						    processData: false,
							data: profpic_data,
							success: function(update_pic_data) {
							  console.log(update_pic_data);
							  if(update_pic_data['status'] == 'success') {
							  	$('.profilepic-message .message').html(update_pic_data['message']).addClass('text-success').removeClass('text-danger');
							  	$('.profilepic-message').addClass('bg-success').removeClass('bg-danger').fadeIn(1500);
							  	// $('.edit-profile-pic').attr('src', update_pic_data['new_pic']);
				  	            UserProfile.getProfilePicture(update_pic_data['new_pic'])
				  		    	.success(function(data){
				  		    		$rootScope.url_profile_pic = data;
				  		    	});
							  }
							  else {
							  	$('.profilepic-message .message').html(update_pic_data['message']).addClass('text-danger').removeClass('text-success');
							  	$('.profilepic-message').addClass('bg-danger').removeClass('bg-success').fadeIn(1500);
							  }
							  setTimeout(function(){
							  	$('.profilepic-message').fadeOut(1500);
							  }, 10000);
							  hideLoader();
							}
						}).error(function(error){
							console.log(error);
							hideLoader();
						});
						break;
					default:
						$('.profilepic-message .message').html(filepic+" is not an image!").addClass('text-danger').removeClass('text-success');
						$('.profilepic-message').addClass('bg-danger').removeClass('bg-success').fadeIn(1500);
						setTimeout(function(){
							$('.profilepic-message').fadeOut(1500);
						}, 10000);
						break;
				}
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});
	app.directive('dUpdateProf', function($rootScope){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				if(!$(".editprof-form").validationEngine('validate', {promptPosition: 'topLeft'})) {
					setTimeout(function(){
						$(".editprof-form").validationEngine('hideAll');
		    		}, 10000);
					return false;
				}
				else{
					showLoader();
					var form_data = $(".editprof-form").serializeArray();
					console.log(form_data);
			        $.ajax({
			            type: "POST",
			            url: "/update_profile",
			            data: form_data,
			            success: function(update_data) {
			              console.log(update_data);
			              if(update_data['status'] == 'success') {
			              	$('.update-message .message').html(update_data['message']).addClass('text-success').removeClass('text-danger');
			              	$('.update-message').addClass('bg-success').removeClass('bg-danger').fadeIn(1500);
			              	$rootScope.user_profile.first_name = update_data['new_firstname'];
			              	$rootScope.user_profile.last_name = update_data['new_lastname'];
			              	$rootScope.user_profile.phone_number = update_data['new_phone_number'];
			              	$rootScope.user_profile.birthdate = update_data['new_birthdate'];
			              	// $rootScope.user_profile.sex = update_data['new_sex'];
			              	$rootScope.user_profile.city = update_data['new_city'];
			              	$rootScope.user_profile.province = update_data['new_province'];
			              }
			              else {
			              	$('.update-message .message').html(update_data['message']).addClass('text-danger').removeClass('text-success');
			              	$('.update-message').addClass('bg-danger').removeClass('bg-success').fadeIn(1500);
			              }
			              setTimeout(function(){
			              	$('.update-message').fadeOut(1500);
			              }, 10000);
			              hideLoader();
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
				}
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dSubmitRating', function($rootScope, $compile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('change', function(){
				if(!$(element).closest(".rate-review-form").validationEngine('validate', {validateNonVisibleFields: true,promptPosition: 'topLeft'})) {
					setTimeout(function(){
						$(element).closest(".rate-review-form").validationEngine('hideAll');
		    		}, 10000);
					return false;
				}
				else if($(element).closest(".rate-review-form").find('.rating-magazine').val() < 1){
					console.log('rating val');
					$(element).closest(".rate-review-form").find('.error-rating-message').fadeIn(1500);
					setTimeout(function(){
						$(element).closest(".rate-review-form").find('.error-rating-message').fadeOut(1500);
					}, 10000);
					return false;
				}
				else{
					var rating_val = $(element).closest(".rate-review-form").find('.rating-magazine').val();
					var rate_confirm_modal =	'<div class="modal signout-dialog-box text-center">\
					  <div class="modal-backdrop in"></div>\
					  <div class="modal-dialog">\
					    <div class="modal-content">\
					      <div class="modal-header">\
					        <h4 class="modal-title">You rate this as <b>'+ rating_val +'</b></h4>\
					      </div>\
					      <div class="modal-footer">\
					        <a href="#" class="close-closest-modal">Cancel</a>\
					        <a href="#" class="sign-out-now dProcessRating close-closest-modal">Continue</a>\
					      </div>\
					    </div>\
					  </div>\
					</div>';
	              	$('body').append($compile(rate_confirm_modal)(scope));
	              	$('.close-closest-modal').on('click', function(){
	              		$(this).closest('.modal').remove();
	              	});
				}
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dProcessRating', function($rootScope, $compile) {
		return {
			restrict: 'C',
			link: function(scope, element, attributes) {
				console.log(element);
				$(element).on('click', function() {
					console.log('clicked');
					showLoader();
					// var magazine = $(".magazines-browser-slider").find('.owl-item.active').find('.magazine-cover-container').attr('data-magazine-id');
					var rating_val = $(".owl-item.active .rate-review-form").find('.rating-magazine').val();
					var revmagazine_id = $(".owl-item.active .rate-review-form").find('input[name="magazine_id"]').val();
					var form_data = $(".owl-item.active .rate-review-form").serializeArray();
					console.log(form_data);
			        $.ajax({
			            type: "POST",
			            url: "/api/ratemagazine",
			            data: form_data,
			            success: function(rate_data) {
			              	console.log(rate_data);
			              	if(rate_data['status'] == 'success') {
			              	
				            var reviewer_review = '<div class="review-rate-container">\
	                            <input type="hidden" class="rating" name="rating" value="'+rate_data['rating']+'" readonly="true" />\
	                            <div class="col-xs-12 no-gutters" style="line-height: 10px"><span>YOU RATED</span></div>\
	                        </div>';
	                        console.log(reviewer_review);

	                        $('.rate-review-form[data-ratemagazine='+revmagazine_id+']').prepend($compile(reviewer_review)(scope));
			              	$('.remove-when-submitted').remove();
			              	console.log($('.dSubmitRating').closest('.review-rate-container'))
			              	setTimeout(function(){
				              	$('.dSubmitRating').closest('.review-rate-container').fadeOut(1500);
				            }, 5000);
			              	}
			              	else {
			              		$('.dSubmitRating').closest(".rate-review-form").find('.review-rate-container').append('<div class="error-rating-message error-rating-magazine">Error rating & reviewing.</div>').fadeIn(1500);
			              		setTimeout(function(){
			              			$('.dSubmitRating').closest(".rate-review-form").find('.error-rating-magazine').remove();
			              		}, 5000);
			              	}
			              	hideLoader();
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
				})
				
			}
		}
	});

	app.directive('dSubmitComment', function($rootScope, $http) {
		return {
			restrict: 'C',
			link: function(scope, element, attributes) {
				console.log(scope);
				$(element).on('click', function(){
					if(!$(element).closest(".comment-form").validationEngine('validate', {validateNonVisibleFields: true,promptPosition: 'topLeft'})) {
						setTimeout(function(){
							$(element).closest(".comment-form").validationEngine('hideAll');
			    		}, 10000);
						return false;
					}
					else{
						showLoader();
						
						var review_text = $(element).closest(".comment-form").find('input[name="comment"]').val();
						var revmagazine_id = $(element).closest(".comment-form").find('input[name="magazine_id"]').val();
						var form_data = $(element).closest(".comment-form").serializeArray();
						console.log(form_data);
				        $.ajax({
				            type: "POST",
				            url: "/api/submit_comment",
				            data: form_data,
				            success: function(rate_data) {
				              	console.log(rate_data);
				              	if(rate_data['status'] == 'success') {
				              	
						            $http.get('/api/get_comments/'+revmagazine_id).success(function(data) {
						            	console.log(data);
										scope.comments = data;
									});
									$(element).closest(".comment-form").find('input[name="comment"]').val('');
				              	} else {
				              		$(element).closest(".comment-form").find('.review-rate-container').append('<div class="error-rating-message error-rating-magazine">Error rating & reviewing.</div>').fadeIn(1500);
				              		setTimeout(function(){
				              			$(element).closest(".comment-form").find('.error-rating-magazine').remove();
				              		}, 5000);
				              	}
				              	hideLoader();
				            },
				            error: function(error) {
				            	console.log(error);
				            	hideLoader();
				            }
				        });
					}
				});
			}
		}
	});

	app.directive('dSubmitInterest', function($rootScope, $timeout, $http) {
		return {
			restrict: 'C',
			link: function(scope, element, attribute) {
				console.log(scope);
				
				$(element).change(function() {
					showLoader();
					var form_data = $('.interests-form').serializeArray();
					console.log(form_data[1].value);
					var cat_id = form_data[1].value;
					$(element).closest('.f-interest-item').hide();
					$.ajax({
			            type: "POST",
			            url: "/api/add_interest",
			            data: {
			            	category_id: cat_id,
			            },
			            success: function(response) {
			            	if(response['status'] == 'success') {
			     //        		$http.get('/api/get_interests').success(function(data) {
				    //           		// console.log(data)
								// 	// if(!scope.$$phase) {
								// 	  	scope.$watch(function() {
								// 	  		// $timeout(function() {
								// 	  			scope.interests = data;
								// 	  			console.log(scope.interests)
								// 	  		// });
								// 	  	});
								// 	// }
								// });
								// $http.get('/api/user_interests').success(function(data) {
								// 	// console.log(data)
								// 	// if(!scope.$$phase) {
								// 	  	scope.$watch(function() {
								// 	  		// $timeout(function() {
								// 	  			scope.userInterests = data;
								// 	  			console.log(scope.userInterests);
								// 	  		// });
								// 	  	});
								// 	// }
								// });

							
				              	hideLoader();
				            } else if(response['status'] == 'exist') {
				            	var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
					                    <div class="modal-backdrop in"></div>\
					                    <div class="modal-dialog">\
					                      <div class="modal-content">\
					                        <div class="modal-body">\
					                          <p>'+response['message']+'</p>\
					                        </div>\
					                        <div class="modal-footer">\
					                          <a class="close-closest-modal" href="#">Continue browsing</a>\
					                        </div>\
					                      </div>\
					                    </div>\
					                  </div>';
					            $('body').append(subscribe_modal);
					            $('.close-closest-modal').on('click', function(){
					              	$('.close-closest-modal').closest('.modal').remove();
					            });
				            }
			              	
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
					$.ajax({
			            type: "POST",
			            url: "/api/submit_interest",
			            data: {
			            	category_id: cat_id,
			            },
			            success: function(response) {
			            	if(response['status'] == 'success') {

			            	} else if($response['status'] == 'exist') {
			            		var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
					                    <div class="modal-backdrop in"></div>\
					                    <div class="modal-dialog">\
					                      <div class="modal-content">\
					                        <div class="modal-body">\
					                          <p>'+response['message']+'</p>\
					                        </div>\
					                        <div class="modal-footer">\
					                          <a class="close-closest-modal" href="#">Continue browsing</a>\
					                        </div>\
					                      </div>\
					                    </div>\
					                  </div>';
					            $('body').append(subscribe_modal);
					            $('.close-closest-modal').on('click', function(){
					              	$('.close-closest-modal').closest('.modal').remove();
					            });
			            	}
			    //           	$http.get('/api/get_interests').success(function(data) {
				   //        		console.log(data)
							// 	// if(!scope.$$phase) {
							// 	//   	scope.$apply(function() {
								  		
							// 	//   	});
							// 	// }
							// 	scope.interests = data;
							// 	scope.$apply();
							// 	console.log(scope.interests);
							// 	console.log(scope);
							// });
							// $http.get('/api/user_interests').success(function(data) {
							// 	console.log(data)
							// 	// if(!scope.$$phase) {
							// 	//   	scope.$apply(function() {
							// 	//   		scope.userInterests = data;
							// 	//   	});
							// 	// }
							// 	scope.userInterests = data;
							// 	scope.$apply();
							// });
			              	hideLoader();
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
				
				});

			}
		}
	});
	
	app.directive('dRemoveInterest', function($http, $timeout) {
		return  {
			restrict: 'C',
			link: function(scope, element, attributes) {
				$(element).change(function() {
					showLoader();
					var form_data = $('.remove-interests-form').serializeArray();
					console.log(form_data[1].value);
					var cat_id = form_data[1].value;
					$(element).closest('.f-interest-item').hide();
					$.ajax({
			            type: "POST",
			            url: "/api/remove_interest",
			            data: {
			            	category_id: cat_id,
			            },
			            success: function(response) {
			            	if(response['status'] == 'success') {
			     //        		$http.get('/api/get_interests').success(function(data) {
				    //           		console.log(data)
								// 	// if(!scope.$$phase) {
								// 	  	// scope.$apply(function() {
								// 	  		$timeout(function() {
								// 	  			scope.interests = data;
								// 	  		});
								// 	  	// });
								// 	// }
								// });
								// $http.get('/api/user_interests').success(function(data) {
								// 	console.log(data)
								// 	// if(!scope.$$phase) {
								// 	  	// scope.$apply(function() {
								// 	  		$timeout(function() {
								// 	  			scope.userInterests = data;
								// 	  		});
								// 	  	// });
								// 	// }
								// });
								
				              	hideLoader();
				            } 
			              	
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
					$.ajax({
			            type: "POST",
			            url: "/api/delete_interest",
			            data: {
			            	category_id: cat_id,
			            },
			            success: function(response) {
			            	if(response['status'] == 'success') {

			            	} 
			    //           	$http.get('/api/get_interests').success(function(data) {
			    //           		console.log(data)
							// 	// if(!scope.$$phase) {
							// 	//   	scope.$apply(function() {
								  		
							// 	//   	});
							// 	// }
								
							// });
							// $http.get('/api/user_interests').success(function(data) {
							// 	console.log(data)
							// 	// if(!scope.$$phase) {
							// 	//   	scope.$apply(function() {
							// 	//   		scope.userInterests = data;
							// 	//   	});
							// 	// }
								
							// });
			              	hideLoader();
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
				});
			}
		}
	});

	// app.directive('dLikeAction', function($rootScope){
	// 	var linkfunction = function(scope, element, attributes){
	// 		var touchtime = 0;
	// 		$(element).on('click', function(){
	// 			if(touchtime == 0){
	// 				touchtime = new Date().getTime();
	// 			}else{
	// 				 if(((new Date().getTime())-touchtime) < 500) {
	// 				 	console.log(scope.magazineTitle);
	// 					// showLoader();
	// 					var page = $(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id');
	// 					console.log(page);

	// 					if(page == undefined || page == 0 || !$rootScope.logged_in){
	// 						// hideLoader();
	// 						return false;
	// 					}
	// 					var page_likes = $('.owl-item.active .m-page-likes');
	// 					var liked_texts = $('.owl-item.active .liked-texts');
	// 					$(".magazines-viewer").find('.owl-item.active .page-to-like').attr('data-page-id', 0);
	// 					var like_heart = '<div class="liked-response-heart">\
	// 					  <i class="fa fa-heart animated bounceIn"></i>\
	// 					</div>';
	// 					page_likes.addClass('i-liked');
	// 					var user_like_texts = liked_texts.html();
	// 					if(user_like_texts=="No like"){
	// 						user_like_texts = "you like this";
	// 					}
	// 					else{
	// 						user_like_texts = 'you and '+user_like_texts;
	// 					}
	// 					liked_texts.html(user_like_texts);
	// 					 $('body').append(like_heart);
	// 					 setTimeout(function(){
	// 					 	$('.liked-response-heart').remove();
	// 					 }, 1000);
	// 	        $.ajax({
	// 	            type: "POST",
	// 	            url: "/api/likemagazine",
	// 	            data: {page_id: page},
	// 	            success: function(like_data) {
	// 	              console.log(like_data);
	// 	              var like_message = like_data['message'];

	// 	              // $('.close-closest-modal').on('click', function(){
	// 	              // 	$('.close-closest-modal').closest('.modal').remove();
	// 	              // });
	// 	              // hideLoader();
	// 	            },
	// 	            error: function(error) {
	// 	            	console.log(error);
	// 	            	// hideLoader();
	// 	            }
	// 	        });
	// 				 	touchtime = 0;
	// 				 }else{
	// 				 	touchtime = new Date().getTime();
	// 				 }
	// 			}
	// 		});
	// 	}

	// 	return {
	// 		restrict: 'C',
	// 		scope: false,
	// 		link: linkfunction
	// 	}
	// });

	// app.directive('dUnlikeAction', function($rootScope){
	// 	var linkfunction = function(scope, element, attr){
	// 		var touchtime = 0;
	// 		$(element).on('click', function(){
	// 			if(touchtime == 0){
	// 				touchtime = new Date().getTime();
	// 			}else{
	// 				 if(((new Date().getTime())-touchtime) < 500) {
	// 					var page_id = $(element).attr('data-magpage-id');
	// 					var like_id = $(element).attr('data-maglike-id');
	// 					var magazine_id = $(element).attr('data-magazine-id');

	// 					if(page_id == undefined || page_id == 0 || !$rootScope.logged_in){
	// 						// hideLoader();
	// 						return false;
	// 					}
	// 					if(like_id == undefined || like_id == 0){
	// 						// hideLoader();
	// 						return false;
	// 					}
	// 					if(magazine_id == undefined || magazine_id == 0){
	// 						// hideLoader();
	// 						return false;
	// 					}
	// 					$(element).remove();
	// 					var new_like_total = $('#like_total').html() - 1;
	// 					$('#like_total').html(new_like_total);
	// 					var unlike_heart = '<div class="unliked-response-heart">\
	// 					  <i class="fa fa-times-circle-o animated bounceIn"></i>\
	// 					</div>';
	// 					 $('body').append(unlike_heart);
	// 					 setTimeout(function(){
	// 					 	$('.unliked-response-heart').remove();
	// 					 }, 1000);
	// 	        $.ajax({
	// 	            type: "POST",
	// 	            url: "/api/unlikemagazine",
	// 	            data: {page_id: page_id, like_id: like_id, magazine_id: magazine_id},
	// 	            success: function(unlike_data) {
	// 	              console.log(unlike_data);
	// 	              var unlike_message = unlike_data['message'];

	// 	              // $('.close-closest-modal').on('click', function(){
	// 	              // 	$('.close-closest-modal').closest('.modal').remove();
	// 	              // });
	// 	              // hideLoader();
	// 	            },
	// 	            error: function(error) {
	// 	            	console.log(error);
	// 	            	// hideLoader();
	// 	            }
	// 	        });
	// 				 	touchtime = 0;
	// 				 }else{
	// 				 	touchtime = new Date().getTime();
	// 				 }
	// 			}
	// 		});
	// 	}

	// 	return {
	// 		restrict: 'C',
	// 		scope: false,
	// 		link: linkfunction
	// 	}
	// });

	app.directive('dCloseModal', function(){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				console.log($(element).closest('.modal').attr('class'));
				$(element).closest('.modal').remove();
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dSubmitSupport', function($rootScope){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				if($(element).attr('dontsendagain') == 'yes'){
					$('.support-message .message').html("Please wait for 2 minutes to send again.").addClass('text-danger').removeClass('text-success');
					$('.support-message').addClass('bg-danger').removeClass('bg-success').fadeIn(1500);
					setTimeout(function(){
		              	$('.support-message').fadeOut(1500);
		            }, 10000);
					return false;
				}
				if(!$(".support-form").validationEngine('validate', {promptPosition: 'topLeft'})) {
		    		setTimeout(function(){
						$(".support-form").validationEngine('hideAll');
		    		}, 10000);
					return false;
				}
				else{
					showLoader();
					var form_data = $(".support-form").serializeArray();
					console.log(form_data);
			        $.ajax({
			            type: "POST",
			            url: "/api/contactsupport",
			            data: form_data,
			            success: function(support_data) {
			              console.log(support_data);
			              if(support_data['status'] == 'success') {
			              	$('.support-message .message').html(support_data['message']).addClass('text-success').removeClass('text-danger');
			              	$('.support-message').addClass('bg-success').removeClass('bg-danger').fadeIn(1500);
			              	$(element).attr('dontsendagain', 'yes');
			              	setTimeout(function(){
			              		$(element).attr('dontsendagain', 'no');
			              	}, 1000*60*2);
			              }
			              else {
			              	$('.support-message .message').html(support_data['message']).addClass('text-danger').removeClass('text-success');
			              	$('.support-message').addClass('bg-danger').removeClass('bg-success').fadeIn(1500);
			              }
			              setTimeout(function(){
			              	$('.support-message').fadeOut(1500);
			              }, 10000);
			              hideLoader();
			            },
			            error: function(error) {
			            	console.log(error);
			            	hideLoader();
			            }
			        });
				}
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dShareAction', function($compile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				var theBaseURL = location.protocol + '//' + location.host;
				console.log(theBaseURL);
				console.log(location)
				var magImage = "", magTitle = "";
				if(attributes.whereShare == 'mbrowser'){
				    magImage = $('.owl-item.active .magazine-cover-photo').attr('src');
					// magTitle = $('.owl-item.active .magazine-title').html();
					var magTitle = $(".magazines-browser-slider").find('.owl-item.active').find('.magazine-cover-container').attr('fmag-title');
				}else if(attributes.whereShare == 'mdashboard'){
					magImage = attributes.fmagImage;
					magTitle = attributes.fmagTitle;
				}
				// var magUrl = 'http://m.freyo.com/';
				var magUrl = 'http://m.freyo.com' + location.pathname;
				// var sTwitter = '<a href="#" class="share-twitter" socialshare socialshare-provider="twitter" socialshare-text="'+magTitle+'"  socialshare-hashtags="Freyo" socialshare-url="'+magUrl+'"><i class="fa fa-twitter"></i> Twitter</a>';
				// var sGoogle = '<a href="#" class="share-google" socialshare socialshare-provider="google+" socialshare-url="'+magUrl+'" socialshare-media="'+magImage+'"><i class="fa fa-google-plus"></i> Google+</a>';
				// var sFacebook = '<a href="#" class="share-facebook" socialshare socialshare-provider="facebook" socialshare-text="'+magTitle+'"  socialshare-url="'+magUrl+'" socialshare-media="'+magImage+'"><i class="fa fa-facebook"></i> Facebook</a>';
				var sTwitter = '<a href="#" class="share-btn" socialshare socialshare-provider="twitter" socialshare-text="'+magTitle+'"  socialshare-hashtags="Freyo" socialshare-url="'+magUrl+'"><img src="'+theBaseURL+'/assets/mobile/images/social-twitter-circular-button.png"/></a>';
				var sGoogle = '<a href="#" class="share-btn" socialshare socialshare-provider="google+" socialshare-url="'+magUrl+'" socialshare-media="'+magImage+'"><img src="'+theBaseURL+'/assets/mobile/images/social-google-plus-circular-button.png"/></a>';
				var sFacebook = '<a href="#" class="share-btn" socialshare socialshare-provider="facebook" socialshare-text="'+magTitle+'"  socialshare-url="'+magUrl+'" socialshare-media="'+magImage+'"><img src="'+theBaseURL+'/assets/mobile/images/social-facebook-circular-button.png"/></a>';
// <div class="modal-body">\
	                    //   <p>Share <b>'+magTitle+'</b></p>\
	                    // </div>\
		        var share_modal = '<div class="modal sharing-modal text-center ng-scope">\
		            <div class="close-share-modal modal-backdrop in"></div>\
	                <div class="modal-dialog">\
	                  <div class="">\
	                    \
	                    <div class="">\
	                      '+sFacebook+sTwitter+sGoogle+'\
	                    </div>\
	                  </div>\
	                </div>\
	                </div>';
		        $('body').append($compile(share_modal)(scope));
		        $('.close-share-modal').on('click', function(){
		        	$(this).closest('.modal').remove();
		        });
		    });
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('dAlphabetOnly', function(){
		var linkfunction = function(scope, element, attributes){
			$(element).bind('keypress', function (event) {
			    var regex = new RegExp("^[ña-zA-Z\. ]+$");
			    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			    if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});
	app.directive('dPhoneNumber', function(){
		var linkfunction = function(scope, element, attributes){
			$(element).bind('keypress', function (event) {
			    var regex = new RegExp("^[0-9\-\+ ]+$");
			    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
			    if (!regex.test(key)) {
			       event.preventDefault();
			       return false;
			    }
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	// app.directive('lazy', function(){
	// 	var linkfunction = function(scope, element, attributes){
	// 		setTimeout(function(){
	//       		$("img.lazy").lazyload({
	// 			    effect : "fadeIn",
	// 			    container: $(".scrollable-content")
	// 			});
	//       	}, 0);

	// 	}

	// 	return {
	// 		restrict: 'C',
	// 		link: linkfunction
	// 	}
	// });

	app.directive('isLoading', function($timeout){
		var linkfunction = function(scope, element, attributes){
			$timeout(function(){
				$(element).append('<div class="cssload-spin-box"></div>');
	      		$(element).imagesLoaded().progress(function(imgLoad, image){
	      			// change class if the image is loaded or broken
	      			var $item = $( image.img ).parent();
	      			$item.removeClass('is-loading');
	      			$item.children('.cssload-spin-box').remove();
	      			$item.removeAttr('style');
	      			if ( !image.isLoaded ) {
	      			    $item.addClass('is-broken');
	      			}
	      		});
	      	});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	//topcharts and new release
	app.directive('dSubscribeActionC', function(){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				showLoader();
				var magazine = $(element).attr('data-magazine-id');
				console.log(magazine);
		        $.ajax({
		            type: "POST",
		            url: "/api/favoritemagazine",
		            data: {magazine_id: magazine},
		            success: function(favorite_data) {
		              console.log(favorite_data);
		              var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
		                    <div class="modal-backdrop in"></div>\
		                    <div class="modal-dialog">\
		                      <div class="modal-content">\
		                        <div class="modal-body">\
		                          <p>'+favorite_data['message']+'</p>\
		                        </div>\
		                        <div class="modal-footer">\
		                          <a class="click-the-magazine btn close-closest-modal" href="#">Go to magazine</a>\
		                          <a class="close-closest-modal" href="#">Continue browsing</a>\
		                        </div>\
		                      </div>\
		                    </div>\
		                  </div>';
		              $('body').append(subscribe_modal);
		              $('.close-closest-modal').on('click', function(){
		              	$(this).closest('.modal').remove();
		              });
		              $('.click-the-magazine').on('click', function(){
		              	$('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover').trigger('click');
		              	console.log($('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover'));
		              });
		              hideLoader();
		            },
		            error: function(error) {
		            	console.log(error);
		            	hideLoader();
		            }
		        });
		        $.ajax({
		            type: "POST",
		            url: "/api/subscribemagazine",
		            data: {magazine_id: magazine},
		            success: function(favorite_data) {
		              console.log(favorite_data);
		              // var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
		              //       <div class="modal-backdrop in"></div>\
		              //       <div class="modal-dialog">\
		              //         <div class="modal-content">\
		              //           <div class="modal-body">\
		              //             <p>'+favorite_data['message']+'</p>\
		              //           </div>\
		              //           <div class="modal-footer">\
		              //             <a class="click-the-magazine btn close-closest-modal" href="#">Go to magazine</a>\
		              //             <a class="close-closest-modal" href="#">Continue browsing</a>\
		              //           </div>\
		              //         </div>\
		              //       </div>\
		              //     </div>';
		              // $('body').append(subscribe_modal);
		              // $('.close-closest-modal').on('click', function(){
		              // 	$(this).closest('.modal').remove();
		              // });
		              // $('.click-the-magazine').on('click', function(){
		              // 	$('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover').trigger('click');
		              // 	console.log($('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover'));
		              // });
		              // hideLoader();
		            },
		            error: function(error) {
		            	console.log(error);
		            	hideLoader();
		            }
		        });
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	//magbrowser
	app.directive('dFavoriteActionC', function(){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				showLoader();
				var magazine = $(".magazines-browser-slider").find('.owl-item.active').find('.magazine-cover-container').attr('data-magazine-id');
				console.log(magazine);
		        $.ajax({
		            type: "POST",
		            url: "/api/favoritemagazine",
		            data: {magazine_id: magazine},
		            success: function(favorite_data) {
		              console.log(favorite_data);
		              var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
		                    <div class="modal-backdrop in"></div>\
		                    <div class="modal-dialog">\
		                      <div class="modal-content">\
		                        <div class="modal-body">\
		                          <p>'+favorite_data['message']+'</p>\
		                        </div>\
		                        <div class="modal-footer">\
		                          <a class="click-the-magazine btn close-closest-modal" href="#">Read the magazine</a>\
		                          <a class="close-closest-modal" href="#">Continue browsing</a>\
		                        </div>\
		                      </div>\
		                    </div>\
		                  </div>';
		              $('body').append(subscribe_modal);
		              $('.close-closest-modal').on('click', function(){
		              	$('.close-closest-modal').closest('.modal').remove();
		              });
		              $('.click-the-magazine').on('click', function(){
		              	$('.owl-item.active .magazine-cover-container a').trigger('click');
		              });
		              hideLoader();
		            },
		            error: function(error) {
		            	console.log(error);
		            	hideLoader();
		            }
		        });
		        $.ajax({
		            type: "POST",
		            url: "/api/subscribemagazine",
		            data: {magazine_id: magazine},
		            success: function(favorite_data) {
		              console.log(favorite_data);
		              // var subscribe_modal =	'<div class="modal signout-dialog-box subscribe-status text-center ng-scope">\
		              //       <div class="modal-backdrop in"></div>\
		              //       <div class="modal-dialog">\
		              //         <div class="modal-content">\
		              //           <div class="modal-body">\
		              //             <p>'+favorite_data['message']+'</p>\
		              //           </div>\
		              //           <div class="modal-footer">\
		              //             <a class="click-the-magazine btn close-closest-modal" href="#">Go to magazine</a>\
		              //             <a class="close-closest-modal" href="#">Continue browsing</a>\
		              //           </div>\
		              //         </div>\
		              //       </div>\
		              //     </div>';
		              // $('body').append(subscribe_modal);
		              // $('.close-closest-modal').on('click', function(){
		              // 	$(this).closest('.modal').remove();
		              // });
		              // $('.click-the-magazine').on('click', function(){
		              // 	$('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover').trigger('click');
		              // 	console.log($('.dSubscribeAction[data-magazine-id='+magazine+']').closest('.f-magazine-box').find('.magazine-cover'));
		              // });
		              // hideLoader();
		            },
		            error: function(error) {
		            	console.log(error);
		            	hideLoader();
		            }
		        });
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	//topcharts and new release
	app.directive('dSubscribeConfirmation', function($compile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				var magazine = $(element).attr('data-magazine-id');
				var magtitle = $(element).attr('fmag-title');
				var subscribe_modal = '<div class="modal signout-dialog-box text-center">\
				  <div class="modal-backdrop in"></div>\
				  <div class="modal-dialog">\
				    <div class="modal-content">\
				      <div class="modal-header">\
				        <h4 class="modal-title">Do you want to subscribe to <b>'+ magtitle +'</b>?</h4>\
				      </div>\
				      <div class="modal-footer">\
				        <a href="#" class="close-closest-modal">Cancel</a>\
				        <a href="#" class="sign-out-now dSubscribeActionC close-closest-modal" data-magazine-id="'+magazine+'">Subscribe</a>\
				      </div>\
				    </div>\
				  </div>\
				</div>';
	            $('body').append($compile(subscribe_modal)(scope));
	            $('.close-closest-modal').on('click', function(){
	              	$(this).closest('.modal').remove();
	            });
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	//mag browser
	app.directive('dConfirmSubscribe', function($compile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				var magtitle = $(".magazines-browser-slider").find('.owl-item.active').find('.magazine-cover-container').attr('fmag-title');
				var subscribe_modal = '<div class="modal signout-dialog-box text-center">\
				  <div class="modal-backdrop in"></div>\
				  <div class="modal-dialog">\
				    <div class="modal-content">\
				      <div class="modal-header">\
				        <h4 class="modal-title">Do you want to subscribe to <b>'+ magtitle +'</b>?</h4>\
				      </div>\
				      <div class="modal-footer">\
				        <a href="#" class="close-closest-modal">Cancel</a>\
				        <a href="#" class="sign-out-now dFavoriteActionC close-closest-modal">Subscribe</a>\
				      </div>\
				    </div>\
				  </div>\
				</div>';
	            $('body').append($compile(subscribe_modal)(scope));
	            $('.close-closest-modal').on('click', function(){
	              	$(this).closest('.modal').remove();
	            });
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});

	app.directive('closeCoachmark', function($compile){
		var linkfunction = function(scope, element, attributes){
			$(element).on('click', function(){
				$('body').removeClass('coach');
			});
		}

		return {
			restrict: 'C',
			link: linkfunction
		}
	});
	//END DIRECTIVES

})();