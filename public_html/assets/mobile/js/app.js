$(window).load(function(){
 	$('.fpreloader').remove();
});
(function(){
	// var loaderhtml='<div class="floader"><div class="floader-load"></div></div>';
	var loaderhtml="<div class='floader'><div class='floader-load'>\
		<style type='text/css'>@-webkit-keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }@keyframes uil-default-anim { 0% { opacity: 1} 100% {opacity: 0} }.uil-default-css > div:nth-of-type(1){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.5s;animation-delay: -0.5s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(2){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: -0.16666666666666669s;animation-delay: -0.16666666666666669s;}.uil-default-css { background:none;width:200px;height:200px;}.uil-default-css > div:nth-of-type(3){-webkit-animation: uil-default-anim 1s linear infinite;animation: uil-default-anim 1s linear infinite;-webkit-animation-delay: 0.16666666666666663s;animation-delay: 0.16666666666666663s;}.uil-default-css { background:none;width:200px;height:200px;}</style><div class='uil-default-css' style='transform:scale(0.6);'><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(0deg) translate(0,-50px);transform:rotate(0deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(120deg) translate(0,-50px);transform:rotate(120deg) translate(0,-50px);border-radius:40px;position:absolute;'></div><div style='top:80px;left:80px;width:40px;height:40px;background:#ffffff;-webkit-transform:rotate(240deg) translate(0,-50px);transform:rotate(240deg) translate(0,-50px);border-radius:40px;position:absolute;'></div></div>\
	</div></div>";
	var showLoader = function(){
		console.log('show');
		$('body').append(loaderhtml);
	}
	var hideLoader = function(){
		console.log('hide');
		$('.floader').remove();
	}

	// START FACEBOOK LOGIN
	$('.signin_fb').click(function(){
		showLoader();
	});
	// END FACEBOOK LOGIN

	// Offline JS
	var run = function(){
      Offline.check();
    }
    window.onbeforeunload = function(ev) {
        showLoader();
    };
    setInterval(run, 3000);
    Offline.on('down', disableAction);
    Offline.on('up', enableAction);
    function disableAction() {
      // showLoader();
      window.onbeforeunload = function(ev) {
        if(Offline.state == 'down') {
          ev.stopPropagation();
          return "Please stay on this page. Any data you have entered may not be saved.";
        }
      };
    }
    function enableAction() {
      // hideLoader();
      window.onbeforeunload = function(ev) {
        // showLoader();
      };
    }
    // END OFFLINE JS

	var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	var aSlideOutLeft = 'animated slideOutLeft';
	var aSlideInLeft = 'animated slideInLeft';
	var aSlideOutRight = 'animated slideOutRight';
	var aSlideInRight = 'animated slideInRight';
	$('.owl-form').owlCarousel({
		items:1,
	    loop:false,
	    singleItem: true, 
	    margin: 20
	});

	$('.walkthrough-img-container').owlCarousel({
	    items:1,
	    loop:false,
	    dotsContainer:'.walkthrough-nav',
	    margin: 100
	});

	$('.walkthrough-img-container').on('changed.owl.carousel', function(event){
		if(event.item.index+1 === event.item.count){
			$('.walkthrough-next').fadeIn(333);
		}
	});

	// $('.btn_forgot_user_pass').click(function() {
	// 	$('.f-launch-screen').fadeOut(444);
	// 	$('.reset-form').fadeIn(666);
	// });

	$('.btn_back_to_sign_in').click(function() {
		$('.reset-form').fadeOut(444);
		$('.f-launch-screen').fadeIn(666);
	});

	// $('.walkthrough-next').click(function(){
	// 	$('.f-walkthrough').fadeOut(444);
	// 	$('.f-launch-screen').fadeIn(666);
	// 	// $('.f-launch-screen').show();
	// 	// $('body').append('<div class="ads-is-viewed"></div>');
	//  //    $('.f-walkthrough').addClass(aSlideOutLeft).one(animationEnd, function(){
	//  //    	$('.f-walkthrough').removeClass(aSlideOutLeft);
	//  //    	$('.f-walkthrough').hide();
	// 	// 	$('div.ads-is-viewed').remove();
	//  //    });
	// });
	
	$('.walkthrough-next').click(function () {
		$('.f-walkthrough').fadeOut(444);
		$('.f-si-formpage').fadeIn(999);
	} );

	$('#show-it').click(function() {
		console.log($('#password').attr('type'))
		var isPassword = $('#password').attr('type') == 'password'? true : false;
		var $passInput = $('#password');
		if(isPassword) {
			$passInput.attr('type', 'text');
		} else {
			$passInput.attr('type', 'password');
		}
	});
 
	$('.btn-page-1').click(function() {
		if(!$(".fsu-form").validationEngine('validate', {promptPosition: 'topLeft'})) {
    		setTimeout(function(){
				$(".fsu-form").validationEngine('hideAll');
    		}, 10000);
    		return false;
    	} else {
    		$('body').append('<div class="ads-is-viewed"></div>');
			    // $('.form-item-1').addClass(aSlideOutLeft).one(animationEnd, function(){
			    // 	$('.form-item-1').removeClass(aSlideOutLeft);
			    	$('.form-item-1').hide();
					$('div.ads-is-viewed').remove();
			    // });


			 //    $('.form-item-2').addClass(aSlideInRight).one(animationEnd, function(){
				// 	$('.form-item-2').removeClass(aSlideInRight);
				// });
				$('.form-item-2').show();
    	}
		
	});

	if($('.f-signup-form').length != 0) {
		var hammerLaunch = new Hammer($('.f-signup-form').get(0));
		// hammerLaunch.on('swipeleft', function(ev) {
		// 	if($('.form-item-2').is(':visible')) {

		// 	} else {
		// 		$('body').append('<div class="ads-is-viewed"></div>');
		// 	    // $('.form-item-1').addClass(aSlideOutLeft).one(animationEnd, function(){
		// 	    // 	$('.form-item-1').removeClass(aSlideOutLeft);
		// 	    	$('.form-item-1').hide();
		// 			$('div.ads-is-viewed').remove();
		// 	    // });


		// 	 //    $('.form-item-2').addClass(aSlideInRight).one(animationEnd, function(){
		// 		// 	$('.form-item-2').removeClass(aSlideInRight);
		// 		// });
		// 		$('.form-item-2').show();
		// 	}


		// 	//For Choose Interests/Category
		//     // $('.f-su-interests').addClass(aSlideInRight).one(animationEnd, function(){
		//     // 	$('.f-su-interests').removeClass(aSlideInRight);
		//     // });
		//     // $('.f-su-interests').show();

		//     return false;
		// });

		hammerLaunch.on('swiperight', function(ev) {
			if($('.form-item-1').is(':visible')) {

			} else {
				$('body').append('<div class="ads-is-viewed"></div>');
			    // $('.form-item-2').addClass(aSlideOutRight).one(animationEnd, function(){
			    // 	$('.form-item-2').removeClass(aSlideOutRight);
			    	$('.form-item-2').hide();
					$('div.ads-is-viewed').remove();
			    // });


			 //    $('.form-item-1').addClass(aSlideInLeft).one(animationEnd, function(){
				// 	$('.form-item-1').removeClass(aSlideInLeft);
				// });
				$('.form-item-1').show();
			}


			//For Choose Interests/Category
		    // $('.f-su-interests').addClass(aSlideInRight).one(animationEnd, function(){
		    // 	$('.f-su-interests').removeClass(aSlideInRight);
		    // });
		    // $('.f-su-interests').show();

		    return false;
		});
	}

	if($('.f-launch-screen').length != 0){
		var hammerLaunch = new Hammer($('.f-launch-screen').get(0));
		hammerLaunch.on('swipeleft', function(ev) {
			$('body').append('<div class="ads-is-viewed"></div>');
		    $('.f-launch-screen').addClass(aSlideOutLeft).one(animationEnd, function(){
		    	$('.f-launch-screen').removeClass(aSlideOutLeft);
		    	$('.f-launch-screen').hide();
				$('div.ads-is-viewed').remove();
		    });


		    $('.f-su-formpage').addClass(aSlideInRight).one(animationEnd, function(){
				$('.f-su-formpage').removeClass(aSlideInRight);
			});
			$('.f-su-formpage').show();


			//For Choose Interests/Category
		    // $('.f-su-interests').addClass(aSlideInRight).one(animationEnd, function(){
		    // 	$('.f-su-interests').removeClass(aSlideInRight);
		    // });
		    // $('.f-su-interests').show();

		    return false;
		});
	}


	$('.btn_go_sign_up').on('click', function(){
		$('body').append('<div class="ads-is-viewed"></div>');
		// $('.f-si-formpage').fadeOut(777);
		$('.f-si-formpage').addClass(aSlideOutLeft).one(animationEnd, function(){
	    	$('.f-si-formpage').removeClass(aSlideOutLeft);
	    	$('.f-si-formpage').hide();
			$('div.ads-is-viewed').remove();
	    });

		$('.f-su-formpage').addClass(aSlideInRight).one(animationEnd, function(){
			$('.f-su-formpage').removeClass(aSlideInRight);
			$('div.ads-is-viewed').remove();
		});
		$('.f-su-formpage').show();
	});
	$('.btn_forgot_user_pass').on('click', function(){
		$('body').append('<div class="ads-is-viewed"></div>');
		// $('.f-si-formpage').fadeOut(777);
		$('.f-si-formpage').addClass(aSlideOutLeft).one(animationEnd, function(){
	    	$('.f-si-formpage').removeClass(aSlideOutLeft);
	    	$('.f-si-formpage').hide();
			$('div.ads-is-viewed').remove();
	    });

		$('.f-fp-formpage').addClass(aSlideInRight).one(animationEnd, function(){
			$('.f-fp-formpage').removeClass(aSlideInRight);
			$('div.ads-is-viewed').remove();
		});
		$('.f-fp-formpage').show();
	});

	//For Choose Interests/Category
	// $('.btn_back_to_welcome').click(function(){		
	//     $('.f-su-interests').addClass(aSlideOutRight).one(animationEnd, function(){
	//     	$('.f-su-interests').removeClass(aSlideOutRight);
	//     	$('.f-su-interests').hide();
	//     });    


	//     $('.f-launch-screen').addClass(aSlideInLeft).one(animationEnd, function(){
	//     	$('.f-launch-screen').removeClass(aSlideInLeft);
	//     });
	//     $('.f-launch-screen').show();

	//     return false;
	// });


	//For Choose Interests/Category
	// $('.btn_next_step').click(function(){
	// 	$('.f-su-interests').addClass(aSlideOutLeft).one(animationEnd, function(){
	// 		$('.f-su-interests').removeClass(aSlideOutLeft);
	// 		$('.f-su-interests').hide();
	// 	});
	// 	$('.f-su-formpage').addClass(aSlideInRight).one(animationEnd, function(){
	// 		$('.f-su-formpage').removeClass(aSlideInRight);
	// 	});
	// 	$('.f-su-formpage').show();

	//     return false;
	// });


	$('.btn_back_to_interests').click(function(){
		$('body').append('<div class="ads-is-viewed"></div>');
		$('.f-su-formpage').addClass(aSlideOutRight).one(animationEnd, function(){
			$('.f-su-formpage').removeClass(aSlideOutRight);
			$('.f-su-formpage').hide();
			$('div.ads-is-viewed').remove();
		});    


		// $('.f-launch-screen').addClass(aSlideInLeft).one(animationEnd, function(){
	 //    	$('.f-launch-screen').removeClass(aSlideInLeft);
	 //    });
	 //    $('.f-launch-screen').show();
	 	$('.f-si-formpage').addClass(aSlideInLeft).one(animationEnd, function(){
	    	$('.f-si-formpage').removeClass(aSlideInLeft);
	    });
	    $('.f-si-formpage').show();


	    //For Choose Interests/Category
		// $('.f-su-interests').addClass(aSlideInLeft).one(animationEnd, function(){
		// 	$('.f-su-interests').removeClass(aSlideInLeft);
		// });
		// $('.f-su-interests').show();


	    return false;
	});
	$('.btn_back_to_interests_from_fp').click(function(){
		$('body').append('<div class="ads-is-viewed"></div>');
		$('.f-fp-formpage').addClass(aSlideOutRight).one(animationEnd, function(){
			$('.f-fp-formpage').removeClass(aSlideOutRight);
			$('.f-fp-formpage').hide();
			$('div.ads-is-viewed').remove();
		});    


		// $('.f-launch-screen').addClass(aSlideInLeft).one(animationEnd, function(){
	 //    	$('.f-launch-screen').removeClass(aSlideInLeft);
	 //    });
	 //    $('.f-launch-screen').show();
	 	$('.f-si-formpage').addClass(aSlideInLeft).one(animationEnd, function(){
	    	$('.f-si-formpage').removeClass(aSlideInLeft);
	    });
	    $('.f-si-formpage').show();


	    //For Choose Interests/Category
		// $('.f-su-interests').addClass(aSlideInLeft).one(animationEnd, function(){
		// 	$('.f-su-interests').removeClass(aSlideInLeft);
		// });
		// $('.f-su-interests').show();


	    return false;
	});


	var sign_in_func = function () {
		$(this).closest('.container-fluid').fadeOut(333);
		$('.f-si-formpage').fadeIn(999);
	} 


	$('.btn_go_sign_in').on('click', sign_in_func);


	$('.btn_si_to_welcome').click(function(){
		$('body').append('<div class="ads-is-viewed"></div>');
		$('.btn_go_sign_in').off();
	    $('.f-si-formpage').addClass(aSlideOutRight).one(animationEnd, function(){
	    	$('.f-si-formpage').removeClass(aSlideOutRight);
	    	$('.f-si-formpage').hide();
			$('div.ads-is-viewed').remove();
	    });    
	    $('.f-launch-screen').addClass(aSlideInLeft).one(animationEnd, function(){
	    	$('.f-launch-screen').removeClass(aSlideInLeft);
	    	$('.btn_go_sign_in').on('click', sign_in_func);
	    });
	    $('.f-launch-screen').show();

	    return false;
	});


	//For Choose Interests/Category
	// $('.f-su-categories').click(function(){
	// 	if($(this).hasClass('cat-interested')){
	// 		$(this).removeClass('cat-interested');	
	// 	}else{
	// 		$(this).addClass('cat-interested');
	// 	}

	//     return false;
	// });

	$('.fsu-input-box').focus(function(){
		$(this).parent('.fsu-input').addClass('fsu-active');
	});

	$('.fsu-input-box').blur(function(){
		if($(this).parent('.fsu-input').hasClass('fsu-active') && !$(this).val()){
			$(this).parent('.fsu-input').removeClass('fsu-active');
		}
	});

	$('.fsu-input-box').each(function(){
		if($(this).val()){
			$(this).parent('.fsu-input').addClass('fsu-active');
		}
	});
	var ph_location = {};
	function getPHLocation() {

		var getPH = $.get('/ajax/getphilippines', function(data){
			ph_location = data;
			var provinceOption = "<option value='0'>Province</option>";
			var cityOption = "<option value='0'>City/Municipality</option>";
			$.each(data, function(key, value){
				provinceOption += "<option data-key='"+key+"' value='"+value.id+"'>"+value.name+"</option>";
				if(key==0){					
					$.each(value.cities, function(key, value){
						cityOption += "<option value='"+value.id+"'>"+value.name+"</option>";
					});
				}
			});
			$('.ph-provinces').html(provinceOption);
			$('.ph-cities').html(cityOption);
		});
	} 
	getPHLocation();
	function getWorld() {
		var countries = {};
		var getWorld = $.get('/ajax/getworld', function(data){
			countries = data;
			countryOption = "<option value='0'>Country</option>";
			$.each(data, function(key, value){
				var selected = '';
				if(value.country_code == 'PH') {
					selected = 'selected';
				} 
				countryOption += "<option data-key='"+key+"' value='"+value.country_code+"' "+selected+">"+value.country_name+"</option>";
				
			});
			$('.country').html(countryOption);
		});
	}
	getWorld();

	$('.country').change(function() {
		var $this = $(this);
		if($this.val() != 'PH') {
			$('.ph-provinces').hide().attr('disabled', 'disabled');
			$('.ph-cities').hide().attr('disabled', 'disabled');
		} else {
			$('.ph-provinces').show().removeAttr('disabled');
			$('.ph-cities').show().removeAttr('disabled');
		}
	});

	$('.ph-provinces').change(function(){
		$(".ph-provinces option:selected").each(function() {
			var provinceKey = $(this).attr('data-key');
			var cityOption = "<option value='0'>City/Municipality</option>";			
			$.each(ph_location[provinceKey].cities, function(key, value){
				cityOption += "<option value='"+value.id+"'>"+value.name+"</option>";
			})
			$('.ph-cities').html(cityOption);
		});
	});


	// Sign in functions
	$('.btn_submit_sign_in').click(function(){
    	if(!$(".sign_in").validationEngine('validate', {promptPosition: 'topLeft'})) {
    		setTimeout(function(){
				$(".sign_in").validationEngine('hideAll');
    		}, 10000);
    		return false;
    	}
    });
    
    $('.sign_in').on('submit', function(){
    		showLoader();
          var form_data = $(".sign_in").serializeArray();
		    console.log(form_data);
		    
          $.ajax({
            type: "POST",
            url: "/?app=banana",
            data: form_data,
            success: function(login_data) {
              console.log(login_data);
              if(login_data['status'] == 'success') {
              	// $('.login_message .message').html(login_data['message']).addClass('text-success').removeClass('text-danger');
              	$('.login_message .message').html(login_data['message']).addClass('f-msg-si');
              	// $('.login_message').addClass('fsi-msg-border bg-success').removeClass('bg-danger').fadeIn(500);
              	$('.login_message').addClass('fsi-msg-border f-msg-bg-si').fadeIn(500);
              	// Insert Redirect Code Here
              	window.location.replace("/dashboard");
              }
              else if(login_data['status'] == 'not_confirmed') {
              	$('.login_message .message').html(login_data['message']).addClass('text-warning').removeClass('text-success text-danger');
              	$('.login_message').addClass('bg-warning').removeClass('bg-success bg-danger').fadeIn(500);
              	hideLoader();
              }
              else {
              	// $('.login_message .message').html(login_data['message']).addClass('text-danger').removeClass('text-success');
              	// $('.login_message').addClass('bg-danger').removeClass('bg-success').fadeIn(500);
              	$('.login_message .message').html(login_data['message']).addClass('f-msg-si');
              	$('.login_message').addClass('fsi-msg-border f-msg-bg-si').fadeIn(500);
              	hideLoader();
              }
              if(login_data['status'] != 'not_confirmed'){
	              setTimeout(function(){ 
	              	$('.login_message').fadeOut(1500);
	              }, 10000);
	          }
            },
            error: function(error) {
            	console.log(error);
            	$('.login_message .message').html('Login failed, please check your internet connection and try again.').addClass('text-danger').removeClass('text-success');
            	$('.login_message').addClass('bg-danger').removeClass('bg-success').fadeIn(500);
            	setTimeout(function(){ 
	              	$('.login_message').fadeOut(1500);
	            }, 10000);
            	hideLoader();
            }
          });

          return false;
    });


    $('.sign_in').on('click', '.resend_email_confirmation', function(){
    	showLoader();
    	var user_email = $(this).attr('data-email');
    	$.ajax({
    		type: "POST",
    		url: "/api/resend_confirmation",
    		data: {
    			email_add: user_email
    		},
    		success: function(resend_data) {
    			console.log(resend_data);
    			if(resend_data['status'] == 'email_sent') {
	              	$('.login_message .message').html(resend_data['message']).addClass('text-success').removeClass('text-danger text-warning');
	              	$('.login_message').addClass('bg-success').removeClass('bg-danger bg-warning').fadeIn(500);
              	}
              	else {
	              	$('.login_message .message').html(resend_data['message']).addClass('text-danger').removeClass('text-success text-warning');
	              	$('.login_message').addClass('bg-danger').removeClass('bg-success bg-warning').fadeIn(500);
              	}
	            hideLoader();
    		},
    		error: function(error) {
            	console.log(error);
            	$('.login_message .message').html('Resend failed, please check your internet connection and try again.').addClass('text-danger').removeClass('text-success text-warning');
            	$('.login_message').addClass('bg-danger').removeClass('bg-success bg-warning').fadeIn(500);
            	setTimeout(function(){ 
	              	$('.login_message').fadeOut(1500);
	            }, 10000);
            	hideLoader();
            }
    	});
    	return false;
    });

	// Sign up functions
	$('.btn_submit_sign_up').click(function(){
    	if(!$(".fsu-form").validationEngine('validate', {promptPosition: 'topLeft'})) {
    		setTimeout(function(){
				$(".fsu-form").validationEngine('hideAll');
    		}, 10000);
    		return false;
    	}
    });

	//Prevent numbers and special charcters
    $('#first_name').bind('keypress', function (event) {
        var regex = new RegExp("^[ña-zA-Z\. ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });
    $('#last_name').bind('keypress', function (event) {
        var regex = new RegExp("^[ña-zA-Z\. ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });
    //Prevent all except numbers and space
    $('#mobile_number').bind('keypress', function (event) {
        var regex = new RegExp("^[0-9\+\- ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });
    
    $('.fsu-form').on('submit', function(){
    		showLoader();
          var form_data = $(".fsu-form").serializeArray();
		    console.log(form_data);
		    
          $.ajax({
            type: "POST",
            url: "/register",
            data: form_data,
            success: function(registration_data) {
              // console.log(registration_data);
              if(registration_data['status'] == 'registered') {
              	// $('.f-su-formpage').find('.f-signup-form').html('<div class="text-success text-center bg-success" style="padding: 20px 10px;"><h3>Registered</h3><p>We\'ve sent email confirmation to your email.</p><p>Please click the link in the message to activate your account.</p><p>Make sure to check your junk or spam folder. The sender is <b>admin@freyo.com</b></p></div>');
              	// $('.registration_message .message').html(registration_data['message']).addClass('text-success').removeClass('text-danger');
              	// $('.registration_message').addClass('bg-success').removeClass('bg-danger').fadeIn(500);
              	// // Insert Redirect Code Here
              	// // window.location.replace("/dashboard");
              	// $('.btn_go_sign_in').trigger('click');
              	$('.registration_message .message').html(registration_data['message']).addClass('f-msg-su');
              	$('.registration_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
              	window.location.replace("/dashboard"); 
              }
              else {
              	// $('.registration_message .message').html(registration_data['message']).addClass('text-danger').removeClass('text-success');
              	// $('.registration_message').addClass('bg-danger').removeClass('bg-success').fadeIn(500);
              	$('.registration_message .message').html(registration_data['message']).addClass('f-msg-su');
              	$('.registration_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
              }
              setTimeout(function(){ 
              	$('.registration_message').fadeOut(1500);
              }, 10000);
              hideLoader();
              
            },
            error: function(error) {
            	console.log(error);
            	$('.registration_message .message').html('Registration failed, please check your internet connection and try again.').addClass('text-danger').removeClass('text-success');
            	$('.registration_message').addClass('bg-danger').removeClass('bg-success').fadeIn(500);
            	setTimeout(function(){ 
	              	$('.registration_message').fadeOut(1500);
	            }, 10000);
            	hideLoader();
            }
          });
          return false;
    });

	// Reset password functions
	$('.btn_forgot_user_pass').click(function(){
		$('#resetModal').show();
		// $('.f-launch-screen').fadeOut(444);
		// $('.reset-form').fadeIn(666);
	});
	$('.btn_submit_resetpassword').click(function(){
		if($(this).attr('data-resetted') == "yes"){
			$('.requestreset_message .message').html('Reset link email has already been sent!').addClass('f-msg-su');
			$('.requestreset_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
			setTimeout(function(){ 
         	 	$('.requestreset_message').fadeOut(1500);
         	}, 10000);
         	return false;
		}
    	if(!$(".reset-password-form").validationEngine('validate', {promptPosition: 'topLeft'})) {
    		return false;
    	}
    });
    
    $('.reset-password-form').on('submit', function(){
    		showLoader();
          var form_data = $(this).serializeArray();
		    console.log(form_data);
		    
          $.ajax({
            type: "POST",
            url: "/requestreset",
            data: form_data,
            success: function(request_data) {
              	console.log(request_data);
              	if(request_data['status'] == 'success') {
              		$('.requestreset_message .message').html(request_data['message']).addClass('f-msg-su');
             	 	$('.requestreset_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
             	 	$('.btn_submit_resetpassword').attr('data-resetted', "yes");
             	}
             	else {
             	 	$('.requestreset_message .message').html(request_data['message']).addClass('f-msg-su');
             	 	$('.requestreset_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
             	}
             	setTimeout(function(){ 
             	 	$('.requestreset_message').fadeOut(1500);
             	}, 10000);
            	hideLoader();
            },
            error: function(error) {
            	console.log(error);
            	$('.requestreset_message .message').html('Request failed, please check your internet connection and try again.').addClass('f-msg-su');
            	$('.requestreset_message').addClass('fsu-msg-border f-msg-bg-su').fadeIn(500);
            	setTimeout(function(){ 
	              	$('.requestreset_message').fadeOut(1500);
	            }, 10000);
            	hideLoader();
            }
          });
          return false;
    });

    $('.update-later').click(function(){
    	$('#update-modal').hide();
    });

})();